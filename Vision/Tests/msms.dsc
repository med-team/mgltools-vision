nodename = MSMS
nodedoc = runs MSMS command line
binary = msms

input name=inputFile, argument=-if %value%, type=file

input name=outputFile, argument=-of %value%, type=string

input name=probe_radius, argument=-probe_radius %value%, type=float, defaultValue=1.5

input name=density, argument=-density %value%, type=float, defaultValue=1.0

input name=header, argument=%value%, type=choice,
            choices={' ' , -no_header}, defaultValue=' '

#  -hdensity float     : surface points high density, [3.0]
#  -surface <tses,ases>: triangulated or Analytical SES, [tses]
#  -no_area            : turns off the analytical surface area computation
#  -socketName servicename : socket connection from a client
#  -socketPort portNumber : socket connection from a client
#  -xdr                : use xdr encoding over socket
#  -sinetd             : inetd server connection
#  -noh                : ignore atoms with radius 1.2
#  -no_rest_on_pbr     : no restart if pb. during triangulation
#  -no_rest            : no restart if pb. are encountered
#  -af filename        : area file
#  -no_header         : do not add comment line to the output
#  -free_vertices      : turns on computation for isolated RS vertices
#  -all_components     : compute all the surfaces components
#  -one_cavity #atoms at1 [at2][at3] : Compute the surface for an internal    
