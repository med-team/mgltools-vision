##################################################################
#
# Authors: Sowjanya Karnati,Michel F Sanner
#
#
####################################################################
#
#
#$Id: test_matplotlibNodes.py,v 1.44.4.2 2015/08/26 18:53:47 sanner Exp $

import sys,unittest,os
import Vision
from Vision.StandardNodes import stdlib, DialIntNE
from Vision.StandardNodes import Eval,SliceData,ReadTable,Cast,FileBrowserNE,AsType,Index
import Image

try:
    from Vision.matplotlibNodes import *
except:
    pass

def pause():
        from time import sleep
        sleep(0.1)


class MatPlotLibBaseTest(unittest.TestCase):

    def setUp(self):
        global ed
        from Vision.VPE import VisualProgramingEnvironment
        ed = VisualProgramingEnvironment(name='Vision', withShell=0,)
        ed.master.update_idletasks()
        ed.configure(withThreads=0)
        ed.addLibraryInstance(stdlib, 'Vision.StandardNodes', 'stdlib')


    def tearDown(self):
        ed.exit_cb()
        import gc
        gc.collect()
        try:
            cmd = "rm -f testfile.py"
            os.system(cmd)
        except:
            pass


##########################
## Tests
##########################
class MatplotlibNodesTest3(MatPlotLibBaseTest):
    def test_1_loadmatplotlibLib(self):
        """testing loading matplotlib libraray"""
        from Vision.matplotlibNodes import matplotliblib
        ed.addLibraryInstance(matplotliblib, 'Vision.matplotlibNodes','matplotliblib')
        ed.master.update_idletasks()
        pause()

    ######################TEST NODES ADDING AND DELETING ######################
    def test_2_matplotlib_Nodes(self):
        """testing adding and deleting nodes"""
        from Vision.matplotlibNodes import matplotliblib
        ed.addLibraryInstance(matplotliblib, 'Vision.matplotlibNodes','matplotliblib')
        ed.master.update_idletasks()    
        masterNet=ed.currentNetwork
        ## add node RandNormDist ##
        RandNormDist_0 = RandNormDist(constrkw = {}, name='RandNormDist', library=matplotliblib)
        masterNet.addNode(RandNormDist_0,96,35)
        #test adding
        self.assertEqual(str(masterNet.nodes),"[<RandNormDist RandNormDist>]")
        #test deleting
        masterNet.nodes[0].cut_cb()
        self.assertEqual(masterNet.nodes,[])
        #SinFunc
        SinFunc_0 = SinFunc(constrkw = {}, name='SinFunc', library=matplotliblib)
        masterNet.addNode(SinFunc_0,96,35)
        #test adding
        self.assertEqual(str(masterNet.nodes),"[<SinFunc SinFunc>]")
        #test deleting
        masterNet.nodes[0].cut_cb()
        self.assertEqual(masterNet.nodes,[]) 

        #SinFuncSerie
        SinFuncSerie_0 = SinFuncSerie(constrkw = {}, name='SinFuncSerie', library=matplotliblib)
        masterNet.addNode(SinFuncSerie_0,96,35)
        #test adding
        self.assertEqual(str(masterNet.nodes),"[<SinFuncSerie SinFuncSerie>]")
        #test deleting
        masterNet.nodes[0].cut_cb()
        self.assertEqual(masterNet.nodes,[])

        #MPLImagePolyNE
        MPLImageNE_0=MPLImageNE(constrkw = {}, name='MPLImageNE', library=matplotliblib)
        masterNet.addNode(MPLImageNE_0,96,35)
        #test adding
        self.assertEqual(str(masterNet.nodes),"[<MPLImageNE MPLImageNE>]")
        #test deleting
        masterNet.nodes[0].cut_cb()
        self.assertEqual(masterNet.nodes,[])

        
        #MPLDrawAreaNE
        MPLDrawAreaNE_0 = MPLDrawAreaNE(constrkw = {}, name='MPLDrawAreaNE', library=matplotliblib)
        masterNet.addNode(MPLDrawAreaNE_0,96,35)
        #test adding
        self.assertEqual(str(masterNet.nodes),"[<MPLDrawAreaNE MPLDrawAreaNE>]")
        #test deleting
        masterNet.nodes[0].cut_cb()
        self.assertEqual(masterNet.nodes,[])

        #MPLFigureNE
        MPLFigureNE_0 = MPLFigureNE(constrkw = {}, name='MPLFigureNE', library=matplotliblib)
        masterNet.addNode(MPLFigureNE_0,96,35)
        #test adding
        self.assertEqual(str(masterNet.nodes),"[<MPLFigureNE MPLFigureNE>]")
        #test deleting
        masterNet.nodes[0].cut_cb()
        self.assertEqual(masterNet.nodes,[])



        #FigImageNE
        FigImageNE_0 = FigImageNE(constrkw = {}, name='FigImageNE', library=matplotliblib)
        masterNet.addNode(FigImageNE_0,96,35)
        #test adding
        self.assertEqual(str(masterNet.nodes),"[<FigImageNE FigImageNE>]")
        #test deleting
        masterNet.nodes[0].cut_cb()
        self.assertEqual(masterNet.nodes,[])




        #HistogramNE
        HistogramNE_0 = HistogramNE(constrkw = {}, name='HistogramNE', library=matplotliblib)
        masterNet.addNode(HistogramNE_0,96,35)
        #test adding
        self.assertEqual(str(masterNet.nodes),"[<HistogramNE HistogramNE>]")
        #test deleting
        masterNet.nodes[0].cut_cb()
        self.assertEqual(masterNet.nodes,[])

        #PieNE
        PieNE_0 = PieNE(constrkw = {}, name='PieNE', library=matplotliblib)
        masterNet.addNode(PieNE_0,96,35)
        #test adding
        self.assertEqual(str(masterNet.nodes),"[<PieNE PieNE>]")
        #test deleting
        masterNet.nodes[0].cut_cb()
        self.assertEqual(masterNet.nodes,[])


        #PlotNE
        PlotNE_0 = PlotNE(constrkw = {}, name='PlotNE', library=matplotliblib)
        masterNet.addNode(PlotNE_0,96,35)
        #test adding
        self.assertEqual(str(masterNet.nodes),"[<PlotNE PlotNE>]")
        #test deleting
        masterNet.nodes[0].cut_cb()
        self.assertEqual(masterNet.nodes,[])


        #ScatterNE
        ScatterNE_0 = ScatterNE(constrkw = {}, name='ScatterNE', library=matplotliblib)
        masterNet.addNode(ScatterNE_0,96,35)
        #test adding
        self.assertEqual(str(masterNet.nodes),"[<ScatterNE ScatterNE>]")
        #test deleting
        masterNet.nodes[0].cut_cb()
        self.assertEqual(masterNet.nodes,[])
        
class  MatplotlibNodesTest2(MatPlotLibBaseTest):
    #RANDOM DIST
    def test_matplotlib_3_RandomDist(self):
        """testing RandomDist node """
        from Vision.matplotlibNodes import matplotliblib
        ed.addLibraryInstance(matplotliblib, 'Vision.matplotlibNodes','matplotliblib')
        ed.master.update_idletasks()
        pause()
        masterNet=ed.currentNetwork
        #add node RandNormDist 
        RandNormDist_0 = RandNormDist(constrkw = {}, name='RandNormDist', library=matplotliblib)
        masterNet.addNode(RandNormDist_0,96,35)
        apply(RandNormDist_0.configure, (), {'expanded': True})
        masterNet.nodes[0].run()
        #set mu,sigma,nbpoints
        node0 = masterNet.nodes[0]
        node0.inputPorts[0].widget.set(50)
        node0.inputPorts[1].widget.set(10)
        node0.inputPorts[2].widget.set(9000)
        self.assertEqual(node0.inputPorts[0].getData(),50)
        self.assertEqual(node0.inputPorts[1].getData(),10)
        self.assertEqual(node0.inputPorts[2].getData(),9000)
        #toggle
        node0.toggleNodeExpand_cb()
        self.assertEqual(node0.isExpanded(),False)
        
    #SIN FUNC
    def test_matplotlib_4_SinFunc(self):
        """testing sin func node """
        from Vision.matplotlibNodes import matplotliblib
        ed.addLibraryInstance(matplotliblib, 'Vision.matplotlibNodes','matplotliblib')
        ed.master.update_idletasks()
        pause()
        masterNet=ed.currentNetwork
        #add node SinFunc
        SinFunc_0 = SinFunc(constrkw = {}, name='SinFunc', library=matplotliblib)
        masterNet.addNode(SinFunc_0,96,35)
        apply(SinFunc_0.configure, (), {'expanded': True})
        masterNet.nodes[0].run()
        #set x0,x1,step
        node0 = masterNet.nodes[0]
        node0.inputPorts[0].widget.set(1.5)
        node0.inputPorts[1].widget.set(10)
        node0.inputPorts[2].widget.set(0.5)
        self.assertEqual(node0.inputPorts[0].getData(),1.5)
        self.assertEqual(node0.inputPorts[1].getData(),10)
        self.assertEqual(node0.inputPorts[2].getData(),0.5)     
        #toggle
        node0.toggleNodeExpand_cb()
        self.assertEqual(node0.isExpanded(),False)
        
    def test_matplotlib_11_histogram(self):
        """testing histogram node """
        from Vision.matplotlibNodes import matplotliblib
        ed.addLibraryInstance(matplotliblib, 'Vision.matplotlibNodes','matplotliblib')
        ed.master.update_idletasks()
        pause()
        masterNet=ed.currentNetwork
        masterNet.runOnNewData.value = True
        #add node RandNormDist 
        RandNormDist_0 = RandNormDist(constrkw = {}, name='RandNormDist', library=matplotliblib)
        masterNet.addNode(RandNormDist_0,96,35)
        apply(RandNormDist_0.configure, (), {'expanded': True})
        masterNet.nodes[0].run()
        #add node Histogram 
        Histogram_1 = HistogramNE(constrkw = {}, name='Histogram', library=matplotliblib)
        masterNet.addNode(Histogram_1,108,202)
        Histogram_1.inputPortByName['bins'].widget.set(25, run=False)
        node0=masterNet.nodes[0]
        node1=masterNet.nodes[1]
        #toggle
        node0.toggleNodeExpand_cb()
        pause()
        self.assertEqual(node0.isExpanded(),False)
        #set bins
        node1.inputPorts[1].widget.set(20)
        self.assertEqual(node1.inputPorts[1].getData(),20)
        self.assertEqual(node1.axes.has_data(),False)
        masterNet.connectNodes(RandNormDist_0, Histogram_1, "data", "values", blocking=True)
        #set normalize
        #off
        node1.inputPorts[2].widget.set(0)
        self.assertEqual(node1.inputPorts[2].getData(),0)
        #self.assertEqual(node1.axes.axis(),(40.0, 160.0, 0.0, 1600.0))
        #on
        node1.inputPorts[2].widget.set(1)
        self.assertEqual(node1.inputPorts[2].getData(),1)
        #self.assertEqual(node1.axes.axis(),(40.0, 160.0, 0.0, 0.029999999999999999))
        
        node0.run()
        self.assertEqual(node1.axes.has_data(),True)
        
        #figure
        self.assertEqual(node1.axes.figure!=None,True)
        self.assertEqual(node1.figure!=None,True)
        self.assertEqual(node1.axes.figure==node1.figure,True)
        self.assertEqual(isinstance(node1.canvas, FigureCanvasTkAgg),True)
        
        self.assertEqual(node1.ownsMaster==False,True)
        self.assertEqual(node1.canvas.figure==node1.axes.figure,True)
        #subplotparams
        node1.axes.figure.subplotpars._update_this("left",0.8)
        node0.run()
        self.assertEqual(node1.axes.figure.subplotpars.left,0.8)
        
        #bins=patches on axes
        node1.inputPorts[1].widget.set(10)
        node0.run()
        self.assertEqual(node1.inputPorts[1].getData(),len(node1.axes.patches))    
    
    def test_matplotlib_13_plot(self):
        """testing plot node """
        from Vision.matplotlibNodes import matplotliblib
        ed.addLibraryInstance(matplotliblib, 'Vision.matplotlibNodes','matplotliblib')
        ed.master.update_idletasks()
        pause()
        masterNet=ed.currentNetwork    
        #add node SinFunc
        SinFunc_0 = SinFunc(constrkw = {}, name='SinFunc', library=matplotliblib)
        masterNet.addNode(SinFunc_0,96,35)
        apply(SinFunc_0.configure, (), {'expanded': True})
        masterNet.nodes[0].run()
        ## add node Plot ##
        Plot_1 = PlotNE(constrkw = {}, name='Plot', library=matplotliblib)
        masterNet.addNode(Plot_1,325,147)
        node0=masterNet.nodes[0]
        node1=masterNet.nodes[1]
        masterNet.connectNodes(SinFunc_0, Plot_1, "x", "x", blocking=True)
        masterNet.connectNodes(SinFunc_0, Plot_1, "y", "y", blocking=True)
        #figure
        self.assertEqual(node1.axes.figure!=None,True)
        self.assertEqual(node1.figure!=None,True)
        self.assertEqual(node1.axes.figure==node1.figure,True)
        self.assertEqual(node1.canvas.figure==node1.axes.figure,True)
        self.assertEqual(isinstance(node1.canvas, FigureCanvasTkAgg),True)    
    
    def test_matplotlib_22_figure(self):
        """testing figure node"""
        from Vision.matplotlibNodes import matplotliblib
        ed.addLibraryInstance(matplotliblib, 'Vision.matplotlibNodes','matplotliblib')
        ed.master.update_idletasks()
        pause()
        masterNet=ed.currentNetwork
        masterNet.runOnNewData.value = True
        ## add node RandNormDist ##
        RandNormDist_0 = RandNormDist(constrkw = {}, name='RandNormDist', library=matplotliblib)
        masterNet.addNode(RandNormDist_0,96,35)
        apply(RandNormDist_0.configure, (), {'expanded': True})
        pause()
        ## add node Histogram ##
        Histogram_1 = HistogramNE(constrkw = {}, name='Histogram', library=matplotliblib)
        masterNet.addNode(Histogram_1,108,202)
        Histogram_1.inputPortByName['bins'].widget.set(25, run=False)
        pause()
        ## add node SinFunc ##
        SinFunc_2 = SinFunc(constrkw = {}, name='SinFunc', library=matplotliblib)   
        masterNet.addNode(SinFunc_2,334,65)
        ## add node Plot ##
        Plot_3 = PlotNE(constrkw = {}, name='Plot', library=matplotliblib)
        masterNet.addNode(Plot_3,325,147)
        ## add node Figure ##
        Figure_4 = MPLFigureNE(constrkw = {}, name='Figure', library=matplotliblib)
        masterNet.addNode(Figure_4,265,24)
        ## add node Draw Area ##
        Draw_Area_5 = MPLDrawAreaNE(constrkw = {}, name='Draw Area', library=matplotliblib)
        masterNet.addNode(Draw_Area_5,210,162)
        Draw_Area_5.inputPortByName['width'].widget.set(0.316666666667, run=False)
        Draw_Area_5.inputPortByName['height'].widget.set(0.697222222222, run=False)
        Draw_Area_6 = MPLDrawAreaNE(constrkw = {}, name='Draw Area', library=matplotliblib)
        ## add node Draw Area ##
        masterNet.addNode(Draw_Area_6,417,93)
        Draw_Area_6.inputPortByName['left'].widget.set(0.516666666667, run=False)
        Draw_Area_6.inputPortByName['width'].widget.set(0.319444444444, run=False)
        Draw_Area_6.inputPortByName['height'].widget.set(0.644444444444, run=False)
        self.assertEqual(Figure_4.axes,None)
        self.assertEqual(Figure_4.ownsMaster,False)
        ## connect ##
        masterNet.connectNodes(
            RandNormDist_0, Histogram_1, "data", "values", blocking=True)
        masterNet.connectNodes(
            Draw_Area_5, Histogram_1, "drawAreaDef", "drawAreaDef", blocking=True)
        ed.runCurrentNet_cb() 
        #ed.networks['Network 0'].run()
            
        
        masterNet.connectNodes(
            SinFunc_2, Plot_3, "y", "y", blocking=True)
        masterNet.connectNodes(
            SinFunc_2, Plot_3, "x", "x", blocking=True)
        masterNet.connectNodes(
            Draw_Area_6, Plot_3, "drawAreaDef", "drawAreaDef", blocking=True)
        ed.runCurrentNet_cb()
        masterNet.connectNodes(
            Histogram_1, Figure_4, "plot", "plots", blocking=True)
        masterNet.connectNodes(Plot_3, Figure_4, "plot", "plots", blocking=True)
        
        
        #test#
        self.assertEqual(Histogram_1.axes.figure,Figure_4.figure)
        self.assertEqual(Plot_3.axes.figure,Figure_4.figure)
        self.assertEqual(Figure_4.axes,None)
        master = Figure_4.canvas._tkcanvas.master
        self.assertEqual(master.winfo_exists(),1)
        masterNet.deleteNodes([Figure_4])
    
    
    #pie
    def test_matplotlib_32_pie(self):
        """testing pie node"""
        from Vision.matplotlibNodes import matplotliblib
        ed.addLibraryInstance(matplotliblib, 'Vision.matplotlibNodes','matplotliblib')
        ed.master.update_idletasks()
        pause()
        masterNet=ed.currentNetwork
        #Pie
        Pie_132 = PieNE(constrkw = {}, name='Pie', library=matplotliblib)
        masterNet.addNode(Pie_132,135,310)
        Pie_132.inputPortByName['shadow'].widget.set(0, run=False)
        apply(Pie_132.configure, (), {'expanded': True})
        #ReadTable
        ReadTable_133 = ReadTable(constrkw = {}, name='ReadTable1', library=stdlib)
        masterNet.addNode(ReadTable_133,272,8)
        ReadTable_133.inputPortByName['filename'].widget.set(
                os.path.abspath("../doc/Examples/matplotlib/Data/pie_data_1.dat"), run=False)
        ReadTable_133.inputPortByName['sep'].widget.set(",", run=False)
        apply(ReadTable_133.configure, (), {'expanded': True})
        #ReadTable
        ReadTable_138 = ReadTable(constrkw = {}, name='ReadTable2', library=stdlib)
        masterNet.addNode(ReadTable_138,60,11)
        ReadTable_138.inputPortByName['filename'].widget.set(
                os.path.abspath("../doc/Examples/matplotlib/Data/pie_data.dat"), run=False)
        ReadTable_138.inputPortByName['sep'].widget.set(",", run=False)
        ReadTable_138.inputPortByName['datatype'].widget.set("float", run=False)
        apply(ReadTable_138.configure, (), {'expanded': True})
        #Index
        Index_139 = Index(constrkw = {}, name='Index', library=stdlib)
        masterNet.addNode(Index_139,67,173)
        apply(Index_139.configure, (), {'expanded': True})
        #Index
        Index_141 = Index(constrkw = {}, name='Index', library=stdlib)
        masterNet.addNode(Index_141,208,170)
        apply(Index_141.configure, (), {'expanded': True})
        masterNet.connectNodes(
            ReadTable_138, Index_139, "data", "data", blocking=True)
        masterNet.connectNodes(
            Index_139, Pie_132, "data", "fractions", blocking=True)
        masterNet.connectNodes(
            ReadTable_133, Index_141, "data", "data", blocking=True)
        masterNet.connectNodes(
            Index_141, Pie_132, "data", "labels", blocking=True)    
        masterNet.run()
        pause()
        self.assertEqual(len(Pie_132.axes.patches)>0,True)
    
    
    #figimage
            
    def test_matplotlib_33_FigImage(self):
        """testing figimage node"""
        from Vision.matplotlibNodes import matplotliblib
        ed.addLibraryInstance(matplotliblib, 'Vision.matplotlibNodes','matplotliblib')
        from Vision.StandardNodes import stdlib
        ed.addLibraryInstance(stdlib, 'Vision.StandardNodes', 'stdlib')
        ed.master.update_idletasks()
        masterNet=ed.currentNetwork
        masterNet.runOnNewData.value = False

        #Eval
        Image_open_in1__4 = Eval(constrkw = {}, name='Image.open(in1)', library=stdlib)
        masterNet.addNode(Image_open_in1__4,211,329)
        Image_open_in1__4.inputPortByName['command'].widget.set("Image.open(in1)", run=False)
        Image_open_in1__4.inputPortByName['importString'].widget.set("import Image", run=False)
        apply(Image_open_in1__4.configure, (), {'expanded': True})
        #FileBrowser
        File_Browser_5 = FileBrowserNE(constrkw = {}, name='File Browser', library=stdlib)
        masterNet.addNode(File_Browser_5,125,66)
        File_Browser_5.inputPortByName['filename'].widget.set(
                os.path.abspath("../doc/Examples/matplotlib/Data/lena.jpg"), run=False)
        apply(File_Browser_5.configure, (), {'expanded': True})
        #Cast
        Cast_6 = Cast(constrkw = {}, name='Cast', library=stdlib)
        masterNet.addNode(Cast_6,184,169)
        apply(Cast_6.inputPortByName['newtype'].widget.configure, (), {'choices': ('2Darray', 'MPLAxes', 'MPLDrawArea', 'MPLFigure', 'None', 'NumericArray', 'Old None', 'array', 'boolean', 'colorRGB', 'colorfloat3or4', 'colorsRGB', 'coord2', 'coord3', 'coordinates3D', 'dict', 'faceIndices', 'float', 'image', 'indice2', 'indice2+', 'indice3or4', 'instancemat', 'int', 'list', 'normal3', 'normals3D', 'str', 'string', 'tkcolor', 'triggerIn', 'triggerOut', 'tuple', 'vector')})
        Cast_6.inputPortByName['newtype'].widget.set("str", run=False)
        apply(Cast_6.configure, (), {'expanded': True})
        #FigImage
        Figimage_7 = FigImageNE(constrkw = {}, name='Figimage', library=matplotliblib)
        masterNet.addNode(Figimage_7,490,387)
        Figimage_7.inputPortByName['origin'].widget.set("lower", run=False)
        masterNet.connectNodes(
            File_Browser_5, Cast_6, "filename", "data", blocking=True)
        masterNet.connectNodes(
            Cast_6, Image_open_in1__4, "result", "in1", blocking=True)
        masterNet.connectNodes(
            Image_open_in1__4, Figimage_7, "result", "data", blocking=True)
        masterNet.run()
        pause()
        self.assertEqual(len(Figimage_7.axes.images)>0,True)
        self.assertEqual(isinstance(Figimage_7.canvas, FigureCanvasTkAgg),True)    


 
    
class  MatplotlibNodesTest4(MatPlotLibBaseTest):    
    #SET MATPLOTLIB OPTIONS NODE
    
        
    def test_matplotlib_5_set_matplotlib_optoins_grid_options(self):
        """testing Histogram node grid options.Grid on or Off,setting grid color,grid line
        style,gridlinewidth"""
        from Vision.matplotlibNodes import matplotliblib
        ed.addLibraryInstance(matplotliblib, 'Vision.matplotlibNodes','matplotliblib')
        ed.master.update_idletasks()
        pause()
        masterNet=ed.currentNetwork
        #add node RandNormDist 
        RandNormDist_0 = RandNormDist(constrkw = {}, name='RandNormDist', library=matplotliblib)
        masterNet.addNode(RandNormDist_0,96,35)
        apply(RandNormDist_0.configure, (), {'expanded': True})
        masterNet.nodes[0].run()
        #add node Histogram 
        Histogram_1 = HistogramNE(constrkw = {}, name='Histogram', library=matplotliblib)
        masterNet.addNode(Histogram_1,108,202)
        node0=masterNet.nodes[0]
        node1=masterNet.nodes[1]
        masterNet.connectNodes(RandNormDist_0, Histogram_1, "data", "values", blocking=True)
        Set_Matplotlib_options_2 = MatPlotLibOptions(constrkw = {}, name='Set Matplotlib options', library=matplotliblib)
        masterNet.addNode(Set_Matplotlib_options_2,168,158)
        masterNet.connectNodes(Set_Matplotlib_options_2, Histogram_1, "matplotlibOptions", "drawAreaDef", blocking=True)
        masterNet.nodes[0].run()
        #setting options
        node1.axes.clear()
        Set_Matplotlib_options_2.toggleNodeExpand_cb ()
        #gridOn
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser.setentry('gridOn')
        Set_Matplotlib_options_2.inputPorts[0].widget.addProp('gridOn')
        Set_Matplotlib_options_2.inputPorts[0].widget.propWidgets['gridOn'][0].invoke()
        #gridlinestyle
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser.setentry('gridlinestyle')
        Set_Matplotlib_options_2.inputPorts[0].widget.addProp('gridlinestyle')
        Set_Matplotlib_options_2.inputPorts[0].widget.setChoice(('gridlinestyle',),'-.')
        #gridcolor
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser.setentry('gridcolor')
        Set_Matplotlib_options_2.inputPorts[0].widget.addProp('gridcolor')
        Set_Matplotlib_options_2.inputPorts[0].widget.setChoice(('gridcolor',),'red')
        #gridlinewidth
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser.setentry('gridlinewidth')
        Set_Matplotlib_options_2.inputPorts[0].widget.addProp('gridlinewidth')
        Set_Matplotlib_options_2.inputPorts[0].widget.setTwValue('gridlinewidth',15)
        Set_Matplotlib_options_2.run()
        node0.run()
        node1.run()
        self.assertEqual(node1.axes._gridOn,True)
        self.assertEqual(node1.axes.get_xgridlines()!=None,True)
        self.assertEqual(node1.axes.get_ygridlines()!=None,True)
        self.assertEqual(node1.axes.xaxis.get_gridlines()[0].get_linestyle(),'-.')
        self.assertEqual(node1.axes.xaxis.get_gridlines()[0].get_color(),'red')
        self.assertEqual(node1.axes.xaxis.get_gridlines()[0].get_linewidth(),15)

       
        
    def test_matplotlib_6_set_matplotlib_optoins_figure_patch_options(self):
        """testing setting Histogram node Frame options:Frame antialiased,frame linewidth,frame edgecolor,frame facecolor"""
        from Vision.matplotlibNodes import matplotliblib
        ed.addLibraryInstance(matplotliblib, 'Vision.matplotlibNodes','matplotliblib')
        ed.master.update_idletasks()
        pause()
        masterNet=ed.currentNetwork
        #add node RandNormDist 
        RandNormDist_0 = RandNormDist(constrkw = {}, name='RandNormDist', library=matplotliblib)
        masterNet.addNode(RandNormDist_0,96,35)
        apply(RandNormDist_0.configure, (), {'expanded': True})
        masterNet.nodes[0].run()
        #add node Histogram 
        Histogram_1 = HistogramNE(constrkw = {}, name='Histogram', library=matplotliblib)
        masterNet.addNode(Histogram_1,108,202)
        node0=masterNet.nodes[0]
        node1=masterNet.nodes[1]
        masterNet.connectNodes(RandNormDist_0, Histogram_1, "data", "values", blocking=True)
        Set_Matplotlib_options_2 = MatPlotLibOptions(constrkw = {}, name='Set Matplotlib options', library=matplotliblib)
        masterNet.addNode(Set_Matplotlib_options_2,168,158)
        masterNet.connectNodes(Set_Matplotlib_options_2, Histogram_1, "matplotlibOptions", "drawAreaDef", blocking=True)
        masterNet.nodes[0].run()
        #setting options
        node1.axes.clear()
        Set_Matplotlib_options_2.toggleNodeExpand_cb ()
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser.setentry('Figure')
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('figpatch_antialiased')
        Set_Matplotlib_options_2.inputPorts[0].widget.addProp('figpatch_antialiased')
        Set_Matplotlib_options_2.inputPorts[0].widget.propWidgets['figpatch_antialiased']=(Set_Matplotlib_options_2.inputPorts[0].widget.propWidgets['figpatch_antialiased'],1)
        node1.axes.figure.figurePatch.set_antialiased(1)
        Set_Matplotlib_options_2.inputPorts[0].widget.optionsDict['figpatch_antialiased']=1
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('figpatch_linewidth')
        Set_Matplotlib_options_2.inputPorts[0].widget.addProp('figpatch_linewidth')
        Set_Matplotlib_options_2.inputPorts[0].widget.setTwValue('figpatch_linewidth',20) 
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('figpatch_facecolor')
        Set_Matplotlib_options_2.inputPorts[0].widget.addProp('figpatch_facecolor')
        Set_Matplotlib_options_2.inputPorts[0].widget.setChoice(('figpatch_facecolor',),'red')
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('figpatch_edgecolor')
        Set_Matplotlib_options_2.inputPorts[0].widget.addProp('figpatch_edgecolor')
        Set_Matplotlib_options_2.inputPorts[0].widget.setChoice(('figpatch_edgecolor',),'green')        
        Set_Matplotlib_options_2.run()
        node0.run()
        node1.run()
        #self.assertEqual(node1.axes.figure.figurePatch.get_antialiased().get(),1)
        self.assertEqual(node1.axes.figure.figurePatch.get_linewidth(),20)
        import matplotlib.colors as colors
        red = colors.colorConverter.to_rgba("red", 1.0)
        green = colors.colorConverter.to_rgba("green", 1.0)
        self.assertEqual(node1.axes.figure.figurePatch.get_facecolor(), red)
        self.assertEqual(node1.axes.figure.figurePatch.get_edgecolor(), green)


    def test_matplotlib_7_set_matplotlib_optoins_axes_color(self):
        """testing  setting Histogram node options:axes edgecolor,axes
        facecolor,xlabelcolor,ylabelcolor,markeredgecolor,marker facecolor"""
        from Vision.matplotlibNodes import matplotliblib
        ed.addLibraryInstance(matplotliblib, 'Vision.matplotlibNodes','matplotliblib')
        ed.master.update_idletasks()
        pause()
        masterNet=ed.currentNetwork
        #add node RandNormDist 
        RandNormDist_0 = RandNormDist(constrkw = {}, name='RandNormDist', library=matplotliblib)
        masterNet.addNode(RandNormDist_0,96,35)
        apply(RandNormDist_0.configure, (), {'expanded': True})
        masterNet.nodes[0].run()
        #add node Histogram 
        Histogram_1 = HistogramNE(constrkw = {}, name='Histogram', library=matplotliblib)
        masterNet.addNode(Histogram_1,108,202)
        node0=masterNet.nodes[0]
        node1=masterNet.nodes[1]
        masterNet.connectNodes(RandNormDist_0, Histogram_1, "data", "values", blocking=True)
        Set_Matplotlib_options_2 = MatPlotLibOptions(constrkw = {}, name='Set Matplotlib options', library=matplotliblib)
        masterNet.addNode(Set_Matplotlib_options_2,168,158)
        masterNet.connectNodes(Set_Matplotlib_options_2, Histogram_1, "matplotlibOptions", "drawAreaDef", blocking=True)
        masterNet.nodes[0].run()
        #setting options
        node1.axes.clear()
        Set_Matplotlib_options_2.toggleNodeExpand_cb () 
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser.setentry('Axes')
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('edgecolor')
        Set_Matplotlib_options_2.inputPorts[0].widget.addProp('edgecolor')
        Set_Matplotlib_options_2.inputPorts[0].widget.setChoice(('edgecolor',),'firebrick')
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('facecolor')
        Set_Matplotlib_options_2.inputPorts[0].widget.addProp('facecolor')
        Set_Matplotlib_options_2.inputPorts[0].widget.setChoice(('facecolor',),'yellow')
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('markeredgecolor')
        Set_Matplotlib_options_2.inputPorts[0].widget.addProp('markeredgecolor')
        Set_Matplotlib_options_2.inputPorts[0].widget.setChoice(('markeredgecolor',),'blue')
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('markerfacecolor')
        Set_Matplotlib_options_2.inputPorts[0].widget.addProp('markerfacecolor')
        Set_Matplotlib_options_2.inputPorts[0].widget.setChoice(('markerfacecolor',),'crimson')
        Set_Matplotlib_options_2.run()
        node0.run()
        node1.run()
        import matplotlib.colors as colors
        firebrick = colors.colorConverter.to_rgba("firebrick", 1.0)
        self.assertEqual(node1.axes.get_axis_bgcolor(), "yellow")
        self.assertEqual(node1.axes.axesPatch.get_edgecolor(), firebrick)
        
        if os.name != 'nt': #sys.platform!="win32":
            self.assertEqual(node1.axes.get_xticklines()[0].get_markeredgecolor(), "blue")
            self.assertEqual(node1.axes.get_xticklines()[0].get_markerfacecolor(), "crimson")

    
    def test_matplotlib_8_set_matplotlib_optoins_ticks(self):    
        from Vision.matplotlibNodes import matplotliblib
        ed.addLibraryInstance(matplotliblib, 'Vision.matplotlibNodes','matplotliblib')
        ed.master.update_idletasks()
        pause()
        masterNet=ed.currentNetwork
        #add node RandNormDist 
        RandNormDist_0 = RandNormDist(constrkw = {}, name='RandNormDist', library=matplotliblib)
        masterNet.addNode(RandNormDist_0,96,35)
        apply(RandNormDist_0.configure, (), {'expanded': True})
        masterNet.nodes[0].run()
        #add node Histogram 
        Histogram_1 = HistogramNE(constrkw = {}, name='Histogram', library=matplotliblib)
        masterNet.addNode(Histogram_1,108,202)
        node0=masterNet.nodes[0]
        node1=masterNet.nodes[1]
        masterNet.connectNodes(RandNormDist_0, Histogram_1, "data", "values", blocking=True)
        Set_Matplotlib_options_2 = MatPlotLibOptions(constrkw = {}, name='Set Matplotlib options', library=matplotliblib)
        masterNet.addNode(Set_Matplotlib_options_2,168,158)
        masterNet.connectNodes(Set_Matplotlib_options_2, Histogram_1, "matplotlibOptions", "drawAreaDef", blocking=True)
        #setting options
        node1.axes.clear()
        Set_Matplotlib_options_2.toggleNodeExpand_cb ()
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser.setentry('Tick')
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('xtick.color')
        Set_Matplotlib_options_2.inputPorts[0].widget.addProp('xtick.color')
        Set_Matplotlib_options_2.inputPorts[0].widget.setChoice(('xtick.color',),'cyan')
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('ytick.color')
        Set_Matplotlib_options_2.inputPorts[0].widget.addProp('ytick.color')
        Set_Matplotlib_options_2.inputPorts[0].widget.setChoice(('ytick.color',),'magenta')
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('xtick.labelsize')
        Set_Matplotlib_options_2.inputPorts[0].widget.addProp('xtick.labelsize')
        Set_Matplotlib_options_2.inputPorts[0].widget.setTwValue('xtick.labelsize',5.0)
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('ytick.labelsize')
        Set_Matplotlib_options_2.inputPorts[0].widget.addProp('ytick.labelsize')
        Set_Matplotlib_options_2.inputPorts[0].widget.setTwValue('ytick.labelsize',5.0)
        Set_Matplotlib_options_2.run()
        masterNet.nodes[0].run()
        masterNet.nodes[1].run()
        self.assertEqual(node1.axes.xaxis.get_ticklabels()[0].get_color(),'cyan')
        self.assertEqual(node1.axes.yaxis.get_ticklabels()[0].get_color(),'magenta')
        self.assertEqual(node1.axes.xaxis.get_ticklabels()[0].get_size(),5.0)
        self.assertEqual(node1.axes.yaxis.get_ticklabels()[0].get_size(),5.0)
    
    def test_matplotlib_9_set_matplotlib_optoins_axes_1(self):
        """testing setting Histogram node axes options,zoomx,zoomy,marker
        edgewidth,xlabelsize,ylabelsize"""
        from Vision.matplotlibNodes import matplotliblib
        ed.addLibraryInstance(matplotliblib, 'Vision.matplotlibNodes','matplotliblib')
        ed.master.update_idletasks()
        pause()
        masterNet=ed.currentNetwork
        #add node RandNormDist 
        RandNormDist_0 = RandNormDist(constrkw = {}, name='RandNormDist', library=matplotliblib)
        masterNet.addNode(RandNormDist_0,96,35)
        apply(RandNormDist_0.configure, (), {'expanded': True})
        masterNet.nodes[0].run()
        #add node Histogram 
        Histogram_1 = HistogramNE(constrkw = {}, name='Histogram', library=matplotliblib)
        masterNet.addNode(Histogram_1,108,202)
        node0=masterNet.nodes[0]
        node1=masterNet.nodes[1]
        masterNet.connectNodes(RandNormDist_0, Histogram_1, "data", "values", blocking=True)
        Set_Matplotlib_options_2 = MatPlotLibOptions(constrkw = {}, name='Set Matplotlib options', library=matplotliblib)
        masterNet.addNode(Set_Matplotlib_options_2,168,158)
        masterNet.connectNodes(Set_Matplotlib_options_2, Histogram_1, "matplotlibOptions", "drawAreaDef", blocking=True)
        masterNet.nodes[0].run()
        #setting options
        node1.axes.clear()
        Set_Matplotlib_options_2.toggleNodeExpand_cb ()
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser.setentry('Axes')
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('axisbelow')
        Set_Matplotlib_options_2.inputPorts[0].widget.addProp('axisbelow')
        Set_Matplotlib_options_2.inputPorts[0].widget.propWidgets['axisbelow'][0].invoke()
        #Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('zoomx')
        #Set_Matplotlib_options_2.inputPorts[0].widget.addProp('zoomx')
        #Set_Matplotlib_options_2.inputPorts[0].widget.setTwValue('zoomx',25)
        #Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('zoomy')
        #Set_Matplotlib_options_2.inputPorts[0].widget.addProp('zoomy')
        #Set_Matplotlib_options_2.inputPorts[0].widget.setTwValue('zoomy',100) 
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('markeredgewidth')
        Set_Matplotlib_options_2.inputPorts[0].widget.addProp('markeredgewidth')
        Set_Matplotlib_options_2.inputPorts[0].widget.setTwValue('markeredgewidth',15)
        Set_Matplotlib_options_2.run()
        node0.run()
        node1.run()
        pause()
        
        
        self.assertEqual(node1.axes.get_xticklines()[0].get_markeredgewidth(),15)
        self.assertEqual(node1.axes._axisbelow,True)
        
    def test_matplotlib_10_set_matplotlib_optoins_axes_2(self):
        """testing setting axes options adjustable,aspect,anchor"""
        from Vision.matplotlibNodes import matplotliblib
        ed.addLibraryInstance(matplotliblib, 'Vision.matplotlibNodes','matplotliblib')
        ed.master.update_idletasks()
        pause()
        masterNet=ed.currentNetwork
        #add node RandNormDist 
        RandNormDist_0 = RandNormDist(constrkw = {}, name='RandNormDist', library=matplotliblib)
        masterNet.addNode(RandNormDist_0,96,35)
        apply(RandNormDist_0.configure, (), {'expanded': True})
        masterNet.nodes[0].run()
        #add node Histogram 
        Histogram_1 = HistogramNE(constrkw = {}, name='Histogram', library=matplotliblib)
        masterNet.addNode(Histogram_1,108,202)
        node0=masterNet.nodes[0]
        node1=masterNet.nodes[1]
        masterNet.connectNodes(RandNormDist_0, Histogram_1, "data", "values", blocking=True)
        Set_Matplotlib_options_2 = MatPlotLibOptions(constrkw = {}, name='Set Matplotlib options', library=matplotliblib)
        masterNet.addNode(Set_Matplotlib_options_2,168,158)
        masterNet.connectNodes(Set_Matplotlib_options_2, Histogram_1, "matplotlibOptions", "drawAreaDef", blocking=True)
        masterNet.nodes[0].run()
        #setting options        
        node1.axes.clear()
        Set_Matplotlib_options_2.toggleNodeExpand_cb ()
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser.setentry('Axes')
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('aspect')
        Set_Matplotlib_options_2.inputPorts[0].widget.addProp('aspect')
        Set_Matplotlib_options_2.inputPorts[0].widget.setChoice(('aspect',),"equal")
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('adjustable')
        Set_Matplotlib_options_2.inputPorts[0].widget.addProp('adjustable')
        Set_Matplotlib_options_2.inputPorts[0].widget.setChoice(('adjustable',),"datalim")
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('anchor')
        Set_Matplotlib_options_2.inputPorts[0].widget.addProp('anchor')
        Set_Matplotlib_options_2.inputPorts[0].widget.setChoice(('anchor',),"S")
        Set_Matplotlib_options_2.run()
        node0.run()
        node1.run()
        self.assertEqual(node1.axes.get_aspect(),"equal")
        self.assertEqual(node1.axes.get_adjustable(),"datalim")
        self.assertEqual(node1.axes.get_anchor(),'S')
        
        
    #HISTOGRAM
    
   
    #Histogram rendering options tests
    def test_matplotlib_12_hist_parampanel_option(self):
        from Vision.matplotlibNodes import matplotliblib
        ed.addLibraryInstance(matplotliblib, 'Vision.matplotlibNodes','matplotliblib')
        ed.master.update_idletasks()
        pause()
        masterNet=ed.currentNetwork     
        #add node RandNormDist 
        RandNormDist_0 = RandNormDist(constrkw = {}, name='RandNormDist', library=matplotliblib)
        masterNet.addNode(RandNormDist_0,96,35)
        apply(RandNormDist_0.configure, (), {'expanded': True})
        masterNet.nodes[0].run()
        #add node Histogram 
        Histogram_1 = HistogramNE(constrkw = {}, name='Histogram', library=matplotliblib)
        masterNet.addNode(Histogram_1,108,202)
        node0=masterNet.nodes[0]
        node1=masterNet.nodes[1]
        masterNet.connectNodes(RandNormDist_0, Histogram_1, "data", "values", blocking=True)
        #setting options
        node1.axes.clear()
        Histogram_1.inputPortByName['patch_linewidth'].widget.set(2.23888888889, run=False)
        Histogram_1.inputPortByName['patch_facecolor'].widget.set("cornflowerblue", run=False)
        Histogram_1.inputPortByName['patch_edgecolor'].widget.set("red", run=False)
        Histogram_1.inputPortByName['patch_antialiased'].widget.set(0, run=False)
        #checking for one patch
        node0.run()
        node1.run()
        import matplotlib.colors as colors
        red = colors.colorConverter.to_rgba("red", 1.0)
        cornflowerblue = colors.colorConverter.to_rgba("cornflowerblue", 1.0)
        self.assertEqual(node1.axes.patches[0].get_facecolor(), cornflowerblue)
        self.assertEqual(node1.axes.patches[0].get_edgecolor(), red)
        self.assertEqual(node1.axes.patches[0].get_linewidth(),2)
        self.assertEqual(node1.axes.patches[0].get_antialiased(),0)
        
    #PLOT
       
         
    def test_matplotlib_14_plot_options(self):
        """testing plot node """
        from Vision.matplotlibNodes import matplotliblib
        ed.addLibraryInstance(matplotliblib, 'Vision.matplotlibNodes','matplotliblib')
        ed.master.update_idletasks()
        pause()
        masterNet=ed.currentNetwork    
        #add node SinFunc
        SinFunc_0 = SinFunc(constrkw = {}, name='SinFunc', library=matplotliblib)
        masterNet.addNode(SinFunc_0,96,35)
        apply(SinFunc_0.configure, (), {'expanded': True})
        masterNet.nodes[0].run()
        ## add node Plot ##
        Plot_1 = PlotNE(constrkw = {}, name='Plot', library=matplotliblib)
        masterNet.addNode(Plot_1,325,147)
        node0=masterNet.nodes[0]
        node1=masterNet.nodes[1]
        masterNet.connectNodes(SinFunc_0, Plot_1, "x", "x", blocking=True)
        masterNet.connectNodes(SinFunc_0, Plot_1, "y", "y", blocking=True)
        #setting options
        node1.axes.clear()
        node1.inputPortByName['lineStyle'].widget.set('solid')
        node1.inputPortByName['line_linewidth'].widget.set(3.0)
        node1.inputPortByName['line_antialiased'].widget.set(1)
        node1.inputPortByName['solid_joinstyle'].widget.set("round")
        node1.inputPortByName['solid_capstyle'].widget.set("round")
        node1.run()
        node0.run()
        self.assertEqual(node1.axes.lines[0].get_linewidth(),3.0)
        self.assertEqual(node1.axes.lines[0].get_antialiased(),True)
        self.assertEqual(node1.axes.lines[0].get_solid_capstyle(),"round")
        #self.assertEqual(node1.axes.lines[0].get_solid_joinstyle(),"round")
        
        node1.inputPortByName['lineStyle'].widget.set('hexagon1')    
        node1.inputPortByName['dash_capstyle'].widget.set("round")
        node1.inputPortByName['dash_joinstyle'].widget.set("round")
        node1.inputPortByName['color'].widget.set("green")
        node0.run()
        node1.run()
        #testing
        self.assertEqual(node1.axes.lines[0].get_dash_capstyle(),"round")
        #self.assertEqual(node1.axes.lines[0].get_dash_joinstyle(),"round")
               
    def test_matplotlib_15_plot_set_matplotlib_grid_options(self):
        """testing plot node grid options.Grid on or Off,setting grid color,grid line
        style,gridlinewidth"""
        from Vision.matplotlibNodes import matplotliblib
        ed.addLibraryInstance(matplotliblib, 'Vision.matplotlibNodes','matplotliblib')
        ed.master.update_idletasks()
        pause()
        masterNet=ed.currentNetwork    
        #add node SinFunc
        SinFunc_0 = SinFunc(constrkw = {}, name='SinFunc', library=matplotliblib)
        masterNet.addNode(SinFunc_0,96,35)
        apply(SinFunc_0.configure, (), {'expanded': True})
        masterNet.nodes[0].run()
        ## add node Plot ##
        Plot_1 = PlotNE(constrkw = {}, name='Plot', library=matplotliblib)
        masterNet.addNode(Plot_1,325,147)
        node0=masterNet.nodes[0]
        node1=masterNet.nodes[1]
        masterNet.connectNodes(SinFunc_0, Plot_1, "x", "x", blocking=True)
        masterNet.connectNodes(SinFunc_0, Plot_1, "y", "y", blocking=True)
        Set_Matplotlib_options_2 = MatPlotLibOptions(constrkw = {}, name='Set Matplotlib options', library=matplotliblib)
        masterNet.addNode(Set_Matplotlib_options_2,168,158)
        masterNet.connectNodes(Set_Matplotlib_options_2, Plot_1, "matplotlibOptions", "drawAreaDef", blocking=True)
        masterNet.nodes[0].run()
        #setting options
        node1.axes.clear()
        Set_Matplotlib_options_2.toggleNodeExpand_cb ()
        #gridOn
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser.setentry('Grid')
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('gridOn')
        Set_Matplotlib_options_2.inputPorts[0].widget.addProp('gridOn')
        Set_Matplotlib_options_2.inputPorts[0].widget.propWidgets['gridOn'][0].invoke()
        #gridlinestyle
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('gridlinestyle')
        Set_Matplotlib_options_2.inputPorts[0].widget.addProp('gridlinestyle')
        Set_Matplotlib_options_2.inputPorts[0].widget.setChoice(('gridlinestyle',),'-.')
        #gridcolor
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('gridcolor')
        Set_Matplotlib_options_2.inputPorts[0].widget.addProp('gridcolor')
        Set_Matplotlib_options_2.inputPorts[0].widget.setChoice(('gridcolor',),'red')
        #gridlinewidth
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('gridlinewidth')
        Set_Matplotlib_options_2.inputPorts[0].widget.addProp('gridlinewidth')
        Set_Matplotlib_options_2.inputPorts[0].widget.setTwValue('gridlinewidth',15)
        Set_Matplotlib_options_2.run()
        node0.run()
        node1.run()
        self.assertEqual(node1.axes._gridOn,True)
        self.assertEqual(node1.axes.get_xgridlines()!=None,True)
        self.assertEqual(node1.axes.get_ygridlines()!=None,True)
        self.assertEqual(node1.axes.xaxis.get_gridlines()[0].get_linestyle(),'-.')
        self.assertEqual(node1.axes.xaxis.get_gridlines()[0].get_color(),'red')
        self.assertEqual(node1.axes.xaxis.get_gridlines()[0].get_linewidth(),15)

    def test_matplotlib_16_plot_set_matplotlib_figure_patch_options(self):
        """testing plot node Frame options:Frame antialiased,frame linewidth,frame edgecolor,frame facecolor"""
        from Vision.matplotlibNodes import matplotliblib
        ed.addLibraryInstance(matplotliblib, 'Vision.matplotlibNodes','matplotliblib')
        ed.master.update_idletasks()
        pause()
        masterNet=ed.currentNetwork    
        #add node SinFunc
        SinFunc_0 = SinFunc(constrkw = {}, name='SinFunc', library=matplotliblib)
        masterNet.addNode(SinFunc_0,96,35)
        apply(SinFunc_0.configure, (), {'expanded': True})
        masterNet.nodes[0].run()
        ## add node Plot ##
        Plot_1 = PlotNE(constrkw = {}, name='Plot', library=matplotliblib)
        masterNet.addNode(Plot_1,325,147)
        node0=masterNet.nodes[0]
        node1=masterNet.nodes[1]
        masterNet.connectNodes(SinFunc_0, Plot_1, "x", "x", blocking=True)
        masterNet.connectNodes(SinFunc_0, Plot_1, "y", "y", blocking=True)
        Set_Matplotlib_options_2 = MatPlotLibOptions(constrkw = {}, name='Set Matplotlib options', library=matplotliblib)
        masterNet.addNode(Set_Matplotlib_options_2,168,158)
        masterNet.connectNodes(Set_Matplotlib_options_2, Plot_1, "matplotlibOptions", "drawAreaDef", blocking=True)
        masterNet.nodes[0].run()
        #setting options
        node1.axes.clear()
        Set_Matplotlib_options_2.toggleNodeExpand_cb ()
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser.setentry('Figure')
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('figpatch_antialiased')
        Set_Matplotlib_options_2.inputPorts[0].widget.addProp('figpatch_antialiased')
        Set_Matplotlib_options_2.inputPorts[0].widget.propWidgets['figpatch_antialiased']=(Set_Matplotlib_options_2.inputPorts[0].widget.propWidgets['figpatch_antialiased'],1)
        node1.axes.figure.figurePatch.set_antialiased(1)
        Set_Matplotlib_options_2.inputPorts[0].widget.optionsDict['figpatch_antialiased']=1
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('figpatch_linewidth')
        Set_Matplotlib_options_2.inputPorts[0].widget.addProp('figpatch_linewidth')
        Set_Matplotlib_options_2.inputPorts[0].widget.setTwValue('figpatch_linewidth',20) 
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('figpatch_facecolor')
        Set_Matplotlib_options_2.inputPorts[0].widget.addProp('figpatch_facecolor')
        Set_Matplotlib_options_2.inputPorts[0].widget.setChoice(('figpatch_facecolor',),'red')
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('figpatch_edgecolor')
        Set_Matplotlib_options_2.inputPorts[0].widget.addProp('figpatch_edgecolor')
        Set_Matplotlib_options_2.inputPorts[0].widget.setChoice(('figpatch_edgecolor',),'green')
        
        Set_Matplotlib_options_2.run()
        node0.run()
        node1.run()
        #self.assertEqual(node1.axes.figure.figurePatch.get_antialiased(),1)
        self.assertEqual(node1.axes.figure.figurePatch.get_linewidth(),20)
        import matplotlib.colors as colors
        red = colors.colorConverter.to_rgba("red", 1.0)
        green = colors.colorConverter.to_rgba("green", 1.0)
        self.assertEqual(node1.axes.figure.figurePatch.get_facecolor(), red)
        self.assertEqual(node1.axes.figure.figurePatch.get_edgecolor(), green)
    
    def test_matplotlib_17_plot_set_matplotlib_axes_color_options(self):
        """testing plot node options:axes edgecolor,axes
        facecolor,xlabelcolor,ylabelcolor,markeredgecolor,marker facecolor"""
        from Vision.matplotlibNodes import matplotliblib
        ed.addLibraryInstance(matplotliblib, 'Vision.matplotlibNodes','matplotliblib')
        ed.master.update_idletasks()
        pause()
        masterNet=ed.currentNetwork    
        #add node SinFunc
        SinFunc_0 = SinFunc(constrkw = {}, name='SinFunc', library=matplotliblib)
        masterNet.addNode(SinFunc_0,96,35)
        apply(SinFunc_0.configure, (), {'expanded': True})
        masterNet.nodes[0].run()
        ## add node Plot ##
        Plot_1 = PlotNE(constrkw = {}, name='Plot', library=matplotliblib)
        masterNet.addNode(Plot_1,325,147)
        node0=masterNet.nodes[0]
        node1=masterNet.nodes[1]
        masterNet.connectNodes(SinFunc_0, Plot_1, "x", "x", blocking=True)
        masterNet.connectNodes(SinFunc_0, Plot_1, "y", "y", blocking=True)
        Set_Matplotlib_options_2 = MatPlotLibOptions(constrkw = {}, name='Set Matplotlib options', library=matplotliblib)
        masterNet.addNode(Set_Matplotlib_options_2,168,158)
        masterNet.connectNodes(Set_Matplotlib_options_2, Plot_1, "matplotlibOptions", "drawAreaDef", blocking=True)
        #node1.inputPortByName['lineStyle'].widget.set('hexagon1')
        #masterNet.nodes[0].run()
        #setting options
        node1.axes.clear()
        Set_Matplotlib_options_2.toggleNodeExpand_cb () 
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser.setentry('Axes')        
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('markeredgewidth')
        Set_Matplotlib_options_2.inputPorts[0].widget.addProp('markeredgewidth')
        Set_Matplotlib_options_2.inputPorts[0].widget.setTwValue('markeredgewidth',2)
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('edgecolor')
        Set_Matplotlib_options_2.inputPorts[0].widget.addProp('edgecolor')
        Set_Matplotlib_options_2.inputPorts[0].widget.setChoice(('edgecolor',),'firebrick')
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('facecolor')
        Set_Matplotlib_options_2.inputPorts[0].widget.addProp('facecolor')
        Set_Matplotlib_options_2.inputPorts[0].widget.setChoice(('facecolor',),'yellow')
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('markeredgecolor')
        Set_Matplotlib_options_2.inputPorts[0].widget.addProp('markeredgecolor')
        Set_Matplotlib_options_2.inputPorts[0].widget.setChoice(('markeredgecolor',),'red')
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('markerfacecolor')
        Set_Matplotlib_options_2.inputPorts[0].widget.addProp('markerfacecolor')
        Set_Matplotlib_options_2.inputPorts[0].widget.setChoice(('markerfacecolor',),'crimson')
        Set_Matplotlib_options_2.run()
        node0.run()
        node1.run()
        import matplotlib.colors as colors
        firebrick = colors.colorConverter.to_rgba("firebrick", 1.0)
        self.assertEqual(node1.axes.get_axis_bgcolor(), "yellow")
        self.assertEqual(node1.axes.axesPatch.get_edgecolor(), firebrick)
        
        if os.name != 'nt': #sys.platform!="win32":
            self.assertEqual(node1.axes.get_xticklines()[0].get_markeredgecolor(), "red")
            self.assertEqual(node1.axes.get_xticklines()[0].get_markerfacecolor(), "crimson")
            
    def test_matplotlib_18_plot_set_matplotlib_axes_options_1(self):
        """testing plot node axes options,zoomx,zoomy,marker
        edgewidth,xlabelsize,ylabelsize"""
        from Vision.matplotlibNodes import matplotliblib
        ed.addLibraryInstance(matplotliblib, 'Vision.matplotlibNodes','matplotliblib')
        ed.master.update_idletasks()
        pause()
        masterNet=ed.currentNetwork    
        #add node SinFunc
        SinFunc_0 = SinFunc(constrkw = {}, name='SinFunc', library=matplotliblib)
        masterNet.addNode(SinFunc_0,96,35)
        apply(SinFunc_0.configure, (), {'expanded': True})
        masterNet.nodes[0].run()
        ## add node Plot ##
        Plot_1 = PlotNE(constrkw = {}, name='Plot', library=matplotliblib)
        masterNet.addNode(Plot_1,325,147)
        node0=masterNet.nodes[0]
        node1=masterNet.nodes[1]
        masterNet.connectNodes(SinFunc_0, Plot_1, "x", "x", blocking=True)
        masterNet.connectNodes(SinFunc_0, Plot_1, "y", "y", blocking=True)
        Set_Matplotlib_options_2 = MatPlotLibOptions(constrkw = {}, name='Set Matplotlib options', library=matplotliblib)
        masterNet.addNode(Set_Matplotlib_options_2,168,158)
        masterNet.connectNodes(Set_Matplotlib_options_2, Plot_1, "matplotlibOptions", "drawAreaDef", blocking=True)
        masterNet.nodes[0].run()
        #setting options
        node1.axes.clear()
        Set_Matplotlib_options_2.toggleNodeExpand_cb ()
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser.setentry('Axes')
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('axisbelow')
        Set_Matplotlib_options_2.inputPorts[0].widget.addProp('axisbelow')
        Set_Matplotlib_options_2.inputPorts[0].widget.propWidgets['axisbelow'][0].invoke()
#        Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('zoomx')
#        Set_Matplotlib_options_2.inputPorts[0].widget.addProp('zoomx')
#        Set_Matplotlib_options_2.inputPorts[0].widget.setTwValue('zoomx',25)
#        Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('zoomy')
#        Set_Matplotlib_options_2.inputPorts[0].widget.addProp('zoomy')
#        Set_Matplotlib_options_2.inputPorts[0].widget.setTwValue('zoomy',100) 
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('markeredgewidth')
        Set_Matplotlib_options_2.inputPorts[0].widget.addProp('markeredgewidth')
        Set_Matplotlib_options_2.inputPorts[0].widget.setTwValue('markeredgewidth',15)
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('adjustable')
        Set_Matplotlib_options_2.inputPorts[0].widget.addProp('adjustable')
        Set_Matplotlib_options_2.inputPorts[0].widget.setChoice(('adjustable',),"datalim")
        
        Set_Matplotlib_options_2.run()
        node0.run()
        node1.run()
        pause()
        self.assertEqual(node1.axes.get_xticklines()[0].get_markeredgewidth(),15)
        self.assertEqual(node1.axes._axisbelow,True)
    
    def test_matplotlib_19_plot_set_matplotlib_axes_options_2(self):
        """testing plot node axes options adjustable,aspect,anchor"""
        from Vision.matplotlibNodes import matplotliblib
        ed.addLibraryInstance(matplotliblib, 'Vision.matplotlibNodes','matplotliblib')
        ed.master.update_idletasks()
        pause()
        masterNet=ed.currentNetwork    
        #add node SinFunc
        SinFunc_0 = SinFunc(constrkw = {}, name='SinFunc', library=matplotliblib)
        masterNet.addNode(SinFunc_0,96,35)
        apply(SinFunc_0.configure, (), {'expanded': True})
        masterNet.nodes[0].run()
        ## add node Plot ##
        Plot_1 = PlotNE(constrkw = {}, name='Plot', library=matplotliblib)
        masterNet.addNode(Plot_1,325,147)
        node0=masterNet.nodes[0]
        node1=masterNet.nodes[1]
        masterNet.connectNodes(SinFunc_0, Plot_1, "x", "x", blocking=True)
        masterNet.connectNodes(SinFunc_0, Plot_1, "y", "y", blocking=True)
        Set_Matplotlib_options_2 = MatPlotLibOptions(constrkw = {}, name='Set Matplotlib options', library=matplotliblib)
        masterNet.addNode(Set_Matplotlib_options_2,168,158)
        masterNet.connectNodes(Set_Matplotlib_options_2, Plot_1, "matplotlibOptions", "drawAreaDef", blocking=True)
        masterNet.nodes[0].run()
        #setting options        
        node1.axes.clear()
        Set_Matplotlib_options_2.toggleNodeExpand_cb ()
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser.setentry('Axes')
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('aspect')
        Set_Matplotlib_options_2.inputPorts[0].widget.addProp('aspect')
        Set_Matplotlib_options_2.inputPorts[0].widget.setChoice(('aspect',),"equal")
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('adjustable')
        Set_Matplotlib_options_2.inputPorts[0].widget.addProp('adjustable')
        Set_Matplotlib_options_2.inputPorts[0].widget.setChoice(('adjustable',),"datalim")
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('anchor')
        Set_Matplotlib_options_2.inputPorts[0].widget.addProp('anchor')
        Set_Matplotlib_options_2.inputPorts[0].widget.setChoice(('anchor',),"S")
        Set_Matplotlib_options_2.run()
        node0.run()
        node1.run()
        self.assertEqual(node1.axes.get_aspect(),"equal")
        self.assertEqual(node1.axes.get_adjustable(),"datalim")
        self.assertEqual(node1.axes.get_anchor(),'S')

    #DRAW AREA
    def test_matplotlib_20_drawarea_histogram(self):
        """testing DrawArea node by connecting to histogram node"""
        from Vision.matplotlibNodes import matplotliblib
        ed.addLibraryInstance(matplotliblib, 'Vision.matplotlibNodes','matplotliblib')
        ed.master.update_idletasks()
        pause()
        masterNet=ed.currentNetwork
        #add node RandNormDist 
        RandNormDist_0 = RandNormDist(constrkw = {}, name='RandNormDist', library=matplotliblib)
        masterNet.addNode(RandNormDist_0,96,35)
        apply(RandNormDist_0.configure, (), {'expanded': True})
        masterNet.nodes[0].run()
        #add node Histogram 
        Histogram_1 = HistogramNE(constrkw = {}, name='Histogram', library=matplotliblib)
        masterNet.addNode(Histogram_1,108,202)
        Histogram_1.inputPortByName['bins'].widget.set(10, run=False)
        
        ## add node Draw Area ##
        Draw_Area_2 = MPLDrawAreaNE(constrkw = {}, name='Draw Area', library=matplotliblib)
        masterNet.addNode(Draw_Area_2,210,162)
        masterNet.connectNodes(RandNormDist_0, Histogram_1, "data", "values", blocking=True)
        masterNet.connectNodes(Draw_Area_2, Histogram_1, "drawAreaDef", "drawAreaDef", blocking=True) 
        node0=masterNet.nodes[0]
        node1=masterNet.nodes[1]
        node2=masterNet.nodes[2]
        node0.run()    
        node2.run()
        node1.run() 
        position_old=Histogram_1.axes.get_position()
        self.assertEqual(position_old.bounds,(0.10000000000000001, 0.10000000000000001,
                                              0.80000000000000004, 0.80000000000000004))
        #adjust drawarea pars
        Draw_Area_2.inputPortByName['left'].widget.set(0.516666666667, run=False)
        Draw_Area_2.inputPortByName['width'].widget.set(0.319444444444, run=False)
        Draw_Area_2.inputPortByName['height'].widget.set(0.644444444444, run=False)
        node0.run()    
        node2.run()
        node1.run() 
        pause()
        position_new=Histogram_1.axes.get_position()
        self.assertEqual(position_old!=position_new,True)
        self.assertEqual(position_new.bounds,(0.51666666666700001, 0.10000000000000001,
                                       0.319444444444, 0.64444444444399995))
        
    def test_matplotlib_21_drawarea_plot(self):
        """testing DrawArea by connecting to Plot Node"""
        from Vision.matplotlibNodes import matplotliblib
        ed.addLibraryInstance(matplotliblib, 'Vision.matplotlibNodes','matplotliblib')
        ed.master.update_idletasks()
        pause()
        masterNet=ed.currentNetwork
        #add node RandNormDist
        SinFunc_0 = SinFunc(constrkw = {}, name='SinFunc', library=matplotliblib)   
        masterNet.addNode(SinFunc_0,334,65)
        masterNet.nodes[0].run()
        ## add node Plot ##
        Plot_1 = PlotNE(constrkw = {}, name='Plot', library=matplotliblib)
        masterNet.addNode(Plot_1,325,147)
        ## add node Draw Area ##
        Draw_Area_2 = MPLDrawAreaNE(constrkw = {}, name='Draw Area', library=matplotliblib)
        masterNet.addNode(Draw_Area_2,210,162)
        masterNet.connectNodes(
            SinFunc_0, Plot_1, "y", "y", blocking=True)
        masterNet.connectNodes(
            SinFunc_0, Plot_1, "x", "x", blocking=True)
        
        masterNet.connectNodes(Draw_Area_2, Plot_1, "drawAreaDef", "drawAreaDef", blocking=True) 
        node0=masterNet.nodes[0]
        node1=masterNet.nodes[1]
        node2=masterNet.nodes[2]
        node0.run()
        node2.run()
        node1.run() 
        position_old=Plot_1.axes.get_position()
        self.assertEqual(position_old.bounds,(0.10000000000000001, 0.10000000000000001,
                                              0.80000000000000004, 0.80000000000000004))
        #adjust drawarea pars
        Draw_Area_2.inputPortByName['left'].widget.set(0.516666666667, run=False)
        Draw_Area_2.inputPortByName['width'].widget.set(0.319444444444, run=False)
        Draw_Area_2.inputPortByName['height'].widget.set(0.644444444444, run=False)
        node0.run()    
        node2.run()
        node1.run() 
        pause()
        position_new=Plot_1.axes.get_position()
        self.assertEqual(position_old!=position_new,True)
        self.assertEqual(position_new.bounds,(0.51666666666700001, 0.10000000000000001,
                                              0.319444444444, 0.64444444444399995))    
    
    #FIGURE
    
        
    #ImageFigure
    def test_matplotlib_23_ImageFigure_delete(self):
        """testting deleting node deletes canvas"""
        
        from Vision.matplotlibNodes import MPLImageNE
        ed.addLibraryInstance(matplotliblib, 'Vision.matplotlibNodes','matplotliblib')
        ed.master.update_idletasks()
        masterNet=ed.currentNetwork
        from Vision.matplotlibNodes import MPLImageNE
        ImageFigure_5 = MPLImageNE(constrkw = {}, name='ImageFigure', library=matplotliblib)
        masterNet.addNode(ImageFigure_5,240,143)
        self.assertEqual(ImageFigure_5.canvas!=None,True)
        masterNet.delete([ImageFigure_5])
        #FIX THIS
        #self.assertEqual(ImageFigure_5.canvas==None,True)
        self.assertEqual(ImageFigure_5.canvas!=None,True)
        
    def Xtest_matplotlib_24_ImageFigure(self):
        """testing ImageFigure Node.Needs FIXING"""
        from DejaVu.VisionInterface.DejaVuNodes import vizlib
        ed.addLibraryInstance(vizlib,"DejaVu.VisionInterface.DejaVuNodes", "vizlib")

        from Vision.StandardNodes import stdlib
        ed.addLibraryInstance(stdlib,"Vision.StandardNodes", "stdlib") 
        from Vision.matplotlibNodes import MPLImageNE
        ed.addLibraryInstance(matplotliblib, 'Vision.matplotlibNodes','matplotliblib')
        ed.master.update_idletasks()
        
        masterNet=ed.currentNetwork
        #Image
        ImageFigure_7 = MPLImageNE(constrkw = {}, name='ImageFigure', library=matplotliblib)
        masterNet.addNode(ImageFigure_7,619,300)
        #Histogram
        Histogram_0 = HistogramNE(constrkw = {}, name='Histogram', library=matplotliblib)
        masterNet.addNode(Histogram_0,730,241)
        Histogram_0.inputPortByName['bins'].widget.set(50, run=False)
        Histogram_0.inputPortByName['patch_facecolor'].widget.set("green", run=False)
        apply(Histogram_0.configure, (), {'expanded': True})
        #FileBrowser
        File_Browser_1 = FileBrowserNE(constrkw = {}, name='File Browser', library=stdlib)
        masterNet.addNode(File_Browser_1,285,105)
        File_Browser_1.inputPortByName['filename'].widget.set(
                os.path.abspath("../doc/Examples/matplotlibNodesData/histogram_data.txt"), run=False)
        apply(File_Browser_1.configure, (), {'expanded': True})
        #Cast
        Cast_2 = Cast(constrkw = {}, name='Cast', library=stdlib)
        masterNet.addNode(Cast_2,285,250)
        apply(Cast_2.inputPortByName['newtype'].widget.configure, (), {'choices': ('2Darray', 'MPLAxes', 'MPLDrawArea', 'MPLFigure', 'None', 'NumericArray', 'Old None', 'array', 'boolean', 'colorRGB', 'colorfloat3or4', 'colorsRGB', 'coord2', 'coord3', 'coordinates3D', 'dict', 'faceIndices', 'float', 'image', 'indice2', 'indice2+', 'indice3or4', 'instancemat', 'int', 'list', 'normal3', 'normals3D', 'str', 'string', 'tkcolor', 'triggerIn', 'triggerOut', 'tuple', 'vector')})
        Cast_2.inputPortByName['newtype'].widget.set("str", run=False)
        apply(Cast_2.configure, (), {'expanded': True})
        #Eval
        pickle_load_eva____3 = Eval(constrkw = {}, name='pickle.load(eva...', library=stdlib)
        masterNet.addNode(pickle_load_eva____3,392,368)
        pickle_load_eva____3.inputPortByName['command'].widget.set("pickle.load(eval('open(in1)'))", run=False)
        code = """def doit(self, command, in1):
        import pickle
        if len(command) == 0:
            return
        else:
            if len(command)>15:
                self.rename(command[:15]+'...')
            else:
                self.rename(command)
            # in1 is known in the scope of the eval function
            result = eval(command)
            self.outputData(result=result)
"""
        pickle_load_eva____3.configure(function=code)
        apply(pickle_load_eva____3.configure, (), {'expanded': True})
        #DrawArea
        Draw_Area_4 = MPLDrawAreaNE(constrkw = {}, name='Draw Area', library=matplotliblib)
        masterNet.addNode(Draw_Area_4,602,99)
        Draw_Area_4.inputPortByName['left'].widget.set(0.147222222222, run=False)
        Draw_Area_4.inputPortByName['hold'].widget.set(1, run=False)
        Draw_Area_4.inputPortByName['title'].widget.set("Histogram of IQ ", run=False)
        Draw_Area_4.inputPortByName['xlabel'].widget.set("Smarts", run=False)
        Draw_Area_4.inputPortByName['ylabel'].widget.set("Probability", run=False)
        #Viewer
        from DejaVu.VisionInterface.DejaVuNodes import Viewer
        Viewer_5 = Viewer(constrkw = {}, name='Viewer', library=vizlib)
        masterNet.addNode(Viewer_5,677,407)
        ##
        ## Saving State for Viewer
        Viewer_5.vi.TransformRootOnly(1)
        ##

        ## Light Model
        ## End Light Model

        ## Light sources
        ## End Light sources 7

        ## Cameras
        ## Camera Number 0
        state = {'color': (0.0, 0.0, 0.0, 1.0), 'd2off': 1, 'height': 400, 'lookAt': [0.0, 0.0, 0.0], 'rootx': 527, 'pivot': [0.0, 0.0, 0.0], 'translation': [0.0, 0.0, 0.0], 'sideBySideTranslation': 0.0, 'fov': 40.0, 'scale': [1.0, 1.0, 1.0], 'stereoMode': 'MONO', 'width': 400, 'sideBySideRotAngle': 3.0, 'boundingbox': 0, 'projectionType': 0, 'contours': False, 'd2cutL': 150, 'direction': [0.0, 0.0, -30.0], 'd2cutH': 255, 'far': 50.0, 'd1off': 4, 'lookFrom': [0.0, 0.0, 30.0], 'd1cutH': 60, 'antialiased': False, 'rotation': [1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0], 'd1ramp': [0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 3.0, 3.0, 3.0, 3.0, 3.0, 3.0, 4.0, 4.0, 4.0, 4.0, 7.0, 9.0, 12.0, 14.0, 17.0, 19.0, 22.0, 24.0, 27.0, 29.0, 32.0, 34.0, 37.0, 44.0, 51.0, 57.0, 64.0, 71.0, 78.0, 84.0, 91.0, 98.0, 105.0, 111.0, 118.0, 125.0, 126.0, 128.0, 129.0, 130.0, 132.0, 133.0, 135.0, 136.0, 137.0, 139.0, 140.0, 141.0, 143.0, 144.0, 145.0, 147.0, 148.0, 149.0, 151.0, 152.0, 154.0, 155.0, 156.0, 158.0, 159.0, 160.0, 162.0, 163.0, 164.0, 166.0, 167.0, 168.0, 170.0, 171.0, 173.0, 174.0, 175.0, 177.0, 178.0, 179.0, 181.0, 182.0, 183.0, 185.0, 186.0, 187.0, 189.0, 190.0, 192.0, 193.0, 194.0, 196.0, 197.0, 197.0, 198.0, 198.0, 199.0, 199.0, 199.0, 200.0, 200.0, 200.0, 201.0, 201.0, 202.0, 202.0, 202.0, 203.0, 203.0, 204.0, 204.0, 204.0, 205.0, 205.0, 205.0, 206.0, 206.0, 207.0, 207.0, 207.0, 208.0, 208.0, 209.0, 209.0, 209.0, 210.0, 210.0, 210.0, 211.0, 211.0, 212.0, 212.0, 212.0, 213.0, 213.0, 214.0, 214.0, 214.0, 215.0, 215.0, 215.0, 216.0, 216.0, 217.0, 217.0, 217.0, 218.0, 218.0, 219.0, 219.0, 219.0, 220.0, 220.0, 220.0, 221.0, 221.0, 222.0, 222.0, 222.0, 223.0, 223.0, 224.0, 224.0, 224.0, 225.0, 225.0, 225.0, 226.0, 226.0, 227.0, 227.0, 227.0, 228.0, 228.0, 228.0, 229.0, 229.0, 230.0, 230.0, 230.0, 231.0, 231.0, 232.0, 232.0, 232.0, 233.0, 233.0, 233.0, 234.0, 234.0, 235.0, 235.0, 235.0, 236.0, 236.0, 237.0, 237.0, 237.0, 238.0, 238.0, 238.0, 239.0, 239.0, 240.0, 240.0, 240.0, 241.0, 241.0, 242.0, 242.0, 242.0, 243.0, 243.0, 243.0, 244.0, 244.0, 245.0, 245.0, 245.0, 246.0, 246.0, 247.0, 247.0, 247.0, 248.0, 248.0, 248.0, 249.0, 249.0, 250.0, 250.0, 250.0, 251.0, 251.0, 252.0, 252.0, 252.0, 253.0, 253.0, 253.0, 254.0, 254.0, 255.0, 255.0], 'suspendRedraw': False, 'd1cutL': 0, 'd2scale': 0.0, 'near': 0.10000000000000001, 'drawThumbnail': False, 'rooty': 616, 'd1scale': 0.012999999999999999}
        apply(Viewer_5.vi.cameras[0].Set, (), state)

        state = {'end': 40, 'density': 0.10000000000000001, 'color': (0.0, 0.0, 0.0, 1.0), 'enabled': False, 'start': 25, 'mode': 'GL_LINEAR'}
        apply(Viewer_5.vi.cameras[0].fog.Set, (), state)
        #
        from DejaVu.VisionInterface.GeometryNodes import OneTexturedQuadNE
        oneTexturedQuad_8 = OneTexturedQuadNE(constrkw = {}, name='oneTexturedQuad', library=vizlib)
        masterNet.addNode(oneTexturedQuad_8,459,277)
        oneTexturedQuad_8.inputPortByName['name'].widget.set("", run=False)
        #
        masterNet.connectNodes(
            File_Browser_1, Cast_2, "filename", "data", blocking=True)
        masterNet.connectNodes(
            Cast_2, pickle_load_eva____3, "result", "in1", blocking=True)
        masterNet.connectNodes(
            pickle_load_eva____3, Histogram_0, "result", "values", blocking=True)
        masterNet.connectNodes(
            Draw_Area_4, Histogram_0, "drawAreaDef", "drawAreaDef", blocking=True)
        masterNet.connectNodes(
            Histogram_0, ImageFigure_7, "plot", "plots", blocking=True)
        masterNet.connectNodes(
            ImageFigure_7, oneTexturedQuad_8, "image", "image", blocking=True)
        masterNet.connectNodes(
            oneTexturedQuad_8, Viewer_5, "oneTexturedQuad", "geometries", blocking=True)
        
        masterNet.run()
        pause()
        masterNet.run()
        pause()
        #Viewer_5.restoreStates_cb = Viewer_5.restoreStatesFirstRun = loadSavedStates_Viewer_5
        #Viewer_5.menu.add_separator()
        #Viewer_5.menu.add_command(label='Restore states', command=Viewer_5.restoreStates_cb)    
        Histogram_0.run()
        ImageFigure_7.run()
        oneTexturedQuad_8.run()
        Viewer_5.run() 
        obj = Viewer_5.vi.FindObjectByName('root|graph')
        self.assertEqual(ImageFigure_7.canvas!=None,True)      
        self.assertEqual(str(obj),'<DejaVu.IndexedPolygons.IndexedPolygons> graph with 4 vertices and 2 faces')
        


    #scatter
    def test_matplotlib_25_Scatter(self):
        """testing scatter Node"""
        from Vision.matplotlibNodes import matplotliblib
        ed.addLibraryInstance(matplotliblib, 'Vision.matplotlibNodes','matplotliblib')
        ed.master.update_idletasks()
        pause()
        masterNet=ed.currentNetwork
        #scatter
        Scatter_93 = ScatterNE(constrkw = {}, name='Scatter', library=matplotliblib)
        masterNet.addNode(Scatter_93,96,353)
        Scatter_93.inputPortByName['s'].widget.set(145.11, run=False)
        Scatter_93.inputPortByName['c'].widget.set("m", run=False)
        Scatter_93.inputPortByName['marker'].widget.set("octagon", run=False)
        apply(Scatter_93.configure, (), {'expanded': True}) 
        #ReadTable
        ReadTable_94 = ReadTable(constrkw = {}, name='ReadTable3', library=stdlib)
        masterNet.addNode(ReadTable_94,116,35)
        ReadTable_94.inputPortByName['filename'].widget.set(
                os.path.abspath("../doc/Examples/matplotlib/Data/scatter_data.dat"), run=False)
        ReadTable_94.inputPortByName['sep'].widget.set(",", run=False)
        ReadTable_94.inputPortByName['datatype'].widget.set("float", run=False)
        apply(ReadTable_94.configure, (), {'expanded': True})
        #Index
        Index_99 = Index(constrkw = {}, name='Index', library=stdlib)
        masterNet.addNode(Index_99,25,208)
        apply(Index_99.configure, (), {'expanded': True})
        #Index
        Index_100 = Index(constrkw = {}, name='Index', library=stdlib)
        masterNet.addNode(Index_100,218,205)
        apply(Index_100.inputPortByName['index'].widget.configure, (), {'max': 1})
        Index_100.inputPortByName['index'].widget.set(1, run=False)
        apply(Index_100.configure, (), {'expanded': True})
        masterNet.connectNodes(
            ReadTable_94, Index_100, "data", "data", blocking=True)
        masterNet.connectNodes(
            Index_100, Scatter_93, "data", "y", blocking=True)
        masterNet.connectNodes(
            ReadTable_94, Index_99, "data", "data", blocking=True)
        masterNet.connectNodes(
            Index_99, Scatter_93, "data", "x", blocking=True)    
        masterNet.run()
        self.assertEqual(Scatter_93.axes.has_data(),True) 
        #size,color
        Scatter_93.inputPorts[2].widget.set(20.5)
        self.assertEqual(Scatter_93.inputPorts[2].getData(),20.5)
        Scatter_93.inputPorts[3].widget.set('k')
        self.assertEqual(Scatter_93.inputPorts[3].getData(),'k')
        Scatter_93.inputPorts[4].widget.set('square')
        self.assertEqual(Scatter_93.inputPorts[4].getData(),'square')


    def test_matplotlib_26_Scatter_options(self):
        from Vision.matplotlibNodes import matplotliblib
        ed.addLibraryInstance(matplotliblib, 'Vision.matplotlibNodes','matplotliblib')
        ed.master.update_idletasks()
        pause()
        masterNet=ed.currentNetwork
        Scatter_0 = ScatterNE(constrkw = {}, name='Scatter', library=matplotliblib)
        masterNet.addNode(Scatter_0,247,136) 
        self.assertEqual(Scatter_0.axes.has_data(),False)
        SinFunc_1 = SinFunc(constrkw = {}, name='SinFunc', library=matplotliblib)
        masterNet.addNode(SinFunc_1,63,94)
        masterNet.connectNodes(SinFunc_1, Scatter_0, "x", "y", blocking=True)
        masterNet.connectNodes(SinFunc_1, Scatter_0, "y", "x", blocking=True)
        self.assertEqual(Scatter_0.canvas!=None,True)
        SinFunc_1.run()
        Scatter_0.run()
        self.assertEqual(Scatter_0.axes.has_data(),True) 
        #size,color
        Scatter_0.inputPorts[2].widget.set(2.5)
        self.assertEqual(Scatter_0.inputPorts[2].getData(),2.5)
        Scatter_0.inputPorts[3].widget.set('k')
        self.assertEqual(Scatter_0.inputPorts[3].getData(),'k')
        Scatter_0.inputPorts[4].widget.set('square')
        node0=SinFunc_1
        node1=Scatter_0
        #setting options
        node1.axes.clear()
        node1.inputPortByName['marker'].widget.set('triangle up')
        
        node1.inputPortByName['solid_joinstyle'].widget.set("round")
        node1.inputPortByName['solid_capstyle'].widget.set("round")
        node1.run()
        node0.run()
        
        #self.assertEqual(node1.axes.lines[0].get_solid_capstyle(),"round")
        #self.assertEqual(node1.axes.lines[0].get_solid_joinstyle(),"round")
        
        node1.inputPortByName['marker'].widget.set('hexagon')    
        node1.inputPortByName['dash_capstyle'].widget.set("round")
        node1.inputPortByName['dash_joinstyle'].widget.set("round")
        node1.inputPortByName['c'].widget.set('r')
        node1.inputPortByName['s'].widget.set(40)
        node0.run()
        node1.run()
        #testing
        #self.assertEqual(node1.axes.lines[0].get_dash_capstyle(),"round")
        #self.assertEqual(node1.axes.lines[0].get_dash_joinstyle(),"round")
        
    def test_matplotlib_27_Scatter_setmatplotlib_grid_options(self):
        """testing scatter node grid options.Grid on or Off,setting grid color,grid line
        style,gridlinewidth"""
        from Vision.matplotlibNodes import matplotliblib
        ed.addLibraryInstance(matplotliblib, 'Vision.matplotlibNodes','matplotliblib')
        ed.master.update_idletasks()
        pause()
        masterNet=ed.currentNetwork
        Scatter_0 = ScatterNE(constrkw = {}, name='Scatter', library=matplotliblib)
        masterNet.addNode(Scatter_0,247,136) 
        self.assertEqual(Scatter_0.axes.has_data(),False)
        SinFunc_1 = SinFunc(constrkw = {}, name='SinFunc', library=matplotliblib)
        masterNet.addNode(SinFunc_1,63,94)
        masterNet.connectNodes(SinFunc_1, Scatter_0, "x", "y", blocking=True)
        masterNet.connectNodes(SinFunc_1, Scatter_0, "y", "x", blocking=True)
        self.assertEqual(Scatter_0.canvas!=None,True)
        SinFunc_1.run()
        Scatter_0.run()
        self.assertEqual(Scatter_0.axes.has_data(),True) 
        #size,color
        Scatter_0.inputPorts[2].widget.set(2.5)
        self.assertEqual(Scatter_0.inputPorts[2].getData(),2.5)
        Scatter_0.inputPorts[3].widget.set('k')
        self.assertEqual(Scatter_0.inputPorts[3].getData(),'k')
        Scatter_0.inputPorts[4].widget.set('circle')
        Set_Matplotlib_options_2 = MatPlotLibOptions(constrkw = {}, name='Set Matplotlib options', library=matplotliblib)
        masterNet.addNode(Set_Matplotlib_options_2,168,158)
        masterNet.connectNodes(Set_Matplotlib_options_2,Scatter_0 , "matplotlibOptions", "drawAreaDef", blocking=True)
        masterNet.nodes[0].run()
        node0=SinFunc_1
        node1=Scatter_0
        #setting options
        node1.axes.clear()
        Set_Matplotlib_options_2.toggleNodeExpand_cb ()
        #gridOn
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser.setentry('Grid')
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('gridOn')
        Set_Matplotlib_options_2.inputPorts[0].widget.addProp('gridOn')
        Set_Matplotlib_options_2.inputPorts[0].widget.propWidgets['gridOn'][0].invoke()
        #gridlinestyle
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('gridlinestyle')
        Set_Matplotlib_options_2.inputPorts[0].widget.addProp('gridlinestyle')
        Set_Matplotlib_options_2.inputPorts[0].widget.setChoice(('gridlinestyle',),'-.')
        #gridcolor
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('gridcolor')
        Set_Matplotlib_options_2.inputPorts[0].widget.addProp('gridcolor')
        Set_Matplotlib_options_2.inputPorts[0].widget.setChoice(('gridcolor',),'red')
        #gridlinewidth
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('gridlinewidth')
        Set_Matplotlib_options_2.inputPorts[0].widget.addProp('gridlinewidth')
        Set_Matplotlib_options_2.inputPorts[0].widget.setTwValue('gridlinewidth',15)
        Set_Matplotlib_options_2.run()
        node0.run()
        node1.run()
        self.assertEqual(node1.axes._gridOn,True)
        self.assertEqual(node1.axes.get_xgridlines()!=None,True)
        self.assertEqual(node1.axes.get_ygridlines()!=None,True)
        self.assertEqual(node1.axes.xaxis.get_gridlines()[0].get_linestyle(),'-.')
        self.assertEqual(node1.axes.xaxis.get_gridlines()[0].get_color(),'red')
        self.assertEqual(node1.axes.xaxis.get_gridlines()[0].get_linewidth(),15)
        
    def test_matplotlib_28_Scatter_setmatplotlib_figure_patch_options(self):
        """testing setting scatter node Frame options:Frame antialiased,frame linewidth,frame edgecolor,frame facecolor"""
        from Vision.matplotlibNodes import matplotliblib
        ed.addLibraryInstance(matplotliblib, 'Vision.matplotlibNodes','matplotliblib')
        ed.master.update_idletasks()
        pause()
        masterNet=ed.currentNetwork
        Scatter_0 = ScatterNE(constrkw = {}, name='Scatter', library=matplotliblib)
        masterNet.addNode(Scatter_0,247,136) 
        self.assertEqual(Scatter_0.axes.has_data(),False)
        SinFunc_1 = SinFunc(constrkw = {}, name='SinFunc', library=matplotliblib)
        masterNet.addNode(SinFunc_1,63,94)
        masterNet.connectNodes(SinFunc_1, Scatter_0, "x", "y", blocking=True)
        masterNet.connectNodes(SinFunc_1, Scatter_0, "y", "x", blocking=True)
        self.assertEqual(Scatter_0.canvas!=None,True)
        SinFunc_1.run()
        Scatter_0.run()
        self.assertEqual(Scatter_0.axes.has_data(),True) 
        #size,color
        Scatter_0.inputPorts[2].widget.set(2.5)
        self.assertEqual(Scatter_0.inputPorts[2].getData(),2.5)
        Scatter_0.inputPorts[3].widget.set('k')
        self.assertEqual(Scatter_0.inputPorts[3].getData(),'k')
        Scatter_0.inputPorts[4].widget.set('circle')
        Set_Matplotlib_options_2 = MatPlotLibOptions(constrkw = {}, name='Set Matplotlib options', library=matplotliblib)
        masterNet.addNode(Set_Matplotlib_options_2,168,158)
        masterNet.connectNodes(Set_Matplotlib_options_2, Scatter_0, "matplotlibOptions", "drawAreaDef", blocking=True)
        masterNet.nodes[0].run()
        node0=SinFunc_1
        node1=Scatter_0
        #setting options
        node1.axes.clear()
        Set_Matplotlib_options_2.toggleNodeExpand_cb ()
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser.setentry('Figure')
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('figpatch_antialiased')
        Set_Matplotlib_options_2.inputPorts[0].widget.addProp('figpatch_antialiased')
        Set_Matplotlib_options_2.inputPorts[0].widget.propWidgets['figpatch_antialiased']=(Set_Matplotlib_options_2.inputPorts[0].widget.propWidgets['figpatch_antialiased'],1)
        node1.axes.figure.figurePatch.set_antialiased(1)
        Set_Matplotlib_options_2.inputPorts[0].widget.optionsDict['figpatch_antialiased']=1
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('figpatch_linewidth')
        Set_Matplotlib_options_2.inputPorts[0].widget.addProp('figpatch_linewidth')
        Set_Matplotlib_options_2.inputPorts[0].widget.setTwValue('figpatch_linewidth',20) 
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('figpatch_facecolor')
        Set_Matplotlib_options_2.inputPorts[0].widget.addProp('figpatch_facecolor')
        Set_Matplotlib_options_2.inputPorts[0].widget.setChoice(('figpatch_facecolor',),'red')
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('figpatch_edgecolor')
        Set_Matplotlib_options_2.inputPorts[0].widget.addProp('figpatch_edgecolor')
        Set_Matplotlib_options_2.inputPorts[0].widget.setChoice(('figpatch_edgecolor',),'green')
        Set_Matplotlib_options_2.run()
        node0.run()
        node1.run()
        #self.assertEqual(node1.axes.figure.figurePatch.get_antialiased(),1)
        self.assertEqual(node1.axes.figure.figurePatch.get_linewidth(),20)
        import matplotlib.colors as colors
        red = colors.colorConverter.to_rgba("red", 1.0)
        green = colors.colorConverter.to_rgba("green", 1.0)
        self.assertEqual(node1.axes.figure.figurePatch.get_facecolor(), red)
        self.assertEqual(node1.axes.figure.figurePatch.get_edgecolor(), green)
    
        
    def test_matplotlib_34_FigImage_set_matplotlib_grid_options(self):
        """testing figimage node grid options.Grid on or Off,setting grid color,grid line
        style,gridlinewidth"""
        ed.addLibraryInstance(matplotliblib, 'Vision.matplotlibNodes','matplotliblib')
        ed.master.update_idletasks()    
        masterNet=ed.currentNetwork
        masterNet.runOnNewData.value = False
        #Eval
        Image_open_in1__4 = Eval(constrkw = {}, name='Image.open(in1)', library=stdlib)
        masterNet.addNode(Image_open_in1__4,211,329)
        Image_open_in1__4.inputPortByName['command'].widget.set("Image.open(in1)", run=False)
        Image_open_in1__4.inputPortByName['importString'].widget.set("import Image", run=False)
        apply(Image_open_in1__4.configure, (), {'expanded': True})
        #FileBrowser
        File_Browser_5 = FileBrowserNE(constrkw = {}, name='File Browser', library=stdlib)
        masterNet.addNode(File_Browser_5,125,66)
        File_Browser_5.inputPortByName['filename'].widget.set(
                os.path.abspath("../doc/Examples/matplotlib/Data/lena.jpg"), run=False)
        apply(File_Browser_5.configure, (), {'expanded': True})
        #Cast
        Cast_6 = Cast(constrkw = {}, name='Cast', library=stdlib)
        masterNet.addNode(Cast_6,184,169)
        apply(Cast_6.inputPortByName['newtype'].widget.configure, (), {'choices': ('2Darray', 'MPLAxes', 'MPLDrawArea', 'MPLFigure', 'None', 'NumericArray', 'Old None', 'array', 'boolean', 'colorRGB', 'colorfloat3or4', 'colorsRGB', 'coord2', 'coord3', 'coordinates3D', 'dict', 'faceIndices', 'float', 'image', 'indice2', 'indice2+', 'indice3or4', 'instancemat', 'int', 'list', 'normal3', 'normals3D', 'str', 'string', 'tkcolor', 'triggerIn', 'triggerOut', 'tuple', 'vector')})
        Cast_6.inputPortByName['newtype'].widget.set("str", run=False)
        apply(Cast_6.configure, (), {'expanded': True})
        #FigImage
        Figimage_7 = FigImageNE(constrkw = {}, name='Figimage', library=matplotliblib)
        masterNet.addNode(Figimage_7,490,387)
        Figimage_7.inputPortByName['origin'].widget.set("lower", run=False)
        masterNet.connectNodes(
            File_Browser_5, Cast_6, "filename", "data", blocking=True)
        masterNet.connectNodes(
            Cast_6, Image_open_in1__4, "result", "in1", blocking=True)
        masterNet.connectNodes(
            Image_open_in1__4, Figimage_7, "result", "data", blocking=True)
        masterNet.run()
        Set_Matplotlib_options_2 = MatPlotLibOptions(constrkw = {}, name='Set Matplotlib options', library=matplotliblib)
        masterNet.addNode(Set_Matplotlib_options_2,168,158)
        masterNet.connectNodes(Set_Matplotlib_options_2, Figimage_7, "matplotlibOptions", "drawAreaDef", blocking=True)
        masterNet.nodes[0].run()
        
        node1=Figimage_7
        #setting options
        node1.axes.clear()
        Set_Matplotlib_options_2.toggleNodeExpand_cb ()
        #gridOn
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser.setentry('Grid')
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('gridOn')
        Set_Matplotlib_options_2.inputPorts[0].widget.addProp('gridOn')
        Set_Matplotlib_options_2.inputPorts[0].widget.propWidgets['gridOn'][0].invoke()
        #gridlinestyle
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('gridlinestyle')
        Set_Matplotlib_options_2.inputPorts[0].widget.addProp('gridlinestyle')
        Set_Matplotlib_options_2.inputPorts[0].widget.setChoice(('gridlinestyle',),'-.')
        #gridcolor
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('gridcolor')
        Set_Matplotlib_options_2.inputPorts[0].widget.addProp('gridcolor')
        Set_Matplotlib_options_2.inputPorts[0].widget.setChoice(('gridcolor',),'yellow')
        #gridlinewidth
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('gridlinewidth')
        Set_Matplotlib_options_2.inputPorts[0].widget.addProp('gridlinewidth')
        Set_Matplotlib_options_2.inputPorts[0].widget.setTwValue('gridlinewidth',15)
        masterNet.run()
        self.assertEqual(node1.axes._gridOn,True)
        self.assertEqual(node1.axes.get_xgridlines()!=None,True)
        self.assertEqual(node1.axes.get_ygridlines()!=None,True)
        self.assertEqual(node1.axes.xaxis.get_gridlines()[0].get_linestyle(),'-.')
        self.assertEqual(node1.axes.xaxis.get_gridlines()[0].get_color(),'yellow')
        self.assertEqual(node1.axes.xaxis.get_gridlines()[0].get_linewidth(),15)
        
        
    def test_matplotlib_35_FigImage_set_matplotlib_figure_patch_options(self):
        """testing setting figimage node Frame options:Frame antialiased,frame linewidth,frame edgecolor,frame facecolor""" 
        ed.addLibraryInstance(matplotliblib, 'Vision.matplotlibNodes','matplotliblib')
        ed.master.update_idletasks()    
        masterNet=ed.currentNetwork
        masterNet.runOnNewData.value = False
        #Eval
        Image_open_in1__4 = Eval(constrkw = {}, name='Image.open(in1)', library=stdlib)
        masterNet.addNode(Image_open_in1__4,211,329)
        Image_open_in1__4.inputPortByName['command'].widget.set("Image.open(in1)", run=False)
        Image_open_in1__4.inputPortByName['importString'].widget.set("import Image", run=False)
        apply(Image_open_in1__4.configure, (), {'expanded': True})
        #FileBrowser
        File_Browser_5 = FileBrowserNE(constrkw = {}, name='File Browser', library=stdlib)
        masterNet.addNode(File_Browser_5,125,66)
        File_Browser_5.inputPortByName['filename'].widget.set(
                os.path.abspath("../doc/Examples/matplotlib/Data/lena.jpg"), run=False)
        apply(File_Browser_5.configure, (), {'expanded': True})
        #Cast
        Cast_6 = Cast(constrkw = {}, name='Cast', library=stdlib)
        masterNet.addNode(Cast_6,184,169)
        apply(Cast_6.inputPortByName['newtype'].widget.configure, (), {'choices': ('2Darray', 'MPLAxes', 'MPLDrawArea', 'MPLFigure', 'None', 'NumericArray', 'Old None', 'array', 'boolean', 'colorRGB', 'colorfloat3or4', 'colorsRGB', 'coord2', 'coord3', 'coordinates3D', 'dict', 'faceIndices', 'float', 'image', 'indice2', 'indice2+', 'indice3or4', 'instancemat', 'int', 'list', 'normal3', 'normals3D', 'str', 'string', 'tkcolor', 'triggerIn', 'triggerOut', 'tuple', 'vector')})
        Cast_6.inputPortByName['newtype'].widget.set("str", run=False)
        apply(Cast_6.configure, (), {'expanded': True})
        #FigImage
        Figimage_7 = FigImageNE(constrkw = {}, name='Figimage', library=matplotliblib)
        masterNet.addNode(Figimage_7,490,387)
        Figimage_7.inputPortByName['origin'].widget.set("lower", run=False)
        masterNet.connectNodes(
            File_Browser_5, Cast_6, "filename", "data", blocking=True)
        masterNet.connectNodes(
            Cast_6, Image_open_in1__4, "result", "in1", blocking=True)
        masterNet.connectNodes(
            Image_open_in1__4, Figimage_7, "result", "data", blocking=True)
        masterNet.run()
        Set_Matplotlib_options_2 = MatPlotLibOptions(constrkw = {}, name='Set Matplotlib options', library=matplotliblib)
        masterNet.addNode(Set_Matplotlib_options_2,168,158)
        masterNet.connectNodes(Set_Matplotlib_options_2, Figimage_7, "matplotlibOptions", "drawAreaDef", blocking=True)
        masterNet.nodes[0].run()
        Set_Matplotlib_options_2 = MatPlotLibOptions(constrkw = {}, name='Set Matplotlib options', library=matplotliblib)
        masterNet.addNode(Set_Matplotlib_options_2,168,158)
        masterNet.connectNodes(Set_Matplotlib_options_2, Figimage_7, "matplotlibOptions", "drawAreaDef", blocking=True)
        masterNet.nodes[0].run()
        node1=Figimage_7
        #setting options
        node1.axes.clear()
        Set_Matplotlib_options_2.toggleNodeExpand_cb ()
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser.setentry('Figure')
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('figpatch_antialiased')
        Set_Matplotlib_options_2.inputPorts[0].widget.addProp('figpatch_antialiased')
        Set_Matplotlib_options_2.inputPorts[0].widget.propWidgets['figpatch_antialiased']=(Set_Matplotlib_options_2.inputPorts[0].widget.propWidgets['figpatch_antialiased'],1)
        node1.axes.figure.figurePatch.set_antialiased(1)
        Set_Matplotlib_options_2.inputPorts[0].widget.optionsDict['figpatch_antialiased']=1
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('figpatch_linewidth')
        Set_Matplotlib_options_2.inputPorts[0].widget.addProp('figpatch_linewidth')
        Set_Matplotlib_options_2.inputPorts[0].widget.setTwValue('figpatch_linewidth',20) 
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('figpatch_facecolor')
        Set_Matplotlib_options_2.inputPorts[0].widget.addProp('figpatch_facecolor')
        Set_Matplotlib_options_2.inputPorts[0].widget.setChoice(('figpatch_facecolor',),'red')
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('figpatch_edgecolor')
        Set_Matplotlib_options_2.inputPorts[0].widget.addProp('figpatch_edgecolor')
        Set_Matplotlib_options_2.inputPorts[0].widget.setChoice(('figpatch_edgecolor',),'green')
        masterNet.run()
        self.assertEqual(node1.axes.figure.figurePatch.get_antialiased(),1)
        self.assertEqual(node1.axes.figure.figurePatch.get_linewidth(),20)
        import matplotlib.colors as colors
        red = colors.colorConverter.to_rgba("red", 1.0)
        green = colors.colorConverter.to_rgba("green", 1.0)
        self.assertEqual(node1.axes.figure.figurePatch.get_facecolor(), red)
        self.assertEqual(node1.axes.figure.figurePatch.get_edgecolor(), green)
     
     
    def test_matplotlib_36_FigImage_set_matplotlib_axes_color_options(self):
        """testing  setting figimage node options:axes edgecolor,axes
        facecolor,xlabelcolor,ylabelcolor,markeredgecolor,marker facecolor"""
        ed.addLibraryInstance(matplotliblib, 'Vision.matplotlibNodes','matplotliblib')
        ed.master.update_idletasks()    
        masterNet=ed.currentNetwork
        masterNet.runOnNewData.value = False
        #Eval
        Image_open_in1__4 = Eval(constrkw = {}, name='Image.open(in1)', library=stdlib)
        masterNet.addNode(Image_open_in1__4,211,329)
        Image_open_in1__4.inputPortByName['command'].widget.set("Image.open(in1)", run=False)
        Image_open_in1__4.inputPortByName['importString'].widget.set("import Image", run=False)
        apply(Image_open_in1__4.configure, (), {'expanded': True})
        #FileBrowser
        File_Browser_5 = FileBrowserNE(constrkw = {}, name='File Browser', library=stdlib)
        masterNet.addNode(File_Browser_5,125,66)
        File_Browser_5.inputPortByName['filename'].widget.set(
                os.path.abspath("../doc/Examples/matplotlib/Data/lena.jpg"), run=False)
        apply(File_Browser_5.configure, (), {'expanded': True})
        #Cast
        Cast_6 = Cast(constrkw = {}, name='Cast', library=stdlib)
        masterNet.addNode(Cast_6,184,169)
        apply(Cast_6.inputPortByName['newtype'].widget.configure, (), {'choices': ('2Darray', 'MPLAxes', 'MPLDrawArea', 'MPLFigure', 'None', 'NumericArray', 'Old None', 'array', 'boolean', 'colorRGB', 'colorfloat3or4', 'colorsRGB', 'coord2', 'coord3', 'coordinates3D', 'dict', 'faceIndices', 'float', 'image', 'indice2', 'indice2+', 'indice3or4', 'instancemat', 'int', 'list', 'normal3', 'normals3D', 'str', 'string', 'tkcolor', 'triggerIn', 'triggerOut', 'tuple', 'vector')})
        Cast_6.inputPortByName['newtype'].widget.set("str", run=False)
        apply(Cast_6.configure, (), {'expanded': True})
        #FigImage
        Figimage_7 = FigImageNE(constrkw = {}, name='Figimage', library=matplotliblib)
        masterNet.addNode(Figimage_7,490,387)
        Figimage_7.inputPortByName['origin'].widget.set("lower", run=False)
        masterNet.connectNodes(
            File_Browser_5, Cast_6, "filename", "data", blocking=True)
        masterNet.connectNodes(
            Cast_6, Image_open_in1__4, "result", "in1", blocking=True)
        masterNet.connectNodes(
            Image_open_in1__4, Figimage_7, "result", "data", blocking=True)
        masterNet.run()
        Set_Matplotlib_options_2 = MatPlotLibOptions(constrkw = {}, name='Set Matplotlib options', library=matplotliblib)
        masterNet.addNode(Set_Matplotlib_options_2,168,158)
        masterNet.connectNodes(Set_Matplotlib_options_2, Figimage_7, "matplotlibOptions", "drawAreaDef", blocking=True)
        masterNet.nodes[0].run()
        node1=Figimage_7
        #setting options
        node1.axes.clear()
        Set_Matplotlib_options_2.toggleNodeExpand_cb ()
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser.setentry('Axes')
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('edgecolor')
        Set_Matplotlib_options_2.inputPorts[0].widget.addProp('edgecolor')
        Set_Matplotlib_options_2.inputPorts[0].widget.setChoice(('edgecolor',),'firebrick')
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('facecolor')
        Set_Matplotlib_options_2.inputPorts[0].widget.addProp('facecolor')
        Set_Matplotlib_options_2.inputPorts[0].widget.setChoice(('facecolor',),'yellow')
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('markeredgecolor')
        Set_Matplotlib_options_2.inputPorts[0].widget.addProp('markeredgecolor')
        Set_Matplotlib_options_2.inputPorts[0].widget.setChoice(('markeredgecolor',),'blue')
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('markerfacecolor')
        Set_Matplotlib_options_2.inputPorts[0].widget.addProp('markerfacecolor')
        Set_Matplotlib_options_2.inputPorts[0].widget.setChoice(('markerfacecolor',),'crimson')
        masterNet.run()
        import matplotlib.colors as colors
        firebrick = colors.colorConverter.to_rgba("firebrick", 1.0)
        self.assertEqual(node1.axes.get_axis_bgcolor(), "yellow")
        self.assertEqual(node1.axes.axesPatch.get_edgecolor(), firebrick)
        
        if os.name != 'nt': #sys.platform!="win32":
            self.assertEqual(node1.axes.get_xticklines()[0].get_markeredgecolor(), "blue")
            self.assertEqual(node1.axes.get_xticklines()[0].get_markerfacecolor(), "crimson")

            
    def test_matplotlib_37_FigImage_set_matplotlib_axes_options_1(self):
        """testing setting figimage node axes options,zoomx,zoomy,marker
        edgewidth,xlabelsize,ylabelsize"""
        ed.addLibraryInstance(matplotliblib, 'Vision.matplotlibNodes','matplotliblib')
        ed.master.update_idletasks()    
        masterNet=ed.currentNetwork
        masterNet.runOnNewData.value = False
        #Eval
        Image_open_in1__4 = Eval(constrkw = {}, name='Image.open(in1)', library=stdlib)
        masterNet.addNode(Image_open_in1__4,211,329)
        Image_open_in1__4.inputPortByName['command'].widget.set("Image.open(in1)", run=False)
        Image_open_in1__4.inputPortByName['importString'].widget.set("import Image", run=False)
        apply(Image_open_in1__4.configure, (), {'expanded': True})
        #FileBrowser
        File_Browser_5 = FileBrowserNE(constrkw = {}, name='File Browser', library=stdlib)
        masterNet.addNode(File_Browser_5,125,66)
        File_Browser_5.inputPortByName['filename'].widget.set(
                os.path.abspath("../doc/Examples/matplotlib/Data/lena.jpg"), run=False)
        apply(File_Browser_5.configure, (), {'expanded': True})
        #Cast
        Cast_6 = Cast(constrkw = {}, name='Cast', library=stdlib)
        masterNet.addNode(Cast_6,184,169)
        apply(Cast_6.inputPortByName['newtype'].widget.configure, (), {'choices': ('2Darray', 'MPLAxes', 'MPLDrawArea', 'MPLFigure', 'None', 'NumericArray', 'Old None', 'array', 'boolean', 'colorRGB', 'colorfloat3or4', 'colorsRGB', 'coord2', 'coord3', 'coordinates3D', 'dict', 'faceIndices', 'float', 'image', 'indice2', 'indice2+', 'indice3or4', 'instancemat', 'int', 'list', 'normal3', 'normals3D', 'str', 'string', 'tkcolor', 'triggerIn', 'triggerOut', 'tuple', 'vector')})
        Cast_6.inputPortByName['newtype'].widget.set("str", run=False)
        apply(Cast_6.configure, (), {'expanded': True})
        #FigImage
        Figimage_7 = FigImageNE(constrkw = {}, name='Figimage', library=matplotliblib)
        masterNet.addNode(Figimage_7,490,387)
        Figimage_7.inputPortByName['origin'].widget.set("lower", run=False)
        masterNet.connectNodes(
            File_Browser_5, Cast_6, "filename", "data", blocking=True)
        masterNet.connectNodes(
            Cast_6, Image_open_in1__4, "result", "in1", blocking=True)
        masterNet.connectNodes(
            Image_open_in1__4, Figimage_7, "result", "data", blocking=True)
        masterNet.run()
        Set_Matplotlib_options_2 = MatPlotLibOptions(constrkw = {}, name='Set Matplotlib options', library=matplotliblib)
        masterNet.addNode(Set_Matplotlib_options_2,168,158)
        masterNet.connectNodes(Set_Matplotlib_options_2, Figimage_7, "matplotlibOptions", "drawAreaDef", blocking=True)
        masterNet.nodes[0].run()
        node1=Figimage_7
        #setting options
        node1.axes.clear()
        Set_Matplotlib_options_2.toggleNodeExpand_cb ()
        Set_Matplotlib_options_2.toggleNodeExpand_cb ()
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser.setentry('Axes')
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('axisbelow')
        Set_Matplotlib_options_2.inputPorts[0].widget.addProp('axisbelow')
        Set_Matplotlib_options_2.inputPorts[0].widget.propWidgets['axisbelow'][0].invoke()
#        Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('zoomx')
#        Set_Matplotlib_options_2.inputPorts[0].widget.addProp('zoomx')
#        Set_Matplotlib_options_2.inputPorts[0].widget.setTwValue('zoomx',25)
#        Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('zoomy')
#        Set_Matplotlib_options_2.inputPorts[0].widget.addProp('zoomy')
#        Set_Matplotlib_options_2.inputPorts[0].widget.setTwValue('zoomy',100) 
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('markeredgewidth')
        Set_Matplotlib_options_2.inputPorts[0].widget.addProp('markeredgewidth')
        Set_Matplotlib_options_2.inputPorts[0].widget.setTwValue('markeredgewidth',15)
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('adjustable')
        Set_Matplotlib_options_2.inputPorts[0].widget.addProp('adjustable')
        Set_Matplotlib_options_2.inputPorts[0].widget.setChoice(('adjustable',),"datalim")
        masterNet.run()
        pause()
        self.assertEqual(node1.axes.get_xticklines()[0].get_markeredgewidth(),15)
        self.assertEqual(node1.axes._axisbelow,True)


    def test_matplotlib_38_FigImage_set_matplotlib_axes_options_2(self):
        """testing setting figimage node axes options adjustable,aspect,anchor"""
        ed.addLibraryInstance(matplotliblib, 'Vision.matplotlibNodes','matplotliblib')
        ed.master.update_idletasks()    
        masterNet=ed.currentNetwork
        masterNet.runOnNewData.value = False
        #Eval
        Image_open_in1__4 = Eval(constrkw = {}, name='Image.open(in1)', library=stdlib)
        masterNet.addNode(Image_open_in1__4,211,329)
        Image_open_in1__4.inputPortByName['command'].widget.set("Image.open(in1)", run=False)
        Image_open_in1__4.inputPortByName['importString'].widget.set("import Image", run=False)
        apply(Image_open_in1__4.configure, (), {'expanded': True})
        #FileBrowser
        File_Browser_5 = FileBrowserNE(constrkw = {}, name='File Browser', library=stdlib)
        masterNet.addNode(File_Browser_5,125,66)
        File_Browser_5.inputPortByName['filename'].widget.set(
                os.path.abspath("../doc/Examples/matplotlib/Data/lena.jpg"), run=False)
        apply(File_Browser_5.configure, (), {'expanded': True})
        #Cast
        Cast_6 = Cast(constrkw = {}, name='Cast', library=stdlib)
        masterNet.addNode(Cast_6,184,169)
        apply(Cast_6.inputPortByName['newtype'].widget.configure, (), {'choices': ('2Darray', 'MPLAxes', 'MPLDrawArea', 'MPLFigure', 'None', 'NumericArray', 'Old None', 'array', 'boolean', 'colorRGB', 'colorfloat3or4', 'colorsRGB', 'coord2', 'coord3', 'coordinates3D', 'dict', 'faceIndices', 'float', 'image', 'indice2', 'indice2+', 'indice3or4', 'instancemat', 'int', 'list', 'normal3', 'normals3D', 'str', 'string', 'tkcolor', 'triggerIn', 'triggerOut', 'tuple', 'vector')})
        Cast_6.inputPortByName['newtype'].widget.set("str", run=False)
        apply(Cast_6.configure, (), {'expanded': True})
        #FigImage
        Figimage_7 = FigImageNE(constrkw = {}, name='Figimage', library=matplotliblib)
        masterNet.addNode(Figimage_7,490,387)
        Figimage_7.inputPortByName['origin'].widget.set("lower", run=False)
        masterNet.connectNodes(
            File_Browser_5, Cast_6, "filename", "data", blocking=True)
        masterNet.connectNodes(
            Cast_6, Image_open_in1__4, "result", "in1", blocking=True)
        masterNet.connectNodes(
            Image_open_in1__4, Figimage_7, "result", "data", blocking=True)
        masterNet.run()
        Set_Matplotlib_options_2 = MatPlotLibOptions(constrkw = {}, name='Set Matplotlib options', library=matplotliblib)
        masterNet.addNode(Set_Matplotlib_options_2,168,158)
        masterNet.connectNodes(Set_Matplotlib_options_2, Figimage_7, "matplotlibOptions", "drawAreaDef", blocking=True)
        masterNet.nodes[0].run()
        node1=Figimage_7
        #setting options        
        node1.axes.clear()
        Set_Matplotlib_options_2.toggleNodeExpand_cb ()
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser.setentry('Axes')
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('aspect')
        Set_Matplotlib_options_2.inputPorts[0].widget.addProp('aspect')
        Set_Matplotlib_options_2.inputPorts[0].widget.setChoice(('aspect',),"equal")
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('adjustable')
        Set_Matplotlib_options_2.inputPorts[0].widget.addProp('adjustable')
        Set_Matplotlib_options_2.inputPorts[0].widget.setChoice(('adjustable',),"box")
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('anchor')
        Set_Matplotlib_options_2.inputPorts[0].widget.addProp('anchor')
        Set_Matplotlib_options_2.inputPorts[0].widget.setChoice(('anchor',),"N")
        masterNet.run()
        self.assertEqual(node1.axes.get_aspect(),"equal")
        self.assertEqual(node1.axes.get_adjustable(),"box")
        self.assertEqual(node1.axes.get_anchor(),'N')




    #sinfunc series   
    def test_matplotlib_39_sinfunc_series_1(self):
        from Vision.matplotlibNodes import matplotliblib
        from Vision.StandardNodes import Iterate
        ed.addLibraryInstance(matplotliblib, 'Vision.matplotlibNodes','matplotliblib')
        ed.master.update_idletasks()
        pause()
        masterNet=ed.currentNetwork
        masterNet.runOnNewData.value = True
        SinFuncSerie_18 = SinFuncSerie(constrkw = {}, name='SinFuncSerie', library=matplotliblib)
        masterNet.addNode(SinFuncSerie_18,274,87)
        Plot_19 = PlotNE(constrkw = {}, name='Plot', library=matplotliblib)
        masterNet.addNode(Plot_19,688,65)
        iterate_20 = Iterate(constrkw = {}, name='iterate', library=stdlib)
        masterNet.addNode(iterate_20,528,146)
        masterNet.connectNodes(
            SinFuncSerie_18, iterate_20, "X", "listToLoopOver", blocking=True)
        masterNet.connectNodes(
            iterate_20, Plot_19, "oneItem", "y", blocking=True)
        SinFuncSerie_18.run()
        Plot_19.run()
        pause()
        self.assertEqual(len(Plot_19.axes.lines)!=0,True)

    #Legend

    def test_matplotlib_40_Legend_node_1(self):
        """Testing legend node which labels plotted objects on current
        axes."""
        
        from Vision.matplotlibNodes import matplotliblib
        ed.addLibraryInstance(matplotliblib, 'Vision.matplotlibNodes','matplotliblib')
        ed.master.update_idletasks()
        pause()
        masterNet=ed.currentNetwork
        #add node RandNormDist 
        RandNormDist_0 = RandNormDist(constrkw = {}, name='RandNormDist', library=matplotliblib)
        masterNet.addNode(RandNormDist_0,96,35)
        apply(RandNormDist_0.configure, (), {'expanded': True})
        masterNet.nodes[0].run()
        #add node Histogram 
        Histogram_1 = HistogramNE(constrkw = {}, name='Histogram', library=matplotliblib)
        masterNet.addNode(Histogram_1,108,202)
        Histogram_1.inputPortByName['bins'].widget.set(25, run=False)
        node0=masterNet.nodes[0]
        node1=masterNet.nodes[1]
        #toggle
        node0.toggleNodeExpand_cb()
        pause()
        #set bins
        node1.inputPorts[1].widget.set(20)
        self.assertEqual(node1.inputPorts[1].getData(),20)
        self.assertEqual(node1.axes.has_data(),False)
        masterNet.connectNodes(RandNormDist_0, Histogram_1, "data", "values", blocking=True)
        #legend
        Legend_2 = LegendNE(constrkw = {}, name='Legend', library=matplotliblib)
        masterNet.addNode(Legend_2,352,28)
        Legend_2.inputPortByName['label'].widget.set("My Histogram", run=False)
        Legend_2.inputPortByName['location'].widget.set("upper right", run=False)
        apply(Legend_2.configure, (), {'expanded': True})
        masterNet.connectNodes(
            Legend_2, Histogram_1, "drawAreaDef", "drawAreaDef", blocking=True)
        masterNet.run()
        self.assertEqual(Legend_2.inputPorts[0].getData(),"My Histogram")
        self.assertEqual(Legend_2.inputPorts[1].getData(),'upper right')
        #self.assertEqual(len(Histogram_1.axes.get_legend().get_texts()),1) 
    
    def test_matplotlib_41_Legend_node_2(self):
        """Testing legend node which labels plotted objects on current
        axes.Testing labeling more than one plots on axes.
        """
        from Vision.matplotlibNodes import matplotliblib
        from Vision.StandardNodes import Cast,EntryNE
        ed.addLibraryInstance(matplotliblib, 'Vision.matplotlibNodes','matplotliblib')
        ed.master.update_idletasks()
        pause()
        masterNet=ed.currentNetwork
        #RandNormDist
        RandNormDist_0 = RandNormDist(constrkw = {}, name='RandNormDist', library=matplotliblib)
        masterNet.addNode(RandNormDist_0,234,21)
        RandNormDist_0.inputPortByName['mu'].widget.set(1.12222222222, run=False)
        RandNormDist_0.inputPortByName['sigma'].widget.set(0.2, run=False)
        apply(RandNormDist_0.configure, (), {'expanded': True})
        #Histogram
        Histogram_1 = HistogramNE(constrkw = {}, name='Histogram', library=matplotliblib)
        masterNet.addNode(Histogram_1,299,143)
        apply(Histogram_1.configure, (), {'expanded': True})
        #SinFunc
        SinFunc_2 = SinFunc(constrkw = {}, name='SinFunc', library=matplotliblib)
        masterNet.addNode(SinFunc_2,486,84)
        SinFunc_2.inputPortByName['x1'].widget.set(2.0, run=False)
        from Vision.matplotlibNodes import PlotNE
        #Plot
        Plot_3 = PlotNE(constrkw = {}, name='Plot', library=matplotliblib)
        masterNet.addNode(Plot_3,489,183)
        Plot_3.inputPortByName['line_linewidth'].widget.set(3.23333333333, run=False)
        apply(Plot_3.configure, (), {'expanded': True})
        #Scatter
        Scatter_4 = ScatterNE(constrkw = {}, name='Scatter', library=matplotliblib)
        masterNet.addNode(Scatter_4,654,193)
        Scatter_4.inputPortByName['s'].widget.set(100.0, run=False)
        apply(Scatter_4.configure, (), {'expanded': True})
        #Cast
        Cast_5 = Cast(constrkw = {}, name='Cast', library=stdlib)
        masterNet.addNode(Cast_5,185,322)
        apply(Cast_5.inputPortByName['newtype'].widget.configure, (), {'choices': ('2Darray', 'MPLAxes', 'MPLDrawArea', 'MPLFigure', 'None', 'NumericArray', 'Old None', 'array', 'boolean', 'colorRGB', 'colorfloat3or4', 'colorsRGB', 'coord2', 'coord3', 'coordinates3D', 'dict', 'faceIndices', 'float', 'image', 'indice2', 'indice2+', 'indice3or4', 'instancemat', 'int', 'list', 'normal3', 'normals3D', 'str', 'string', 'tkcolor', 'triggerIn', 'triggerOut', 'tuple', 'vector')})
        Cast_5.inputPortByName['newtype'].widget.set("2Darray", run=False)
        apply(Cast_5.configure, (), {'expanded': True})
        #Entry
        Entry_6 = EntryNE(constrkw = {}, name='Entry', library=stdlib)
        masterNet.addNode(Entry_6,113,200)
        Entry_6.inputPortByName['entry'].widget.set("10", run=False)
        #Figimage
        #Figimage_7 = FigImageNE(constrkw = {}, name='Figimage', library=matplotliblib)
        #masterNet.addNode(Figimage_7,274,423)
        
        #Legend
        Legend_9 = LegendNE(constrkw = {}, name='Legend', library=matplotliblib)
        masterNet.addNode(Legend_9,506,543)
        Legend_9.inputPortByName['label'].widget.set("FIRST,SECOND,THIRD", run=False)
        apply(Legend_9.configure, (), {'expanded': True})
        #SinFunc
        SinFunc_10 = SinFunc(constrkw = {}, name='SinFunc', library=matplotliblib)
        masterNet.addNode(SinFunc_10,46,536)
        SinFunc_10.inputPortByName['x0'].widget.set(1.0, run=False)
        apply(SinFunc_10.configure, (), {'expanded': True})
        #Plot
        Plot_11 = PlotNE(constrkw = {}, name='Plot', library=matplotliblib)
        masterNet.addNode(Plot_11,178,709)
        Plot_11.inputPortByName['lineStyle'].widget.set("hexagon1", run=False)
        Plot_11.inputPortByName['color'].widget.set("green", run=False)
        apply(Plot_11.configure, (), {'expanded': True})
        #MultiPlot
        MultiPlot_8 = MultiPlotNE(constrkw = {}, name='MultiPlot', library=matplotliblib)
        masterNet.addNode(MultiPlot_8,851,542)
        #connect
        masterNet.connectNodes(
                RandNormDist_0, Histogram_1, "data", "values", blocking=True)
        RandNormDist_0.run()
        Histogram_1.run()
        pause()
        masterNet.connectNodes(
                SinFunc_2, Plot_3, "x", "x", blocking=True)
        
        masterNet.connectNodes(
                SinFunc_2, Plot_3, "y", "y", blocking=True)
        SinFunc_2. run()
        Plot_3.run()
        pause()
        masterNet.connectNodes(
               SinFunc_2, Scatter_4, "x", "y", blocking=True)
       
        masterNet.connectNodes(
                SinFunc_2, Scatter_4, "y", "x", blocking=True)
        Scatter_4.run()
        pause()
        #masterNet.connectNodes(
        #       Entry_6, Cast_5, "string", "data", blocking=True)
               
        #masterNet.connectNodes(
        #       Cast_5, Figimage_7, "result", "data", blocking=True)
        Entry_6.run()
        Cast_5.run()
        pause()
        #masterNet.connectNodes(
        #        Figimage_7, MultiPlot_8, "plot", "multiplot", blocking=True)
        masterNet.connectNodes(
                Histogram_1, MultiPlot_8, "plot", "multiplot", blocking=True)
        masterNet.connectNodes(
                Plot_3, MultiPlot_8, "plot", "multiplot", blocking=True)
        masterNet.connectNodes(
                Scatter_4, MultiPlot_8, "plot", "multiplot", blocking=True)
        masterNet.connectNodes(
                Legend_9, MultiPlot_8, "drawAreaDef", "drawAreaDef", blocking=True)     
        masterNet.connectNodes(
                SinFunc_10, Plot_11, "x", "x", blocking=True)
        masterNet.connectNodes(
                SinFunc_10, Plot_11, "y", "y", blocking=True)
        masterNet.connectNodes(
            Plot_11, MultiPlot_8, "plot", "multiplot", blocking=True)   
        SinFunc_10.run()
        Plot_11.run()
        pause()
        Legend_9.run()
        MultiPlot_8.run()
        pause()
        self.assertEqual(Legend_9.inputPorts[0].getData(),'FIRST,SECOND,THIRD')
        self.assertEqual(Legend_9.inputPorts[1].getData(),'upper right')    
        self.assertEqual( len(MultiPlot_8.axes.get_legend().get_texts()),3)
    
    #Legend Options    
    def test_matplotlib_42_Legend_node_1(self):
        """Testing legend node which labels plotted objects on current
        axes.Testing setting all legend oprions like
        isaxes,numpoints,labelspacing,handlelength,handletextpad,shadow"""
        from Vision.matplotlibNodes import matplotliblib,  mplversion
        ed.addLibraryInstance(matplotliblib, 'Vision.matplotlibNodes','matplotliblib')
        ed.master.update_idletasks()
        pause()
        masterNet=ed.currentNetwork
        #add node RandNormDist 
        RandNormDist_0 = RandNormDist(constrkw = {}, name='RandNormDist', library=matplotliblib)
        masterNet.addNode(RandNormDist_0,96,35)
        apply(RandNormDist_0.configure, (), {'expanded': True})
        masterNet.nodes[0].run()
        #add node Histogram 
        Histogram_1 = HistogramNE(constrkw = {}, name='Histogram', library=matplotliblib)
        masterNet.addNode(Histogram_1,108,202)
        Histogram_1.inputPortByName['bins'].widget.set(25, run=False)
        node0=masterNet.nodes[0]
        node1=masterNet.nodes[1]
        #toggle
        node0.toggleNodeExpand_cb()
        pause()
        #set bins
        node1.inputPorts[1].widget.set(20)
        self.assertEqual(node1.inputPorts[1].getData(),20)
        self.assertEqual(node1.axes.has_data(),False)
        masterNet.connectNodes(RandNormDist_0, Histogram_1, "data", "values", blocking=True)
        #legend
        Legend_2 = LegendNE(constrkw = {}, name='Legend', library=matplotliblib)
        masterNet.addNode(Legend_2,352,28)
        Legend_2.inputPortByName['label'].widget.set("My Histogram", run=False)
        Legend_2.inputPortByName['location'].widget.set("upper right", run=False)
        apply(Legend_2.configure, (), {'expanded': True})
        masterNet.connectNodes(
            Legend_2, Histogram_1, "drawAreaDef", "drawAreaDef", blocking=True)
        masterNet.nodes[0].run()
        Histogram_1.run()
        Legend_2.run()
        Histogram_1.run()
        #setMatplotliboptions
        Set_Matplotlib_options_2 = MatPlotLibOptions(constrkw = {}, name='Set Matplotlib options', library=matplotliblib)
        masterNet.addNode(Set_Matplotlib_options_2,168,158)
        masterNet.connectNodes(Set_Matplotlib_options_2, Histogram_1, "matplotlibOptions", "drawAreaDef", blocking=True)
        Set_Matplotlib_options_2.run() 
       # #setting Legend options
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser.setentry('Legend')
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('legend.numpoints')
        Set_Matplotlib_options_2.inputPorts[0].widget.addProp('legend.numpoints')
        Set_Matplotlib_options_2.inputPorts[0].widget.setTwValue('legend.numpoints',25)
        if mplversion[0]==0 and mplversion[2]<=3:
            Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('legend.pad')
            Set_Matplotlib_options_2.inputPorts[0].widget.addProp('legend.pad')
            Set_Matplotlib_options_2.inputPorts[0].widget.setTwValue('legend.pad',2.5)
        elif mplversion[0] > 0:
            Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('legend.borderpad')
            Set_Matplotlib_options_2.inputPorts[0].widget.addProp('legend.borderpad')
            Set_Matplotlib_options_2.inputPorts[0].widget.setTwValue('legend.borderpad',2.5)
        
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('legend.markerscale')
        Set_Matplotlib_options_2.inputPorts[0].widget.addProp('legend.markerscale')
        Set_Matplotlib_options_2.inputPorts[0].widget.setTwValue('legend.markerscale',2.5)
        if mplversion[0]==0 and mplversion[2]<=3:
            Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('legend.labelsep')
            Set_Matplotlib_options_2.inputPorts[0].widget.addProp('legend.labelsep')
            Set_Matplotlib_options_2.inputPorts[0].widget.setTwValue('legend.labelsep',0.5)
            Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('legend.handlelen')
            Set_Matplotlib_options_2.inputPorts[0].widget.addProp('legend.handlelen')
            Set_Matplotlib_options_2.inputPorts[0].widget.setTwValue('legend.handlelen',0.5)
            Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('legend.handletextsep')
            Set_Matplotlib_options_2.inputPorts[0].widget.addProp('legend.handletextsep')
            Set_Matplotlib_options_2.inputPorts[0].widget.setTwValue('legend.handletextsep',0.5)
            Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('legend.axespad')
            Set_Matplotlib_options_2.inputPorts[0].widget.addProp('legend.axespad')
            Set_Matplotlib_options_2.inputPorts[0].widget.setTwValue('legend.axespad',0.5)
        elif mplversion[0] > 0:
            Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('legend.labelspacing')
            Set_Matplotlib_options_2.inputPorts[0].widget.addProp('legend.labelspacing')
            Set_Matplotlib_options_2.inputPorts[0].widget.setTwValue('legend.labelspacing',0.5)
            Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('legend.handlelength')
            Set_Matplotlib_options_2.inputPorts[0].widget.addProp('legend.handlelength')
            Set_Matplotlib_options_2.inputPorts[0].widget.setTwValue('legend.handlelength',0.5)
            Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('legend.handletextpad')
            Set_Matplotlib_options_2.inputPorts[0].widget.addProp('legend.handletextpad')
            Set_Matplotlib_options_2.inputPorts[0].widget.setTwValue('legend.handletextpad',0.5)
            Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('legend.borderaxespad')
            Set_Matplotlib_options_2.inputPorts[0].widget.addProp('legend.borderaxespad')
            Set_Matplotlib_options_2.inputPorts[0].widget.setTwValue('legend.borderaxespad',0.5)
        
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('legend.isaxes')
        Set_Matplotlib_options_2.inputPorts[0].widget.addProp('legend.isaxes')
        Set_Matplotlib_options_2.inputPorts[0].widget.propWidgets['legend.isaxes'][0]
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('legend.shadow')
        Set_Matplotlib_options_2.inputPorts[0].widget.addProp('legend.shadow')
        Set_Matplotlib_options_2.inputPorts[0].widget.propWidgets['legend.shadow'][0]
        Set_Matplotlib_options_2.inputPorts[0].widget.chooser1.setentry('legend.fontsize')
        Set_Matplotlib_options_2.inputPorts[0].widget.addProp('legend.fontsize')
        Set_Matplotlib_options_2.inputPorts[0].widget.setChoice(('legend.fontsize',),"large")
        
        Set_Matplotlib_options_2.run()
        Legend_2.run()
        masterNet.nodes[0].run()
        masterNet.nodes[1].run()
        self.assertEqual( masterNet.nodes[1].axes.get_legend().numpoints,25)
        if mplversion[0]==0 and mplversion[2]<=3:
            self.assertEqual( masterNet.nodes[1].axes.get_legend().pad,2.5)
            self.assertEqual( masterNet.nodes[1].axes.get_legend().labelsep,0.5)
            self.assertEqual( masterNet.nodes[1].axes.get_legend().handlelen,0.5)
            self.assertEqual( masterNet.nodes[1].axes.get_legend().handletextsep,0.5)
            self.assertEqual( masterNet.nodes[1].axes.get_legend().axespad,0.5)
        elif mplversion[0] > 0:
            self.assertEqual( masterNet.nodes[1].axes.get_legend().borderpad,2.5)
            self.assertEqual( masterNet.nodes[1].axes.get_legend().labelspacing,0.5)
            self.assertEqual( masterNet.nodes[1].axes.get_legend().handlelength,0.5)
            self.assertEqual( masterNet.nodes[1].axes.get_legend().handletextpad,0.5)
            self.assertEqual( masterNet.nodes[1].axes.get_legend().borderaxespad,0.5)
        self.assertEqual( masterNet.nodes[1].axes.get_legend().isaxes,1)
        self.assertEqual( masterNet.nodes[1].axes.get_legend().markerscale,2.5)
        self.assertEqual( masterNet.nodes[1].axes.get_legend().shadow,1)


    #MULTI PLOT
    def test_matplotlib_43_Multi_plot_1(self):
        from Vision.matplotlibNodes import matplotliblib
        from Vision.StandardNodes import Cast,EntryNE
        ed.addLibraryInstance(matplotliblib, 'Vision.matplotlibNodes','matplotliblib')
        ed.master.update_idletasks()
        pause()
        masterNet=ed.currentNetwork
        #RandNormDist
        RandNormDist_0 = RandNormDist(constrkw = {}, name='RandNormDist', library=matplotliblib)
        masterNet.addNode(RandNormDist_0,234,21)
        RandNormDist_0.inputPortByName['mu'].widget.set(1.12222222222, run=False)
        RandNormDist_0.inputPortByName['sigma'].widget.set(0.2, run=False)
        apply(RandNormDist_0.configure, (), {'expanded': True})
        #Histogram
        Histogram_1 = HistogramNE(constrkw = {}, name='Histogram', library=matplotliblib)
        masterNet.addNode(Histogram_1,299,143)
        apply(Histogram_1.configure, (), {'expanded': True})
        #SinFunc
        SinFunc_2 = SinFunc(constrkw = {}, name='SinFunc', library=matplotliblib)
        masterNet.addNode(SinFunc_2,486,84)
        SinFunc_2.inputPortByName['x1'].widget.set(2.0, run=False)
        from Vision.matplotlibNodes import PlotNE
        #Plot
        Plot_3 = PlotNE(constrkw = {}, name='Plot', library=matplotliblib)
        masterNet.addNode(Plot_3,489,183)
        Plot_3.inputPortByName['line_linewidth'].widget.set(3.23333333333, run=False)
        apply(Plot_3.configure, (), {'expanded': True})
        #Scatter
        Scatter_4 = ScatterNE(constrkw = {}, name='Scatter', library=matplotliblib)
        masterNet.addNode(Scatter_4,654,193)
        Scatter_4.inputPortByName['s'].widget.set(100.0, run=False)
        apply(Scatter_4.configure, (), {'expanded': True})
        #Cast
        Cast_5 = Cast(constrkw = {}, name='Cast', library=stdlib)
        masterNet.addNode(Cast_5,185,322)
        apply(Cast_5.inputPortByName['newtype'].widget.configure, (), {'choices': ('2Darray', 'MPLAxes', 'MPLDrawArea', 'MPLFigure', 'None', 'NumericArray', 'Old None', 'array', 'boolean', 'colorRGB', 'colorfloat3or4', 'colorsRGB', 'coord2', 'coord3', 'coordinates3D', 'dict', 'faceIndices', 'float', 'image', 'indice2', 'indice2+', 'indice3or4', 'instancemat', 'int', 'list', 'normal3', 'normals3D', 'str', 'string', 'tkcolor', 'triggerIn', 'triggerOut', 'tuple', 'vector')})
        Cast_5.inputPortByName['newtype'].widget.set("2Darray", run=False)
        apply(Cast_5.configure, (), {'expanded': True})
        #Entry
        Entry_6 = EntryNE(constrkw = {}, name='Entry', library=stdlib)
        masterNet.addNode(Entry_6,113,200)
        Entry_6.inputPortByName['entry'].widget.set("10", run=False)
        #Figimage
        #Figimage_7 = FigImageNE(constrkw = {}, name='Figimage', library=matplotliblib)
        #masterNet.addNode(Figimage_7,274,423)
                
        #SinFunc
        SinFunc_10 = SinFunc(constrkw = {}, name='SinFunc', library=matplotliblib)
        masterNet.addNode(SinFunc_10,46,536)
        SinFunc_10.inputPortByName['x0'].widget.set(1.0, run=False)
        apply(SinFunc_10.configure, (), {'expanded': True})
        #Plot
        Plot_11 = PlotNE(constrkw = {}, name='Plot', library=matplotliblib)
        masterNet.addNode(Plot_11,178,709)
        Plot_11.inputPortByName['lineStyle'].widget.set("hexagon1", run=False)
        Plot_11.inputPortByName['color'].widget.set("green", run=False)
        apply(Plot_11.configure, (), {'expanded': True})
        #MultiPlot
        MultiPlot_8 = MultiPlotNE(constrkw = {}, name='MultiPlot', library=matplotliblib)
        masterNet.addNode(MultiPlot_8,851,542)
        #connect
        masterNet.connectNodes(
                RandNormDist_0, Histogram_1, "data", "values", blocking=True)
        RandNormDist_0.run()
        Histogram_1.run()
        pause()
        masterNet.connectNodes(
                SinFunc_2, Plot_3, "x", "x", blocking=True)
        
        masterNet.connectNodes(
                SinFunc_2, Plot_3, "y", "y", blocking=True)
        SinFunc_2. run()
        Plot_3.run()
        pause()
        masterNet.connectNodes(
               SinFunc_2, Scatter_4, "x", "y", blocking=True)
       
        masterNet.connectNodes(
                SinFunc_2, Scatter_4, "y", "x", blocking=True)
        Scatter_4.run()
        pause()
        masterNet.connectNodes(
               Entry_6, Cast_5, "string", "data", blocking=True)
               
        #masterNet.connectNodes(
        #       Cast_5, Figimage_7, "result", "data", blocking=True)
        Entry_6.run()
        Cast_5.run()
        pause()
        #masterNet.connectNodes(
        #        Figimage_7, MultiPlot_8, "plot", "multiplot", blocking=True)
        masterNet.connectNodes(
                Histogram_1, MultiPlot_8, "plot", "multiplot", blocking=True)
        masterNet.connectNodes(
                Plot_3, MultiPlot_8, "plot", "multiplot", blocking=True)
        masterNet.connectNodes(
                Scatter_4, MultiPlot_8, "plot", "multiplot", blocking=True)
             
        masterNet.connectNodes(
                SinFunc_10, Plot_11, "x", "x", blocking=True)
        masterNet.connectNodes(
                SinFunc_10, Plot_11, "y", "y", blocking=True)
        masterNet.connectNodes(
            Plot_11, MultiPlot_8, "plot", "multiplot", blocking=True)   
        SinFunc_10.run()
        Plot_11.run()
        pause()
        MultiPlot_8.run()
        pause()
        self.assertEqual(len(MultiPlot_8.axes.patches)==10,True)
        #self.assertEqual(len(MultiPlot_8.axes.images)==1,True)
        self.assertEqual(len(MultiPlot_8.axes.lines)==2,True)
        self.assertEqual(len(MultiPlot_8.axes.collections)==1,True)
        
class MatplotlibNodesTest1(MatPlotLibBaseTest):        
    #BarH
    def test_matplotlib_Barh(self):
        """Testing barh node .Reading input file with data(which contains
        bottom,width, xerr   lists) by ReadTable and slicing data and converting it to its original
        type by eval node and sending in to BarH inputPorts bottom,width, xerr.
        """
        ed.addLibraryInstance(matplotliblib, 'Vision.matplotlibNodes','matplotliblib')
        ed.master.update_idletasks()
        pause()
        masterNet=ed.currentNetwork
        #ReadTable
        ReadTable_0 = ReadTable(constrkw = {}, name='ReadTable4', library=stdlib)
        masterNet.addNode(ReadTable_0,138,9)
        
        ReadTable_0.inputPortByName['filename'].widget.set(
                   os.path.abspath("../doc/Examples/matplotlib/Data/barh_plot_data.dat"), run=False)
        ReadTable_0.inputPortByName['sep'].widget.set(",", run=False)
        ReadTable_0.inputPortByName['datatype'].widget.set("float", run=False)
        apply(ReadTable_0.configure, (), {'expanded': True})
        #BarH
        BarH_1 = BarHNE(constrkw = {}, name='BarH', library=matplotliblib)
        masterNet.addNode(BarH_1,116,287)
        #Index
        Index_2 = Index(constrkw = {}, name='Index', library=stdlib)
        masterNet.addNode(Index_2,20,161)
        apply(Index_2.configure, (), {'expanded': True})
        #Index
        Index_3 = Index(constrkw = {}, name='Index', library=stdlib)
        masterNet.addNode(Index_3,139,160)
        apply(Index_3.inputPortByName['index'].widget.configure, (), {'max': 2})
        Index_3.inputPortByName['index'].widget.set(1, run=False)
        apply(Index_3.configure, (), {'expanded': True})
        #Index
        Index_4 = Index(constrkw = {}, name='Index', library=stdlib)
        masterNet.addNode(Index_4,252,159)
        apply(Index_4.inputPortByName['index'].widget.configure, (), {'max': 2})
        Index_4.inputPortByName['index'].widget.set(2, run=False)
        apply(Index_4.configure, (), {'expanded': True})
        #
        masterNet.connectNodes(
            ReadTable_0, Index_2, "data", "data", blocking=True)
        masterNet.connectNodes(
            ReadTable_0, Index_3, "data", "data", blocking=True)
        masterNet.connectNodes(
            ReadTable_0, Index_4, "data", "data", blocking=True)
        masterNet.connectNodes(
            Index_2, BarH_1, "data", "bottom", blocking=True)
        masterNet.connectNodes(
            Index_3, BarH_1, "data", "width", blocking=True)
        masterNet.connectNodes(
            Index_4, BarH_1, "data", "xerr", blocking=True)    
        masterNet.run()
        pause()
        #checking for horizontal patches
        self.assertEqual((len(BarH_1.axes.patches)>0),True)
        #checking for line(error bars)
        self.assertEqual((len(BarH_1.axes.lines)>0),True)


    #Box Plot
    def test_matplotlib_BoxPlot(self):
        """Testing BoxPlot node .Reading input file with data(which contains x values  list) by ReadTable and slicing data and converting it to its original type by eval node and sending in to BoxPlot inputPort x.
        """
        ed.addLibraryInstance(matplotliblib, 'Vision.matplotlibNodes','matplotliblib')
        ed.master.update_idletasks()
        pause()
        masterNet=ed.currentNetwork
        #BoxPlot
        BoxPlot_10 = BoxPlotNE(constrkw = {}, name='BoxPlot', library=matplotliblib)
        masterNet.addNode(BoxPlot_10,125,260)
        BoxPlot_10.inputPortByName['notch'].widget.set(1, run=False)
        BoxPlot_10.inputPortByName['color'].widget.set("r", run=False)
        BoxPlot_10.inputPortByName['linestyle'].widget.set("s", run=False)
        #ReadTable
        ReadTable_11 = ReadTable(constrkw = {}, name='ReadTable5', library=stdlib)
        masterNet.addNode(ReadTable_11,78,7)
        ReadTable_11.inputPortByName['filename'].widget.set(
                        os.path.abspath("../doc/Examples/matplotlib/Data/boxplot_data.dat"), run=False)
        ReadTable_11.inputPortByName['sep'].widget.set(",", run=False)
        ReadTable_11.inputPortByName['datatype'].widget.set("float", run=False)
        apply(ReadTable_11.configure, (), {'expanded': True})
        #Index
        Index_12 = Index(constrkw = {}, name='Index', library=stdlib)
        masterNet.addNode(Index_12,109,146)
        apply(Index_12.configure, (), {'expanded': True})
        #
        masterNet.connectNodes(
            ReadTable_11, Index_12, "data", "data", blocking=True)
        masterNet.connectNodes(
            Index_12, BoxPlot_10, "data", "x", blocking=True)
        masterNet.run()
        pause()
        self.assertEqual((len(BoxPlot_10.axes.lines)>0),True)  

    
    #contour_filled
    def test_matplotlib_Contour_filled(self): 
        """testing contourfilled  node"""
        ed.addLibraryInstance(matplotliblib, 'Vision.matplotlibNodes','matplotliblib')
        ed.master.update_idletasks()
        pause()
        masterNet=ed.currentNetwork
        #Contour
        Contour_0 = ContourNE(constrkw = {}, name='Contour', library=matplotliblib)
        masterNet.addNode(Contour_0,132,283)
        Contour_0.inputPortByName['contour'].widget.set("filledcontour", run=False)
        Contour_0.inputPortByName['length_colors'].widget.set("10", run=False)
        apply(Contour_0.configure, (), {'expanded': True})
        #ReadTable
        ReadTable_1 = ReadTable(constrkw = {}, name='ReadTable6', library=stdlib)
        masterNet.addNode(ReadTable_1,12,20)
        ReadTable_1.inputPortByName['filename'].widget.set(
                        os.path.abspath("../doc/Examples/matplotlib/Data/contour_data_1.dat"), run=False)
        ReadTable_1.inputPortByName['sep'].widget.set(",", run=False)
        ReadTable_1.inputPortByName['datatype'].widget.set("float", run=False)
        apply(ReadTable_1.configure, (), {'expanded': True})
        #ReadTable
        ReadTable_2 = ReadTable(constrkw = {}, name='ReadTable7', library=stdlib)
        masterNet.addNode(ReadTable_2,179,19)
        ReadTable_2.inputPortByName['filename'].widget.set(
                        os.path.abspath("../doc/Examples/matplotlib/Data/contour_data_2.dat"), run=False)
        ReadTable_2.inputPortByName['sep'].widget.set(",", run=False)
        ReadTable_2.inputPortByName['datatype'].widget.set("float", run=False)
        apply(ReadTable_2.configure, (), {'expanded': True})
        #ReadTable
        ReadTable_3 = ReadTable(constrkw = {}, name='ReadTable8', library=stdlib)
        masterNet.addNode(ReadTable_3,355,22)
        ReadTable_3.inputPortByName['filename'].widget.set(
                        os.path.abspath("../doc/Examples/matplotlib/Data/contour_data_3.dat"), run=False)
        ReadTable_3.inputPortByName['sep'].widget.set(",", run=False)
        ReadTable_3.inputPortByName['datatype'].widget.set("float", run=False)
        apply(ReadTable_3.configure, (), {'expanded': True})
        #As_Type
        As_Type_4 = AsType(constrkw = {}, name='As Type', library=stdlib)
        masterNet.addNode(As_Type_4,28,172)
        #As_Type
        As_Type_5 = AsType(constrkw = {}, name='As Type', library=stdlib)
        masterNet.addNode(As_Type_5,203,159)
        #As_Type
        As_Type_6 = AsType(constrkw = {}, name='As Type', library=stdlib)
        masterNet.addNode(As_Type_6,382,167)
        #ColorBar
        ColorBar_7 = ColorBarNE(constrkw = {}, name='ColorBar', library=matplotliblib)
        masterNet.addNode(ColorBar_7,206,418)
        #
        masterNet.connectNodes(
            ReadTable_1, As_Type_4, "data", "inArrayList", blocking=True) 
        masterNet.connectNodes(
            ReadTable_2, As_Type_5, "data", "inArrayList", blocking=True)
        masterNet.connectNodes(
            ReadTable_3, As_Type_6, "data", "inArrayList", blocking=True)
        masterNet.connectNodes(
            As_Type_4, Contour_0, "outArray", "arraylistx", blocking=True)
        masterNet.connectNodes(
            As_Type_5, Contour_0, "outArray", "arraylisty", blocking=True)

        masterNet.connectNodes(
            As_Type_6, Contour_0, "outArray", "arraylistz", blocking=True)
        masterNet.connectNodes(
            Contour_0, ColorBar_7, "axes", "plot", blocking=True)
        masterNet.connectNodes(
            Contour_0, ColorBar_7, "contour", "current_image", blocking=True)    
        masterNet.run()
        pause()
        self.assertEqual(len(Contour_0.axes.collections)>0,True)

    
    #contour
    def test_matplotlib_Contour(self): 
        """testing contour node"""
        ed.addLibraryInstance(matplotliblib, 'Vision.matplotlibNodes','matplotliblib')
        ed.master.update_idletasks()
        pause()
        masterNet=ed.currentNetwork
        #Contour
        Contour_0 = ContourNE(constrkw = {}, name='Contour', library=matplotliblib)
        masterNet.addNode(Contour_0,132,283)
        Contour_0.inputPortByName['contour'].widget.set("default", run=False)
        Contour_0.inputPortByName['length_colors'].widget.set("", run=False)
        apply(Contour_0.configure, (), {'expanded': True})
        #ReadTable
        ReadTable_1 = ReadTable(constrkw = {}, name='ReadTableA', library=stdlib)
        masterNet.addNode(ReadTable_1,12,20)
        ReadTable_1.inputPortByName['filename'].widget.set(
                        os.path.abspath("../doc/Examples/matplotlib/Data/contour_data_1.dat"), run=False)
        ReadTable_1.inputPortByName['sep'].widget.set(",", run=False)
        ReadTable_1.inputPortByName['datatype'].widget.set("float", run=False)
        apply(ReadTable_1.configure, (), {'expanded': True})
        #ReadTable
        ReadTable_2 = ReadTable(constrkw = {}, name='ReadTableB', library=stdlib)
        masterNet.addNode(ReadTable_2,179,19)
        ReadTable_2.inputPortByName['filename'].widget.set(
                        os.path.abspath("../doc/Examples/matplotlib/Data/contour_data_2.dat"), run=False)
        ReadTable_2.inputPortByName['sep'].widget.set(",", run=False)
        ReadTable_2.inputPortByName['datatype'].widget.set("float", run=False)
        apply(ReadTable_2.configure, (), {'expanded': True})
        #ReadTable
        ReadTable_3 = ReadTable(constrkw = {}, name='ReadTableC', library=stdlib)
        masterNet.addNode(ReadTable_3,355,22)
        ReadTable_3.inputPortByName['filename'].widget.set(
                        os.path.abspath("../doc/Examples/matplotlib/Data/contour_data_3.dat"), run=False)
        ReadTable_3.inputPortByName['sep'].widget.set(",", run=False)
        ReadTable_3.inputPortByName['datatype'].widget.set("float", run=False)
        apply(ReadTable_3.configure, (), {'expanded': True})
        #As_Type
        As_Type_4 = AsType(constrkw = {}, name='As Type', library=stdlib)
        masterNet.addNode(As_Type_4,28,172)
        #As_Type
        As_Type_5 = AsType(constrkw = {}, name='As Type', library=stdlib)
        masterNet.addNode(As_Type_5,203,159)
        #As_Type
        As_Type_6 = AsType(constrkw = {}, name='As Type', library=stdlib)
        masterNet.addNode(As_Type_6,382,167)
        #ColorBar
        ColorBar_7 = ColorBarNE(constrkw = {}, name='ColorBar', library=matplotliblib)
        masterNet.addNode(ColorBar_7,206,418)
        #
        masterNet.connectNodes(
            ReadTable_1, As_Type_4, "data", "inArrayList", blocking=True) 
        masterNet.connectNodes(
            ReadTable_2, As_Type_5, "data", "inArrayList", blocking=True)
        masterNet.connectNodes(
            ReadTable_3, As_Type_6, "data", "inArrayList", blocking=True)
        masterNet.connectNodes(
            As_Type_4, Contour_0, "outArray", "arraylistx", blocking=True)
        masterNet.connectNodes(
            As_Type_5, Contour_0, "outArray", "arraylisty", blocking=True)

        masterNet.connectNodes(
            As_Type_6, Contour_0, "outArray", "arraylistz", blocking=True)
        masterNet.connectNodes(
            Contour_0, ColorBar_7, "axes", "plot", blocking=True)
        masterNet.connectNodes(
            Contour_0, ColorBar_7, "contour", "current_image", blocking=True)
        masterNet.run()
        pause()
        self.assertEqual(len(Contour_0.axes.collections)>0,True)
        
    #colorbar
    def test_matplotlib_Colorbar(self):
        """testing colorbar node"""
        ed.addLibraryInstance(matplotliblib, 'Vision.matplotlibNodes','matplotliblib')
        ed.master.update_idletasks()
        pause()
        masterNet=ed.currentNetwork
        #Contour
        Contour_0 = ContourNE(constrkw = {}, name='Contour', library=matplotliblib)
        masterNet.addNode(Contour_0,132,283)
        Contour_0.inputPortByName['contour'].widget.set("filledcontour", run=False)
        Contour_0.inputPortByName['length_colors'].widget.set("10", run=False)
        apply(Contour_0.configure, (), {'expanded': True})
        #ReadTable
        ReadTable_1 = ReadTable(constrkw = {}, name='ReadTableD', library=stdlib)
        masterNet.addNode(ReadTable_1,12,20)
        ReadTable_1.inputPortByName['filename'].widget.set(
                        os.path.abspath("../doc/Examples/matplotlib/Data/contour_data_1.dat"), run=False)
        ReadTable_1.inputPortByName['sep'].widget.set(",", run=False)
        ReadTable_1.inputPortByName['datatype'].widget.set("float", run=False)
        apply(ReadTable_1.configure, (), {'expanded': True})
        #ReadTable
        ReadTable_2 = ReadTable(constrkw = {}, name='ReadTableE', library=stdlib)
        masterNet.addNode(ReadTable_2,179,19)
        ReadTable_2.inputPortByName['filename'].widget.set(
                        os.path.abspath("../doc/Examples/matplotlib/Data/contour_data_2.dat"), run=False)
        ReadTable_2.inputPortByName['sep'].widget.set(",", run=False)
        ReadTable_2.inputPortByName['datatype'].widget.set("float", run=False)
        apply(ReadTable_2.configure, (), {'expanded': True})
        #ReadTable
        ReadTable_3 = ReadTable(constrkw = {}, name='ReadTableF', library=stdlib)
        masterNet.addNode(ReadTable_3,355,22)
        ReadTable_3.inputPortByName['filename'].widget.set(
                        os.path.abspath("../doc/Examples/matplotlib/Data/contour_data_3.dat"), run=False)
        ReadTable_3.inputPortByName['sep'].widget.set(",", run=False)
        ReadTable_3.inputPortByName['datatype'].widget.set("float", run=False)
        apply(ReadTable_3.configure, (), {'expanded': True})
        #As_Type
        As_Type_4 = AsType(constrkw = {}, name='As Type', library=stdlib)
        masterNet.addNode(As_Type_4,28,172)
        #As_Type
        As_Type_5 = AsType(constrkw = {}, name='As Type', library=stdlib)
        masterNet.addNode(As_Type_5,203,159)
        #As_Type
        As_Type_6 = AsType(constrkw = {}, name='As Type', library=stdlib)
        masterNet.addNode(As_Type_6,382,167)
        #ColorBar
        ColorBar_7 = ColorBarNE(constrkw = {}, name='ColorBar', library=matplotliblib)
        masterNet.addNode(ColorBar_7,206,418)
        #
        masterNet.connectNodes(
            ReadTable_1, As_Type_4, "data", "inArrayList", blocking=True) 
        masterNet.connectNodes(
            ReadTable_2, As_Type_5, "data", "inArrayList", blocking=True)
        masterNet.connectNodes(
            ReadTable_3, As_Type_6, "data", "inArrayList", blocking=True)
        masterNet.connectNodes(
            As_Type_4, Contour_0, "outArray", "arraylistx", blocking=True)
        masterNet.connectNodes(
            As_Type_5, Contour_0, "outArray", "arraylisty", blocking=True)

        masterNet.connectNodes(
            As_Type_6, Contour_0, "outArray", "arraylistz", blocking=True)
        masterNet.connectNodes(
            Contour_0, ColorBar_7, "axes", "plot", blocking=True)
        masterNet.connectNodes(
            Contour_0, ColorBar_7, "contour", "current_image", blocking=True)
        masterNet.run()
        pause()
        #
        self.assertEqual(len(Contour_0.axes.figure.axes)==2,True)


    #csdplot
    def test_matplotlib_CSD(self):
        """testing csd node"""
        ed.addLibraryInstance(matplotliblib, 'Vision.matplotlibNodes','matplotliblib')
        ed.master.update_idletasks()
        pause()
        masterNet=ed.currentNetwork
        #CSD
        CSD_12 = CSDNE(constrkw = {}, name='CSD', library=matplotliblib)
        masterNet.addNode(CSD_12,275,439)
        CSD_12.inputPortByName['NFFT'].widget.set("256", run=False)
        CSD_12.inputPortByName['nOverlap'].widget.set("0", run=False)
        #ReadTable
        ReadTable_13 = ReadTable(constrkw = {}, name='ReadTableG', library=stdlib)
        masterNet.addNode(ReadTable_13,236,19)
        ReadTable_13.inputPortByName['filename'].widget.set(
                        os.path.abspath("../doc/Examples/matplotlib/Data/csd_plot_data.dat"), run=False)
        ReadTable_13.inputPortByName['sep'].widget.set(",", run=False)
        ReadTable_13.inputPortByName['datatype'].widget.set("float", run=False)
        apply(ReadTable_13.configure, (), {'expanded': True})
        #Index
        Index_20 = Index(constrkw = {}, name='Index', library=stdlib)
        masterNet.addNode(Index_20,122,162)
        apply(Index_20.configure, (), {'expanded': True})
        #Index
        Index_21 = Index(constrkw = {}, name='Index', library=stdlib)
        masterNet.addNode(Index_21,348,159)
        apply(Index_21.inputPortByName['index'].widget.configure, (), {'max': 1})
        Index_21.inputPortByName['index'].widget.set(1, run=False)
        apply(Index_21.configure, (), {'expanded': True})
        #As_Type
        As_Type_24 = AsType(constrkw = {}, name='As Type', library=stdlib)
        masterNet.addNode(As_Type_24,128,305)
        apply(As_Type_24.configure, (), {'expanded': True})
        #As_Type
        As_Type_25 = AsType(constrkw = {}, name='As Type', library=stdlib)
        masterNet.addNode(As_Type_25,325,301)
        apply(As_Type_25.configure, (), {'expanded': True})
        #
        masterNet.connectNodes(ReadTable_13, Index_20, "data", "data", blocking=True)
        masterNet.connectNodes(ReadTable_13, Index_21, "data", "data", blocking=True)    
        masterNet.connectNodes(Index_20, As_Type_24, "data", "inArrayList", blocking=True)
        masterNet.connectNodes(As_Type_24, CSD_12, "outArray", "arraylistx", blocking=True)
        masterNet.connectNodes(Index_21, As_Type_25, "data", "inArrayList", blocking=True)    
        masterNet.connectNodes(As_Type_25, CSD_12, "outArray", "arraylisty", blocking=True)
        masterNet.run()
        pause()
        self.assertEqual(len(CSD_12.axes.lines),1)
        
    #errorbar
    def test_matplotlib_Errorbar(self):
        """testing errorbar node"""
        ed.addLibraryInstance(matplotliblib, 'Vision.matplotlibNodes','matplotliblib')
        ed.master.update_idletasks()
        pause()
        masterNet=ed.currentNetwork
        #ErrorBar
        ErrorBar_43 = ErrorBarNE(constrkw = {}, name='ErrorBar', library=matplotliblib)
        masterNet.addNode(ErrorBar_43,193,336)
        ErrorBar_43.inputPortByName['format'].widget.set("o", run=False)
        #ReadTable
        ReadTable_34 = ReadTable(constrkw = {}, name='ReadTableH', library=stdlib)
        masterNet.addNode(ReadTable_34,154,22)
        ReadTable_34.inputPortByName['filename'].widget.set(
                        os.path.abspath("../doc/Examples/matplotlib/Data/errorbar_data.dat"), run=False)
        ReadTable_34.inputPortByName['sep'].widget.set(",", run=False)
        ReadTable_34.inputPortByName['datatype'].widget.set("float", run=False)
        apply(ReadTable_34.configure, (), {'expanded': True})
        #Index
        Index_44 = Index(constrkw = {}, name='Index', library=stdlib)
        masterNet.addNode(Index_44,4,167)
        apply(Index_44.configure, (), {'expanded': True})
        #Index
        Index_45 = Index(constrkw = {}, name='Index', library=stdlib)
        masterNet.addNode(Index_45,137,168)
        apply(Index_45.inputPortByName['index'].widget.configure, (), {'max': 3})
        Index_45.inputPortByName['index'].widget.set(1, run=False)
        apply(Index_45.configure, (), {'expanded': True})
        #Index
        Index_46 = Index(constrkw = {}, name='Index', library=stdlib)
        masterNet.addNode(Index_46,269,165)
        apply(Index_46.inputPortByName['index'].widget.configure, (), {'max': 3})
        Index_46.inputPortByName['index'].widget.set(2, run=False)
        apply(Index_46.configure, (), {'expanded': True})
        #Index
        Index_47 = Index(constrkw = {}, name='Index', library=stdlib)
        masterNet.addNode(Index_47,395,165)
        apply(Index_47.inputPortByName['index'].widget.configure, (), {'max': 3})
        Index_47.inputPortByName['index'].widget.set(3, run=False)
        apply(Index_47.configure, (), {'expanded': True})
        #
        masterNet.connectNodes(
            ReadTable_34, Index_44, "data", "data", blocking=True)
        masterNet.connectNodes(
            ReadTable_34, Index_45, "data", "data", blocking=True)
        masterNet.connectNodes(
            ReadTable_34, Index_46, "data", "data", blocking=True)
        masterNet.connectNodes(
            ReadTable_34, Index_47, "data", "data", blocking=True)
        masterNet.connectNodes(
            Index_44, ErrorBar_43, "data", "x", blocking=True)
        masterNet.connectNodes(
            Index_45, ErrorBar_43, "data", "y", blocking=True)
        masterNet.connectNodes(
            Index_46, ErrorBar_43, "data", "xerr", blocking=True)
        masterNet.connectNodes(
            Index_47, ErrorBar_43, "data", "yerr", blocking=True)    
        masterNet.run()
        pause()
        self.assertEqual(len(ErrorBar_43.axes.lines)>0,True)

    #fill
    def test_matplotlib_Fill(self):
        """testing Fill node"""
        ed.addLibraryInstance(matplotliblib, 'Vision.matplotlibNodes','matplotliblib')
        ed.master.update_idletasks()
        pause()
        masterNet=ed.currentNetwork
        #FillColor
        FillColor_68 = FillNE(constrkw = {}, name='FillColor', library=matplotliblib)
        masterNet.addNode(FillColor_68,105,371)
        FillColor_68.inputPortByName['fillcolor'].widget.set("red", run=False)
        apply(FillColor_68.configure, (), {'expanded': True})
        #ReadTable
        ReadTable_69 = ReadTable(constrkw = {}, name='ReadTableI', library=stdlib)
        masterNet.addNode(ReadTable_69,123,38)
        ReadTable_69.inputPortByName['filename'].widget.set(
                        os.path.abspath("../doc/Examples/matplotlib/Data/fillcolor_data.dat"), run=False)
        ReadTable_69.inputPortByName['sep'].widget.set(",", run=False)
        ReadTable_69.inputPortByName['datatype'].widget.set("float", run=False)
        apply(ReadTable_69.configure, (), {'expanded': True})
        #Set_Matplotlib_options
        Set_Matplotlib_options_74 = MatPlotLibOptions(constrkw = {}, name='Set Matplotlib options', library=matplotliblib)
        masterNet.addNode(Set_Matplotlib_options_74,339,233)
        Set_Matplotlib_options_74.inputPortByName['matplotlibOptions'].widget.set({'gridOn': 1}, run=False)
        #Index
        Index_75 = Index(constrkw = {}, name='Index', library=stdlib)
        masterNet.addNode(Index_75,29,202)
        apply(Index_75.configure, (), {'expanded': True})
        #Index
        Index_76 = Index(constrkw = {}, name='Index', library=stdlib)
        masterNet.addNode(Index_76,209,201)
        apply(Index_76.inputPortByName['index'].widget.configure, (), {'max': 1})
        Index_76.inputPortByName['index'].widget.set(1, run=False)
        apply(Index_76.configure, (), {'expanded': True})
        #
        masterNet.connectNodes(
            Set_Matplotlib_options_74, FillColor_68, "matplotlibOptions", "drawAreaDef", blocking=True)
        masterNet.connectNodes(
            ReadTable_69, Index_75, "data", "data", blocking=True)
        masterNet.connectNodes(
            ReadTable_69, Index_76, "data", "data", blocking=True)
        masterNet.connectNodes(
            Index_75, FillColor_68, "data", "x", blocking=True)
        masterNet.connectNodes(
            Index_76, FillColor_68, "data", "y", blocking=True)    
        masterNet.connectNodes(
            Index_76, FillColor_68, "data", "y", blocking=True)
        masterNet.run()
        pause()     
        self.assertEqual(FillColor_68.axes.patches[0].get_facecolor()[:3],(1.0, 0.0, 0.0))
        
    #log
    def test_matplotlib_Log(self):
        """testing log node"""
        ed.addLibraryInstance(matplotliblib, 'Vision.matplotlibNodes','matplotliblib')
        ed.master.update_idletasks()
        pause()
        masterNet=ed.currentNetwork
        #LogCurve
        LogCurve_0 = LogCurveNE(constrkw = {}, name='LogCurve', library=matplotliblib)
        masterNet.addNode(LogCurve_0,109,300)
        #ReadTable
        ReadTable_1 = ReadTable(constrkw = {}, name='ReadTableJ', library=stdlib)
        masterNet.addNode(ReadTable_1,63,8)
        ReadTable_1.inputPortByName['filename'].widget.set(
                        os.path.abspath("../doc/Examples/matplotlib/Data/log_data.dat"), run=False)
        ReadTable_1.inputPortByName['sep'].widget.set(",", run=False)
        ReadTable_1.inputPortByName['datatype'].widget.set("float", run=False)
        apply(ReadTable_1.configure, (), {'expanded': True})
        #Set_Matplotlib_options
        Set_Matplotlib_options_6 = MatPlotLibOptions(constrkw = {}, name='Set Matplotlib options', library=matplotliblib)
        masterNet.addNode(Set_Matplotlib_options_6,366,218)
        Set_Matplotlib_options_6.inputPortByName['matplotlibOptions'].widget.set({'gridOn': 1, 'whichgrid': 'minor'}, run=False)
        #Index
        Index_7 = Index(constrkw = {}, name='Index', library=stdlib)
        masterNet.addNode(Index_7,28,182)
        apply(Index_7.configure, (), {'expanded': True})
        #Index
        Index_8 = Index(constrkw = {}, name='Index', library=stdlib)
        masterNet.addNode(Index_8,178,174)
        apply(Index_8.inputPortByName['index'].widget.configure, (), {'max': 1})
        Index_8.inputPortByName['index'].widget.set(1, run=False)
        apply(Index_8.configure, (), {'expanded': True})
        #
        masterNet.connectNodes(
            Set_Matplotlib_options_6, LogCurve_0, "matplotlibOptions", "drawAreaDef", blocking=True)
        masterNet.connectNodes(
            ReadTable_1, Index_7, "data", "data", blocking=True)
        masterNet.connectNodes(
            ReadTable_1, Index_8, "data", "data", blocking=True)
        masterNet.connectNodes(
            Index_7, LogCurve_0, "data", "x", blocking=True)    
        masterNet.connectNodes(
            Index_8, LogCurve_0, "data", "y", blocking=True)
        masterNet.run()
        pause()
        self.assertEqual(len(LogCurve_0.axes.lines)>0,True)
    
    #Pcolor
    def test_matplotlib_Pcolor(self): 
        """testing Pcolor node"""
        ed.addLibraryInstance(matplotliblib, 'Vision.matplotlibNodes','matplotliblib')
        ed.master.update_idletasks()
        pause()
        masterNet=ed.currentNetwork
        #Pcolor
        Pcolor_42 = PcolorNE(constrkw = {}, name='Pcolor', library=matplotliblib)
        masterNet.addNode(Pcolor_42,211,310)
        #ReadTable
        ReadTable_32 = ReadTable(constrkw = {}, name='ReadTableK', library=stdlib)
        masterNet.addNode(ReadTable_32,390,24)
        ReadTable_32.inputPortByName['filename'].widget.set(
                os.path.abspath("../doc/Examples/matplotlib/Data/pcolormesh_data_3.dat"), run=False)
        ReadTable_32.inputPortByName['sep'].widget.set(",", run=False)
        ReadTable_32.inputPortByName['datatype'].widget.set("float", run=False)
        apply(ReadTable_32.configure, (), {'expanded': True})
        #As_Type
        As_Type_34 = AsType(constrkw = {}, name='As Type', library=stdlib)
        masterNet.addNode(As_Type_34,388,181)
        #ReadTable
        ReadTable_36 = ReadTable(constrkw = {}, name='ReadTableL', library=stdlib)
        masterNet.addNode(ReadTable_36,8,17)
        ReadTable_36.inputPortByName['filename'].widget.set(
                os.path.abspath("../doc/Examples/matplotlib/Data/pcolormesh_data_1.dat"), run=False)
        ReadTable_36.inputPortByName['sep'].widget.set(",", run=False)
        ReadTable_36.inputPortByName['datatype'].widget.set("float", run=False)
        apply(ReadTable_36.configure, (), {'expanded': True})
        #ReadTable
        ReadTable_37 = ReadTable(constrkw = {}, name='ReadTableM', library=stdlib)
        masterNet.addNode(ReadTable_37,210,21)
        ReadTable_37.inputPortByName['filename'].widget.set(
                os.path.abspath("../doc/Examples/matplotlib/Data/pcolormesh_data_2.dat"), run=False)
        ReadTable_37.inputPortByName['sep'].widget.set(",", run=False)
        ReadTable_37.inputPortByName['datatype'].widget.set("float", run=False)
        apply(ReadTable_37.configure, (), {'expanded': True})
        #As_Type
        As_Type_38 = AsType(constrkw = {}, name='As Type', library=stdlib)
        masterNet.addNode(As_Type_38,60,191)
        #As_Type
        As_Type_39 = AsType(constrkw = {}, name='As Type', library=stdlib)
        masterNet.addNode(As_Type_39,213,187)
        #
        masterNet.connectNodes(
            ReadTable_32, As_Type_34, "data", "inArrayList", blocking=True)
        masterNet.connectNodes(
            ReadTable_36, As_Type_38, "data", "inArrayList", blocking=True)
        masterNet.connectNodes(
            ReadTable_37, As_Type_39, "data", "inArrayList", blocking=True)
        masterNet.connectNodes(
            As_Type_38, Pcolor_42, "outArray", "arraylistx", blocking=True)
        masterNet.connectNodes(
            As_Type_39, Pcolor_42, "outArray", "arraylisty", blocking=True)
        masterNet.connectNodes(
            As_Type_34, Pcolor_42, "outArray", "arraylistz", blocking=True)    
        masterNet.run()
        pause()
        self.assertEqual(len(Pcolor_42.axes.collections)>0,True)

    #PcolorMesh
    def test_matplotlib_PcolorMesh(self):  
        """testing  PcolorMesh node """
        ed.addLibraryInstance(matplotliblib, 'Vision.matplotlibNodes','matplotliblib')
        ed.master.update_idletasks()
        masterNet=ed.currentNetwork
        pause()
        #Pcolor
        PcolorMesh_42 = PcolorMeshNE(constrkw = {}, name='PcolorMesh', library=matplotliblib)
        masterNet.addNode(PcolorMesh_42,211,310)
        #ReadTable
        ReadTable_32 = ReadTable(constrkw = {}, name='ReadTableN', library=stdlib)
        masterNet.addNode(ReadTable_32,390,24)
        ReadTable_32.inputPortByName['filename'].widget.set(
                os.path.abspath("../doc/Examples/matplotlib/Data/pcolormesh_data_3.dat"), run=False)
        ReadTable_32.inputPortByName['sep'].widget.set(",", run=False)
        ReadTable_32.inputPortByName['datatype'].widget.set("float", run=False)
        apply(ReadTable_32.configure, (), {'expanded': True})
        #As_Type
        As_Type_34 = AsType(constrkw = {}, name='As Type', library=stdlib)
        masterNet.addNode(As_Type_34,388,181)
        #ReadTable
        ReadTable_36 = ReadTable(constrkw = {}, name='ReadTableO', library=stdlib)
        masterNet.addNode(ReadTable_36,8,17)
        ReadTable_36.inputPortByName['filename'].widget.set(
                os.path.abspath("../doc/Examples/matplotlib/Data/pcolormesh_data_1.dat"), run=False)
        ReadTable_36.inputPortByName['sep'].widget.set(",", run=False)
        ReadTable_36.inputPortByName['datatype'].widget.set("float", run=False)
        apply(ReadTable_36.configure, (), {'expanded': True})
        #ReadTable
        ReadTable_37 = ReadTable(constrkw = {}, name='ReadTableP', library=stdlib)
        masterNet.addNode(ReadTable_37,210,21)
        ReadTable_37.inputPortByName['filename'].widget.set(
                os.path.abspath("../doc/Examples/matplotlib/Data/pcolormesh_data_2.dat"), run=False)
        ReadTable_37.inputPortByName['sep'].widget.set(",", run=False)
        ReadTable_37.inputPortByName['datatype'].widget.set("float", run=False)
        apply(ReadTable_37.configure, (), {'expanded': True})
        #As_Type
        As_Type_38 = AsType(constrkw = {}, name='As Type', library=stdlib)
        masterNet.addNode(As_Type_38,60,191)
        #As_Type
        As_Type_39 = AsType(constrkw = {}, name='As Type', library=stdlib)
        masterNet.addNode(As_Type_39,213,187)
        #
        masterNet.connectNodes(
            ReadTable_32, As_Type_34, "data", "inArrayList", blocking=True)
        masterNet.connectNodes(
            ReadTable_36, As_Type_38, "data", "inArrayList", blocking=True)
        masterNet.connectNodes(
            ReadTable_37, As_Type_39, "data", "inArrayList", blocking=True)
        masterNet.connectNodes(
            As_Type_38, PcolorMesh_42, "outArray", "arraylistx", blocking=True)
        masterNet.connectNodes(
            As_Type_39, PcolorMesh_42, "outArray", "arraylisty", blocking=True)
        masterNet.connectNodes(
            As_Type_34, PcolorMesh_42, "outArray", "arraylistz", blocking=True)
        masterNet.run()
        pause()
        self.assertEqual(len(PcolorMesh_42.axes.collections)>0,True)

    #PolarAxes
    def test_matplotlib_PolorAxes(self): 
        """testing  PolorAxes node """
        ed.addLibraryInstance(matplotliblib, 'Vision.matplotlibNodes','matplotliblib')
        ed.master.update_idletasks()
        pause()
        masterNet=ed.currentNetwork
        #PolarAxes
        PolarAxes_63 = PolarAxesNE(constrkw = {}, name='PolarAxes', library=matplotliblib)
        masterNet.addNode(PolarAxes_63,173,307)
        #ReadTable
        ReadTable_64 = ReadTable(constrkw = {}, name='ReadTableQ', library=stdlib)
        masterNet.addNode(ReadTable_64,156,28)
        ReadTable_64.inputPortByName['filename'].widget.set(
                os.path.abspath("../doc/Examples/matplotlib/Data/polar_data.dat"), run=False)
        ReadTable_64.inputPortByName['sep'].widget.set(",", run=False)
        ReadTable_64.inputPortByName['datatype'].widget.set("float", run=False)
        apply(ReadTable_64.configure, (), {'expanded': True})
        #Index
        Index_65 = Index(constrkw = {}, name='Index', library=stdlib)
        masterNet.addNode(Index_65,89,163)
        apply(Index_65.configure, (), {'expanded': True})
        #Index
        Index_66 = Index(constrkw = {}, name='Index', library=stdlib)
        masterNet.addNode(Index_66,284,170)
        apply(Index_66.inputPortByName['index'].widget.configure, (), {'max': 1})
        Index_66.inputPortByName['index'].widget.set(1, run=False)
        apply(Index_66.configure, (), {'expanded': True})
        #
        masterNet.connectNodes(
            ReadTable_64, Index_65, "data", "data", blocking=True)
        masterNet.connectNodes(
            ReadTable_64, Index_66, "data", "data", blocking=True)
        masterNet.connectNodes(
            Index_65, PolarAxes_63, "data", "y", blocking=True)
        masterNet.connectNodes(
            Index_66, PolarAxes_63, "data", "x", blocking=True)    
        masterNet.run()
        pause()
        self.assertEqual(PolarAxes_63.axes.get_xscale(),'linear') # was polar in .91.4
        self.assertEqual(PolarAxes_63.axes.get_yscale(),'linear') # was polar in .91.4


    #psdplot
    def test_matplotlib_Psdplot(self):  
        """testing psdplot node """
        ed.addLibraryInstance(matplotliblib, 'Vision.matplotlibNodes','matplotliblib')
        ed.master.update_idletasks()
        pause()
        masterNet=ed.currentNetwork
        #ReadTable        
        ReadTable_67 = ReadTable(constrkw = {}, name='ReadTableR', library=stdlib)
        masterNet.addNode(ReadTable_67,76,22)
        ReadTable_67.inputPortByName['filename'].widget.set(
                os.path.abspath("../doc/Examples/matplotlib/Data/psd_plot_data.dat"), run=False)
        ReadTable_67.inputPortByName['sep'].widget.set(",", run=False)
        ReadTable_67.inputPortByName['datatype'].widget.set("float", run=False)
        apply(ReadTable_67.configure, (), {'expanded': True})
        #PSD
        PSD_71 = PSDNE(constrkw = {}, name='PSD', library=matplotliblib)
        masterNet.addNode(PSD_71,117,329)
        PSD_71.inputPortByName['NFFT'].widget.set("512", run=False)
        PSD_71.inputPortByName['nOverlap'].widget.set("0", run=False)
        #Index
        Index_77 = Index(constrkw = {}, name='Index', library=stdlib)
        masterNet.addNode(Index_77,93,181)
        apply(Index_77.configure, (), {'expanded': True})
        #
        masterNet.connectNodes(
            ReadTable_67, Index_77, "data", "data", blocking=True)
        masterNet.connectNodes(
            Index_77, PSD_71, "data", "arraylistx", blocking=True)
        masterNet.run()
        pause()
        self.assertEqual(len(PSD_71.axes.lines),1)
        
    #semilogx
    def test_matplotlib_Semilogx(self): 
        """testing Semilogx node """
        ed.addLibraryInstance(matplotliblib, 'Vision.matplotlibNodes','matplotliblib')
        ed.master.update_idletasks()
        pause()
        masterNet=ed.currentNetwork
        #Semilogx
        Semilogx_107 = SemilogxNE(constrkw = {}, name='Semilogx', library=matplotliblib)
        masterNet.addNode(Semilogx_107,126,343)
        #ReadTable
        ReadTable_101 = ReadTable(constrkw = {}, name='ReadTableS', library=stdlib)
        masterNet.addNode(ReadTable_101,123,12)
        ReadTable_101.inputPortByName['filename'].widget.set(
                os.path.abspath("../doc/Examples/matplotlib/Data/semilogx_data.dat"), run=False)
        ReadTable_101.inputPortByName['sep'].widget.set(",", run=False)
        ReadTable_101.inputPortByName['datatype'].widget.set("float", run=False)
        apply(ReadTable_101.configure, (), {'expanded': True})
        #Set_Matplotlib_options
        Set_Matplotlib_options_106 = MatPlotLibOptions(constrkw = {}, name='Set Matplotlib options', library=matplotliblib)
        masterNet.addNode(Set_Matplotlib_options_106,325,268)
        Set_Matplotlib_options_106.inputPortByName['matplotlibOptions'].widget.set({'gridcolor': 'black', 'gridOn': 1, 'whichgrid': 'minor', 'gridlinewidth': 0.24999999999999989}, run=False)
        #Index
        Index_108 = Index(constrkw = {}, name='Index', library=stdlib)
        masterNet.addNode(Index_108,25,163)
        apply(Index_108.configure, (), {'expanded': True})
        #Index
        Index_109 = Index(constrkw = {}, name='Index', library=stdlib)
        masterNet.addNode(Index_109,237,164)
        apply(Index_109.inputPortByName['index'].widget.configure, (), {'max': 1})
        Index_109.inputPortByName['index'].widget.set(1, run=False)
        apply(Index_109.configure, (), {'expanded': True})
        #
        masterNet.connectNodes(
            Set_Matplotlib_options_106, Semilogx_107, "matplotlibOptions", "drawAreaDef", blocking=True)
        masterNet.connectNodes(
            ReadTable_101, Index_108, "data", "data", blocking=True)
        masterNet.connectNodes(
            ReadTable_101, Index_109, "data", "data", blocking=True)
        masterNet.connectNodes(
            Index_109, Semilogx_107, "data", "y", blocking=True)
        masterNet.connectNodes(
            Index_108, Semilogx_107, "data", "x", blocking=True)    
        masterNet.run()
        pause()
        self.assertEqual(len(Semilogx_107.axes.lines)>0,True)

    #Semilogy
    def test_matplotlib_Semilogy(self):  
        """testing Semilogy node """
        ed.addLibraryInstance(matplotliblib, 'Vision.matplotlibNodes','matplotliblib')
        ed.master.update_idletasks()
        pause()
        masterNet=ed.currentNetwork
        #Semilogy
        Semilogy_107 = SemilogyNE(constrkw = {}, name='Semilogx', library=matplotliblib)
        masterNet.addNode(Semilogy_107,126,343)
        #ReadTable
        ReadTable_101 = ReadTable(constrkw = {}, name='ReadTableT', library=stdlib)
        masterNet.addNode(ReadTable_101,123,12)
        ReadTable_101.inputPortByName['filename'].widget.set(
                os.path.abspath("../doc/Examples/matplotlib/Data/semilogy_data.dat"), run=False)
        ReadTable_101.inputPortByName['sep'].widget.set(",", run=False)
        ReadTable_101.inputPortByName['datatype'].widget.set("float", run=False)
        apply(ReadTable_101.configure, (), {'expanded': True})
        #Set_Matplotlib_options
        Set_Matplotlib_options_106 = MatPlotLibOptions(constrkw = {}, name='Set Matplotlib options', library=matplotliblib)
        masterNet.addNode(Set_Matplotlib_options_106,325,268)
        Set_Matplotlib_options_106.inputPortByName['matplotlibOptions'].widget.set({'gridcolor': 'black', 'gridOn': 1, 'whichgrid': 'minor', 'gridlinewidth': 0.24999999999999989}, run=False)
        #Index
        Index_108 = Index(constrkw = {}, name='Index', library=stdlib)
        masterNet.addNode(Index_108,25,163)
        apply(Index_108.configure, (), {'expanded': True})
        #Index
        Index_109 = Index(constrkw = {}, name='Index', library=stdlib)
        masterNet.addNode(Index_109,237,164)
        apply(Index_109.inputPortByName['index'].widget.configure, (), {'max': 1})
        Index_109.inputPortByName['index'].widget.set(1, run=False)
        apply(Index_109.configure, (), {'expanded': True})
        #
        masterNet.connectNodes(
            Set_Matplotlib_options_106, Semilogy_107, "matplotlibOptions", "drawAreaDef", blocking=True)
        masterNet.connectNodes(
            ReadTable_101, Index_108, "data", "data", blocking=True)
        masterNet.connectNodes(
            ReadTable_101, Index_109, "data", "data", blocking=True)
        masterNet.connectNodes(
            Index_109, Semilogy_107, "data", "y", blocking=True)
        masterNet.connectNodes(
            Index_108, Semilogy_107, "data", "x", blocking=True)    
        masterNet.run()
        pause()
        self.assertEqual(len(Semilogy_107.axes.lines)>0,True)

    

    #Specgram
    def test_matplotlib_Specgram(self): 
        """testing Specgram node"""
        ed.addLibraryInstance(matplotliblib, 'Vision.matplotlibNodes','matplotliblib')
        ed.master.update_idletasks()
        pause()
        masterNet=ed.currentNetwork
        #Specgram
        Specgram_42 = SpecgramNE(constrkw = {}, name='Specgram', library=matplotliblib)
        masterNet.addNode(Specgram_42,101,312)
        Specgram_42.inputPortByName['NFFT'].widget.set("256", run=False)
        Specgram_42.inputPortByName['nOverlap'].widget.set("0", run=False)
        #ReadTable
        ReadTable_40 = ReadTable(constrkw = {}, name='ReadTableU', library=stdlib)
        masterNet.addNode(ReadTable_40,65,22)
        ReadTable_40.inputPortByName['filename'].widget.set(
                os.path.abspath("../doc/Examples/matplotlib/Data/specgram_data.dat"), run=False)
        ReadTable_40.inputPortByName['sep'].widget.set(",", run=False)
        ReadTable_40.inputPortByName['datatype'].widget.set("float", run=False)
        apply(ReadTable_40.configure, (), {'expanded': True})
        #Index
        Index_41 = Index(constrkw = {}, name='Index', library=stdlib)
        masterNet.addNode(Index_41,85,174)
        apply(Index_41.configure, (), {'expanded': True})
        #ColorBar
        ColorBar_43 = ColorBarNE(constrkw = {}, name='ColorBar', library=matplotliblib)
        masterNet.addNode(ColorBar_43,103,375)
        #
        masterNet.connectNodes(
            ReadTable_40, Index_41, "data", "data", blocking=True)
        masterNet.connectNodes(
            Index_41, Specgram_42, "data", "arraylistx", blocking=True)
        masterNet.connectNodes(
            Specgram_42, ColorBar_43, "axes", "plot", blocking=True)
        masterNet.connectNodes(
            Specgram_42, ColorBar_43, "image", "current_image", blocking=True)    
        masterNet.run()
        pause()
        self.assertEqual(len(Specgram_42.axes.images),1)   
        
    #Spy
    def test_matplotlib_Spy(self):
        """testing spy node"""
        ed.addLibraryInstance(matplotliblib, 'Vision.matplotlibNodes','matplotliblib')
        ed.master.update_idletasks()
        pause()
        masterNet=ed.currentNetwork
        #Spy
        Spy_35 = SpyNE(constrkw = {}, name='Spy', library=matplotliblib)
        masterNet.addNode(Spy_35,75,259)
        #ReadTable
        ReadTable_5 = ReadTable(constrkw = {}, name='ReadTableV', library=stdlib)
        masterNet.addNode(ReadTable_5,69,33)
        ReadTable_5.inputPortByName['filename'].widget.set(
                os.path.abspath("../doc/Examples/matplotlib/Data/spy_data.dat"), run=False)
        ReadTable_5.inputPortByName['sep'].widget.set(",", run=False)
        ReadTable_5.inputPortByName['datatype'].widget.set("float", run=False)
        apply(ReadTable_5.configure, (), {'expanded': True})
        #As_Type
        As_Type_30 = AsType(constrkw = {}, name='As Type', library=stdlib)
        masterNet.addNode(As_Type_30,78,178)
        apply(As_Type_30.configure, (), {'expanded': True})
        #
        masterNet.connectNodes(
            ReadTable_5, As_Type_30, "data", "inArrayList", blocking=True)
        masterNet.connectNodes(
            As_Type_30, Spy_35, "outArray", "Z", blocking=True)
        masterNet.run()
        pause()
        self.assertEqual(len(Spy_35.axes.lines)>0,True)
        
    #Stem
    def test_matplotlib_Stem(self):
        """testing Stem node"""
        ed.addLibraryInstance(matplotliblib, 'Vision.matplotlibNodes','matplotliblib')
        ed.master.update_idletasks()
        pause()
        masterNet=ed.currentNetwork
        #Stem
        Stem_143 = StemNE(constrkw = {}, name='Stem', library=matplotliblib)
        masterNet.addNode(Stem_143,175,408)
        #ReadTable
        ReadTable_138 = ReadTable(constrkw = {}, name='ReadTableW', library=stdlib)
        masterNet.addNode(ReadTable_138,142,39)
        ReadTable_138.inputPortByName['filename'].widget.set(
                os.path.abspath("../doc/Examples/matplotlib/Data/stem_plot_data.dat"), run=False)
        ReadTable_138.inputPortByName['sep'].widget.set(",", run=False)
        ReadTable_138.inputPortByName['datatype'].widget.set("float", run=False)
        apply(ReadTable_138.configure, (), {'expanded': True})
        #Index
        Index_144 = Index(constrkw = {}, name='Index', library=stdlib)
        masterNet.addNode(Index_144,58,242)
        apply(Index_144.configure, (), {'expanded': True})
        #Index
        Index_145 = Index(constrkw = {}, name='Index', library=stdlib)
        masterNet.addNode(Index_145,275,247)
        apply(Index_145.inputPortByName['index'].widget.configure, (), {'max': 1})
        Index_145.inputPortByName['index'].widget.set(1, run=False)
        apply(Index_145.configure, (), {'expanded': True})
        #
        masterNet.connectNodes(
            ReadTable_138, Index_144, "data", "data", blocking=True)
        masterNet.connectNodes(
            ReadTable_138, Index_145, "data", "data", blocking=True)
        masterNet.connectNodes(
            Index_144, Stem_143, "data", "x", blocking=True)
        masterNet.connectNodes(
            Index_145, Stem_143, "data", "y", blocking=True)    
        masterNet.run()
        pause()
        self.assertEqual(len(Stem_143.axes.lines)>0,True)
        
    #vline
    def test_matplotlib_Vline(self):  
        """testing vline node"""
        ed.addLibraryInstance(matplotliblib, 'Vision.matplotlibNodes','matplotliblib')
        ed.master.update_idletasks()
        pause()
        masterNet=ed.currentNetwork
        #Vline
        Vline_165 = VlineNE(constrkw = {}, name='Vline', library=matplotliblib)
        masterNet.addNode(Vline_165,117,358)
        #ReadTable
        
        
        ReadTable_158 = ReadTable(constrkw = {}, name='ReadTableX', library=stdlib)
        masterNet.addNode(ReadTable_158,94,21)
        ReadTable_158.inputPortByName['filename'].widget.set(
                os.path.abspath("../doc/Examples/matplotlib/Data/vlines_data.dat"), run=False)
        ReadTable_158.inputPortByName['sep'].widget.set(",", run=False)
        ReadTable_158.inputPortByName['datatype'].widget.set("float", run=False)
        apply(ReadTable_158.configure, (), {'expanded': True})
        #Index
        Index_166 = Index(constrkw = {}, name='Index', library=stdlib)
        masterNet.addNode(Index_166,12,187)
        apply(Index_166.configure, (), {'expanded': True})
        
        DialInt_167 = DialIntNE(constrkw = {}, name='Index', library=stdlib)
        masterNet.addNode(DialInt_167,120,187)

        #Index
        Index_168 = Index(constrkw = {}, name='Index', library=stdlib)
        masterNet.addNode(Index_168,271,185)
        apply(Index_168.inputPortByName['index'].widget.configure, (), {'max': 2})
        Index_168.inputPortByName['index'].widget.set(1, run=False)
        apply(Index_168.configure, (), {'expanded': True})
        #
        masterNet.connectNodes(
            ReadTable_158, Index_166, "data", "data", blocking=True)
        masterNet.connectNodes(
            ReadTable_158, Index_168, "data", "data", blocking=True)
        masterNet.connectNodes(
            Index_166, Vline_165, "data", "x", blocking=True)
        masterNet.connectNodes(
            DialInt_167, Vline_165, "value", "ymin", blocking=True)
        masterNet.connectNodes(
            Index_168, Vline_165, "data", "ymax", blocking=True)    
        masterNet.run()
        pause()
        #self.assertEqual(len(Vline_165.axes.lines)>0,True)
        
    #Table
    def test_matplotlib_Table(self):  
        """testing table node"""
        ed.addLibraryInstance(matplotliblib, 'Vision.matplotlibNodes','matplotliblib')
        ed.master.update_idletasks()
        pause()
        masterNet=ed.currentNetwork
        #TablePlot
        TablePlot_0 = TablePlotNE(constrkw = {}, name='TablePlot', library=matplotliblib)
        masterNet.addNode(TablePlot_0,111,400)
        #ReadTable
        ReadTable_1 = ReadTable(constrkw = {}, name='ReadTableY', library=stdlib)
        masterNet.addNode(ReadTable_1,19,36)
        ReadTable_1.inputPortByName['filename'].widget.set(
                os.path.abspath("../doc/Examples/matplotlib/Data/table_data_values.dat"), run=False)
        ReadTable_1.inputPortByName['sep'].widget.set(",", run=False)
        apply(ReadTable_1.configure, (), {'expanded': True})
        #Draw_Area
        Draw_Area_12 = MPLDrawAreaNE(constrkw = {}, name='Draw Area', library=matplotliblib)
        masterNet.addNode(Draw_Area_12,389,360)
        Draw_Area_12.inputPortByName['left'].widget.set(0.2, run=False)
        Draw_Area_12.inputPortByName['bottom'].widget.set(0.2, run=False)
        Draw_Area_12.inputPortByName['width'].widget.set(0.7, run=False)
        Draw_Area_12.inputPortByName['height'].widget.set(0.666666666667, run=False)
        Draw_Area_12.inputPortByName['title'].widget.set("", run=False)
        Draw_Area_12.inputPortByName['xlabel'].widget.set("", run=False)
        Draw_Area_12.inputPortByName['ylabel'].widget.set("", run=False)
        #ReadTable
        ReadTable_13 = ReadTable(constrkw = {}, name='ReadTableZ', library=stdlib)
        masterNet.addNode(ReadTable_13,209,39)
        ReadTable_13.inputPortByName['filename'].widget.set(
                os.path.abspath("../doc/Examples/matplotlib/Data/table_data_string.dat"), run=False)
        ReadTable_13.inputPortByName['sep'].widget.set(",", run=False)
        apply(ReadTable_13.configure, (), {'expanded': True})
        #Index
        Index_14 = Index(constrkw = {}, name='Index', library=stdlib)
        masterNet.addNode(Index_14,146,200)
        apply(Index_14.configure, (), {'expanded': True})
        #Index
        Index_15 = Index(constrkw = {}, name='Index', library=stdlib)
        masterNet.addNode(Index_15,295,202)
        apply(Index_15.inputPortByName['index'].widget.configure, (), {'max': 1})
        Index_15.inputPortByName['index'].widget.set(1, run=False)
        apply(Index_15.configure, (), {'expanded': True})
        #ReadTable
        ReadTable_28 = ReadTable(constrkw = {}, name='ReadTableAA', library=stdlib)
        masterNet.addNode(ReadTable_28,407,41)
        ReadTable_28.inputPortByName['filename'].widget.set(
                os.path.abspath("../doc/Examples/matplotlib/Data/table_data_colors.dat"), run=False)
        ReadTable_28.inputPortByName['sep'].widget.set(",", run=False)
        ReadTable_28.inputPortByName['datatype'].widget.set("float", run=False)
        apply(ReadTable_28.configure, (), {'expanded': True})
        #As_Type
        As_Type_29 = AsType(constrkw = {}, name='As Type', library=stdlib)
        masterNet.addNode(As_Type_29,436,225)
        #
        masterNet.connectNodes(
            Draw_Area_12, TablePlot_0, "drawAreaDef", "drawAreaDef", blocking=True)
        masterNet.connectNodes(
            ReadTable_1, TablePlot_0, "data", "values", blocking=True)
        masterNet.connectNodes(
            ReadTable_13, Index_14, "data", "data", blocking=True)
        masterNet.connectNodes(
            ReadTable_13, Index_15, "data", "data", blocking=True)
        masterNet.connectNodes(
            Index_14, TablePlot_0, "data", "rowLabels", blocking=True)
        masterNet.connectNodes(
            Index_15, TablePlot_0, "data", "colLabels", blocking=True) 
        masterNet.connectNodes(
            ReadTable_28, As_Type_29, "data", "inArrayList", blocking=True)
        masterNet.connectNodes(
            As_Type_29, TablePlot_0, "outArray", "rowColors", blocking=True)    
        masterNet.run()
        pause()
        self.assertEqual(len(TablePlot_0.axes.patches)>0,True)    

    #Quiver
    def test_matplotlib_Quiver(self):  
        """testing quiver node"""
        ed.addLibraryInstance(matplotliblib, 'Vision.matplotlibNodes','matplotliblib')
        ed.master.update_idletasks()
        pause()
        masterNet=ed.currentNetwork
        #Quiver
        Quiver_81 = QuiverNE(constrkw = {}, name='Quiver', library=matplotliblib)
        masterNet.addNode(Quiver_81,279,395)
        Quiver_81.inputPortByName['color'].widget.set("r", run=False)
        Quiver_81.inputPortByName['pivot'].widget.set("mid", run=False)
        Quiver_81.inputPortByName['units'].widget.set("inches", run=False)
        code = """def doit(self, x, y, u, v, color, S, width, pivot, units, drawAreaDef):
        self.axes.clear()
        self.setDrawAreaDef(drawAreaDef)
        self.axes.quiver( x[::3, ::3], y[::3, ::3], u[::3, ::3], v[::3, ::3],
                pivot=pivot, color=color, units=units )
        self.canvas.draw()
        self.outputData(axes=self.axes)
""" 
        Quiver_81.configure(function=code)
        #ReadTable
        ReadTable_94 = ReadTable(constrkw = {}, name='ReadTableAB', library=stdlib)
        masterNet.addNode(ReadTable_94,37,56)
        ReadTable_94.inputPortByName['filename'].widget.set(
                os.path.abspath("../doc/Examples/matplotlib/Data/quiver_data_1.dat"), run=False)
        ReadTable_94.inputPortByName['sep'].widget.set(",", run=False)
        ReadTable_94.inputPortByName['datatype'].widget.set("float", run=False)
        apply(ReadTable_94.configure, (), {'expanded': True})
        #ReadTable
        ReadTable_95 = ReadTable(constrkw = {}, name='ReadTableAC', library=stdlib)
        masterNet.addNode(ReadTable_95,217,60)
        ReadTable_95.inputPortByName['filename'].widget.set(
                os.path.abspath("../doc/Examples/matplotlib/Data/quiver_data_2.dat"), run=False)
        ReadTable_95.inputPortByName['sep'].widget.set(",", run=False)
        ReadTable_95.inputPortByName['datatype'].widget.set("float", run=False)
        apply(ReadTable_95.configure, (), {'expanded': True})
        #ReadTable
        ReadTable_96 = ReadTable(constrkw = {}, name='ReadTableAD', library=stdlib)
        masterNet.addNode(ReadTable_96,404,68)
        ReadTable_96.inputPortByName['filename'].widget.set(
                os.path.abspath("../doc/Examples/matplotlib/Data/quiver_data_3.dat"), run=False)
        ReadTable_96.inputPortByName['sep'].widget.set(",", run=False)
        ReadTable_96.inputPortByName['datatype'].widget.set("float", run=False)
        apply(ReadTable_96.configure, (), {'expanded': True})
        #ReadTable
        ReadTable_97 = ReadTable(constrkw = {}, name='ReadTableAE', library=stdlib)
        masterNet.addNode(ReadTable_97,583,68)
        ReadTable_97.inputPortByName['filename'].widget.set(
                os.path.abspath("../doc/Examples/matplotlib/Data/quiver_data_4.dat"), run=False)
        ReadTable_97.inputPortByName['sep'].widget.set(",", run=False)
        ReadTable_97.inputPortByName['datatype'].widget.set("float", run=False)
        apply(ReadTable_97.configure, (), {'expanded': True})
        #As_Type
        As_Type_98 = AsType(constrkw = {}, name='As Type', library=stdlib)
        masterNet.addNode(As_Type_98,60,242)
        #As_Type
        As_Type_99 = AsType(constrkw = {}, name='As Type', library=stdlib)
        masterNet.addNode(As_Type_99,241,240)
        #As_Type
        As_Type_100 = AsType(constrkw = {}, name='As Type', library=stdlib)
        masterNet.addNode(As_Type_100,415,240)
        #As_Type
        As_Type_101 = AsType(constrkw = {}, name='As Type', library=stdlib)
        masterNet.addNode(As_Type_101,597,247)
        #
        masterNet.connectNodes(
            ReadTable_94, As_Type_98, "data", "inArrayList", blocking=True)
        masterNet.connectNodes(
            ReadTable_95, As_Type_99, "data", "inArrayList", blocking=True)    
        masterNet.connectNodes(
            ReadTable_96, As_Type_100, "data", "inArrayList", blocking=True)
            
        masterNet.connectNodes(
            ReadTable_97, As_Type_101, "data", "inArrayList", blocking=True)
        
        masterNet.connectNodes(
            As_Type_98, Quiver_81, "outArray", "x", blocking=True)
        masterNet.connectNodes(
            As_Type_101, Quiver_81, "outArray", "v", blocking=True)
        masterNet.connectNodes(
            As_Type_99, Quiver_81, "outArray", "y", blocking=True) 
        masterNet.connectNodes(
            As_Type_100, Quiver_81, "outArray", "u", blocking=True)
            
        masterNet.run()
        pause()
        self.assertEqual(len(Quiver_81.axes.collections)>0,True)

    #plot_date
    def test_matplotlib_PlotDate(self):  
        """testing  PlotDate node"""
        ed.addLibraryInstance(matplotliblib, 'Vision.matplotlibNodes','matplotliblib')
        ed.master.update_idletasks()
        pause()
        masterNet=ed.currentNetwork
        #PlotDate
        PlotDate_39 = PlotDateNE(constrkw = {}, name='PlotDate', library=matplotliblib)
        masterNet.addNode(PlotDate_39,72,343)
        PlotDate_39.inputPortByName['tz'].widget.set("US/Pacific", run=False)
        apply(PlotDate_39.configure, (), {'expanded': True})
        #ReadTable
        ReadTable_40 = ReadTable(constrkw = {}, name='ReadTableAF', library=stdlib)
        masterNet.addNode(ReadTable_40,102,17)
        ReadTable_40.inputPortByName['filename'].widget.set(
                os.path.abspath("../doc/Examples/matplotlib/Data/plot_date_data.dat"), run=False)
        ReadTable_40.inputPortByName['sep'].widget.set(",", run=False)
        ReadTable_40.inputPortByName['datatype'].widget.set("float", run=False)
        apply(ReadTable_40.configure, (), {'expanded': True})
        #Draw_Area
        Draw_Area_45 = MPLDrawAreaNE(constrkw = {}, name='Draw Area', library=matplotliblib)
        masterNet.addNode(Draw_Area_45,338,172)
        Draw_Area_45.inputPortByName['bottom'].widget.set(0.222222222222, run=False)
        Draw_Area_45.inputPortByName['height'].widget.set(0.702777777778, run=False)
        Draw_Area_45.inputPortByName['title'].widget.set("Plot Date", run=False)
        #Set_Matplotlib_options
        Set_Matplotlib_options_46 = MatPlotLibOptions(constrkw = {}, name='Set Matplotlib options', library=matplotliblib)
        masterNet.addNode(Set_Matplotlib_options_46,272,289)
        Set_Matplotlib_options_46.inputPortByName['matplotlibOptions'].widget.set({'xtick.labelrotation': 90.0}, run=False)
        #Index
        Index_55 = Index(constrkw = {}, name='Index', library=stdlib)
        masterNet.addNode(Index_55,21,160)
        apply(Index_55.configure, (), {'expanded': True})
        #Index
        Index_56 = Index(constrkw = {}, name='Index', library=stdlib)
        masterNet.addNode(Index_56,207,161)
        apply(Index_56.inputPortByName['index'].widget.configure, (), {'max': 1})
        Index_56.inputPortByName['index'].widget.set(1, run=False)
        apply(Index_56.configure, (), {'expanded': True})
        #
        masterNet.connectNodes(
            Draw_Area_45, PlotDate_39, "drawAreaDef", "drawAreaDef", blocking=True)
        masterNet.connectNodes(
            Set_Matplotlib_options_46, PlotDate_39, "matplotlibOptions", "drawAreaDef", blocking=True)
        masterNet.connectNodes(
            ReadTable_40, Index_55, "data", "data", blocking=True)
        masterNet.connectNodes(
            ReadTable_40, Index_56, "data", "data", blocking=True)
        masterNet.connectNodes(
            Index_56, PlotDate_39, "data", "y", blocking=True)
        masterNet.connectNodes(
            Index_55, PlotDate_39, "data", "x", blocking=True)
        masterNet.run()
        pause()
        try:    
            import pytz
            self.assertEqual(len(PlotDate_39.axes.lines)>0,True)    
        except:
            pass










if __name__ == '__main__':
    unittest.main()

            
