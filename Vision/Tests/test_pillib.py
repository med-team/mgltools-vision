#########################################################################
#
# Date: Aug 2003  Authors: Daniel Stoffler, Michel Sanner
#
#       stoffler@scripps.edu
#       sanner@scripps.edu
#
# Copyright: Daniel Stoffler, Michel Sanner, and TSRI
#
#########################################################################

import sys
ed = None

def setUp():
    global ed
    from Vision.VPE import VisualProgramingEnvironment
    ed = VisualProgramingEnvironment(name='Vision', withShell=0,)
    ed.master.update_idletasks()
    ed.configure(withThreads=0)


def tearDown():
    ed.exit_cb()
    import gc
    gc.collect()

##########################
## Helper methods
##########################

def pause(sleepTime=0.1):
    from time import sleep
    ed.master.update()
    sleep(sleepTime)

##########################
## Tests
##########################

def test_01_loadPILLib():
    from Vision.PILNodes import imagelib
    ed.addLibraryInstance(imagelib, 'Vision.PILNodes', 'imagelib')
    ed.master.update_idletasks()
    pause()


def test_02_addAllNodes():
    # test all nodes in this library by adding them to the canvas, show widgets
    # in node if available and show paramPanel if available, then delete node
    from Vision.PILNodes import imagelib
    ed.addLibraryInstance(imagelib, 'Vision.PILNodes', 'imagelib')
    ed.master.update_idletasks()
    pause()
    lib = 'Imaging'
    libs = ed.libraries
    posx = 150
    posy = 150

    for cat in libs[lib].libraryDescr.keys():
        #for node in libs[lib].libraryDescr[cat]['nodes']:
        nodes_to_test = libs[lib].libraryDescr[cat]['nodes']
        nodes_to_test.sort()
        for node in nodes_to_test:
            klass = node.nodeClass
            kw = node.kw
            args = node.args
            netNode = apply( klass, args, kw )
            print 'testing: '+node.name # begin node test
            #add node to canvas
            ed.currentNetwork.addNode(netNode,posx,posy)
            # show widget in node if available:
            widgetsInNode = netNode.getWidgetsForMaster('Node')
            if len( widgetsInNode.items() ):
                if not netNode.isExpanded():
                    netNode.toggleNodeExpand_cb()
                    ed.master.update_idletasks()
                    pause()
                else:
                    pause()
                # and then hide it
                netNode.toggleNodeExpand_cb()
                ed.master.update_idletasks()
                pause()

            # show widgets in param panel if available:
            widgetsInPanel = netNode.getWidgetsForMaster('ParamPanel')
            if len(widgetsInPanel.items()):
                netNode.paramPanel.show()
                ed.master.update_idletasks()
                pause()
                #and then hide it
                netNode.paramPanel.hide()
                ed.master.update_idletasks()

            # and now delete the node
            ed.currentNetwork.deleteNodes([netNode])
            ed.master.update_idletasks()
            print 'passed: '+node.name # end node test
            
