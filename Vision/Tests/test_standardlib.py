#########################################################################
#
# Date: Aug [0]003  Authors: Daniel Stoffler, Michel Sanner,Sowjanya Karnati 
#
#       stoffler@scripps.edu
#       sanner@scripps.edu
#
# Copyright: Daniel Stoffler, Michel Sanner, and TSRI
#
#########################################################################

import sys,unittest,os
import numpy

ed = None
import Vision
def pause():
        from time import sleep
        sleep(0.3)
class StandardLibraryBaseTest(unittest.TestCase):

    def setUp(self):
        global ed
        from Vision.VPE import VisualProgramingEnvironment
        ed = VisualProgramingEnvironment(name='Vision', withShell=0,)
        ed.master.update_idletasks()
        ed.configure(withThreads=0)


    def tearDown(self):
        ed.exit_cb()
        import gc
        gc.collect()
        try:
            cmd = "rm -f testfile.py"
            os.system(cmd)
        except:
            pass

##########################
## Tests
##########################

    def test_01_loadStandardLib(self):
        
        from Vision.StandardNodes import stdlib
        ed.addLibraryInstance(stdlib, 'Vision.StandardNodes', 'stdlib')
        ed.master.update_idletasks()
        pause()


    def test_02_addAllNodes(self):
        # test all nodes in this library by adding them to the canvas, show widgets
        # in node if available and show paramPanel if available, then delete node
        from Vision.StandardNodes import stdlib
        ed.addLibraryInstance(stdlib, 'Vision.StandardNodes', 'stdlib')
        ed.master.update_idletasks()
        pause()
        lib = 'Standard'
        libs = ed.libraries
        posx = 150
        posy = 150
        net = ed.currentNetwork
        for cat in libs[lib].libraryDescr.keys():
            for node in libs[lib].libraryDescr[cat]['nodes']:
                klass = node.nodeClass
                kw = node.kw
                args = node.args
                netNode = apply( klass, args, kw )
                print 'testing: '+node.name # begin node test
                #add node to canvas
                net.addNode(netNode,posx,posy)
                # show widget in node if available:
                widgetsInNode = netNode.getWidgetsForMaster('Node')
                if len( widgetsInNode.items() ):
                    if not netNode.isExpanded():
                        netNode.toggleNodeExpand_cb()
                        ed.master.update_idletasks()
                        pause()
                    else:
                        pause()
                    # and then hide it
                    netNode.toggleNodeExpand_cb()
                    ed.master.update_idletasks()
                    pause()

                # show widgets in param panel if available:
                widgetsInPanel = netNode.getWidgetsForMaster('ParamPanel')
                if len(widgetsInPanel.items()):
                    netNode.paramPanel.show()
                    ed.master.update_idletasks()
                    pause()
                    #and then hide it
                    netNode.paramPanel.hide()
                    ed.master.update_idletasks()

                # and now delete the node
                net.deleteNodes([netNode])
                ed.master.update_idletasks()
                print 'passed: '+node.name # end node test

########################################################
#   Filter Nodes Tests                                 #
########################################################

    def test_filter_cast(self):
        """testing cast node
        """
        from Vision.StandardNodes import stdlib
        ed.addLibraryInstance(stdlib, 'Vision.StandardNodes', 'stdlib')
        ed.master.update_idletasks()
        pause()
        net =ed.currentNetwork
        net.runOnNewData.value = True
        #Cast node
        node1 = Vision.StandardNodes.Cast(library =Vision.StandardNodes.stdlib)
        net.addNode(node1,150,150)
        #entry node
        node2 = Vision.StandardNodes.EntryNE(library =Vision.StandardNodes.stdlib)
        net.addNode(node2,50,50)
        #print node
        node3 = Vision.StandardNodes.Print(library =Vision.StandardNodes.stdlib)
        net.addNode(node3,200,50)
        net.connectNodes(node1,node3)
        net.connectNodes(node2,node1)
        node1.toggleNodeExpand_cb()
        #INPUT IS 1
        #int
        node1.inputPorts[1].widget.set('int')
        print "testing castnode entry int,input 1,output is:"
        node2.inputPorts[0].widget.set(1)
        self.assertEqual(node3.inputPorts[0].getData(),1)
        #list
        print "testing castnode entry list,input 1,output is:"
        node1.inputPorts[1].widget.set('list')
        self.assertEqual(node3.inputPorts[0].getData(),[1])
        #str
        print "testing castnode entry str,input 1,output is:"
        node1.inputPorts[1].widget.set('str')
        self.assertEqual(node3.inputPorts[0].getData(),'1')
#        #0Darray
#        print "testing castnode entry 0Darray,input 1,output is:"
#        node1.inputPorts[1].widget.set('0Darray')
#        self.assertEqual(node3.inputPorts[0].getData(),'1')
#        #0Dcomplex
#        print "testing castnode entry 0Dcomplex,input 1,output is:"
#        node1.inputPorts[1].widget.set('0Dcomplex')
#        self.assertEqual(node3.inputPorts[0].getData(),'1')
#        #0Ddictionary
#        print "testing castnode entry 0Ddictionary,input 1,output is:"
#        node1.inputPorts[1].widget.set('0Ddictionary')
#        self.assertEqual(node3.inputPorts[0].getData(),'1')
#        #0Ddouble
#        print "testing castnode entry 0Ddouble,input 1,output is:"
#        node1.inputPorts[1].widget.set('0Ddouble')
#        self.assertEqual(node3.inputPorts[0].getData(),'1')
#        #0Dfloat
#        print "testing castnode entry 0Dfloat,input 1,output is:"
#        node1.inputPorts[1].widget.set('0Dfloat')
#        self.assertEqual(node3.inputPorts[0].getData(),'1')
#        #0Dint
#        print "testing castnode entry 0Dint,input 1,output is:"
#        node1.inputPorts[1].widget.set('0Dint')
#        self.assertEqual(node3.inputPorts[0].getData(),'1')
#        #0Dlist
#        print "testing castnode entry 0Dlist,input 1,output is:"
#        node1.inputPorts[1].widget.set('0Dlist')
#        self.assertEqual(node3.inputPorts[0].getData(),'1')
#        #0Dlong
#        print "testing castnode entry 0Dlong,input 1,output is:"
#        node1.inputPorts[1].widget.set('0Dlong')
#        self.assertEqual(node3.inputPorts[0].getData(),'1')
#        #0Dstring
#        print "testing castnode entry 0Dstring,input 1,output is:"
#        node1.inputPorts[1].widget.set('0Dstring')
#        self.assertEqual(node3.inputPorts[0].getData(),'1')
#        #0Dtuple
#        print "testing castnode entry 0Dtuple,input 1,output is:"
#        node1.inputPorts[1].widget.set('0Dtuple')
#        self.assertEqual(node3.inputPorts[0].getData(),'1')
#        #1Darray
#        print "testing castnode entry 1Darray,input 1,output is:"
#        node1.inputPorts[1].widget.set('1Darray')
#        self.assertEqual(node3.inputPorts[0].getData(),'1')
#        #1Dcomplex
#        print "testing castnode entry 1Dcomplex,input 1,output is:"
#        node1.inputPorts[1].widget.set('1Dcomplex')
#        self.assertEqual(node3.inputPorts[0].getData(),'1')
#        #1Ddictionary
#        print "testing castnode entry 1Ddictionary,input 1,output is:"
#        node1.inputPorts[1].widget.set('1Ddictionary')
#        self.assertEqual(node3.inputPorts[0].getData(),'1')
#        #1Ddouble
#        print "testing castnode entry 1Ddouble,input 1,output is:"
#        node1.inputPorts[1].widget.set('1Ddouble')
#        self.assertEqual(node3.inputPorts[0].getData(),'1')
#        #1Dfloat
#        print "testing castnode entry 1Dfloat,input 1,output is:"
#        node1.inputPorts[1].widget.set('1Dfloat')
#        self.assertEqual(node3.inputPorts[0].getData(),'1')
#        #1Dint
#        print "testing castnode entry 1Dint,input 1,output is:"
#        node1.inputPorts[1].widget.set('1Dint')
#        self.assertEqual(node3.inputPorts[0].getData(),'1')
#        #1Dlist
#        print "testing castnode entry 1Dlist,input 1,output is:"
#        node1.inputPorts[1].widget.set('1Dlist')
#        self.assertEqual(node3.inputPorts[0].getData(),'1')
#        #1Dlong
#        print "testing castnode entry 1Dlong,input 1,output is:"
#        node1.inputPorts[1].widget.set('1Dlong')
#        self.assertEqual(node3.inputPorts[0].getData(),'1')
#        #1Dstring
#        print "testing castnode entry 1Dstring,input 1,output is:"
#        node1.inputPorts[1].widget.set('1Dstring')
#        self.assertEqual(node3.inputPorts[0].getData(),'1')
#        #1Dtuple
#        print "testing castnode entry 1Dtuple,input 1,output is:"
#        node1.inputPorts[1].widget.set('1Dtuple')
#        self.assertEqual(node3.inputPorts[0].getData(),'1')            
#        #2Darray
#        print "testing castnode entry 2Darray,input 1,output is:"
#        node1.inputPorts[1].widget.set('2Darray')
#        self.assertEqual(node3.inputPorts[0].getData(),'1')
#        #2Dcomplex
#        print "testing castnode entry 2Dcomplex,input 1,output is:"
#        node1.inputPorts[1].widget.set('2Dcomplex')
#        self.assertEqual(node3.inputPorts[0].getData(),'1')
#        #2Ddictionary
#        print "testing castnode entry 2Ddictionary,input 1,output is:"
#        node1.inputPorts[1].widget.set('2Ddictionary')
#        self.assertEqual(node3.inputPorts[0].getData(),'1')
#        #2Ddouble
#        print "testing castnode entry 2Ddouble,input 1,output is:"
#        node1.inputPorts[1].widget.set('2Ddouble')
#        self.assertEqual(node3.inputPorts[0].getData(),'1')
#        #2Dfloat
#        print "testing castnode entry 2Dfloat,input 1,output is:"
#        node1.inputPorts[1].widget.set('2Dfloat')
#        self.assertEqual(node3.inputPorts[0].getData(),'1')
#        #2Dint
#        print "testing castnode entry 2Dint,input 1,output is:"
#        node1.inputPorts[1].widget.set('2Dint')
#        self.assertEqual(node3.inputPorts[0].getData(),'1')
#        #2Dlist
#        print "testing castnode entry 2Dlist,input 1,output is:"
#        node1.inputPorts[1].widget.set('2Dlist')
#        self.assertEqual(node3.inputPorts[0].getData(),'1')
#        #2Dlong
#        print "testing castnode entry 2Dlong,input 1,output is:"
#        node1.inputPorts[1].widget.set('2Dlong')
#        self.assertEqual(node3.inputPorts[0].getData(),'1')
#        #2Dstring
#        print "testing castnode entry 2Dstring,input 1,output is:"
#        node1.inputPorts[1].widget.set('2Dstring')
#        self.assertEqual(node3.inputPorts[0].getData(),'1')
#        #2Dtuple
#        node1.inputPorts[1].widget.set('2Dtuple')
#        print "testing castnode entry 2Dtuple,input 1,output is:"
#        self.assertEqual(node3.inputPorts[0].getData(),'1')
#        #3Darray
#        print "testing castnode entry 3Darray,input 1,output is:"
#        node1.inputPorts[1].widget.set('3Darray')
#        self.assertEqual(node3.inputPorts[0].getData(),'1')
#        #3Dcomplex
#        print "testing castnode entry 3Dcomplex,input 1,output is:"
#        node1.inputPorts[1].widget.set('3Dcomplex')
#        self.assertEqual(node3.inputPorts[0].getData(),'1')
#        #3Ddictionary
#        print "testing castnode entry 3Ddictionary,input 1,output is:"
#        node1.inputPorts[1].widget.set('3Ddictionary')
#        self.assertEqual(node3.inputPorts[0].getData(),'1')
#        #3Ddouble
#        print "testing castnode entry 3Ddouble,input 1,output is:"
#        node1.inputPorts[1].widget.set('3Ddouble')
#        self.assertEqual(node3.inputPorts[0].getData(),'1')
#        #3Dfloat
#        print "testing castnode entry 3Dfloat,input 1,output is:"
#        node1.inputPorts[1].widget.set('3Dfloat')
#        self.assertEqual(node3.inputPorts[0].getData(),'1')
#        #3Dint
#        print "testing castnode entry 3Dint,input 1,output is:"
#        node1.inputPorts[1].widget.set('3Dint')
#        self.assertEqual(node3.inputPorts[0].getData(),'1')
#        #3Dlist
#        print "testing castnode entry 3Dlist,input 1,output is:"
#        node1.inputPorts[1].widget.set('3Dlist')
#        self.assertEqual(node3.inputPorts[0].getData(),'1')
#        #3Dlong
#        print "testing castnode entry 3Dlong,input 1,output is:"
#        node1.inputPorts[1].widget.set('3Dlong')
#        self.assertEqual(node3.inputPorts[0].getData(),'1')
#        #3Dstring
#        print "testing castnode entry 3Dstring,input 1,output is:"
#        node1.inputPorts[1].widget.set('3Dstring')
#        self.assertEqual(node3.inputPorts[0].getData(),'1')
#        #3Dtuple
#        print "testing castnode entry 3Dtuple,input 1,output is:"
#        node1.inputPorts[1].widget.set('3Dtuple')
#        self.assertEqual(node3.inputPorts[0].getData(),'1')
#        #4Darray
#        print "testing castnode entry 4Darray,input 1,output is:"
#        node1.inputPorts[1].widget.set('4Darray')
#        self.assertEqual(node3.inputPorts[0].getData(),'1')
#        #4Dcomplex
#        print "testing castnode entry 4Dcomplex,input 1,output is:"
#        node1.inputPorts[1].widget.set('4Dcomplex')
#        self.assertEqual(node3.inputPorts[0].getData(),'1')
#        #4Ddictionary
#        print "testing castnode entry 4Ddictionary,input 1,output is:"
#        node1.inputPorts[1].widget.set('4Ddictionary')
#        self.assertEqual(node3.inputPorts[0].getData(),'1')
#        #4Ddouble
#        print "testing castnode entry 4Ddouble,input 1,output is:"
#        node1.inputPorts[1].widget.set('4Ddouble')
#        self.assertEqual(node3.inputPorts[0].getData(),'1')
#        #4Dfloat
#        print "testing castnode entry 4Dfloat,input 1,output is:"
#        node1.inputPorts[1].widget.set('4Dfloat')
#        self.assertEqual(node3.inputPorts[0].getData(),'1')
#        #4Dint
#        print "testing castnode entry 4Dint,input 1,output is:" 
#        node1.inputPorts[1].widget.set('4Dint')
#        self.assertEqual(node3.inputPorts[0].getData(),'1')
#        #4Dlist
#        print "testing castnode entry 4Dlist,input 1,output is:"
#        node1.inputPorts[1].widget.set('4Dlist')
#        self.assertEqual(node3.inputPorts[0].getData(),'1')
#        #4Dlong
#        print "testing castnode entry 4Dlong,input 1,output is:"
#        node1.inputPorts[1].widget.set('4Dlong')
#        self.assertEqual(node3.inputPorts[0].getData(),'1')
#        #4Dstring
#        print "testing castnode entry 4Dstring,input 1,output is:"
#        node1.inputPorts[1].widget.set('4Dstring')
#        self.assertEqual(node3.inputPorts[0].getData(),'1')
#        #4Dtuple
#        print "testing castnode entry 4Dtuple,input 1,output is:"
#        node1.inputPorts[1].widget.set('4Dtuple')
#        self.assertEqual(node3.inputPorts[0].getData(),'1')
#        #5Darray
#        print "testing castnode entry 5Darray,input 1,output is:"
#        node1.inputPorts[1].widget.set('5Darray')
#        self.assertEqual(node3.inputPorts[0].getData(),'1')
#        #5Dcomplex
#        print "testing castnode entry 5Dcomplex,input 1,output is:"
#        node1.inputPorts[1].widget.set('5Dcomplex')
#        self.assertEqual(node3.inputPorts[0].getData(),'1')
#        #5Ddictionary
#        print "testing castnode entry 5Ddictionary,input 1,output is:"
#        node1.inputPorts[1].widget.set('5Ddictionary')
#        self.assertEqual(node3.inputPorts[0].getData(),'1')
#        #5Ddouble
#        print "testing castnode entry 5Ddouble,input 1,output is:"
#        node1.inputPorts[1].widget.set('5Ddouble')
#        self.assertEqual(node3.inputPorts[0].getData(),'1')
#        #5Dfloat
#        print "testing castnode entry 5Dfloat,input 1,output is:"
#        node1.inputPorts[1].widget.set('5Dfloat')
#        self.assertEqual(node3.inputPorts[0].getData(),'1')
#        #5Dint
#        print "testing castnode entry 5Dint,input 1,output is:"
#        node1.inputPorts[1].widget.set('5Dint')
#        self.assertEqual(node3.inputPorts[0].getData(),'1')
#        #5Dlist
#        print "testing castnode entry 5Dlist,input 1,output is:"
#        node1.inputPorts[1].widget.set('5Dlist')
#        self.assertEqual(node3.inputPorts[0].getData(),'1')
#        #5Dlong
#        print "testing castnode entry 5Dlong,input 1,output is:"
#        node1.inputPorts[1].widget.set('5Dlong')
#        self.assertEqual(node3.inputPorts[0].getData(),'1')
#        #5Dstring
#        print "testing castnode entry 5Dstring,input 1,output is:"
#        node1.inputPorts[1].widget.set('5Dstring')
#        self.assertEqual(node3.inputPorts[0].getData(),'1')
#        #5Dtuple
#        print "testing castnode entry 5Dtuple,input 1,output is:"
#        node1.inputPorts[1].widget.set('5Dtuple')
#        self.assertEqual(node3.inputPorts[0].getData(),'1')
#        #6Darray
#        print "testing castnode entry 6Darray,input 1,output is:"
#        node1.inputPorts[1].widget.set('6Darray')
#        self.assertEqual(node3.inputPorts[0].getData(),'1')
#        #6Dcomplex
#        print "testing castnode entry 6Dcomplex,input 1,output is:"
#        node1.inputPorts[1].widget.set('6Dcomplex')
#        self.assertEqual(node3.inputPorts[0].getData(),'1')
#        #6Ddictionary
#        print "testing castnode entry 6Ddictionary,input 1,output is:"
#        node1.inputPorts[1].widget.set('6Ddictionary')
#        self.assertEqual(node3.inputPorts[0].getData(),'1')
#        #6Ddouble
#        print "testing castnode entry 6Ddouble,input 1,output is:"
#        node1.inputPorts[1].widget.set('6Ddouble')
#        self.assertEqual(node3.inputPorts[0].getData(),'1')
#        #6Dfloat
#        print "testing castnode entry 6Dfloat,input 1,output is:"
#        node1.inputPorts[1].widget.set('6Dfloat')
#        self.assertEqual(node3.inputPorts[0].getData(),'1')
#        #6Dint
#        print "testing castnode entry 6Dint,input 1,output is:"
#        node1.inputPorts[1].widget.set('6Dint')
#        self.assertEqual(node3.inputPorts[0].getData(),'1')
#        #6Dlist
#        print "testing castnode entry 6Dlist,input 1,output is:"
#        node1.inputPorts[1].widget.set('6Dlist')
#        self.assertEqual(node3.inputPorts[0].getData(),'1')
#        #6Dlong
#        print "testing castnode entry 6Dlong,input 1,output is:"
#        node1.inputPorts[1].widget.set('6Dlong')
#        self.assertEqual(node3.inputPorts[0].getData(),'1')
#        #6Dstring
#        print "testing castnode entry 6Dstring,input 1,output is:"
#        node1.inputPorts[1].widget.set('6Dstring')
#        self.assertEqual(node3.inputPorts[0].getData(),'1')
#        #6Dtuple
#        print "testing castnode entry 6Dtuple,input 1,output is:"
#        node1.inputPorts[1].widget.set('6Dtuple')
#        self.assertEqual(node3.inputPorts[0].getData(),'1')
        #None
        print "testing castnode entry None,input 1,output is:"
        node1.inputPorts[1].widget.set('None')
        self.assertEqual(node3.inputPorts[0].getData(),'1')
        #NumericArray
        print "testing castnode entry NumericArray,input 1,output is:"
        node1.inputPorts[1].widget.set('NumericArray')
        self.assertEqual(node3.inputPorts[0].getData(),'1')
        #array
        #print "testing castnode entry array,input 1,output is:"
        #node1.inputPorts[1].widget.set('array')
        #self.assertEqual(node3.inputPorts[0].getData(),'1')
        #dict
        #print "testing castnode entry dict,input 1,output is:"
        #node1.inputPorts[1].widget.set('dict')
        #self.assertEqual(node3.inputPorts[0].getData(),'1')
        #float
        print "testing castnode entry float,input 1,output is:"
        node1.inputPorts[1].widget.set('float')
        self.assertEqual(node3.inputPorts[0].getData(),1.0)

#        #instanceMatrices
#        print "testing castnode entry instanceMatrices,input 1,output is:"
#        node1.inputPorts[1].widget.set('instancemat')
#        self.assertEqual(node3.inputPorts[0].getData(),1.0)

        #string
        print "testing castnode entry string,input 1,output is:"
        node1.inputPorts[1].widget.set('string')
        self.assertEqual(node3.inputPorts[0].getData(),'1')
        #triggerIn
        print "testing castnode entry triggerIn,input 1,output is:"
        node1.inputPorts[1].widget.set('triggerIn')
        self.assertEqual(node3.inputPorts[0].getData(),'1')
        #triggerOut
        print "testing castnode entry triggerIn,input 1,output is:"
        node1.inputPorts[1].widget.set('triggerIn')
        self.assertEqual(node3.inputPorts[0].getData(),'1')
        #tuple
        print "testing castnode entry tuple,input 1,output is:"
        node1.inputPorts[1].widget.set('tuple')
        self.assertEqual(node3.inputPorts[0].getData(),('1',))
        #vector
        print "testing castnode entry vector,input 1,output is:"
        node1.inputPorts[1].widget.set('vector')
        self.assertEqual(node3.inputPorts[0].getData(),('1',))


    def test_filter_duplicate(self):
        """tests duplicate node when input is 1,p,[0] and entry is 2
        """
        from Vision.StandardNodes import stdlib
        ed.addLibraryInstance(stdlib, 'Vision.StandardNodes', 'stdlib')
        ed.master.update_idletasks()
        pause()
        net = ed.currentNetwork
        net.runOnNewData.value = True
        #duplicate node
        node1 = Vision.StandardNodes.Duplicate(library =Vision.StandardNodes.stdlib)
        net.addNode(node1,150,150)
        #entry node
        node2 = Vision.StandardNodes.EntryNE(library =Vision.StandardNodes.stdlib)
        net.addNode(node2,50,50)
        #pritn node
        node3 = Vision.StandardNodes.Print(library =Vision.StandardNodes.stdlib)
        net.addNode(node3,200,50)
        #connecting nodes
        net.connectNodes(node1,node3)
        net.connectNodes(node2,node1)
        #toggle node to expand
        node1.toggleNodeExpand_cb()
        #setting values to duplicate node and entry node
        node2.inputPorts[0].widget.set(1)
        node1.inputPorts[1].widget.set(2)
        print "tests when duplicate node entry 2,input 1,output is:"
        ed.master.update_idletasks()
        pause()
        self.assertEqual(node3.inputPorts[0].getData(),['1', '1'])
        print "tests when duplicate node entry 2,input p,output is:"
        node2.inputPorts[0].widget.set('p')
        ed.master.update_idletasks()
        pause()
        self.assertEqual(node3.inputPorts[0].getData(),['p', 'p'])
        print "tests when duplicate node entry 2,input [0],output is:"
        node2.inputPorts[0].widget.set([0])
        ed.master.update_idletasks()
        pause()
        self.assertEqual(node3.inputPorts[0].getData(),['[0]', '[0]'])
        


#    def Xtest_filter_index(self):
#        """tests index node when input is hello and entry 1
#        """
#        from Vision.StandardNodes import stdlib
#        ed.addLibraryInstance(stdlib, 'Vision.StandardNodes', 'stdlib')
#        ed.master.update_idletasks()
#        pause()
#        net = ed.currentNetwork
#        #Index node
#        node1 = Vision.StandardNodes.Index(library =Vision.StandardNodes.stdlib)
#        net.addNode(node1,150,150)
#        #entry node
#        node2 = Vision.StandardNodes.EntryNE(library =Vision.StandardNodes.stdlib)
#        net.addNode(node2,50,50)
#        #print node
#        node3 = Vision.StandardNodes.Print(library =Vision.StandardNodes.stdlib)
#        net.addNode(node3,200,50)
#        #connect nodes
#        net.connectNodes(node1,node3)
#        net.connectNodes(node2,node1)
#        node1.toggleNodeExpand_cb() 
#        #setting values for index,entry nodes
#        print "tests when indexnode entry is 1,input hello,output:"
#        node2.inputPorts[0].widget.set("hello")
#        node1.inputPorts[1].widget.set(1)
#        ed.master.update_idletasks()
#        pause()
#        self.assertEqual(node3.inputPorts[0].getData(),'e')
#        print "tests when indexnode entry is 1,input 759087436,output:"
#        node2.inputPorts[0].widget.set("759087436")
#        ed.master.update_idletasks()
#        pause()
#        self.assertEqual(node3.inputPorts[0].getData(),'5')
#        print "tests when indexnode entry is 9,input is 75908743674 ,output is :"
#        node2.inputPorts[0].widget.set("75908743674")
#        node1.inputPorts[1].widget.set(9)
#        ed.master.update_idletasks()
#        pause()
#        self.assertEqual(node3.inputPorts[0].getData(),'7')
#        #raising IndexError but not catching it
#        #self.assertRaises(IndexError,node2.inputPorts[0].widget.set,' ')
    
    def test_filter_listOf(self):
        """tests listOf node when input is 12 output is [12]
        """
        from Vision.StandardNodes import stdlib
        ed.addLibraryInstance(stdlib, 'Vision.StandardNodes', 'stdlib')
        ed.master.update_idletasks()
        pause()
        net = ed.currentNetwork
        #listof node
        node1 = Vision.StandardNodes.ListOf(library =Vision.StandardNodes.stdlib)
        net.addNode(node1,150,150)
        #generic node
        node2 = Vision.StandardNodes.Generic(library =Vision.StandardNodes.stdlib)
        net.addNode(node2,50,50)
        #print node
        node3 = Vision.StandardNodes.Print(library =Vision.StandardNodes.stdlib)
        net.addNode(node3,200,50)
        node2.addOutputPort()
        node2.setFunction("def doit(self):\n\tself.outputData(out0=12)\n")
        net.connectNodes(node1,node3)
        print "tests when listOfnode ,input 12 ,output :"
        #adding an input port for list of
        node1.addInputPort()
        #connecting generic nad list of node
        port3 =node2.outputPorts[0]
        port4 =node1.inputPorts[1] 
        apply( net.connectNodes,(port3.node, port4.node, port3.number, port4.number),{})
        ed.runCurrentNet_cb()
        self.assertEqual(node3.inputPorts[0].getData(),[12])
        
    def test_filter_operator_2_apply_to_elments_1(self):
        
        from Vision.StandardNodes import stdlib
        ed.addLibraryInstance(stdlib, 'Vision.StandardNodes', 'stdlib')
        ed.master.update_idletasks()
        pause()
        net = ed.currentNetwork
        #operator2 node
        node1 = Vision.StandardNodes.Operator2(library =Vision.StandardNodes.stdlib)
        net.addNode(node1,150,150)
        #generic node
        node0 = Vision.StandardNodes.Generic(library =Vision.StandardNodes.stdlib)
        net.addNode(node0,50,50)
        #generic node
        node2 = Vision.StandardNodes.Generic(library =Vision.StandardNodes.stdlib)
        net.addNode(node2,200,10)
        #print node
        node3 = Vision.StandardNodes.Print(library =Vision.StandardNodes.stdlib)
        net.addNode(node3,400,100)
        #adding outports and setting edit functions for generic nodes
        node0.addOutputPort()
        node0.setFunction("def doit(self):\n\tself.outputData(out0=['1','2','3','4','5'])\n")
        node2.addOutputPort()
        node2.setFunction("def doit(self):\n\tself.outputData(out0=['6','7','8','9','10'])\n")
        #toggle On apply to elements button
        node1.inputPorts[3].widget.set(1)
        #connecting nodes
        net.connectNodes(node1,node3)
        #connecting first generic node to operator2 
        port3 =node0.outputPorts[0]
        port4 =node1.inputPorts[0] 
        apply( net.connectNodes,(port3.node, port4.node, port3.number, port4.number),{})
        #connecting second generic node to operator2
        port3 =node2.outputPorts[0]
        port4 =node1.inputPorts[1] 
        apply( net.connectNodes,(port3.node, port4.node, port3.number, port4.number),{})
        print "tests operator2 entry concat,input ['1','2','3','4','5'],['6','7','8','9','10'],apply to elements On,output is:"
        node1.inputPorts[2].widget.set('concat')
        ed.runCurrentNet_cb()
        ed.master.update_idletasks()
        pause()
        self.assertEqual(node3.inputPorts[0].getData(),['16', '27', '38', '49', '510'])
        print "tests operator2 entry contains,input ['1','2','3','4','5'],['6','7','8','9','10'],apply to elements On,output is:"
        node1.inputPorts[2].widget.set('contains')
        ed.runCurrentNet_cb()
        ed.master.update_idletasks()
        pause()
        self.assertEqual(node3.inputPorts[0].getData(),[False, False, False, False, False])
        print "tests operator2 entry countOf,input ['1','2','3','4','5'],['6','7','8','9','10'],apply to elements On,output is:"
        node1.inputPorts[2].widget.set('countOf')
        ed.runCurrentNet_cb()
        ed.master.update_idletasks()
        pause()
        self.assertEqual(node3.inputPorts[0].getData(),[0, 0, 0, 0, 0])
        
    def test_filter_operator_2_apply_to_elments_2(self):
        
        from Vision.StandardNodes import stdlib
        ed.addLibraryInstance(stdlib, 'Vision.StandardNodes', 'stdlib')
        ed.master.update_idletasks()
        pause()
        net = ed.currentNetwork
        net.runOnNewData.value = True
        #operator2 node
        node1 = Vision.StandardNodes.Operator2(library =Vision.StandardNodes.stdlib)
        net.addNode(node1,150,150)
        #generic node
        node0 = Vision.StandardNodes.Generic(library =Vision.StandardNodes.stdlib)
        net.addNode(node0,50,50)
        #generic node
        node2 = Vision.StandardNodes.Generic(library =Vision.StandardNodes.stdlib)
        net.addNode(node2,200,10)
        #print node
        node3 = Vision.StandardNodes.Print(library =Vision.StandardNodes.stdlib)
        net.addNode(node3,400,100)
        #adding outports ,setting edit functions for generic node
        node0.addOutputPort()
        node0.setFunction("def doit(self):\n\tself.outputData(out0=[1,2,3,4,5])\n")
        node2.addOutputPort()
        node2.setFunction("def doit(self):\n\tself.outputData(out0=[6,7,8,9,10])\n")
        #toggle On apply to elements button
        node1.inputPorts[3].widget.set(1)
        #connecting nodes
        net.connectNodes(node1,node3)
        port3 =node0.outputPorts[0]
        port4 =node1.inputPorts[0] 
        apply( net.connectNodes,(port3.node, port4.node, port3.number, port4.number),{})
        port3 =node2.outputPorts[0]
        port4 =node1.inputPorts[1] 
        apply( net.connectNodes,(port3.node, port4.node, port3.number, port4.number),{})
        #setting operation lt in operator2
        print "tests operator2 entry lt,input [1,2,3,4,5],[6,7,8,9,10],apply to elements On ,output is:"
        node1.inputPorts[2].widget.set('lt')
        ed.runCurrentNet_cb()
        ed.master.update_idletasks()
        pause()
        self.assertEqual(node3.inputPorts[0].getData(),[True, True, True, True, True])
        print "tests operator2 entry le,input [1,2,3,4,5],[6,7,8,9,10],apply to elements On,output is:"
        #setting operation le in operator2
        node1.inputPorts[2].widget.set('le')
        ed.master.update_idletasks()
        pause()
        self.assertEqual(node3.inputPorts[0].getData(),[True, True, True, True, True])
        print "tests operator2 entry eq,input [1,2,3,4,5],[6,7,8,9,10],apply to elements On,output is:"
        #setting operation eq in operator2
        node1.inputPorts[2].widget.set('eq')
        ed.master.update_idletasks()
        pause()
        self.assertEqual(node3.inputPorts[0].getData(),[False, False, False, False, False])
        print "tests operator2 entry ne,input [1,2,3,4,5],[6,7,8,9,10],apply to elements On,output is:"
        #setting operation ne in operator2
        node1.inputPorts[2].widget.set('ne')
        ed.master.update_idletasks()
        pause()
        self.assertEqual(node3.inputPorts[0].getData(),[True, True, True, True, True])
        print "tests operator2 entry gt,input [1,2,3,4,5],[6,7,8,9,10],apply to elements On,output is:"
        #setting operation gt in operator2
        node1.inputPorts[2].widget.set('gt')
        ed.master.update_idletasks()
        pause()
        self.assertEqual(node3.inputPorts[0].getData(),[False, False, False, False, False])
        print "tests operator2 entry ge,input [1,2,3,4,5],[6,7,8,9,10],apply to elements On,output is:"
        #setting operation ge in operator2
        node1.inputPorts[2].widget.set('ge')
        ed.master.update_idletasks()
        pause()
        self.assertEqual(node3.inputPorts[0].getData(),[False, False, False, False, False])
        print "tests operator2 entry is_,input [1,2,3,4,5],[6,7,8,9,10],apply to elements On,output is:"
        #setting operation is_ in operator2
        node1.inputPorts[2].widget.set('is_')
        ed.master.update_idletasks()
        pause()
        self.assertEqual(node3.inputPorts[0].getData(),[False, False, False, False, False])
        print "tests operator2 entry is_not,input [1,2,3,4,5],[6,7,8,9,10],apply to elements On,output is:"
        #setting operation is_not in operator2
        node1.inputPorts[2].widget.set('is_not')
        ed.master.update_idletasks()
        pause()
        self.assertEqual(node3.inputPorts[0].getData(),[True, True, True, True, True])
        print "tests operator2 entry add,input [1,2,3,4,5],[6,7,8,9,10],apply to elements On,output is:"
        #setting operation add in opeartor2
        node1.inputPorts[2].widget.set('add')
        ed.master.update_idletasks()
        pause()
        self.assertEqual(node3.inputPorts[0].getData(),[7, 9, 11, 13, 15])
        print "tests operator2 entry div,input [1,2,3,4,5],[6,7,8,9,10],apply to elements On,output is:"
        #setting opeartion div in opeartor2
        node1.inputPorts[2].widget.set('div')
        ed.master.update_idletasks()
        pause()
        self.assertEqual(node3.inputPorts[0].getData(),[0, 0, 0, 0, 0])
        print "tests operator2 entry floordiv,input [1,2,3,4,5],[6,7,8,9,10],apply to elements On,output is:"
        #setting opeartion floordiv in opeartor2
        node1.inputPorts[2].widget.set('floordiv')
        ed.master.update_idletasks()
        pause()
        self.assertEqual(node3.inputPorts[0].getData(),[0, 0, 0, 0, 0])
        print "tests operator2 entry mod,input [1,2,3,4,5],[6,7,8,9,10],apply to elements On,output is:"
        #setting opeartion mod in opeartor2
        node1.inputPorts[2].widget.set('mod')
        ed.master.update_idletasks()
        pause()
        self.assertEqual(node3.inputPorts[0].getData(),[1, 2, 3, 4, 5])
        print "tests operator2 entry mul,input [1,2,3,4,5],[6,7,8,9,10],apply to elements On,output is:"
        #setting opeartion mul in opeartor2
        node1.inputPorts[2].widget.set('mul')
        ed.master.update_idletasks()
        pause()
        self.assertEqual(node3.inputPorts[0].getData(),[6, 14, 24, 36, 50])
        print "tests operator2 entry pow,input [1,2,3,4,5],[6,7,8,9,10],apply to elements On,output is:"
        #setting opeartion pow in opeartor2
        node1.inputPorts[2].widget.set('pow')
        ed.runCurrentNet_cb()
        ed.master.update_idletasks()
        pause()
        self.assertEqual(node3.inputPorts[0].getData(),[1, 128, 6561, 262144, 9765625])
        print "tests operator2 entry sub,input [1,2,3,4,5],[6,7,8,9,10],apply to elements On,output is:"
        #setting opeartion sub in opeartor2
        node1.inputPorts[2].widget.set('sub')
        ed.master.update_idletasks()
        pause()
        self.assertEqual(node3.inputPorts[0].getData(),[-5, -5, -5, -5, -5])
        print "tests operator2 entry rshift,input [1,2,3,4,5],[6,7,8,9,10],apply to elements On,output is:"
        #setting opeartion rshift in opeartor2
        node1.inputPorts[2].widget.set('rshift')
        ed.master.update_idletasks()
        pause()
        self.assertEqual(node3.inputPorts[0].getData(),[0, 0, 0, 0, 0])
        print "tests operator2 entry lshift,input [1,2,3,4,5],[6,7,8,9,10],apply to elements On,output is:"
        #setting opeartion lshift in opeartor2
        node1.inputPorts[2].widget.set('lshift')
        ed.master.update_idletasks()
        pause()
        self.assertEqual(node3.inputPorts[0].getData(),[64, 256, 768, 2048, 5120])
 
        
    def test_filter_operator_2_1(self):
        from Vision.StandardNodes import stdlib
        ed.addLibraryInstance(stdlib, 'Vision.StandardNodes', 'stdlib')
        ed.master.update_idletasks()
        pause()
        net = ed.currentNetwork
        net.runOnNewData.value = True
        #operator2 node
        node1 = Vision.StandardNodes.Operator2(library =Vision.StandardNodes.stdlib)
        net.addNode(node1,150,150)
        #generic node
        node0 = Vision.StandardNodes.Generic(library =Vision.StandardNodes.stdlib)
        net.addNode(node0,50,50)
        #generic node
        node2 = Vision.StandardNodes.Generic(library =Vision.StandardNodes.stdlib)
        net.addNode(node2,200,10)
        #print node
        node3 = Vision.StandardNodes.Print(library =Vision.StandardNodes.stdlib)
        net.addNode(node3,400,100)
        #adding outports ,setting edit functions for generic node
        node0.addOutputPort()
        node0.setFunction("def doit(self):\n\tself.outputData(out0=[1,2,3,4,5])\n")
        node2.addOutputPort()
        node2.setFunction("def doit(self):\n\tself.outputData(out0 = 3)\n")
        net.connectNodes(node1,node3)
        port3 =node0.outputPorts[0]
        port4 =node1.inputPorts[0] 
        apply( net.connectNodes,(port3.node, port4.node, port3.number, port4.number),{})
        port3 =node2.outputPorts[0]
        port4 =node1.inputPorts[1] 
        apply( net.connectNodes,(port3.node, port4.node, port3.number, port4.number),{})
        print "tests operator2 entry indexOf,input [1,2,3,4,5],3,output:"
        node1.inputPorts[2].widget.set('indexOf')
        ed.runCurrentNet_cb()
        self.assertEqual(node3.inputPorts[0].getData(),2)
        print "tests operator2 entry getitem,input [1,2,3,4,5],3,output:"
        node1.inputPorts[2].widget.set('getitem')
        self.assertEqual(node3.inputPorts[0].getData(),4)
        print "tests operator2 entry delitem,input [1,2,3,4,5],3,output:"
        node1.inputPorts[2].widget.set('delitem')
        self.assertEqual(node3.inputPorts[0].getData(),[1,2,3,5])


    def test_filter_Operator2(self):
        """tests operator2 entry lt,le,eq,ne,gt,ge,is_,is_not,concat,contains,countOf,input through entry buttons
        """
        from Vision.StandardNodes import stdlib
        ed.addLibraryInstance(stdlib, 'Vision.StandardNodes', 'stdlib')
        ed.master.update_idletasks()
        pause()
        net = ed.currentNetwork
        net.runOnNewData.value = True
        #opearator2 node
        node1 = Vision.StandardNodes.Operator2(library =Vision.StandardNodes.stdlib)
        net.addNode(node1,150,150)
        #entry node
        node0 = Vision.StandardNodes.EntryNE(library =Vision.StandardNodes.stdlib)
        net.addNode(node0,50,50)
        #entry node
        node2 = Vision.StandardNodes.EntryNE(library =Vision.StandardNodes.stdlib)
        net.addNode(node2,200,10)
        #print node
        node3 = Vision.StandardNodes.Print(library =Vision.StandardNodes.stdlib)
        net.addNode(node3,400,100)
        #connecting nodes
        net.connectNodes(node1,node3)
        port1 = node2.inputPorts[0]
        port2 = node1.inputPorts[0]
        apply( net.connectNodes,(port1.node, port2.node, port1.number, port2.number),{})
        port3 =node0.inputPorts[0]
        port4 =node1.inputPorts[1] 
        apply( net.connectNodes,(port3.node, port4.node, port3.number, port4.number),{})
        node0.inputPorts[0].widget.set(10)
        node2.inputPorts[0].widget.set(20)
        print "tests operator2 entry lt,input 10,20,output:"
        #setting operator lt
        node1.inputPorts[2].widget.set('lt')
        ed.master.update_idletasks()
        pause()
        self.assertEqual(node3.inputPorts[0].getData(),False)
        print "tests operator2 entry le,input 10,20,output:"
        #setting operator le
        node1.inputPorts[2].widget.set('le')
        ed.master.update_idletasks()
        pause()
        self.assertEqual(node3.inputPorts[0].getData(),False)
        print "tests operator2 entry eq,input 10,20,output:"
        #setting operator eq
        node1.inputPorts[2].widget.set('eq')
        ed.master.update_idletasks()
        pause()
        self.assertEqual(node3.inputPorts[0].getData(),False)
        print "tests operator2 entry ne,input 10,20,output:"
        #setting operator ne
        node1.inputPorts[2].widget.set('ne')
        ed.master.update_idletasks()
        pause()
        self.assertEqual(node3.inputPorts[0].getData(),True)
        print "tests operator2 entry gt,input 10,20,output:"
        #setting operator gt
        node1.inputPorts[2].widget.set('gt')
        ed.master.update_idletasks()
        pause()
        self.assertEqual(node3.inputPorts[0].getData(),True)
        print "tests operator2 entry ge,input 10,20,output:"
        #setting operator ge
        node1.inputPorts[2].widget.set('ge')
        ed.master.update_idletasks()
        pause()
        self.assertEqual(node3.inputPorts[0].getData(),True)
        print "tests operator2 entry is_,input 10,20,output:"
        #setting operator is_
        node1.inputPorts[2].widget.set('is_')
        ed.master.update_idletasks()
        pause()
        self.assertEqual(node3.inputPorts[0].getData(),False)
        print "tests operator2 entry is_not,input 10,20,output:"
        #setting operator is_not
        node1.inputPorts[2].widget.set('is_not')
        ed.master.update_idletasks()
        pause()
        ed.master.update_idletasks()
        pause()
        self.assertEqual(node3.inputPorts[0].getData(),True)
        print "tests operator2 entry concat,input 10,20,output:"
        #setting operator concat
        node1.inputPorts[2].widget.set('concat')
        ed.master.update_idletasks()
        pause()
        self.assertEqual(node3.inputPorts[0].getData(),'2010')
        print "tests operator2 entry contains,input 10,20,output:"
        #setting operator contains
        node1.inputPorts[2].widget.set('contains')
        ed.master.update_idletasks()
        pause()
        self.assertEqual(node3.inputPorts[0].getData(),False)
        print "tests operator2 entry countOf,input 10,20,output:"
        #setting operator countOf
        node1.inputPorts[2].widget.set('countOf')
        ed.master.update_idletasks()
        pause()
        self.assertEqual(node3.inputPorts[0].getData(),0) 
        
       
    def test_filter_operator2_2(self):
        """tests operator2 entry add,div,floordiv,mod,mul,pow,lshift,rshift,input through entry button 
        """
        from Vision.StandardNodes import stdlib
        ed.addLibraryInstance(stdlib, 'Vision.StandardNodes', 'stdlib')
        ed.master.update_idletasks()
        pause()
        net = ed.currentNetwork
        net.runOnNewData.value = True
        #node1 is opeartor2
        node1 = Vision.StandardNodes.Operator2(library =Vision.StandardNodes.stdlib)
        #adding node1
        net.addNode(node1,150,150)
        #node0 entry button1
        node0 = Vision.StandardNodes.EntryNE(library =Vision.StandardNodes.stdlib)
        #adding node0
        net.addNode(node0,50,50)
        #node2 is entry button2
        node2 = Vision.StandardNodes.EntryNE(library =Vision.StandardNodes.stdlib)
        #adding node2
        net.addNode(node2,200,10)
        #node3 is print 
        node3 = Vision.StandardNodes.Print(library =Vision.StandardNodes.stdlib)
        #adding node3 
        net.addNode(node3,400,100)
        #connecting operator2 and print
        net.connectNodes(node1,node3)
        #connecting entry button1 o/p to operator2 i/p
        port1 = node2.outputPorts[0]
        port2 = node1.inputPorts[0]
        apply( net.connectNodes,(port1.node, port2.node, port1.number, port2.number),{})
        #connecting entry button2 o/p to operator2 i/p
        port3 =node0.inputPorts[0]
        port4 =node1.inputPorts[1] 
        apply( net.connectNodes,(port3.node, port4.node, port3.number, port4.number),{})
        
        #setting value in entry button 1
        node0.inputPorts[0].widget.set(20)
        #setting value in entry button 2
        node2.inputPorts[0].widget.set(10)
        #setting output ports of entry button1 data type to int
        node0.outputPorts[0].edit()
        node0.outputPorts[0].objEditor.dataType.set('int')
        node0.outputPorts[0].objEditor.OK()
        #setting input port1 of opeartor 2 data type to int
        node1.inputPorts[0].edit()
        node1.inputPorts[0].objEditor.dataType.set('int')
        node1.inputPorts[0].objEditor.OK()
        #setting output ports of entry button2 data type to int
        node2.outputPorts[0].edit()
        node2.outputPorts[0].objEditor.dataType.set('int')
        node2.outputPorts[0].objEditor.OK()
        #setting input port2 of opeartor 2 data type to int
        node1.inputPorts[1].edit()
        node1.inputPorts[1].objEditor.dataType.set('int')
        node1.inputPorts[1].objEditor.OK()
        #setting operation add in opeartor2
        print "tests operator2 entry add ,input 20,10,output is:"
        node1.inputPorts[2].widget.set('add')
        ed.runCurrentNet_cb()
        #ed.master.update_idletasks()
        #pause()
        self.assertEqual(node3.inputPorts[0].getData(),30)
        print "tests operator2 entry div ,input 20,10,output is:"
        #setting opeartion div in opeartor2
        node1.inputPorts[2].widget.set('div')
        ed.master.update_idletasks()
        pause()
        self.assertEqual(node3.inputPorts[0].getData(),0)
        print "tests operator2 entry floordiv ,input 20,10,output is:"
        #setting opeartion floordiv in opeartor2
        node1.inputPorts[2].widget.set('floordiv')
        ed.master.update_idletasks()
        pause()
        self.assertEqual(node3.inputPorts[0].getData(),0)
        print "tests operator2 entry mod ,input 20,10,output is:"
        #setting opeartion mod in opeartor2
        node1.inputPorts[2].widget.set('mod')
        ed.master.update_idletasks()
        pause()
        self.assertEqual(node3.inputPorts[0].getData(),10)
        print "tests operator2 entry mul ,input 20,10,output is:"
        #setting opeartion mul in opeartor2
        node1.inputPorts[2].widget.set('mul')
        ed.master.update_idletasks()
        pause()
        self.assertEqual(node3.inputPorts[0].getData(),200)
        #setting value in entry button 1
        node0.inputPorts[0].widget.set(2)
        #setting value in entry button 2
        node2.inputPorts[0].widget.set(1)
        #setting opeartion pow in opeartor2
        print "tests operator2 entry pow ,input 20,10,output is:"
        node1.inputPorts[2].widget.set('pow')
        ed.runCurrentNet_cb()
        ed.master.update_idletasks()
        pause()
        self.assertEqual(node3.inputPorts[0].getData(),1)
        #setting opeartion sub in opeartor2
        print "tests operator2 entry sub ,input 20,10,output is:"
        node1.inputPorts[2].widget.set('sub')
        ed.master.update_idletasks()
        pause()
        self.assertEqual(node3.inputPorts[0].getData(),-1)
        #setting opeartion rshift in opeartor2
        print "tests operator2 entry rshift ,input 20,10,output is:"
        node1.inputPorts[2].widget.set('rshift')
        ed.master.update_idletasks()
        pause()
        self.assertEqual(node3.inputPorts[0].getData(),0)
        #setting opeartion lshift in opeartor2
        print "tests operator2 entry lshift ,input 20,10,output is:"
        node1.inputPorts[2].widget.set('lshift')
        ed.master.update_idletasks()
        pause()
        self.assertEqual(node3.inputPorts[0].getData(),4)
        
    def test_filter_pass(self):
        """tests pass,input through entry button
        """
        from Vision.StandardNodes import stdlib
        ed.addLibraryInstance(stdlib, 'Vision.StandardNodes', 'stdlib')
        ed.master.update_idletasks()
        pause()
        net = ed.currentNetwork
        #Pass node
        node1 = Vision.StandardNodes.Pass(library =Vision.StandardNodes.stdlib)
        net.addNode(node1,150,150)
        #entry node
        node2 = Vision.StandardNodes.EntryNE(library =Vision.StandardNodes.stdlib)
        net.addNode(node2,200,10)
        #print node
        node3 = Vision.StandardNodes.Print(library =Vision.StandardNodes.stdlib)
        net.addNode(node3,400,100)
        #connecting nodes
        net.connectNodes(node1,node3)
        net.connectNodes(node2,node1)
        print "tests pass button,input 2,output is:"
        node2.inputPorts[0].widget.set(2)
        ed.runCurrentNet_cb()
        self.assertEqual(node3.inputPorts[0].getData(),'2')

    def test_filter_opeartor_1_apply_to_elements(self):
        """tests operator1 entry abs,not_,truth,inv,neg,pos,input through generic is [1, 2, 3, 4, 5]
        """
        from Vision.StandardNodes import stdlib
        ed.addLibraryInstance(stdlib, 'Vision.StandardNodes', 'stdlib')
        ed.master.update_idletasks()
        pause()
        net = ed.currentNetwork
        net.runOnNewData.value = True
        #node1 is opeartor2
        node1 = Vision.StandardNodes.Operator1(library =Vision.StandardNodes.stdlib)
        #adding node1
        net.addNode(node1,150,150)
        #node2 is entry button
        node2 = Vision.StandardNodes.Generic(library =Vision.StandardNodes.stdlib)
        #adding node2
        net.addNode(node2,200,10)
        #node3 is print 
        node3 = Vision.StandardNodes.Print(library =Vision.StandardNodes.stdlib)
        #adding node3 
        net.addNode(node3,400,100)
        node2.addOutputPort()
        node2.setFunction("def doit(self):\n\tself.outputData(out0=[1,2,3,4,5])\n")
        node1.inputPorts[2].widget.set(1)
        net.connectNodes(node1,node3)
        net.connectNodes(node2,node1)
        #setting 'abs' in operator1
        print "tests operator1 entry abs,input [1,2,3,4,5],apply to elements button On,ouput:"
        node1.inputPorts[1].widget.set('abs')
        ed.runCurrentNet_cb()
        self.assertEqual(str(node3.inputPorts[0].getData()),'[1, 2, 3, 4, 5]')
        #setting 'not_' in operator1
        print "tests operator1 entry not_,input [1,2,3,4,5],apply to elements button On,ouput:"
        node1.inputPorts[1].widget.set('not_')
        self.assertEqual(str(node3.inputPorts[0].getData()),'[False, False, False, False, False]')
        #setting 'truth' in operator1
        print "tests operator1 entry truth,input [1,2,3,4,5],apply to elements button On,ouput:"
        node1.inputPorts[1].widget.set('truth')
        self.assertEqual(str(node3.inputPorts[0].getData()),'[True, True, True, True, True]')
        #setting 'inv' in operator1
        print "tests operator1 entry inv,input [1,2,3,4,5],apply to elements button On,ouput:"
        node1.inputPorts[1].widget.set('inv')
        self.assertEqual(str(node3.inputPorts[0].getData()),'[-2, -3, -4, -5, -6]')
        #setting 'neg' in operator1
        print "tests operator1 entry neg,input [1,2,3,4,5],apply to elements button On,ouput:"
        node1.inputPorts[1].widget.set('neg')
        self.assertEqual(str(node3.inputPorts[0].getData()),'[-1, -2, -3, -4, -5]')
        #setting 'pos' in operator1
        print "tests operator1 entry pos,input [1,2,3,4,5],apply to elements button On,ouput:"
        node1.inputPorts[1].widget.set('pos')
        self.assertEqual(str(node3.inputPorts[0].getData()),'[1, 2, 3, 4, 5]')
        
    def test_filter_opeartor_1(self):
        """operator1 entry abs,not_,truth,inv,neg,pos ,input through entry
        button
        """
        from Vision.StandardNodes import stdlib
        ed.addLibraryInstance(stdlib, 'Vision.StandardNodes', 'stdlib')
        ed.master.update_idletasks()
        pause()
        net = ed.currentNetwork
        net.runOnNewData.value = True
        #node1 is opeartor2
        node1 = Vision.StandardNodes.Operator1(library =Vision.StandardNodes.stdlib)
        #adding node1
        net.addNode(node1,150,150)
        #node2 is entry button
        node2 = Vision.StandardNodes.EntryNE(library =Vision.StandardNodes.stdlib)
        #adding node2
        net.addNode(node2,200,10)
        #node3 is print 
        node3 = Vision.StandardNodes.Print(library =Vision.StandardNodes.stdlib)
        #adding node3 
        net.addNode(node3,400,100)
        #connecting nodes
        net.connectNodes(node1,node3)
        net.connectNodes(node2,node1)
        #setting output ports of entry button1 data type to int
        node2.outputPorts[0].edit()
        node2.outputPorts[0].objEditor.dataType.set('int')
        node2.outputPorts[0].objEditor.OK()
        #setting input port1 of opeartor 2 data type to int
        node1.inputPorts[0].edit()
        node1.inputPorts[0].objEditor.dataType.set('int')
        node1.inputPorts[0].objEditor.OK()
        #setting value in entry 
        node2.inputPorts[0].widget.set(70)
        #setting 'abs' in operator1
        print "tests operator1 entry abs,input 70,output is:"
        node1.inputPorts[1].widget.set('abs')
        self.assertEqual(node3.inputPorts[0].getData(),70)
        #setting 'not_' in operator1
        print "tests operator1 entry not_,input 70,output is:"
        node1.inputPorts[1].widget.set('not_')
        self.assertEqual(node3.inputPorts[0].getData(),False)
        #setting 'truth' in operator1
        print "tests operator1 entry truth,input 70,output is:"
        node1.inputPorts[1].widget.set('truth')
        self.assertEqual(node3.inputPorts[0].getData(),True)
        #setting 'inv' in operator1
        print "tests operator1 entry inv,input 70,output is:"
        node1.inputPorts[1].widget.set('inv')
        self.assertEqual(node3.inputPorts[0].getData(),-71)
        #setting 'neg' in operator1
        print "tests operator1 entry neg,input 70,output is:"
        node1.inputPorts[1].widget.set('neg')
        self.assertEqual(node3.inputPorts[0].getData(),-70)
        #setting 'pos' in operator1
        print "tests operator1 entry pos,input 70,output is:"
        node1.inputPorts[1].widget.set('pos')
        self.assertEqual(node3.inputPorts[0].getData(),70)
   
    
    
    def test_filter_slice_1_input_through_generic(self):
        """tests slice when input is ['1','2','3','4','5']
        """
        from Vision.StandardNodes import stdlib
        ed.addLibraryInstance(stdlib, 'Vision.StandardNodes', 'stdlib')
        ed.master.update_idletasks()
        pause()
        net = ed.currentNetwork
        #node1 is Slice
        node1 = Vision.StandardNodes.Slice(library =Vision.StandardNodes.stdlib)
        #adding node1
        net.addNode(node1,150,150)
        #node2 is generic
        node2 = Vision.StandardNodes.Generic(library =Vision.StandardNodes.stdlib)
        #adding node2
        net.addNode(node2,200,10)
        #node3 is print 
        node3 = Vision.StandardNodes.Print(library =Vision.StandardNodes.stdlib)
        #adding node3 
        net.addNode(node3,400,100)
        node2.addOutputPort()
        node2.setFunction("def doit(self):\n\tself.outputData(out0=['1','2','3','4','5'])\n")
        #connecting nodes
        net.connectNodes(node1,node3)
        net.connectNodes(node2,node1)
        node1.toggleNodeExpand_cb()
        #setting slice 
        print "tests slice from 1,to 0,step 1,input ['1','2','3','4','5'],output :"
        node1.inputPorts[1].widget.set(1)
        ed.runCurrentNet_cb()
        ed.master.update_idletasks()
        pause()
        #self.assertEqual(node3.inputPorts[0].getData(), 'Stop')
        self.assertEqual(node3.inputPorts[0].getData(),[])
        print "tests slice from 1,to 3,step 1,input ['1','2','3','4','5'],output :"
        node1.inputPorts[2].widget.set(3)
        node1.run()
        ed.master.update_idletasks()
        pause()
        self.assertEqual(node3.inputPorts[0].getData(),['2', '3'])
        print "tests slice from 1,to 3,step 2,input ['1','2','3','4','5'],output :"
        node1.inputPorts[3].widget.set(2)
        node1.run()
        ed.master.update_idletasks()
        pause()
        self.assertEqual(node3.inputPorts[0].getData(),['2'])
        
    def test_filter_slice_1_input_through_entry(self):
        """tests slicing list  Range->slice->print
        """
        from Vision.StandardNodes import stdlib
        ed.addLibraryInstance(stdlib, 'Vision.StandardNodes', 'stdlib')
        ed.master.update_idletasks()
        pause()
        net = ed.currentNetwork
        net.runOnNewData.value = True

        #node1 is a Range Node
        node1 = Vision.StandardNodes.Range(library =Vision.StandardNodes.stdlib)

        #node2 is slice
        node2 = Vision.StandardNodes.Slice(library =Vision.StandardNodes.stdlib)

        #node3 is print 
        node3 = Vision.StandardNodes.Print(library =Vision.StandardNodes.stdlib)

        #adding nodes
        net.addNode(node1,100,20)
        net.addNode(node2,100,190)
        net.addNode(node3,100,230)

        #connecting nodes
        net.connectNodes(node1,node2)
        net.connectNodes(node2,node3)
        node1.inputPorts[1].widget.set(10)

        print "testing that range 1 to 0 with step 1 fives empty list"
        node2.inputPorts[1].widget.set(1)
        ed.master.update_idletasks()
        pause()
        #self.assertEqual(node3.inputPorts[0].getData(),'Stop')
        self.assertEqual(node3.inputPorts[0].getData(),[])
        print "tests slice to 3,input through entry button [1,2,3,4,5,6,7,8,9],output is:"
        node2.inputPorts[2].widget.set(3)
        node2.run()
        ed.master.update_idletasks()
        pause()
        self.assertEqual(node3.inputPorts[0].getData(),[1,2])
        print "tests slice from 1,to 3,step 2,input through entry button [1,2,3,4,5,6,7,8,9],output is:"
        node2.inputPorts[3].widget.set(2)
        node2.run()
        ed.master.update_idletasks()
        pause()
        self.assertEqual(node3.inputPorts[0].getData(),[1])
        
        
    def test_filter_slicedata(self):
        
        from Vision.StandardNodes import stdlib
        ed.addLibraryInstance(stdlib, 'Vision.StandardNodes', 'stdlib')
        ed.master.update_idletasks()
        pause()
        net = ed.currentNetwork
        net.runOnNewData.value = True
        #node1 is slicedata
        node1 = Vision.StandardNodes.SliceData(library =Vision.StandardNodes.stdlib)
        #adding node1
        net.addNode(node1,150,150)
        #node2 is generic
        node2 = Vision.StandardNodes.Generic(library =Vision.StandardNodes.stdlib)
        #adding node2
        net.addNode(node2,200,10)
        #node3 is print 
        node3 = Vision.StandardNodes.Print(library =Vision.StandardNodes.stdlib)
        #adding node3 
        net.addNode(node3,400,100)
        #connecting nodes
        node2.addOutputPort()
        net.connectNodes(node1,node3)
        net.connectNodes(node2,node1)
        node1.toggleNodeExpand_cb()    
        node2.setFunction("def doit(self):\n\tself.outputData(out0=[1,2,3,4,5,6,7,8,9])\n")
        ed.runCurrentNet_cb()
        print "tests slicedata entry [1:4],input is [1,2,3,4,5,6,7,8,9],output:"
        node1.inputPorts[1].widget.set('[1:4]')
        self.assertEqual(node3.inputPorts[0].getData(),[2,3,4])
        
         

############################################################
#        Numeric Nodes Tests                               #
############################################################
    
    def test_filter_AsType(self):
        
        from Vision.StandardNodes import stdlib
        ed.addLibraryInstance(stdlib, 'Vision.StandardNodes', 'stdlib')
        ed.master.update_idletasks()
        pause()
        net = ed.currentNetwork
        net.runOnNewData.value = True
        #node1 is astype
        node1 = Vision.StandardNodes.AsType(library =Vision.StandardNodes.stdlib)
        #adding node1
        net.addNode(node1,150,150)
        #node2 is entry button
        node2 = Vision.StandardNodes.EntryNE(library =Vision.StandardNodes.stdlib)
        #adding node2
        net.addNode(node2,200,10)
        #node3 is print 
        node3 = Vision.StandardNodes.Print(library =Vision.StandardNodes.stdlib)
        #adding node3 
        net.addNode(node3,400,100)
        #connecting nodes
        net.connectNodes(node1,node3)
        net.connectNodes(node2,node1)
        node1.toggleNodeExpand_cb()    
        #setting input ports datatype int
        node2.outputPorts[0].edit()
        node2.outputPorts[0].objEditor.dataType.set('list')
        node2.outputPorts[0].objEditor.OK()
        #node1.inputPorts[1].widget.set('Character')
        #ed.master.update_idletasks()
        #pause()
        #self.assertEqual(node3.inputPorts[0].getData(),[[2,3,]])
        print "tests AsType entry complex,input is [1,2],output:"
        node1.inputPorts[1].widget.set('complex')
        node2.inputPorts[0].widget.set([1,2])
        ed.master.update_idletasks()
        pause()
        #self.assertEqual(str(node3.inputPorts[0].getData()),'[ 91.+0.j  49.+0.j  44.+0.j  32.+0.j  50.+0.j  93.+0.j]')
        lTestList = [ (1.+0.j), (2.+0.j) ]
        for i in range(len(lTestList)):
            self.assertEqual(node3.inputPorts[0].getData()[i], lTestList[i])
        print "tests AsType entry complex64,input is [1,2],output:"
        node1.inputPorts[1].widget.set('complex64')
        ed.master.update_idletasks()
        pause()
        #self.assertEqual(str(node3.inputPorts[0].getData()),'[ 91.+0.j  49.+0.j  44.+0.j  32.+0.j  50.+0.j  93.+0.j]')
        for i in range(len(lTestList)):
            self.assertEqual(node3.inputPorts[0].getData()[i], lTestList[i])
        print "tests AsType entry complex16,input is [1,2],output:"
        node1.inputPorts[1].widget.set('Complex16')
        ed.master.update_idletasks()
        pause()
        #self.assertEqual(str(node3.inputPorts[0].getData()),'[ 91.+0.j  49.+0.j  44.+0.j  32.+0.j  50.+0.j  93.+0.j]')

        for i in range(len(lTestList)):
            self.assertEqual(node3.inputPorts[0].getData()[i], lTestList[i])
        print "tests AsType entry complex128,input is [1,2],output:"
        node1.inputPorts[1].widget.set('complex128')
        ed.master.update_idletasks()
        pause()
        #self.assertEqual(str(node3.inputPorts[0].getData()),'[ 91.+0.j  49.+0.j  44.+0.j  32.+0.j  50.+0.j  93.+0.j]')
        ## for i in range(len(lTestList)):
        ##     self.assertEqual(node3.inputPorts[0].getData()[i], lTestList[i])
        ## print "tests AsType entry complex8,input is [1,2],output:"
        ## node1.inputPorts[1].widget.set('Complex8')
        ## ed.master.update_idletasks()
        ## pause()
        ## #self.assertEqual(str(node3.inputPorts[0].getData()),'[ 91.+0.j  49.+0.j  44.+0.j  32.+0.j  50.+0.j  93.+0.j]')
        for i in range(len(lTestList)):
            self.assertEqual(node3.inputPorts[0].getData()[i], lTestList[i])
        print "tests AsType entry float,input is [1,2],output:"
        node1.inputPorts[1].widget.set('float')
        ed.master.update_idletasks()
        pause()
        #self.assertEqual(str(node3.inputPorts[0].getData()),'[ 91.  49.  44.  32.  50.  93.]')
        #lTestList = [ 91.,  49.,  44.,  32.,  50.,  93.]
        lTestList = [ 1., 2. ]
        for i in range(len(lTestList)):
            self.assertEqual(node3.inputPorts[0].getData()[i], lTestList[i])

        print "tests AsType entry float16,input is [1,2],output:"
        node1.inputPorts[1].widget.set('float16')
        ed.master.update_idletasks()
        pause()
        #self.assertEqual(str(node3.inputPorts[0].getData()),'[ 91.  49.  44.  32.  50.  93.]')
        for i in range(len(lTestList)):
            self.assertEqual(node3.inputPorts[0].getData()[i], lTestList[i])
        print "tests AsType entry float32,input is [1,2],output:"
        node1.inputPorts[1].widget.set('float32')
        ed.master.update_idletasks()
        pause()
        #self.assertEqual(str(node3.inputPorts[0].getData()),'[ 91.  49.  44.  32.  50.  93.]')
        for i in range(len(lTestList)):
            self.assertEqual(node3.inputPorts[0].getData()[i], lTestList[i])
        print "tests AsType entry float64,input is [1,2],output:"
        node1.inputPorts[1].widget.set('float64')
        ed.master.update_idletasks()
        pause()
        #self.assertEqual(str(node3.inputPorts[0].getData()),'[ 91.  49.  44.  32.  50.  93.]')
        print "tests AsType entry int,input is [1,2],output:"
        node1.inputPorts[1].widget.set('Int')
        ed.master.update_idletasks()
        pause()
        #self.assertEqual(str(node3.inputPorts[0].getData()),'[91 49 44 32 50 93]')
        #lTestList = [ 91, 49, 44, 32, 50, 93]
        lTestList = [ 1, 2]
        for i in range(len(lTestList)):
            self.assertEqual(node3.inputPorts[0].getData()[i], lTestList[i])
        print "tests AsType entry int0,input is [1,2],output:"
        node1.inputPorts[1].widget.set('int0')
        ed.master.update_idletasks()
        pause()
        #self.assertEqual(str(node3.inputPorts[0].getData()),'[91 49 44 32 50 93]')
        for i in range(len(lTestList)):
            self.assertEqual(node3.inputPorts[0].getData()[i], lTestList[i])
        print "tests AsType entry int16,input is [1,2],output:"
        node1.inputPorts[1].widget.set('int16')
        ed.master.update_idletasks()
        pause()
        #self.assertEqual(str(node3.inputPorts[0].getData()),'[91 49 44 32 50 93]')
        for i in range(len(lTestList)):
            self.assertEqual(node3.inputPorts[0].getData()[i], lTestList[i])
        print "tests AsType entry int32,input is [1,2],output:"
        node1.inputPorts[1].widget.set('int32')
        ed.master.update_idletasks()
        pause()
        #self.assertEqual(str(node3.inputPorts[0].getData()),'[91 49 44 32 50 93]')
        for i in range(len(lTestList)):
            self.assertEqual(node3.inputPorts[0].getData()[i], lTestList[i])
        print "tests AsType entry int8,input is [1,2],output:"
        node1.inputPorts[1].widget.set('int8')
        ed.master.update_idletasks()
        pause()
        #self.assertEqual(str(node3.inputPorts[0].getData()),'[91 49 44 32 50 93]')
        for i in range(len(lTestList)):
            self.assertEqual(node3.inputPorts[0].getData()[i], lTestList[i])
        print "tests AsType entry uint,input is [1,2],output:"
        node1.inputPorts[1].widget.set('uint')
        ed.master.update_idletasks()
        pause()
        #self.assertEqual(str(node3.inputPorts[0].getData()),'[91 49 44 32 50 93]')
        
        
        for i in range(len(lTestList)):
            self.assertEqual(node3.inputPorts[0].getData()[i], lTestList[i])
        print "tests AsType entry uint16,input is [1,2],output:"
        node1.inputPorts[1].widget.set('uint16')
        ed.master.update_idletasks()
        pause()
        #self.assertEqual(str(node3.inputPorts[0].getData()),'[91 49 44 32 50 93]')
        for i in range(len(lTestList)):
            self.assertEqual(node3.inputPorts[0].getData()[i], lTestList[i])
        print "tests AsType entry uint32,input is [1,2],output:"
        node1.inputPorts[1].widget.set('uint32')
        ed.master.update_idletasks()
        pause()
        #self.assertEqual(str(node3.inputPorts[0].getData()),'[91 49 44 32 50 93]')
        for i in range(len(lTestList)):
            self.assertEqual(node3.inputPorts[0].getData()[i], lTestList[i])
        print "tests AsType entry uint8,input is [1,2],output:"
        node1.inputPorts[1].widget.set('uint8')
        ed.master.update_idletasks()
        pause()
        #self.assertEqual(str(node3.inputPorts[0].getData()),'[91 49 44 32 50 93]')

        print "tests AsType entry UnsignedInteger,input is [1,2],output:"
        node1.inputPorts[1].widget.set('unsignedinteger')
        ed.master.update_idletasks()
        pause()
        #self.assertEqual(str(node3.inputPorts[0].getData()),'[91 49 44 32 50 93]')
        for i in range(len(lTestList)):
            self.assertEqual(node3.inputPorts[0].getData()[i], lTestList[i])
        
    def test_Numeric_ArrayFunc1(self):
        """tests arrayfunc1 arccosh,arcsinh,arctan,cos,cosh,exp,log,log10,sin,sinh,sqrt,tan,tanh,input is 
        [10,20,30]
        """
        from Vision.StandardNodes import stdlib
        ed.addLibraryInstance(stdlib, 'Vision.StandardNodes', 'stdlib')
        ed.master.update_idletasks()
        pause()
        net = ed.currentNetwork
        #node1 is Unary Func
        node1 = Vision.StandardNodes.UnaryFuncs(library =Vision.StandardNodes.stdlib)
        #adding node1
        net.addNode(node1,150,150)
        #node3 is print 
        node3 = Vision.StandardNodes.Print(library =Vision.StandardNodes.stdlib)
        #adding node3 
        net.addNode(node3,400,100)
        #node4 is generic
        node4 = Vision.StandardNodes.Generic(library =Vision.StandardNodes.stdlib)
        #add node4
        net.addNode(node4,200,100)
        node4.addOutputPort()
        #connecting nodes
        net.connectNodes(node4,node1)
        net.connectNodes(node1,node3)
        node4.setFunction("def doit(self):\n\tself.outputData(out0=[10,20,30])\n")
        #node1.inputPorts[1].widget.set('Character')
        #ed.master.update_idletasks()
        #pause()
        #self.assertEqual(node3.inputPorts[0].getData(),[[2,3,]])
        node1.toggleNodeExpand_cb()
        print "tests arrayfunc1 entry cos,input [10,20,30],output :"
        node1.inputPorts[1].widget.set('cos')
        node1.run()
        ed.runCurrentNet_cb()
        #self.assertEqual(str(node3.inputPorts[0].getData()),'[-0.83907153  0.40808206  0.15425145]')
        self.assertEqual(node3.inputPorts[0].getData()[0], numpy.cos(10))
        self.assertEqual(node3.inputPorts[0].getData()[1], numpy.cos(20))
        self.assertEqual(node3.inputPorts[0].getData()[2], numpy.cos(30))

        print "tests arrayfunc1 entry cosh,input [10,20,30],output :"
        node1.inputPorts[1].widget.set('cosh')
        node1.run()
        ed.master.update_idletasks()
        pause()
        #self.assertEqual(str(node3.inputPorts[0].getData()),'[ 1.10132329e+04  2.42582598e+08 5.34323729e+012]')
	self.assertEqual(node3.inputPorts[0].getData()[0], numpy.cosh(10))
	self.assertEqual(node3.inputPorts[0].getData()[1], numpy.cosh(20))
	self.assertEqual(node3.inputPorts[0].getData()[2], numpy.cosh(30))
        print "tests arrayfunc1 entry exp,input [10,20,30],output :"
        node1.inputPorts[1].widget.set('exp')
        node1.run()
        ed.master.update_idletasks()
        pause()
        #self.assertEqual(str(node3.inputPorts[0].getData()),'[  2.20264658e+04   4.85165195e+08   1.06864746e+13]')
	self.assertEqual(node3.inputPorts[0].getData()[0], numpy.exp(10))
	self.assertEqual(node3.inputPorts[0].getData()[1], numpy.exp(20))
	self.assertEqual(node3.inputPorts[0].getData()[2], numpy.exp(30))
        print "tests arrayfunc1 entry log,input [10,20,30],output :"
        node1.inputPorts[1].widget.set('log')
        node1.run()
        ed.master.update_idletasks()
        pause()
        #self.assertEqual(str(node3.inputPorts[0].getData()),'[ 2.30258509  2.99573227  3.40119738]')
        self.assertEqual(node3.inputPorts[0].getData()[0], numpy.log(10))
        self.assertEqual(node3.inputPorts[0].getData()[1], numpy.log(20))
        self.assertEqual(node3.inputPorts[0].getData()[2], numpy.log(30))
        print "tests arrayfunc1 entry log10,input [10,20,30],output :"
        node1.inputPorts[1].widget.set('log10')
        node1.run()
        ed.master.update_idletasks()
        pause()
        #self.assertEqual(str(node3.inputPorts[0].getData()),'[ 1.          1.30103     1.47712125]')
        self.assertEqual(node3.inputPorts[0].getData()[0], numpy.log10(10))
        self.assertEqual(node3.inputPorts[0].getData()[1], numpy.log10(20))
        self.assertEqual(node3.inputPorts[0].getData()[2], numpy.log10(30))
        print "tests arrayfunc1 entry sin,input [10,20,30],output :"
        node1.inputPorts[1].widget.set('sin')
        node1.run()
        ed.master.update_idletasks()
        pause()
        #self.assertEqual(str(node3.inputPorts[0].getData()),'[-0.54402111  0.91294525 -0.98803162]')
        self.assertEqual(node3.inputPorts[0].getData()[0], numpy.sin(10))
        self.assertEqual(node3.inputPorts[0].getData()[1], numpy.sin(20))
        self.assertEqual(node3.inputPorts[0].getData()[2], numpy.sin(30))
        print "tests arrayfunc1 entry sinh,input [10,20,30],output :"
        node1.inputPorts[1].widget.set('sinh')
        node1.run()
        ed.master.update_idletasks()
        pause()
        #self.assertEqual(str(node3.inputPorts[0].getData()),'[  1.10132329e+04   2.42582598e+08   5.34323729e+12]')
	self.assertEqual(node3.inputPorts[0].getData()[0], numpy.sinh(10))
	self.assertEqual(node3.inputPorts[0].getData()[1], numpy.sinh(20))
	self.assertEqual(node3.inputPorts[0].getData()[2], numpy.sinh(30))
        print "tests arrayfunc1 entry sqrt,input [10,20,30],output :"
        node1.inputPorts[1].widget.set('sqrt')
        node1.run()
        ed.master.update_idletasks()
        pause()
        #self.assertEqual(str(node3.inputPorts[0].getData()),'[ 3.16227766  4.47213595  5.47722558]')
        self.assertEqual(node3.inputPorts[0].getData()[0], numpy.sqrt(10))
        self.assertEqual(node3.inputPorts[0].getData()[1], numpy.sqrt(20))
        self.assertEqual(node3.inputPorts[0].getData()[2], numpy.sqrt(30))
        print "tests arrayfunc1 entry tan,input [10,20,30],output :"
        node1.inputPorts[1].widget.set('tan')
        node1.run()
        ed.master.update_idletasks()
        pause()
        #self.assertEqual(str(node3.inputPorts[0].getData()),'[ 0.64836083  2.23716094 -6.4053312 ]')
        self.assertEqual(node3.inputPorts[0].getData()[0], numpy.tan(10))
        self.assertEqual(node3.inputPorts[0].getData()[1], numpy.tan(20))
        self.assertEqual(node3.inputPorts[0].getData()[2], numpy.tan(30))
        print "tests arrayfunc1 entry tanh,input [10,20,30],output :"
        node1.inputPorts[1].widget.set('tanh')
        node1.run()
        ed.master.update_idletasks()
        pause()
        #self.assertEqual(str(node3.inputPorts[0].getData()),'[ 1.  1.  1.]')
        self.assertEqual(node3.inputPorts[0].getData()[0], numpy.tanh(10))
        self.assertEqual(node3.inputPorts[0].getData()[1], numpy.tanh(20))
        self.assertEqual(node3.inputPorts[0].getData()[2], numpy.tanh(30))
        print "tests arrayfunc1 entry arccosh,input [10,20,30],output :"
        node1.inputPorts[1].widget.set('arccosh')
        node1.run()
        ed.master.update_idletasks()
        pause()
        #self.assertEqual(str(node3.inputPorts[0].getData()),'[ 2.99322285  3.68825387  4.09406667]')
        self.assertEqual(node3.inputPorts[0].getData()[0], numpy.arccosh(10))
        self.assertEqual(node3.inputPorts[0].getData()[1], numpy.arccosh(20))
        self.assertEqual(node3.inputPorts[0].getData()[2], numpy.arccosh(30))
        print "tests arrayfunc1 entry arcsinh,input [10,20,30],output :"
        node1.inputPorts[1].widget.set('arcsinh')
        node1.run()
        ed.master.update_idletasks()
        pause()
        #self.assertEqual(str(node3.inputPorts[0].getData()),'[ 2.99822295  3.68950387  4.09462222]')
        self.assertEqual(node3.inputPorts[0].getData()[0], numpy.arcsinh(10))
        self.assertEqual(node3.inputPorts[0].getData()[1], numpy.arcsinh(20))
        self.assertEqual(node3.inputPorts[0].getData()[2], numpy.arcsinh(30))
        print "tests arrayfunc1 entry arctan,input [10,20,30],output :"
        node1.inputPorts[1].widget.set('arctan')
        node1.run()
        ed.master.update_idletasks()
        pause()
        #self.assertEqual(str(node3.inputPorts[0].getData()),'[ 1.47112767  1.52083793  1.53747533]')
        self.assertEqual(node3.inputPorts[0].getData()[0], numpy.arctan(10))
        self.assertEqual(node3.inputPorts[0].getData()[1], numpy.arctan(20))
        self.assertEqual(node3.inputPorts[0].getData()[2], numpy.arctan(30))
        
        
    def test_Numeric_ArrayFunc2(self):
        """tests Arrayfunc2 add,substract,multiply,divide,power,remainder inputs are [3,4,5],[10,20,30]
        """
        from Vision.StandardNodes import stdlib
        ed.addLibraryInstance(stdlib, 'Vision.StandardNodes', 'stdlib')
        ed.master.update_idletasks()
        pause()
        net = ed.currentNetwork
        net.runOnNewData.value = True
        #node1 is Binary Func
        node1 = Vision.StandardNodes.BinaryFuncs(library =Vision.StandardNodes.stdlib)
        #adding node1
        net.addNode(node1,150,150)
        #node2 is generic
        node2 = Vision.StandardNodes.Generic(library =Vision.StandardNodes.stdlib)
        #add node2
        net.addNode(node2,20,20)
        #node3 is print 
        node3 = Vision.StandardNodes.Print(library =Vision.StandardNodes.stdlib)
        #adding node3 
        net.addNode(node3,400,100)
        #node4 is generic
        node4 = Vision.StandardNodes.Generic(library =Vision.StandardNodes.stdlib)
        #add node4
        net.addNode(node4,200,100)
        node4.addOutputPort()
        node2.addOutputPort()
        #connecting nodes
        net.connectNodes(node4,node1)
        net.connectNodes(node1,node3)
        port1 = node2.outputPorts[0]
        port2 = node1.inputPorts[1]
        apply( net.connectNodes,(port1.node, port2.node, port1.number, port2.number),{})
        node2.setFunction("def doit(self):\n\tself.outputData(out0=[3,4,5])\n")
        node4.setFunction("def doit(self):\n\tself.outputData(out0=[10,20,30])\n")
        node1.toggleNodeExpand_cb()
        print "tests arrayfunc2 add,input  [3,4,5],[10,20,30],output:"
        node1.inputPorts[2].widget.set('add')
        ed.runCurrentNet_cb()
        #self.assertEqual(str(node3.inputPorts[0].getData()),'[13 24 35]')
        self.assertEqual(node3.inputPorts[0].getData()[0], 13)
        self.assertEqual(node3.inputPorts[0].getData()[1], 24)
        self.assertEqual(node3.inputPorts[0].getData()[2], 35)
        print "tests arrayfunc2 subtract,input  [3,4,5],[10,20,30],output:"
        node1.inputPorts[2].widget.set('subtract')
        ed.master.update_idletasks()
        pause()
        #self.assertEqual(str(node3.inputPorts[0].getData()),'[ 7 16 25]')
        self.assertEqual(node3.inputPorts[0].getData()[0], 7)
        self.assertEqual(node3.inputPorts[0].getData()[1], 16)
        self.assertEqual(node3.inputPorts[0].getData()[2], 25)
        print "tests arrayfunc2 multiply,input  [3,4,5],[10,20,30],output:"
        node1.inputPorts[2].widget.set('multiply')
        ed.master.update_idletasks()
        pause()
        #self.assertEqual(str(node3.inputPorts[0].getData()),'[ 30  80 150]')
        self.assertEqual(node3.inputPorts[0].getData()[0], 30)
        self.assertEqual(node3.inputPorts[0].getData()[1], 80)
        self.assertEqual(node3.inputPorts[0].getData()[2], 150)
        print "tests arrayfunc2 divide,input  [3,4,5],[10,20,30],output:"
        node1.inputPorts[2].widget.set('divide')
        ed.master.update_idletasks()
        pause()
        #self.assertEqual(str(node3.inputPorts[0].getData()),'[3 5 6]')
        self.assertEqual(node3.inputPorts[0].getData()[0], 3)
        self.assertEqual(node3.inputPorts[0].getData()[1], 5)
        self.assertEqual(node3.inputPorts[0].getData()[2], 6)
        print "tests arrayfunc2 remainder,input  [3,4,5],[10,20,30],output:"
        node1.inputPorts[2].widget.set('remainder')
        ed.master.update_idletasks()
        pause()
        #self.assertEqual(str(node3.inputPorts[0].getData()),'[1 0 0]')
        self.assertEqual(node3.inputPorts[0].getData()[0], 1)
        self.assertEqual(node3.inputPorts[0].getData()[1], 0)
        self.assertEqual(node3.inputPorts[0].getData()[2], 0)
        print "tests arrayfunc2 power,input  [3,4,5],[10,20,30],output:"
        node1.inputPorts[2].widget.set('power')
        ed.master.update_idletasks()
        pause()
        #self.assertEqual(str(node3.inputPorts[0].getData()),'[    1000   160000 24300000]')
        self.assertEqual(node3.inputPorts[0].getData()[0], 1000)
        self.assertEqual(node3.inputPorts[0].getData()[1], 160000)
        self.assertEqual(node3.inputPorts[0].getData()[2], 24300000)
        

    def test_Input_button(self):
        """tests button 
        """
        from Vision.StandardNodes import stdlib
        ed.addLibraryInstance(stdlib, 'Vision.StandardNodes', 'stdlib')
        ed.master.update_idletasks()
        pause()
        net = ed.currentNetwork
        node1 = Vision.StandardNodes.ButtonNE(library =Vision.StandardNodes.stdlib)
        #adding node1
        net.addNode(node1,150,150)
        #node3 is print 
        node3 = Vision.StandardNodes.Print(library =Vision.StandardNodes.stdlib)
        #adding node3 
        net.addNode(node3,400,100)
        net.connectNodes(node1,node3)
        print "tests button,toggle on,output:"
        node1.inputPorts[0].widget.set(1)
        node1.run()
        self.assertEqual(node3.inputPorts[0].getData(),1)


    def test_Input_checkbutton(self):
        """tests checkbutton on or off
        """
        from Vision.StandardNodes import stdlib
        ed.addLibraryInstance(stdlib, 'Vision.StandardNodes', 'stdlib')
        ed.master.update_idletasks()
        pause()
        net = ed.currentNetwork
        node1 = Vision.StandardNodes.CheckButtonNE(library =Vision.StandardNodes.stdlib)
        #adding node1
        net.addNode(node1,150,150)
        #node3 is print 
        node3 = Vision.StandardNodes.Print(library =Vision.StandardNodes.stdlib)
        #adding node3 
        net.addNode(node3,400,100)
        net.connectNodes(node1,node3)
        print "tests check button toggle on,off,output:"
        node1.inputPorts[0].widget.set(1)
        node1.run()
        self.assertEqual(node3.inputPorts[0].getData(),1)
        node1.inputPorts[0].widget.set(0)
        node1.run()
        self.assertEqual(node3.inputPorts[0].getData(),0)

    def test_Input_Dial(self):
        """tests dial setting input and getting output
        """
        from Vision.StandardNodes import stdlib
        ed.addLibraryInstance(stdlib, 'Vision.StandardNodes', 'stdlib')
        ed.master.update_idletasks()
        pause()
        net = ed.currentNetwork
        node1 = Vision.StandardNodes.DialNE(library =Vision.StandardNodes.stdlib)
        #adding node1
        net.addNode(node1,150,150)
        #node3 is print 
        node3 = Vision.StandardNodes.Print(library =Vision.StandardNodes.stdlib)
        #adding node3 
        net.addNode(node3,400,100)
        net.connectNodes(node1,node3)
        print "tests dial setting 10,output:"
        node1.inputPorts[0].widget.set(10)
        node1.run()
        self.assertEqual(round(node3.inputPorts[0].getData()),10)    

    def test_Input_Entry(self):
        """tests entry node setting values
        """
        from Vision.StandardNodes import stdlib
        ed.addLibraryInstance(stdlib, 'Vision.StandardNodes', 'stdlib')
        ed.master.update_idletasks()
        pause()
        net = ed.currentNetwork
        node1 = Vision.StandardNodes.EntryNE(library =Vision.StandardNodes.stdlib)
        #adding node1
        net.addNode(node1,150,150)
        #node3 is print 
        node3 = Vision.StandardNodes.Print(library =Vision.StandardNodes.stdlib)
        #adding node3 
        net.addNode(node3,400,100)
        net.connectNodes(node1,node3)
        print "tests entry button setting a,output is :"
        node1.inputPorts[0].widget.set('a')
        net.run()
        self.assertEqual(node3.inputPorts[0].getData(),'a')    
        print "tests entry button setting '1',output is :"
        node1.inputPorts[0].widget.set(1)
        net.run()
        self.assertEqual(node3.inputPorts[0].getData(),'1')

    def test_Input_vector3DNE(self):
        """tests vector3D by setting a value
        """
        from Vision.StandardNodes import stdlib
        ed.addLibraryInstance(stdlib, 'Vision.StandardNodes', 'stdlib')
        ed.master.update_idletasks()
        pause()
        net = ed.currentNetwork
        node1 = Vision.StandardNodes.Vector3DNE(library =Vision.StandardNodes.stdlib)
        #adding node1
        net.addNode(node1,150,150)
        #node3 is print 
        node3 = Vision.StandardNodes.Print(library =Vision.StandardNodes.stdlib)
        #adding node3 
        net.addNode(node3,400,100)
        net.connectNodes(node1,node3)
        node1.toggleNodeExpand_cb() 
        print "tests vector3DNE entry [1,1,1],output:"
        node1.inputPorts[0].widget.set([1,1,1])
        node1.run()
        x = node3.inputPorts[0].getData()
        z =[]
        for i in range(0,len(x)): 
            z =z+[round(x[i])]
        self.assertEqual(z,[1.0,1.0,1.0])

    def test_Input_fileBrowser(self):
        """tests file browser 
        """
        from Vision.StandardNodes import stdlib
        ed.addLibraryInstance(stdlib, 'Vision.StandardNodes', 'stdlib')
        ed.master.update_idletasks()
        pause()
        net = ed.currentNetwork
        node1 = Vision.StandardNodes.FileBrowserNE(library =Vision.StandardNodes.stdlib)
        #adding node1
        net.addNode(node1,150,150)
        #node3 is print 
        node3 = Vision.StandardNodes.Print(library =Vision.StandardNodes.stdlib)
        #adding node3 
        net.addNode(node3,400,100)
        net.connectNodes(node1,node3)
        node1.toggleNodeExpand_cb() 
        print "tests file browser entry  'note1',output:"
        node1.inputPorts[0].widget.set('note1')
        node1.run()
        self.assertEqual(node3.inputPorts[0].getData(),'note1')


    def test_Input_filelist(self):
        
        from Vision.StandardNodes import stdlib
        ed.addLibraryInstance(stdlib, 'Vision.StandardNodes', 'stdlib')
        ed.master.update_idletasks()
        pause()
        net = ed.currentNetwork
        node1 = Vision.StandardNodes.Filelist(library =Vision.StandardNodes.stdlib)
        #adding node1
        net.addNode(node1,150,150)
        #node3 is print 
        node3 = Vision.StandardNodes.Print(library =Vision.StandardNodes.stdlib)
        #adding node3 
        net.addNode(node3,400,100)
        net.connectNodes(node1,node3)
        node1.toggleNodeExpand_cb()
        print "tests filelist entry *note1*,output:"
        node1.inputPorts[0].widget.set(os.getcwd())
        node1.inputPorts[1].widget.set('*note1*')
        node1.run()
        self.assertEqual(node3.inputPorts[0].getData(),[os.path.abspath('note1')])
        #self.assertEqual(node3.inputPorts[0].getData(),['note1'])


    def test_Input_filename(self):
        
        from Vision.StandardNodes import stdlib
        ed.addLibraryInstance(stdlib, 'Vision.StandardNodes', 'stdlib')
        ed.master.update_idletasks()
        pause()
        net = ed.currentNetwork
        node1 = Vision.StandardNodes.Filename(library =Vision.StandardNodes.stdlib)
        #adding node1
        net.addNode(node1,50,50)
        node2 = Vision.StandardNodes.EntryNE(library =Vision.StandardNodes.stdlib)
        #adding node2
        net.addNode(node2,150,150)
        #node3 is print 
        node3 = Vision.StandardNodes.Print(library =Vision.StandardNodes.stdlib)
        #adding node3 
        net.addNode(node3,400,100)
        port2 = node1.inputPorts[1]
        port1 = node2.outputPorts[0]
        apply( net.connectNodes,(port1.node, port2.node, port1.number, port2.number),{})

        #net.connectNodes(node2,node1)
        net.connectNodes(node1,node3)
        node1.toggleNodeExpand_cb()
        #setting outputPorts datatype as int
        node2.outputPorts[0].edit()
        node2.outputPorts[0].objEditor.dataType.set('int')
        node2.outputPorts[0].objEditor.OK()
        print "tests filename entry 9,output:"
        node2.inputPorts[0].widget.set(9)
        net.run()
        self.assertEqual(str(node3.inputPorts[0].getData()),'file00000009.png')



    def test_Input_MulticheckButton(self):
        
        from Vision.StandardNodes import stdlib
        ed.addLibraryInstance(stdlib, 'Vision.StandardNodes', 'stdlib')
        ed.master.update_idletasks()
        pause()
        net = ed.currentNetwork
        net.runOnNewData.value = True
        node1 = Vision.StandardNodes.MultiCheckbuttonsNE(library =Vision.StandardNodes.stdlib)
        #adding node1
        net.addNode(node1,150,150)
        node2 = Vision.StandardNodes.EntryNE(library =Vision.StandardNodes.stdlib)
        #adding node1
        net.addNode(node2,200,10)
        #node3 is print 
        node3 = Vision.StandardNodes.Print(library =Vision.StandardNodes.stdlib)
        #adding node3 
        net.addNode(node3,400,100)
        node2.inputPorts[0].widget.set('abcd')
        net.connectNodes(node2,node1)
        port1 = node1.outputPorts[2]
        port2 = node3.inputPorts[0]
        apply( net.connectNodes,(port1.node, port2.node, port1.number, port2.number),{})
        node1.toggleNodeExpand_cb()
        print "tests MulticheckButton entry a,b,c,d On,output:"
        x = node1.inputPorts[1].widget
        x.widget.reSelect('a','check')
        x.widget.reSelect('b','check')
        x.widget.reSelect('c','check')
        x.widget.reSelect('d','check')
        ed.runCurrentNet_cb()
        ed.master.update_idletasks()
        pause()
        self.assertEqual(node3.inputPorts[0].getData(),[('a', 1), ('b', 1), ('c', 1), ('d', 1)])
        node1.toggleNodeExpand_cb()

    def test_Input_Generic(self):
        
        from Vision.StandardNodes import stdlib
        ed.addLibraryInstance(stdlib, 'Vision.StandardNodes', 'stdlib')
        ed.master.update_idletasks()
        pause()
        net = ed.currentNetwork
        node1 = Vision.StandardNodes.Generic(library =Vision.StandardNodes.stdlib)
        #adding node1
        net.addNode(node1,200,10)
        #node2 is print 
        node2 = Vision.StandardNodes.Print(library =Vision.StandardNodes.stdlib)
        net.addNode(node2,100,100)
        node1.addOutputPort()
        node1.setFunction("def doit(self):\n\tself.outputData(out0=[10,20,30])\n")
        print "tests generic entry [10,20,30],ouput:"
        net.connectNodes(node1,node2)
        ed.runCurrentNet_cb()
        self.assertEqual(node2.inputPorts[0].getData(),[10,20,30])
        

    def test_Input_Read_Lines(self):
        from Vision.StandardNodes import stdlib
        ed.addLibraryInstance(stdlib, 'Vision.StandardNodes', 'stdlib')
        ed.master.update_idletasks()
        pause()
        net = ed.currentNetwork
        node1 = Vision.StandardNodes.ReadFile(library =Vision.StandardNodes.stdlib)
        #adding node1
        net.addNode(node1,200,10)
        #node2 is print 
        node2 = Vision.StandardNodes.Print(library =Vision.StandardNodes.stdlib)
        net.addNode(node2,100,100)
        net.connectNodes(node1,node2)
        print "tests Readlines entry myfile,output:"
        node1.inputPorts[0].widget.set(os.path.abspath('myfile'))
        net.run()
        self.assertEqual(node2.inputPorts[0].getData()[0],'Hi This file is created for tests')


    def test_input_thumbwheel(self):
        from Vision.StandardNodes import stdlib
        ed.addLibraryInstance(stdlib, 'Vision.StandardNodes', 'stdlib')
        ed.master.update_idletasks()
        pause()
        net = ed.currentNetwork
        node1 = Vision.StandardNodes.ThumbWheelNE(library =Vision.StandardNodes.stdlib)
        #adding node1
        net.addNode(node1,200,10)
        #node2 is print 
        node2 = Vision.StandardNodes.Print(library =Vision.StandardNodes.stdlib)
        net.addNode(node2,100,100)
        net.connectNodes(node1,node2)
        print "tests Thumbwheel entry 10,output:"
        node1.inputPorts[0].widget.set(10)
        node1.run()
        ed.master.update_idletasks()
        pause()
        self.assertEqual(round(node2.inputPorts[0].getData()),10)


    def test_input_ScrolledText(self):
        from Vision.StandardNodes import stdlib
        ed.addLibraryInstance(stdlib, 'Vision.StandardNodes', 'stdlib')
        ed.master.update_idletasks()
        pause()
        net = ed.currentNetwork
        net.runOnNewData.value = True
        node1 = Vision.StandardNodes.ScrolledTextNE(library =Vision.StandardNodes.stdlib)
        #adding node1
        net.addNode(node1,200,10)
        #node2 is print 
        node2 = Vision.StandardNodes.Print(library =Vision.StandardNodes.stdlib)
        net.addNode(node2,100,100)
        net.connectNodes(node1,node2)
        print "tests ScrolledText entry 'Hi how are you?',output:"
        node1.inputPorts[0].widget.set("Hi how are you?")
        ed.master.update_idletasks()
        pause()
        self.assertEqual(node2.inputPorts[0].getData(),"Hi how are you?\n")

    
    def test_input_comboBox(self):
        from Vision.StandardNodes import stdlib
        ed.addLibraryInstance(stdlib, 'Vision.StandardNodes', 'stdlib')
        ed.master.update_idletasks()
        pause()
        net = ed.currentNetwork
        net.runOnNewData.value = True
        #node1 is opeartor3
        node1 = Vision.StandardNodes.ComboBoxNE(library =Vision.StandardNodes.stdlib)
        #adding node1
        net.addNode(node1,150,150)
        #node2 is generic
        node2 = Vision.StandardNodes.Generic(library =Vision.StandardNodes.stdlib)
        #adding node2
        net.addNode(node2,200,10)
        #node3 is print 
        node3 = Vision.StandardNodes.Print(library =Vision.StandardNodes.stdlib)
        #adding node3 
        net.addNode(node3,400,100)
        #connecting nodes
        node2.addOutputPort()
        node2.setFunction("def doit(self):\n\tself.outputData(out0=[1,2,3,4,5,6,7])\n")
        net.connectNodes(node1,node3)
        net.connectNodes(node2,node1)
        node1.toggleNodeExpand_cb()
        ed.runCurrentNet_cb()
        print "tests combobox entry 3,input [1,2,3,4,5,6,7],output:"
        node1.inputPorts[2].widget.set('3')
        ed.master.update_idletasks()
        pause()
        self.assertEqual(node3.inputPorts[0].getData(),3)
        print "tests combobox entry 6,input [1,2,3,4,5,6,7],output:"
        node1.inputPorts[2].widget.set('6')
        ed.master.update_idletasks()
        pause()
        self.assertEqual(node3.inputPorts[0].getData(),6)


    def test_Input_read_field(self):
        if (sys.platform == 'linux2') and (os.popen('uname -m').read() == 'x86_64\n'):
            return
        from Vision.StandardNodes import stdlib
        ed.addLibraryInstance(stdlib, 'Vision.StandardNodes', 'stdlib')
        ed.master.update_idletasks()
        pause()
        net = ed.currentNetwork
        #node1 is ReadField
        node1 = Vision.StandardNodes.ReadField(library =Vision.StandardNodes.stdlib)
        #adding node1
        net.addNode(node1,150,150)
        #node3 is print 
        node3 = Vision.StandardNodes.Print(library =Vision.StandardNodes.stdlib)
        #adding node3 
        net.addNode(node3,400,100)
        net.connectNodes(node1,node3)
        print "tests readfield entry myfile2,output:"
        node1.inputPorts[0].widget.set(os.path.abspath('myfile2'))
        ed.runCurrentNet_cb()
        #self.assertEqual(str(node3.inputPorts[0].getData()),'[1 2 3 4]')
        self.assertEqual(node3.inputPorts[0].getData()[0], 1)
        self.assertEqual(node3.inputPorts[0].getData()[1], 2)
        self.assertEqual(node3.inputPorts[0].getData()[2], 3)
        self.assertEqual(node3.inputPorts[0].getData()[3], 4)


    def test_read_table(self):
        from Vision.StandardNodes import stdlib
        ed.addLibraryInstance(stdlib, 'Vision.StandardNodes', 'stdlib')
        ed.master.update_idletasks()
        pause()
        net = ed.currentNetwork
        #node1 is ReadTable
        node1 = Vision.StandardNodes.ReadTable(library =Vision.StandardNodes.stdlib)
        #adding node1
        net.addNode(node1,150,150)
        #node2 is print 
        node2 = Vision.StandardNodes.Print(library =Vision.StandardNodes.stdlib)
        #adding node2 
        net.addNode(node2,400,100)
        net.connectNodes(node1,node2)
        node1.toggleNodeExpand_cb()
        node1.inputPortByName['datatype'].widget.set('int')
        print "tests read table note,output:"
        node1.inputPorts[0].widget.set(os.path.abspath('note'))
        ed.runCurrentNet_cb()
        self.assertEqual(node2.inputPorts[0].getData().tolist(),
                                   [[1],[2],[3],[4],[5],[6],[7],[8],[9]])
        

#########################################################
#    Python Nodes Tests                                 #
#########################################################


    def test_Python_while(self):
        from Vision.StandardNodes import stdlib
        ed.addLibraryInstance(stdlib, 'Vision.StandardNodes', 'stdlib')
        ed.master.update_idletasks()
        pause()
        net = ed.currentNetwork    
        #node1 is while
        node1 = Vision.StandardNodes.While(library =Vision.StandardNodes.stdlib)
        #adding node1
        net.addNode(node1,50,50)
        #node2 is generic
        node2 = Vision.StandardNodes.Generic(library =Vision.StandardNodes.stdlib)
        #adding node2
        net.addNode(node2,100,100)
        #node3 is print 
        node3 = Vision.StandardNodes.Print(library =Vision.StandardNodes.stdlib)
        #adding node3 
        net.addNode(node3,200,200)
        #node4 is counter
        node4 = Vision.StandardNodes.Counter(library =Vision.StandardNodes.stdlib)
        net.addNode(node4,300,300)
        #node5 is IsTrue_Stop
        node5 = Vision.StandardNodes.IsTrue_Stop(library =Vision.StandardNodes.stdlib)
        net.addNode(node5,200,50)
        node2.addOutputPort()
        print "tests while val1 = 2,when input is 2,cond <4,counter,is true stop"
        node2.setFunction("def doit(self):\n\tself.outputData(out0=2)")
        node1.setFunction("def doit(self, condition, val1=None, val2=None, val3=None):\n    iter = 0\n    net = self.network\n    p = self.getOutputPortByName('run')\n    roots = map( lambda x: x.node, p.children )\n    allNodes = net.getAllNodes(roots)\n    if self in allNodes:\n        allNodes.remove(self)\n    \n    while True:\n        # get a dict of {'portname':values}\n        d = self.refreshInputPortData()\n        # set the function arguments to the current values\n        for k,v in d.items():\n            # if it is a string we want the set the variable to the string\n            # rather than evaluating the string\n            if type(v) is types.StringType:\n                exec('%s=str(%s)'%(k,v))\n            else: # we assign the value (NOT SURE THIS WORKS WITH OBJECTS!)\n                exec('%s=%s'%(k,str(v)))\n        net.canvas.update()\n        stop = self.network.checkExecStatus()\n        if stop:\n            break\n        if not eval(condition):\n            break\n        self.outputData(run=1)\n        self.forceExecution = 1\n        # this is needed for iterate inside macros to fire children of\n        # macro nodes when macronetwork isnot current network\n        self.network.forceExecution = 1\n\n        if len(allNodes):\n            net.runNodes(allNodes)\n\n        if iter<4:\n            iter = iter + 1\n        else:\n            break\n")
        node1.inputPorts[0].widget.set('val1 == 2')
        node5.inputPorts[1].widget.set('data < 4')
        port1 = node2.outputPorts[0]
        port2 = node1.inputPorts[1]
        apply( net.connectNodes,(port1.node, port2.node, port1.number, port2.number),{})
        port1 = node4.outputPorts[1]
        port2 = node5.inputPorts[0]
        apply( net.connectNodes,(port1.node, port2.node, port1.number, port2.number),{})
        #net.connectnodes(node4,node5)
        net.connectNodes(node1,node4)
        net.connectNodes(node5,node3)
        ed.master.update_idletasks()
        pause()
        ed.runCurrentNet_cb()
        #tests last value printed <4 
        self.assertEqual(node3.inputPorts[0].getData(),'Stop')
        
        
    def test_Python_len(self):
        from Vision.StandardNodes import stdlib
        ed.addLibraryInstance(stdlib, 'Vision.StandardNodes', 'stdlib')
        ed.master.update_idletasks()
        pause()
        net = ed.currentNetwork
        #node1 is opeartor3
        node1 = Vision.StandardNodes.Len(library =Vision.StandardNodes.stdlib)
        #adding node1
        net.addNode(node1,150,150)
        #node2 is generic
        node2 = Vision.StandardNodes.Generic(library =Vision.StandardNodes.stdlib)
        #adding node2
        net.addNode(node2,200,10)
        #node3 is print 
        node3 = Vision.StandardNodes.Print(library =Vision.StandardNodes.stdlib)
        #adding node3 
        net.addNode(node3,400,100)
        node2.addOutputPort()
        node2.setFunction("def doit(self):\n\tself.outputData(out0=[1,2,3,4,5,6,7])\n")
        net.connectNodes(node1,node3)
        print "tests len ,input [1,2,3,4,5,6,7],output:"
        net.connectNodes(node2,node1)
        ed.runCurrentNet_cb()
        self.assertEqual(node3.inputPorts[0].getData(),7)
        print "tests len ,input 'abcdefgh',output:"
        node2.setFunction("def doit(self):\n\tself.outputData(out0='abcdefgh')\n")
        ed.runCurrentNet_cb()
        self.assertEqual(node3.inputPorts[0].getData(),8)
        
    def test_Python_range(self):
        from Vision.StandardNodes import stdlib
        ed.addLibraryInstance(stdlib, 'Vision.StandardNodes', 'stdlib')
        ed.master.update_idletasks()
        pause()
        net = ed.currentNetwork
        #node1 is opeartor3
        node1 = Vision.StandardNodes.Range(library =Vision.StandardNodes.stdlib)
        #adding node1
        net.addNode(node1,150,150)
        #node3 is print 
        node3 = Vision.StandardNodes.Print(library =Vision.StandardNodes.stdlib)
        #adding node3 
        net.addNode(node3,400,100)
        print "tests range from 1,to 10,step 2,output:"
        net.connectNodes(node1,node3)
        node1.inputPorts[0].widget.set(1)
        node1.inputPorts[1].widget.set(10)
        node1.inputPorts[2].widget.set(2)
        node1.run()
        self.assertEqual(str(node3.inputPorts[0].getData()),'[1, 3, 5, 7, 9]')
        

    def test_Python_built_in(self):
        from Vision.StandardNodes import stdlib
        ed.addLibraryInstance(stdlib, 'Vision.StandardNodes', 'stdlib')
        ed.master.update_idletasks()
        pause()
        net = ed.currentNetwork
        #node1 is Builtin
        node1 = Vision.StandardNodes.Builtin(library =Vision.StandardNodes.stdlib)
        #adding node1
        net.addNode(node1,150,150)
        #node2 is EntryNE
        node2 = Vision.StandardNodes.EntryNE(library =Vision.StandardNodes.stdlib)
        #adding node1
        net.addNode(node2,50,50)
        #node3 is print 
        node3 = Vision.StandardNodes.Print(library =Vision.StandardNodes.stdlib)
        #adding node3 
        net.addNode(node3,400,100) 
        node1.toggleNodeExpand_cb()
        net.connectNodes(node1,node3)
        net.connectNodes(node2,node1)
        node2.inputPorts[0].widget.set(89)
        #setting output ports of entry button1 data type to int
        node2.outputPorts[0].edit()
        node2.outputPorts[0].objEditor.dataType.set('int')
        node2.outputPorts[0].objEditor.OK()
        #setting input ports 
        node1.inputPorts[0].edit()
        node1.inputPorts[0].objEditor.dataType.set('int')
        node1.inputPorts[0].objEditor.OK()
        ed.master.update_idletasks()
        pause()
        #setting input in built in func
        ed.runCurrentNet_cb()
        print "tests builtin entry lambda x:x+x,input 89,output:"
        node1.inputPorts[1].widget.set('lambda x:x+x')
        node1.run()
        self.assertEqual(node3.inputPorts[0].getData(),178)
        print "tests builtin entry lambda x:x*x,input 89,output:"
        node1.inputPorts[1].widget.set('lambda x:x*x')
        node1.run()
        self.assertEqual(node3.inputPorts[0].getData(),7921)
        
        
    def test_Python_iterate(self):
        from Vision.StandardNodes import stdlib
        ed.addLibraryInstance(stdlib, 'Vision.StandardNodes', 'stdlib')
        ed.master.update_idletasks()
        pause()
        net = ed.currentNetwork
        #node1 is 
        node1 = Vision.StandardNodes.Iterate(library =Vision.StandardNodes.stdlib)
        #adding node1
        net.addNode(node1,150,150)
        #node2 is generic
        node2 = Vision.StandardNodes.Generic(library =Vision.StandardNodes.stdlib)
        #adding node2
        net.addNode(node2,200,10)
        #node3 is print 
        node3 = Vision.StandardNodes.Print(library =Vision.StandardNodes.stdlib)
        #adding node3 
        net.addNode(node3,400,100)
        node2.addOutputPort()
        node2.setFunction("def doit(self):\n\tself.outputData(out0=[10,20,30])\n")
        print "tests iterate ,input [10,20,30]:,,connecting print with,outputport0,output:"
        net.connectNodes(node2,node1)    
        #connecting print with iterate outputport0(one item of the list)
        port3 =node1.outputPorts[0]
        port4 =node3.inputPorts[0] 
        apply( net.connectNodes,(port3.node, port4.node, port3.number, port4.number),{})
        ed.runCurrentNet_cb()
        self.assertEqual(node3.inputPorts[0].getData(),30)
        net.deleteConnectionsNoCB(net.connections[1])
        #connecting print with iterate outputport1(the current index into the list of items to iterate over)
        print "tests iterate ,input [10,20,30]:,,connecting print with,outputport1,output:"
        port3 =node1.outputPorts[1]
        port4 =node3.inputPorts[0] 
        apply( net.connectNodes,(port3.node, port4.node, port3.number, port4.number),{})
        self.assertEqual(node3.inputPorts[0].getData(),2)
        net.deleteConnectionsNoCB(net.connections[1])
        #connecting print with iterate outputport2(at the begin of an iteration, output True)
        print "tests iterate ,input [10,20,30]:,,connecting print with,outputport2,output:"
        port3 =node1.outputPorts[2]
        port4 =node3.inputPorts[0] 
        apply( net.connectNodes,(port3.node, port4.node, port3.number, port4.number),{})
        self.assertEqual(node3.inputPorts[0].getData(),False)
        net.deleteConnectionsNoCB(net.connections[1])
        #connecting print with iterate outputport3(at the end of an iteration, output True)
        print "tests iterate ,input [10,20,30]:,,connecting print with,outputport3,output:"
        port3 =node1.outputPorts[3]
        port4 =node3.inputPorts[0] 
        apply( net.connectNodes,(port3.node, port4.node, port3.number, port4.number),{})
        self.assertEqual(node3.inputPorts[0].getData(),True)
        net.deleteConnectionsNoCB(net.connections[1])
        print "tests iterate ,input [10,20,30]:,,connecting print with,outputport4,output:"
        #connecting print with iterate outputport4(the lenght of the list to loop over)
        port3 =node1.outputPorts[4]
        port4 =node3.inputPorts[0] 
        apply( net.connectNodes,(port3.node, port4.node, port3.number, port4.number),{})
        self.assertEqual(node3.inputPorts[0].getData(),3)
        
    def test_Python_accum(self):
        
        from Vision.StandardNodes import stdlib
        ed.addLibraryInstance(stdlib, 'Vision.StandardNodes', 'stdlib')
        ed.master.update_idletasks()
        pause()
        net = ed.currentNetwork
        #node1 is iterate
        node1 = Vision.StandardNodes.Iterate(library =Vision.StandardNodes.stdlib)
        #adding node1
        net.addNode(node1,150,150)
        #node2 is generic
        node2 = Vision.StandardNodes.Generic(library =Vision.StandardNodes.stdlib)
        #adding node2
        net.addNode(node2,200,10)
        #node3 is print 
        node3 = Vision.StandardNodes.Print(library =Vision.StandardNodes.stdlib)
        #adding node3 
        net.addNode(node3,400,100)
        #node4 is accum
        node4 = Vision.StandardNodes.Accumulate(library =Vision.StandardNodes.stdlib)
        #adding node4
        net.addNode(node4,50,50)
        #node5 is check button
        node5 = Vision.StandardNodes.CheckButtonNE(library =Vision.StandardNodes.stdlib)
        #ading node5
        net.addNode(node5,200,200)
        node2.addOutputPort()
        node2.setFunction("def doit(self):\n\tself.outputData(out0=[10,20,30])\n")
        net.connectNodes(node2,node1)
        net.connectNodes(node4,node3)
        #connecting iterate o/p port0 to accum i/p port0
        port1 =node1.outputPorts[0]
        port2 =node4.inputPorts[0] 
        apply( net.connectNodes,(port1.node, port2.node, port1.number, port2.number),{})
        #connecting iterate o/p port3 to accum i/p port2
        port1 =node1.outputPorts[3]
        port2 =node4.inputPorts[2] 
        apply( net.connectNodes,(port1.node, port2.node, port1.number, port2.number),{})
        #connecting check button o/p port1 to accum i/p port1
        port1 =node5.outputPorts[0]
        port2 =node4.inputPorts[1]
        apply( net.connectNodes,(port1.node, port2.node, port1.number, port2.number),{})
        print "tests accum connected to iterate,input [10, 20, 30],output:"
        node5.inputPorts[0].widget.set(0)
        ed.runCurrentNet_cb()
        self.assertEqual(node3.inputPorts[0].getData(),[10, 20, 30])
        print "tests accum connected to iterate,input [10, 20, 30],output for second run :"
        ed.runCurrentNet_cb()
        self.assertEqual(node3.inputPorts[0].getData(),[10, 20, 30, 10, 20, 30])
        print "tests accum connected to iterate,input [10, 20, 30],output for third run :"
        ed.runCurrentNet_cb()
        self.assertEqual(node3.inputPorts[0].getData(),[10, 20, 30, 10, 20, 30, 10, 20, 30])

    def test_Python_map(self):
        
        from Vision.StandardNodes import stdlib
        ed.addLibraryInstance(stdlib, 'Vision.StandardNodes', 'stdlib')
        ed.master.update_idletasks()
        pause()
        net = ed.currentNetwork
        #node1 is map
        node1 = Vision.StandardNodes.Map(library =Vision.StandardNodes.stdlib)
        #adding node1
        net.addNode(node1,150,150)
        #node2 is generic
        node2 = Vision.StandardNodes.Generic(library =Vision.StandardNodes.stdlib)
        #adding node2
        net.addNode(node2,200,10)
        #node3 is print 
        node3 = Vision.StandardNodes.Print(library =Vision.StandardNodes.stdlib)
        #adding node3 
        net.addNode(node3,400,100) 
        node2.addOutputPort()
        node2.setFunction("def doit(self):\n\tself.outputData(out0=[10,20,30])\n")
        net.connectNodes(node1,node3)
        net.connectNodes(node2,node1)
        node2.outputPorts[0].edit()
        node2.outputPorts[0].objEditor.dataType.set('list')
        node2.outputPorts[0].objEditor.OK()
        node1.inputPorts[1].widget.set('lambda x: str(x)')
        print "tests map entry lambda x: str(x),input [10,20,30],output:"
        ed.runCurrentNet_cb()
        self.assertEqual(node3.inputPorts[0].getData(),['10', '20', '30'])
        node1.inputPorts[1].widget.set('lambda x: x+x')
        print "tests map entry lambda x: x+x,input [10,20,30],output:"
        ed.runCurrentNet_cb()
        self.assertEqual(str(node3.inputPorts[0].getData()), '[20, 40, 60]') 
        

    def test_Python_isTrue_Stop(self):
        from Vision.StandardNodes import stdlib
        ed.addLibraryInstance(stdlib, 'Vision.StandardNodes', 'stdlib')
        ed.master.update_idletasks()
        pause()
        net = ed.currentNetwork
        #node1 is isTrue_stop
        node1 = Vision.StandardNodes.IsTrue_Stop(library =Vision.StandardNodes.stdlib)
        #adding node1
        net.addNode(node1,150,150)
        #node2 is entry
        node2 = Vision.StandardNodes.EntryNE(library =Vision.StandardNodes.stdlib)
        #adding node2
        net.addNode(node2,200,10)
        #node3 is print 
        node3 = Vision.StandardNodes.Print(library =Vision.StandardNodes.stdlib)
        #adding node3 
        net.addNode(node3,400,100)
        net.connectNodes(node1,node3)
        net.connectNodes(node2,node1)
        node2.outputPorts[0].edit()
        node2.outputPorts[0].objEditor.dataType.set('int')
        node2.outputPorts[0].objEditor.OK()
        node2.inputPorts[0].widget.set(100)
        print "tests isTrue is 'data == 10',entry 100,output:"
        node1.inputPorts[1].widget.set('data == 10')
        node1.run()
        self.assertEqual(node3.inputPorts[0].getData(),'Stop')
        print "tests isTrue is 'data == 100',entry 100,output:"
        node1.inputPorts[1].widget.set('data == 100')
        net.run()
        self.assertEqual(node3.inputPorts[0].getData(),100)
        
        
    def test_Python_if_else(self):
        from Vision.StandardNodes import stdlib
        ed.addLibraryInstance(stdlib, 'Vision.StandardNodes', 'stdlib')
        ed.master.update_idletasks()
        pause()
        net = ed.currentNetwork
        #node1 is if else
        node1 = Vision.StandardNodes.IfElseNode(library =Vision.StandardNodes.stdlib)
        #adding node1
        net.addNode(node1,150,150)
        #node2 is entry
        node2 = Vision.StandardNodes.EntryNE(library =Vision.StandardNodes.stdlib)
        #adding node2
        net.addNode(node2,200,10)
        #node3 is print 
        node3 = Vision.StandardNodes.Print(library =Vision.StandardNodes.stdlib)
        #adding node3 
        net.addNode(node3,400,100)
        #node4 is entry
        node4 = Vision.StandardNodes.EntryNE(library =Vision.StandardNodes.stdlib)
        #adding node4
        net.addNode(node4,50,50)
        net.connectNodes(node1,node3)
        port1 =node2.outputPorts[0]
        port2 =node1.inputPorts[0] 
        apply( net.connectNodes,(port1.node, port2.node, port1.number, port2.number),{})
        port1 =node4.outputPorts[0]
        port2 =node1.inputPorts[1] 
        apply( net.connectNodes,(port1.node, port2.node, port1.number, port2.number),{})
        node2.outputPorts[0].edit()
        node2.outputPorts[0].objEditor.dataType.set('int')
        node2.outputPorts[0].objEditor.OK()
        node4.outputPorts[0].edit()
        node4.outputPorts[0].objEditor.dataType.set('int')
        node4.outputPorts[0].objEditor.OK()
        node1.inputPorts[2].widget.set('False')
        node2.inputPorts[0].widget.set(100)
        print "tests if_else entry False,inputs 100,90,output:"
        node4.inputPorts[0].widget.set(90)
        net.run()
        self.assertEqual(node3.inputPorts[0].getData(),90)
        print "tests if_else entry True,inputs 100,90,output:"
        node1.inputPorts[2].widget.set('True')
        node1.run()
        self.assertEqual(node3.inputPorts[0].getData(),100)

     
    
    def test_Python_eval(self):
        from Vision.StandardNodes import stdlib
        ed.addLibraryInstance(stdlib, 'Vision.StandardNodes', 'stdlib')
        ed.master.update_idletasks()
        pause()
        net = ed.currentNetwork
        #node1 is eval
        node1 = Vision.StandardNodes.Eval(library =Vision.StandardNodes.stdlib)
        #adding node1
        net.addNode(node1,150,150)
        #node3 is print 
        node3 = Vision.StandardNodes.Print(library =Vision.StandardNodes.stdlib)
        #adding node3 
        net.addNode(node3,400,100)
        net.connectNodes(node1,node3)
        node1.toggleNodeExpand_cb()
        print "tests eval entry 2+3 ,output:"
        node1.inputPorts[0].widget.set('2+3')
        node1.run()
        self.assertEqual(node3.inputPorts[0].getData(),5)
        print "tests eval entry 2+3!=5 ,output:"
        node1.inputPorts[0].widget.set('2+3 !=5')
        node1.run()
        self.assertEqual(node3.inputPorts[0].getData(),False)
        
##    def Xtest_Python_eval_with_input_port(self):
##        from Vision.StandardNodes import stdlib
##        ed.addLibraryInstance(stdlib, 'Vision.StandardNodes', 'stdlib')
##        ed.master.update_idletasks()
##        pause()
##        net = ed.currentNetwork
##        #node1 is if else
##        node1 = Vision.StandardNodes.Eval(library =Vision.StandardNodes.stdlib)
##        #adding node1
##        net.addNode(node1,150,150)
##        #node2 is entry
##        node2 = Vision.StandardNodes.Generic(library =Vision.StandardNodes.stdlib)
##        #adding node2
##        net.addNode(node2,50,50)
##        #node3 is print 
##        node3 = Vision.StandardNodes.Print(library =Vision.StandardNodes.stdlib)
##        #adding node3 
##        net.addNode(node3,400,100)
##        node2.addOutputPort()
##        node2.setFunction("def doit(self):\n\tself.outputData(out0=[1,2,3,4,5])\n")
##        port1 =node2.outputPorts[0]
##        port2 =node1.inputPorts[1] 
##        apply( net.connectNodes,(port1.node, port2.node, port1.number, port2.number),{})
##        #net.connectNodes(node2,node1)
##        net.connectNodes(node1,node3)
##        ed.runCurrentNet_cb()
##        print "tests eval entry in1[1:2],input [1,2,3,4,5],output:"
##        node1.inputPorts[0].widget.set('in1[1:2]')
##        self.assertEqual(node3.inputPorts[0].getData(),[2])
        


        
    def test_Python_get_attr(self):
        from Vision.StandardNodes import stdlib
        ed.addLibraryInstance(stdlib, 'Vision.StandardNodes', 'stdlib')
        ed.master.update_idletasks()
        pause()
        net = ed.currentNetwork
        #node1 is Get attr
        node1 = Vision.StandardNodes.GetAttr(library =Vision.StandardNodes.stdlib)
        #adding node1
        net.addNode(node1,150,150)
        #node2 is generic
        node2 = Vision.StandardNodes.Generic(library =Vision.StandardNodes.stdlib)
        #adding node2
        net.addNode(node2,200,10)
        #node3 is print 
        node3 = Vision.StandardNodes.Print(library =Vision.StandardNodes.stdlib)
        #adding node3 
        net.addNode(node3,400,100) 
        node2.addOutputPort()
        node2.setFunction("def doit(self):\n\timport Vision\n\tnode1 =Vision.StandardNodes\n\tself.outputData(out0=node1)\n")
        net.connectNodes(node1,node3)
        net.connectNodes(node2,node1)
        node1.toggleNodeExpand_cb()
        ed.runCurrentNet_cb()
        print "tests get attr  entry Filelist,input Vision.StandardNodes,output:"
        node1.inputPorts[1].widget.set('Filelist')
        ed.runCurrentNet_cb()
        from string import split
        self.assertEqual(split(str(node3.inputPorts[0].getData()[0]))[1],"'Vision.StandardNodes.Filelist'>")
        
    def test_Python_set_attr(self):
        from Vision.StandardNodes import stdlib
        ed.addLibraryInstance(stdlib, 'Vision.StandardNodes', 'stdlib')
        ed.master.update_idletasks()
        pause()
        net = ed.currentNetwork
        #node1 is map
        node1 = Vision.StandardNodes.SetAttr(library =Vision.StandardNodes.stdlib)
        #adding node1
        net.addNode(node1,150,150)
        #node2 is generic
        node2 = Vision.StandardNodes.Generic(library =Vision.StandardNodes.stdlib)
        #adding node2
        net.addNode(node2,200,10)
        #node3 is print 
        node3 = Vision.StandardNodes.Print(library =Vision.StandardNodes.stdlib)
        #adding node3 
        net.addNode(node3,400,100) 
        node4 = Vision.StandardNodes.EntryNE(library =Vision.StandardNodes.stdlib)
        net.addNode(node4,50,50)
        node2.addOutputPort()
        node2.setFunction("def doit(self):\n\timport Vision\n\tnode1 =Vision.StandardNodes\n\tself.outputData(out0=node1)\n")
        net.connectNodes(node1,node3)
        port1 =node2.outputPorts[0]
        port2 =node1.inputPorts[0] 
        apply( net.connectNodes,(port1.node, port2.node, port1.number, port2.number),{})
        port1 =node4.outputPorts[0]
        port2 =node1.inputPorts[1] 
        apply( net.connectNodes,(port1.node, port2.node, port1.number, port2.number),{})
        node1.toggleNodeExpand_cb()
        #ed.runCurrentNet_cb()
        print "tests setattr  entry Filelist,input Vision.StandardNodes,output:"
        node1.inputPorts[2].widget.set("newattr")
        node4.inputPorts[0].widget.set(100)
        ed.runCurrentNet_cb()
        self.assertEqual(Vision.StandardNodes.newattr,'100')
        
    def test_Python_call_method(self):
        from Vision.StandardNodes import stdlib
        ed.addLibraryInstance(stdlib, 'Vision.StandardNodes', 'stdlib')
        ed.master.update_idletasks()
        pause()
        net = ed.currentNetwork
        #node1 is Generic
        node1 = Vision.StandardNodes.Generic(library =Vision.StandardNodes.stdlib)
        #adding node1
        net.addNode(node1,100,50)
        #node2 is Generic
        node2 = Vision.StandardNodes.CallMethod(library =Vision.StandardNodes.stdlib)
        #adding node2
        net.addNode(node2,100,100)
        node1.addOutputPort()
        node1.setFunction("def doit(self):\n\tfrom Vision.Tests.myclass import Test\n\tmyTest = Test()\n\tself.outputData(out0=myTest)\n")
        node3 =Vision.StandardNodes.Print(library =Vision.StandardNodes.stdlib)
        net.addNode(node3,100,200)
        node2.inputPorts[1].widget.set('Set %a %b %c %d')
        #node0 is Generic
        node0 = Vision.StandardNodes.Generic(library =Vision.StandardNodes.stdlib)
        #adding node0
        net.addNode(node0,100,150)
        node0.setFunction("def doit(self):\n\tself.outputData(out0=[in0.in1,in0.in2,in0.in3,in0.in4])\n")
        node0.addOutputPort()
        node0.addInputPort()
        net.connectNodes(node1,node2)
        net.connectNodes(node0,node3)
        port1 =node2.outputPorts[0]
        port2 =node0.inputPorts[0] 
        apply( net.connectNodes,(port1.node, port2.node, port1.number, port2.number),{})
        ed.runCurrentNet_cb()
        node4 = Vision.StandardNodes.EntryNE(library =Vision.StandardNodes.stdlib)
        net.addNode(node4,200,50)
        node5 = Vision.StandardNodes.EntryNE(library =Vision.StandardNodes.stdlib)
        net.addNode(node5,200,150)
        node6 = Vision.StandardNodes.EntryNE(library =Vision.StandardNodes.stdlib)
        net.addNode(node6,200,250)
        node7 = Vision.StandardNodes.EntryNE(library =Vision.StandardNodes.stdlib)
        net.addNode(node7,200,320)
        node4.inputPorts[0].widget.set(1)
        node5.inputPorts[0].widget.set(2)
        node6.inputPorts[0].widget.set(3)
        node7.inputPorts[0].widget.set(4)
        print "tests call method ,input myclass instance ,inputs for 4 ports is 1,2,3,4, output:"
        port1 =node4.outputPorts[0]
        port2 =node2.inputPorts[2] 
        apply( net.connectNodes,(port1.node, port2.node, port1.number, port2.number),{})
        port1 =node5.outputPorts[0]
        port2 =node2.inputPorts[3] 
        apply( net.connectNodes,(port1.node, port2.node, port1.number, port2.number),{})
        port1 =node6.outputPorts[0]
        port2 =node2.inputPorts[4] 
        apply( net.connectNodes,(port1.node, port2.node, port1.number, port2.number),{})
        port1 =node7.outputPorts[0]
        port2 =node2.inputPorts[5] 
        apply( net.connectNodes,(port1.node, port2.node, port1.number, port2.number),{})
        ed.runCurrentNet_cb()
        self.assertEqual(node3.inputPorts[0].getData(),['1', '2', '3', '4'])
        
################################################
#       Vision Tests                           #
################################################

    def test_Python_set_counter(self):
        from Vision.StandardNodes import stdlib
        ed.addLibraryInstance(stdlib, 'Vision.StandardNodes', 'stdlib')
        ed.master.update_idletasks()
        pause()
        net = ed.currentNetwork
        net.runOnNewData.value = False
        #node1 is counter
        node1 = Vision.StandardNodes.Counter(library =Vision.StandardNodes.stdlib)
        #adding node1
        net.addNode(node1,150,150)
        #node2 is print
        node2 = Vision.StandardNodes.Print(library =Vision.StandardNodes.stdlib)
        #adding node2
        net.addNode(node2,200,10)
        #node3 is print 
        node3 = Vision.StandardNodes.Print(library =Vision.StandardNodes.stdlib)
        #adding node3 
        net.addNode(node3,400,100) 
        node4 = Vision.StandardNodes.EntryNE(library =Vision.StandardNodes.stdlib)
        net.addNode(node4,50,50)
        net.connectNodes(node4,node1)
        port1 =node1.outputPorts[0]
        port2 =node2.inputPorts[0] 
        apply( net.connectNodes,(port1.node, port2.node, port1.number, port2.number),{})
        port1 =node1.outputPorts[1]
        port2 =node3.inputPorts[0] 
        apply( net.connectNodes,(port1.node, port2.node, port1.number, port2.number),{})
        node1.toggleNodeExpand_cb()
        net.runOnNewData.value = True
        print "tests counter when run thrice,input 2,output:"
        node4.inputPorts[0].widget.set(2)
        ed.runCurrentNet_cb()
        ed.runCurrentNet_cb()
        self.assertEqual(node2.inputPorts[0].getData(),'2')
        self.assertEqual(node3.inputPorts[0].getData(),3)
        print "tests counter when reset toggled input 2,output:"
        node1.inputPorts[1].widget.widget.invoke()
        self.assertEqual(node2.inputPorts[0].getData(),'2')
        self.assertEqual(node3.inputPorts[0].getData(),0)
        

    def test_Python_change_back_ground_color(self):
        from Vision.StandardNodes import stdlib
        ed.addLibraryInstance(stdlib, 'Vision.StandardNodes', 'stdlib')
        ed.master.update_idletasks()
        pause()
        net = ed.currentNetwork
        #node1 is counter
        node1 = Vision.StandardNodes.ChangeBackground(library =Vision.StandardNodes.stdlib)
        #adding node1
        net.addNode(node1,150,150)
        print "tests change back ground color:"
        node1.inputPorts[0].widget.set('blue')
        self.assertEqual(node1.inputPorts[0].widget.get(),'blue')
        

#    def test_Python_show_hide_gui(self):
#        from Vision.StandardNodes import stdlib
#        ed.addLibraryInstance(stdlib, 'Vision.StandardNodes', 'stdlib')
#        ed.master.update_idletasks()
#        pause()
#        net = ed.currentNetwork
#        node1 = Vision.StandardNodes.ShowHideGUI(library =Vision.StandardNodes.stdlib)
#        #adding node1
#        net.addNode(node1,150,150)
#	
#        #print "tests show hide gui toggle off:"
#        node1.inputPorts[0].widget.set(0)
#        self.assertEqual(node1.inputPorts[0].widget.widget.winfo_viewable(),0)
#        
#        #print "tests show hide gui toggle on:"
#        ed.showGUI()
#        node1.inputPorts[0].widget.set(1)
#        ed.master.update_idletasks()
#        pause()
#
#        if sys.platform != 'win32':
#        	self.assertEqual(node1.inputPorts[0].widget.widget.winfo_viewable(),1)


    def test_Python_show_hide_param_panel(self):
        from Vision.StandardNodes import stdlib
        ed.addLibraryInstance(stdlib, 'Vision.StandardNodes', 'stdlib')
        ed.master.update_idletasks()
        pause()
        net = ed.currentNetwork
        node1 = Vision.StandardNodes.ShowHideParamPanel(library =Vision.StandardNodes.stdlib)
        #adding node1
        net.addNode(node1,150,150)    
        node2 =  Vision.StandardNodes.EntryNE(library =Vision.StandardNodes.stdlib)
        #adding node2
        net.addNode(node2,50,50)
        net.connectNodes(node2,node1)
        node2.inputPorts[0].widget.set(25)
        print "tests parampanel toggle on:"
        node1.inputPorts[1].widget.set(0)
        print "tests parampanel toggle off:"
        self.assertEqual(node2.paramPanel.visible,0)
        node1.inputPorts[1].widget.set(1)
        node1.run()
        self.assertEqual(node2.paramPanel.visible,1)
        
    def test_has_new_data(self):
        from Vision.StandardNodes import stdlib
        ed.addLibraryInstance(stdlib, 'Vision.StandardNodes', 'stdlib')
        ed.master.update_idletasks()
        pause()
        net = ed.currentNetwork
        net.runOnNewData.value = True
        node1 = Vision.StandardNodes.Generic(library =Vision.StandardNodes.stdlib)
        net.addNode(node1,200,10)
        node1.addOutputPort()
        node1.addOutputPort()
        node1.addInputPort()
        node1.setFunction("""def doit(self, in1):\n\tif in1:\n\t\tself.outputData(out0='On')\n\telse:\n\t\tself.outputData(out1='Off')""")
        apply(node1.inputPorts[0].createWidget, (), {'descr':{'initialValue': 0, 'labelGridCfg': {'column': 0, 'row': 0}, 'master': 'node', 'widgetGridCfg': {'labelSide': 'left', 'column': 1, 'row': 0}, 'labelCfg': {'text': ''}, 'class': 'NECheckButton'}})
        node2 =  Vision.StandardNodes.HasNewData(library =Vision.StandardNodes.stdlib)
        net.addNode(node2,200,100)
        node3 =  Vision.StandardNodes.Print(library =Vision.StandardNodes.stdlib)
        net.addNode(node3,200,200)
        net.connectNodes(node2,node3)
        node3.inputPorts[0].edit()
        node3.inputPorts[0].objEditor.singleConnectionTk.set('False')
        node3.inputPorts[0].objEditor.OK()
        port1 =node1.outputPorts[1]
        port2 =node2.inputPorts[0]
        apply( net.connectNodes,(port1.node, port2.node, port1.number, port2.number),{})
        port1 =node1.outputPorts[0]
        port2 =node3.inputPorts[0]
        apply( net.connectNodes,(port1.node, port2.node, port1.number, port2.number),{})
        node1.toggleNodeExpand_cb()
        self.assertEqual(node3.inputPorts[0].getData(),'Stop')
        node1.inputPorts[0].widget.set(1)
        self.assertEqual(node3.inputPorts[0].getData(),['On'])
        node1.inputPorts[0].widget.set(0)
        print "testing hasNewData node,generic with inputports widget type Check button,when off,output is:"
        self.assertEqual(node3.inputPorts[0].getData(),['Off'])
        

        
##############################################
#   Output Tests                             #
##############################################

    def test_save_field(self):
                
        from Vision.StandardNodes import stdlib
        ed.addLibraryInstance(stdlib, 'Vision.StandardNodes', 'stdlib')
        ed.master.update_idletasks()
        pause()
        net = ed.currentNetwork
        #node1 is save field 
        node1 = Vision.StandardNodes.SaveField(library =Vision.StandardNodes.stdlib)
        #adding node1
        net.addNode(node1,150,150)
        #node2 is print
        node2 = Vision.StandardNodes.Generic(library =Vision.StandardNodes.stdlib)
        #adding node2
        net.addNode(node2,200,10)
        node2.addOutputPort()
        node2.setFunction("def doit(self):\n\tself.outputData(out0=[1,2,3,4])\n")
        net.connectNodes(node2,node1)
        node1.toggleNodeExpand_cb()
        node1.inputPorts[1].widget.set(os.path.abspath('testfile'))
        ed.runCurrentNet_cb()
        fptr = open('testfile')
        alllines = fptr.readlines()
        self.assertEqual(len(alllines)>0,True)
        

    def test_save_lines(self):
        cmd = "rm -f note3"
        os.system(cmd)        
        from Vision.StandardNodes import stdlib
        ed.addLibraryInstance(stdlib, 'Vision.StandardNodes', 'stdlib')
        ed.master.update_idletasks()
        pause()
        net = ed.currentNetwork
        #node1 is save field 
        node1 = Vision.StandardNodes.SaveLines(library =Vision.StandardNodes.stdlib)
        #adding node1
        net.addNode(node1,150,150)
        #node2 is print
        node2 = Vision.StandardNodes.EntryNE(library =Vision.StandardNodes.stdlib)
        #adding node2
        net.addNode(node2,200,10)
        #node3 is print 
        node3 = Vision.StandardNodes.Print(library =Vision.StandardNodes.stdlib)
        #adding node1
        net.addNode(node3,50,50)
        net.connectNodes(node2,node1)
        node2.inputPorts[0].widget.set("hi how are you?")
        node1.inputPorts[1].widget.set(os.path.abspath('note3'))
        net.run()
        fptr =open(os.path.abspath('note3'))
        alllines = fptr.readlines()
        self.assertEqual(len(alllines)>0,True)

        

    def test__output_PrintFS(self):
        from Vision.StandardNodes import stdlib
        ed.addLibraryInstance(stdlib, 'Vision.StandardNodes', 'stdlib')
        ed.master.update_idletasks()
        pause()
        net = ed.currentNetwork
        #printFS node
        node1 = Vision.StandardNodes.PrintFormatedString(library =Vision.StandardNodes.stdlib)
        net.addNode(node1,150,150)
        #entry node
        node2 = Vision.StandardNodes.EntryNE(library =Vision.StandardNodes.stdlib)
        net.addNode(node2,250,250)
        #entry node
        node3 = Vision.StandardNodes.EntryNE(library =Vision.StandardNodes.stdlib)
        net.addNode(node3,200,200)
        #entry node
        node4 = Vision.StandardNodes.EntryNE(library =Vision.StandardNodes.stdlib)
        net.addNode(node4,100,200)
        #print node
        node5 = Vision.StandardNodes.Print(library =Vision.StandardNodes.stdlib)
        net.addNode(node5,300,200)
        #adding inputports for printFS
        node1.addInputPort()
        node1.addInputPort()
        #connecting printFS and entry nodes
        port1 =node3.outputPorts[0]
        port2 =node1.inputPorts[2] 
        apply( net.connectNodes,(port1.node, port2.node, port1.number, port2.number),{})
        port1 =node4.outputPorts[0]
        port2 =node1.inputPorts[3] 
        apply( net.connectNodes,(port1.node, port2.node, port1.number, port2.number),{})
        node3.outputPorts[0].edit()
        node3.outputPorts[0].objEditor.dataType.set('float')
        node3.outputPorts[0].objEditor.OK()
        node4.outputPorts[0].edit()
        node4.outputPorts[0].objEditor.dataType.set('float')
        node4.outputPorts[0].objEditor.OK()
        node2.inputPorts[0].widget.set('%d %6.2f')
        node3.inputPorts[0].widget.set(2.3456)
        node4.inputPorts[0].widget.set(4.23)
        net.connectNodes(node2,node1)
        net.connectNodes(node1,node5)
        print "tests printFS '%d %6.2f',inputs 2.3456,4.23,output :"
        ed.runCurrentNet_cb()
        self.assertEqual(str(node5.inputPorts[0].getData()),'2   4.23')
        

if __name__ == '__main__':
    unittest.main()
