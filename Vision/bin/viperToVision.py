### DISCLAIMER: THIS PROGRAM IS PROVIDED AS IS WITHOUT ANY GUARANTEES OR
### WARRANTY. ALTHOUGH THE AUTHOR OF THIS SCRIPT HAS ATTEMPTED TO FIND AND
### CORRECT ANY BUGS IN THIS FREE SOFTWARE, THE AUTHOR IS NOT RESPONSIBLE
### FOR ANY DAMAGE OR LOSSES OF ANY KIND CAUSED BY THE USE OR MISUSE OF THIS
### SCRIPT.


"""Copy this Python script in a directory containing old ViPEr networks.
Next, backup all your old networks (just in case).
Then, run this script with 'python viperToVision.py'
The scrips simply opens all files in the current directory that end with
'_net.py', updates the text to be compatible with Vision, renames the old
viper network file and saves new the file with the original filename.

Here is an example:

Let's say you have a network called 'doSomething_net.py':
the script will rename this file to 'doSomething_net.py.viper', then modify the
source code to be compatible with Vision, and save it as 'doSomething_net.py"""

import glob
import string
import os

files = glob.glob("*_net.py")
for file in files:
    currentFile =  open(file, 'r')
    lines = currentFile.readlines()
    currentFile.close()
    os.rename(file, file+".viper")

    newlines = []
    for line in lines:

        # Replace Header
        newline = string.replace(line,
                       "## file written by viper", "## file written by Vision")

        if newline != line:
            newlines.append(newline)
            continue

        newline = string.replace(line,
                       "## file written by ViPEr", "## file written by Vision")

        if newline != line:
            newlines.append(newline)
            continue

        # Replace Vision Standard library
        newline = string.replace(line,
                       "ViPEr.StandardNodes", "Vision.StandardNodes")

        if newline != line:
            newlines.append(newline)
            continue

        # Replace Vision PIL library
        newline = string.replace(line,
                       "ViPEr.PILNodes", "Vision.PILNodes")
        if newline != line:
            newlines.append(newline)
            continue
       
        # Replace DejaVu library
        newline = string.replace(line,
                       "DejaVu.ViPEr", "DejaVu.VisionInterface")
        if newline != line:
            newlines.append(newline)
            continue

        # Replace FlexTreelibrary
        newline = string.replace(line,
                       "FlexTree.ViPEr", "FlexTree.VisionInterface")

        if newline != line:
            newlines.append(newline)
            continue

        # Replace MolKit library
        newline = string.replace(line,
                       "MolKit.ViPEr", "MolKit.VisionInterface")

        if newline != line:
            newlines.append(newline)
            continue

        # Replace Pmv library
        newline = string.replace(line,
                       "Pmv.PmvViPEr", "Pmv.VisionInterface")

        if newline != line:
            newlines.append(newline)
            continue

        # Replace Volume library
        newline = string.replace(line,
                       "Volume.ViPEr", "Volume.VisionInterface")

        if newline != line:
            newlines.append(newline)
            continue

       # Replace symserv library
        newline = string.replace(line,
                       "symserv.ViPEr", "symserv.VisionInterface")
        
        if newline != line:
            newlines.append(newline)
            continue

        newlines.append(line)
        
    newfile = open(file, 'w')
    newfile.writelines(newlines)
    newfile.close()







