#!/bin/ksh ~/.mgltools/pythonsh
########################################################################
#
#    Vision Network - Python source code - file generated by vision
#    Thursday 30 July 2009 15:56:18 
#    
#       The Scripps Research Institute (TSRI)
#       Molecular Graphics Lab
#       La Jolla, CA 92037, USA
#
# Copyright: Daniel Stoffler, Michel Sanner and TSRI
#   
# revision: Guillaume Vareille
#  
#########################################################################
#
# $Header: /opt/cvs/python/packages/share1.5/Vision/doc/Tutorial/cartoon/cartoon2_net.py,v 1.6 2009/07/31 00:54:52 vareille Exp $
#
# $Id: cartoon2_net.py,v 1.6 2009/07/31 00:54:52 vareille Exp $
#


if __name__=='__main__':
    from sys import argv
    if '--help' in argv or '-h' in argv or '-w' in argv: # run without Vision
        withoutVision = True
        from Vision.VPE import NoGuiExec
        ed = NoGuiExec()
        from NetworkEditor.net import Network
        import os
        masterNet = Network("process-"+str(os.getpid()))
        ed.addNetwork(masterNet)
    else: # run as a stand alone application while vision is hidden
        withoutVision = False
        from Vision import launchVisionToRunNetworkAsApplication, mainLoopVisionToRunNetworkAsApplication
	if '-noSplash' in argv:
	    splash = False
	else:
	    splash = True
        masterNet = launchVisionToRunNetworkAsApplication(splash=splash)
        import os
        masterNet.filename = os.path.abspath(__file__)
from traceback import print_exc
## loading libraries ##
from Vision.PILNodes import imagelib
from Vision.StandardNodes import stdlib
try:
    masterNet
except (NameError, AttributeError): # we run the network outside Vision
    from NetworkEditor.net import Network
    masterNet = Network()

masterNet.getEditor().addLibraryInstance(imagelib,"Vision.PILNodes", "imagelib")

masterNet.getEditor().addLibraryInstance(stdlib,"Vision.StandardNodes", "stdlib")

try:
    ## saving node len ##
    from Vision.StandardNodes import Len
    len_1 = Len(constrkw={}, name='len', library=stdlib)
    masterNet.addNode(len_1,58,117)
    apply(len_1.configure, (), {'paramPanelImmediate': 1, 'expanded': False})
except:
    print "WARNING: failed to restore Len named len in network masterNet"
    print_exc()
    len_1=None

try:
    ## saving node Read Image ##
    from Vision.PILNodes import ReadImage
    Read_Image_2 = ReadImage(constrkw={}, name='Read Image', library=imagelib)
    masterNet.addNode(Read_Image_2,15,342)
    apply(Read_Image_2.inputPortByName['filename'].configure, (), {'datatype': 'string', 'originalDatatype': 'str'})
    Read_Image_2.inputPortByName['filename'].rebindWidget()
    Read_Image_2.inputPortByName['filename'].widget.set(r"", run=False)
    Read_Image_2.inputPortByName['filename'].unbindWidget()
except:
    print "WARNING: failed to restore ReadImage named Read Image in network masterNet"
    print_exc()
    Read_Image_2=None

try:
    ## saving node Scale ##
    from Vision.PILNodes import ResizeImage
    Scale_3 = ResizeImage(constrkw={}, name='Scale', library=imagelib)
    masterNet.addNode(Scale_3,155,272)
    Scale_3.inputPortByName['scale'].widget.set(1.0, run=False)
    Scale_3.inputPortByName['filter'].widget.set(r"NEAREST", run=False)
    apply(Scale_3.configure, (), {'paramPanelImmediate': 1})
except:
    print "WARNING: failed to restore ResizeImage named Scale in network masterNet"
    print_exc()
    Scale_3=None

try:
    ## saving node Show Image ##
    from Vision.PILNodes import ShowImage
    Show_Image_4 = ShowImage(constrkw={}, name='Show Image', library=imagelib)
    masterNet.addNode(Show_Image_4,155,375)
    apply(Show_Image_4.configure, (), {'paramPanelImmediate': 1, 'expanded': False})
except:
    print "WARNING: failed to restore ShowImage named Show Image in network masterNet"
    print_exc()
    Show_Image_4=None

try:
    ## saving node sub ##
    from Vision.StandardNodes import Operator2
    sub_5 = Operator2(constrkw={}, name='sub', library=stdlib)
    masterNet.addNode(sub_5,163,146)
    sub_5.inputPortByName['operation'].widget.set(r"sub", run=False)
    sub_5.inputPortByName['applyToElements'].widget.set(0, run=False)
    apply(sub_5.configure, (), {'paramPanelImmediate': 1})
except:
    print "WARNING: failed to restore Operator2 named sub in network masterNet"
    print_exc()
    sub_5=None

try:
    ## saving node ThumbWheelInt ##
    from Vision.StandardNodes import ThumbWheelIntNE
    ThumbWheelInt_6 = ThumbWheelIntNE(constrkw={}, name='ThumbWheelInt', library=stdlib)
    masterNet.addNode(ThumbWheelInt_6,32,183)
    apply(ThumbWheelInt_6.inputPortByName['thumbwheel'].widget.configure, (), {'max': 100, 'oneTurn': 30.0, 'min': 0})
    ThumbWheelInt_6.inputPortByName['thumbwheel'].widget.set(82, run=False)
    apply(ThumbWheelInt_6.configure, (), {'paramPanelImmediate': 1})
except:
    print "WARNING: failed to restore ThumbWheelIntNE named ThumbWheelInt in network masterNet"
    print_exc()
    ThumbWheelInt_6=None

try:
    ## saving node DialInt ##
    from Vision.StandardNodes import DialIntNE
    DialInt_7 = DialIntNE(constrkw={}, name='DialInt', library=stdlib)
    masterNet.addNode(DialInt_7,719,121)
    DialInt_7.inputPortByName['dial'].widget.set(0, run=False)
except:
    print "WARNING: failed to restore DialIntNE named DialInt in network masterNet"
    print_exc()
    DialInt_7=None

try:
    ## saving node Index ##
    from Vision.StandardNodes import Index
    Index_8 = Index(constrkw={}, name='Index', library=stdlib)
    masterNet.addNode(Index_8,15,284)
    Index_8.inputPortByName['index'].rebindWidget()
    Index_8.inputPortByName['index'].widget.set(0, run=False)
    Index_8.inputPortByName['index'].unbindWidget()
    apply(Index_8.configure, (), {'paramPanelImmediate': 1})
except:
    print "WARNING: failed to restore Index named Index in network masterNet"
    print_exc()
    Index_8=None

try:
    ## saving node eval: 1 ##
    from Vision.StandardNodes import Eval
    eval__1_9 = Eval(constrkw={}, name='eval: 1', library=stdlib)
    masterNet.addNode(eval__1_9,227,44)
    eval__1_9.inputPortByName['command'].widget.set(r"1", run=False)
    eval__1_9.inputPortByName['importString'].widget.set(r"", run=False)
    apply(eval__1_9.configure, (), {'paramPanelImmediate': 1})
except:
    print "WARNING: failed to restore Eval named eval: 1 in network masterNet"
    print_exc()
    eval__1_9=None

try:
    ## saving node Filelist ##
    from Vision.StandardNodes import Filelist
    Filelist_10 = Filelist(constrkw={}, name='Filelist', library=stdlib)
    masterNet.addNode(Filelist_10,15,8)
    Filelist_10.inputPortByName['directory'].widget.set(r".", run=False)
    Filelist_10.inputPortByName['match_str'].widget.set(r"cartoonMovie/*.png*", run=False)
    apply(Filelist_10.configure, (), {'paramPanelImmediate': 1})
except:
    print "WARNING: failed to restore Filelist named Filelist in network masterNet"
    print_exc()
    Filelist_10=None

#masterNet.run()
masterNet.freeze()

## saving connections for network cartoon2 ##
if Read_Image_2 is not None and Scale_3 is not None:
    try:
        masterNet.connectNodes(
            Read_Image_2, Scale_3, "image", "image", blocking=True
            , splitratio=[0.69668420020870314, 0.58985245641201722])
    except:
        print "WARNING: failed to restore connection between Read_Image_2 and Scale_3 in network masterNet"
if Scale_3 is not None and Show_Image_4 is not None:
    try:
        masterNet.connectNodes(
            Scale_3, Show_Image_4, "scaledImage", "image", blocking=True
            , splitratio=[0.64123215875445383, 0.3681467467042544])
    except:
        print "WARNING: failed to restore connection between Scale_3 and Show_Image_4 in network masterNet"
if len_1 is not None and sub_5 is not None:
    try:
        masterNet.connectNodes(
            len_1, sub_5, "length", "data1", blocking=True
            , splitratio=[0.28972257612418201, 0.31351108469097216])
    except:
        print "WARNING: failed to restore connection between len_1 and sub_5 in network masterNet"
if ThumbWheelInt_6 is not None and Index_8 is not None:
    try:
        masterNet.connectNodes(
            ThumbWheelInt_6, Index_8, "value", "index", blocking=True
            , splitratio=[0.54147444106580034, 0.71421687281773871])
    except:
        print "WARNING: failed to restore connection between ThumbWheelInt_6 and Index_8 in network masterNet"
if Index_8 is not None and Read_Image_2 is not None:
    try:
        masterNet.connectNodes(
            Index_8, Read_Image_2, "data", "filename", blocking=True
            , splitratio=[0.54590743165622846, 0.27798070052767054])
    except:
        print "WARNING: failed to restore connection between Index_8 and Read_Image_2 in network masterNet"
if sub_5 is not None and ThumbWheelInt_6 is not None:
    try:
        masterNet.connectNodes(
            sub_5, ThumbWheelInt_6, "result", "maxi", blocking=True
            , splitratio=[0.16098798849707854, -0.36585365853658564])
    except:
        print "WARNING: failed to restore connection between sub_5 and ThumbWheelInt_6 in network masterNet"
if eval__1_9 is not None and sub_5 is not None:
    try:
        masterNet.connectNodes(
            eval__1_9, sub_5, "result", "data2", blocking=True
            , splitratio=[0.57735369787351343, 0.40279622129828041])
    except:
        print "WARNING: failed to restore connection between eval__1_9 and sub_5 in network masterNet"
if Filelist_10 is not None and len_1 is not None:
    try:
        masterNet.connectNodes(
            Filelist_10, len_1, "filelist", "in1", blocking=True
            , splitratio=[0.74524718813629964, 0.44840132206243905])
    except:
        print "WARNING: failed to restore connection between Filelist_10 and len_1 in network masterNet"
if Filelist_10 is not None and Index_8 is not None:
    try:
        masterNet.connectNodes(
            Filelist_10, Index_8, "filelist", "data", blocking=True
            , splitratio=[1.0, 0.28384215471539581])
    except:
        print "WARNING: failed to restore connection between Filelist_10 and Index_8 in network masterNet"
masterNet.runOnNewData.value = False

if __name__=='__main__':
    from sys import argv
    lNodePortValues = []
    if (len(argv) > 0) and argv[1].startswith('-'):
        lArgIndex = 2
    else:
        lArgIndex = 1
    while lArgIndex < len(argv) and argv[lArgIndex][-3:]!='.py':
        lNodePortValues.append(argv[lArgIndex])
        lArgIndex += 1
    masterNet.setNodePortValues(lNodePortValues)
    if '--help' in argv or '-h' in argv: # show help
        masterNet.helpForNetworkAsApplication()
    elif '-w' in argv: # run without Vision and exit
         # create communicator
        from NetworkEditor.net import Communicator
        masterNet.communicator = Communicator(masterNet)
        print 'Communicator listening on port:', masterNet.communicator.port

        import socket
        f = open(argv[0]+'.sock', 'w')
        f.write("%s %i"%(socket.gethostbyname(socket.gethostname()),
                         masterNet.communicator.port))
        f.close()

        # create communication socket
        import socket
        HOST = ''                 # Symbolic name meaning the local host
        PORT = 50010              # Arbitrary non-privileged port
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.bind((HOST, PORT))
        s.listen(5)
        s.setblocking(0)
        masterNet.socket = s
        masterNet.socketConnections = []
        masterNet.HOST = HOST
        masterNet.PORT = PORT

        masterNet.run()

    else: # stand alone application while vision is hidden
        if '-e' in argv: # run and exit
            masterNet.run()
        elif '-r' in argv or len(masterNet.userPanels) == 0: # no user panel => run
            masterNet.run()
            mainLoopVisionToRunNetworkAsApplication(masterNet.editor)
        else: # user panel
            mainLoopVisionToRunNetworkAsApplication(masterNet.editor)

