#!/bin/ksh ~/.mgltools/pythonsh
########################################################################
#
#    Vision Network - Python source code - file generated by vision
#    Thursday 30 July 2009 16:48:41 
#    
#       The Scripps Research Institute (TSRI)
#       Molecular Graphics Lab
#       La Jolla, CA 92037, USA
#
# Copyright: Daniel Stoffler, Michel Sanner and TSRI
#   
# revision: Guillaume Vareille
#  
#########################################################################
#
# $Header: /opt/cvs/python/packages/share1.5/Vision/doc/Tutorial/isorad/isorad_net.py,v 1.13 2009/07/31 00:54:53 vareille Exp $
#
# $Id: isorad_net.py,v 1.13 2009/07/31 00:54:53 vareille Exp $
#


if __name__=='__main__':
    from sys import argv
    if '--help' in argv or '-h' in argv or '-w' in argv: # run without Vision
        withoutVision = True
        from Vision.VPE import NoGuiExec
        ed = NoGuiExec()
        from NetworkEditor.net import Network
        import os
        masterNet = Network("process-"+str(os.getpid()))
        ed.addNetwork(masterNet)
    else: # run as a stand alone application while vision is hidden
        withoutVision = False
        from Vision import launchVisionToRunNetworkAsApplication, mainLoopVisionToRunNetworkAsApplication
	if '-noSplash' in argv:
	    splash = False
	else:
	    splash = True
        masterNet = launchVisionToRunNetworkAsApplication(splash=splash)
        import os
        masterNet.filename = os.path.abspath(__file__)
from traceback import print_exc
## loading libraries ##
from Vision.StandardNodes import stdlib
from symserv.VisionInterface.SymservNodes import symlib
from DejaVu.VisionInterface.DejaVuNodes import vizlib
from Volume.VisionInterface.VolumeNodes import vollib
try:
    masterNet
except (NameError, AttributeError): # we run the network outside Vision
    from NetworkEditor.net import Network
    masterNet = Network()

masterNet.getEditor().addLibraryInstance(stdlib,"Vision.StandardNodes", "stdlib")

masterNet.getEditor().addLibraryInstance(symlib,"symserv.VisionInterface.SymservNodes", "symlib")

masterNet.getEditor().addLibraryInstance(vizlib,"DejaVu.VisionInterface.DejaVuNodes", "vizlib")

masterNet.getEditor().addLibraryInstance(vollib,"Volume.VisionInterface.VolumeNodes", "vollib")

try:
    ## saving node Isocontour ##
    from Volume.VisionInterface.VolumeNodes import Isocontour
    Isocontour_0 = Isocontour(constrkw={}, name='Isocontour', library=vollib)
    masterNet.addNode(Isocontour_0,171,24)
    apply(Isocontour_0.inputPortByName['isovalue'].widget.configure, (), {'max': 5.0850772857666016, 'min': -7.5834474563598633})
    Isocontour_0.inputPortByName['isovalue'].widget.set(-0.131374078638, run=False)
    Isocontour_0.inputPortByName['calculatesignatures'].widget.set(1, run=False)
    Isocontour_0.inputPortByName['verbosity'].widget.set(0, run=False)
except:
    print "WARNING: failed to restore Isocontour named Isocontour in network masterNet"
    print_exc()
    Isocontour_0=None

try:
    ## saving node indexedPolygons ##
    from DejaVu.VisionInterface.GeometryNodes import IndexedPolygonsNE
    indexedPolygons_1 = IndexedPolygonsNE(constrkw={}, name='indexedPolygons', library=vizlib)
    masterNet.addNode(indexedPolygons_1,42,212)
    indexedPolygons_1.inputPortByName['name'].widget.set(r"", run=False)
    indexedPolygons_1.inputPortByName['geoms'].widget.set(r"indexedPolygons", run=False)
    apply(indexedPolygons_1.configure, (), {'paramPanelImmediate': 1, 'expanded': False})
except:
    print "WARNING: failed to restore IndexedPolygonsNE named indexedPolygons in network masterNet"
    print_exc()
    indexedPolygons_1=None

try:
    ## saving node Grid3DBB ##
    from Volume.VisionInterface.VolumeNodes import BoundingBox
    Grid3DBB_2 = BoundingBox(constrkw={}, name='Grid3DBB', library=vollib)
    masterNet.addNode(Grid3DBB_2,3,130)
    Grid3DBB_2.inputPortByName['name'].widget.set(r"", run=False)
    Grid3DBB_2.inputPortByName['geoms'].widget.set(r"Grid3DBB", run=False)
    apply(Grid3DBB_2.configure, (), {'paramPanelImmediate': 1, 'expanded': False})
except:
    print "WARNING: failed to restore BoundingBox named Grid3DBB in network masterNet"
    print_exc()
    Grid3DBB_2=None

try:
    ## saving node DistanceToPoint ##
    from symserv.VisionInterface.SymservNodes import DistanceToPoint
    DistanceToPoint_3 = DistanceToPoint(constrkw={}, name='DistanceToPoint', library=symlib)
    masterNet.addNode(DistanceToPoint_3,243,208)
except:
    print "WARNING: failed to restore DistanceToPoint named DistanceToPoint in network masterNet"
    print_exc()
    DistanceToPoint_3=None

try:
    ## saving node [0,0,0] ##
    from Vision.StandardNodes import Eval
    _0_0_0__4 = Eval(constrkw={}, name='[0,0,0]', library=stdlib)
    masterNet.addNode(_0_0_0__4,279,151)
    _0_0_0__4.inputPortByName['command'].widget.set(r"[0,0,0]", run=False)
    _0_0_0__4.inputPortByName['importString'].widget.set(r"", run=False)
    apply(_0_0_0__4.configure, (), {'paramPanelImmediate': 1, 'expanded': False})
except:
    print "WARNING: failed to restore Eval named [0,0,0] in network masterNet"
    print_exc()
    _0_0_0__4=None

try:
    ## saving node ReadCCP4 ##
    from Volume.VisionInterface.VolumeNodes import ReadCCP4file
    ReadCCP4_5 = ReadCCP4file(constrkw={}, name='ReadCCP4', library=vollib)
    masterNet.addNode(ReadCCP4_5,18,26)
    ReadCCP4_5.inputPortByName['filename'].widget.set(r"lig.ccp4", run=False)
    ReadCCP4_5.inputPortByName['normalize'].widget.set(1, run=False)
except:
    print "WARNING: failed to restore ReadCCP4file named ReadCCP4 in network masterNet"
    print_exc()
    ReadCCP4_5=None

try:
    ## saving node Color Map ##
    from DejaVu.VisionInterface.DejaVuNodes import ColorMapNE
    Color_Map_6 = ColorMapNE(constrkw={}, name='Color Map', library=vizlib)
    masterNet.addNode(Color_Map_6,181,258)
    Color_Map_6.inputPortByName['colorMap'].widget.set({'mini': 153.47560501003863, 'initialValue': None, 'name': 'cmap', 'ramp': [[1.1920928955078125e-07, 0.0, 1.0, 1.0], [0.0, 0.26666653156280518, 1.0, 1.0], [0.0, 0.5333331823348999, 1.0, 1.0], [0.0, 0.79999983310699463, 1.0, 1.0], [0.0, 1.0, 0.93333333730697632, 1.0], [0.0, 1.0, 0.66666668653488159, 1.0], [0.0, 1.0, 0.40000003576278687, 1.0], [0.0, 1.0, 0.13333338499069214, 1.0], [0.13333326578140259, 1.0, 0.0, 1.0], [0.39999991655349731, 1.0, 0.0, 1.0], [0.66666662693023682, 1.0, 0.0, 1.0], [0.93333327770233154, 1.0, 0.0, 1.0], [1.0, 0.80000007152557373, 0.0, 1.0], [1.0, 0.53333336114883423, 0.0, 1.0], [1.0, 0.26666668057441711, 0.0, 1.0], [1.0, 7.7715611723760958e-16, 0.0, 1.0]], 'labelGridCfg': {'column': 0, 'row': 1}, 'maxi': 288.12698700984026, 'master': 'ParamPanel', 'widgetGridCfg': {'column': 0, 'labelSide': 'top', 'row': 2}, 'labelCfg': {'text': 'colormap'}, 'class': 'NEColorMap'}, run=False)
    Color_Map_6.inputPortByName['filename'].widget.set(r"", run=False)
except:
    print "WARNING: failed to restore ColorMapNE named Color Map in network masterNet"
    print_exc()
    Color_Map_6=None

try:
    ## saving node Set Geom Options ##
    from DejaVu.VisionInterface.DejaVuNodes import GeomOptions
    Set_Geom_Options_7 = GeomOptions(constrkw={}, name='Set Geom Options', library=vizlib)
    masterNet.addNode(Set_Geom_Options_7,105,125)
    Set_Geom_Options_7.inputPortByName['geomOptions'].widget.set({'frontPolyMode': 'line'}, run=False)
except:
    print "WARNING: failed to restore GeomOptions named Set Geom Options in network masterNet"
    print_exc()
    Set_Geom_Options_7=None

try:
    ## saving node Viewer ##
    from DejaVu.VisionInterface.DejaVuNodes import Viewer
    Viewer_8 = Viewer(constrkw={}, name='Viewer', library=vizlib)
    masterNet.addNode(Viewer_8,21,291)
    ##
        ## Saving State for Viewer
    Viewer_8.vi.TransformRootOnly(1)
    ##

    ## Light Model
    ## End Light Model

    ## Light sources
    ## End Light sources 7

    ## Cameras
    ## Camera Number 0
    state = {'color': (0.0, 0.0, 0.0, 1.0), 'd2off': 1, 'height': 400, 'lookAt': [0.0, 0.0, 0.0], 'rootx': 786, 'pivot': [0.0, 0.0, 0.0], 'translation': [0.0, 0.0, 0.0], 'sideBySideTranslation': 0.0, 'fov': 40.011250908291053, 'scale': [1.0, 1.0, 1.0], 'stereoMode': 'MONO', 'width': 400, 'sideBySideRotAngle': 3.0, 'boundingbox': 0, 'projectionType': 0, 'contours': False, 'd2cutL': 150, 'direction': [0.0, 0.0, -30.0], 'd2cutH': 255, 'far': 50.0, 'd1off': 4, 'lookFrom': [0.0, 0.0, 30.0], 'd1cutH': 60, 'antialiased': 0, 'rotation': [1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0], 'd1ramp': [0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 3.0, 3.0, 3.0, 3.0, 3.0, 3.0, 4.0, 4.0, 4.0, 4.0, 7.0, 9.0, 12.0, 14.0, 17.0, 19.0, 22.0, 24.0, 27.0, 29.0, 32.0, 34.0, 37.0, 44.0, 51.0, 57.0, 64.0, 71.0, 78.0, 84.0, 91.0, 98.0, 105.0, 111.0, 118.0, 125.0, 126.0, 128.0, 129.0, 130.0, 132.0, 133.0, 135.0, 136.0, 137.0, 139.0, 140.0, 141.0, 143.0, 144.0, 145.0, 147.0, 148.0, 149.0, 151.0, 152.0, 154.0, 155.0, 156.0, 158.0, 159.0, 160.0, 162.0, 163.0, 164.0, 166.0, 167.0, 168.0, 170.0, 171.0, 173.0, 174.0, 175.0, 177.0, 178.0, 179.0, 181.0, 182.0, 183.0, 185.0, 186.0, 187.0, 189.0, 190.0, 192.0, 193.0, 194.0, 196.0, 197.0, 197.0, 198.0, 198.0, 199.0, 199.0, 199.0, 200.0, 200.0, 200.0, 201.0, 201.0, 202.0, 202.0, 202.0, 203.0, 203.0, 204.0, 204.0, 204.0, 205.0, 205.0, 205.0, 206.0, 206.0, 207.0, 207.0, 207.0, 208.0, 208.0, 209.0, 209.0, 209.0, 210.0, 210.0, 210.0, 211.0, 211.0, 212.0, 212.0, 212.0, 213.0, 213.0, 214.0, 214.0, 214.0, 215.0, 215.0, 215.0, 216.0, 216.0, 217.0, 217.0, 217.0, 218.0, 218.0, 219.0, 219.0, 219.0, 220.0, 220.0, 220.0, 221.0, 221.0, 222.0, 222.0, 222.0, 223.0, 223.0, 224.0, 224.0, 224.0, 225.0, 225.0, 225.0, 226.0, 226.0, 227.0, 227.0, 227.0, 228.0, 228.0, 228.0, 229.0, 229.0, 230.0, 230.0, 230.0, 231.0, 231.0, 232.0, 232.0, 232.0, 233.0, 233.0, 233.0, 234.0, 234.0, 235.0, 235.0, 235.0, 236.0, 236.0, 237.0, 237.0, 237.0, 238.0, 238.0, 238.0, 239.0, 239.0, 240.0, 240.0, 240.0, 241.0, 241.0, 242.0, 242.0, 242.0, 243.0, 243.0, 243.0, 244.0, 244.0, 245.0, 245.0, 245.0, 246.0, 246.0, 247.0, 247.0, 247.0, 248.0, 248.0, 248.0, 249.0, 249.0, 250.0, 250.0, 250.0, 251.0, 251.0, 252.0, 252.0, 252.0, 253.0, 253.0, 253.0, 254.0, 254.0, 255.0, 255.0], 'suspendRedraw': False, 'd1cutL': 0, 'd2scale': 0.0, 'near': 0.10000000000000001, 'drawThumbnail': False, 'rooty': 135, 'd1scale': 0.012999999999999999}
    apply(Viewer_8.vi.cameras[0].Set, (), state)

    state = {'end': 43.551136183847859, 'density': 0.10000000000000001, 'color': (0.0, 0.0, 0.0, 1.0), 'enabled': 1, 'start': 17.079126677341922, 'mode': 'GL_LINEAR'}
    apply(Viewer_8.vi.cameras[0].fog.Set, (), state)

    ## End Cameras

    ## Clipping planes
    ## End Clipping planes

    ## Root object
    state = {'scissorAspectRatio': 1.0, 'inheritStippleLines': 0, 'stippleLines': False, 'disableStencil': False, 'replace': True, 'visible': True, 'immediateRendering': False, 'inheritLighting': False, 'invertNormals': False, 'pivot': [70.136146545410156, 119.68631744384766, 96.355606079101562], 'rotation': [-0.91799319, 0.31828204, -0.23661187, 0.0, 0.12465535, 0.7979309, 0.5897181, 0.0, 0.37649655, 0.51186222, -0.7721706, 0.0, 0.0, 0.0, 0.0, 1.0], 'instanceMatricesFromFortran': [[1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0]], 'scissorH': 200, 'frontPolyMode': 'fill', 'blendFunctions': ('GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA'), 'outline': False, 'vertexArrayFlag': False, 'scissorX': 0, 'scissorY': 0, 'listed': True, 'inheritPointWidth': 0, 'pickable': 1, 'pointWidth': 3, 'scissorW': 200, 'needsRedoDpyListOnResize': False, 'stipplePolygons': False, 'pickableVertices': False, 'inheritMaterial': False, 'depthMask': 1, 'inheritSharpColorBoundaries': False, 'scale': [0.12026986, 0.12026986, 0.12026986], 'lighting': True, 'inheritCulling': False, 'inheritShading': False, 'shading': 'smooth', 'translation': [-69.665695, -122.89187, -96.355606], 'transparent': False, 'sharpColorBoundaries': True, 'culling': 'back', 'name': 'root', 'backPolyMode': 'fill', 'inheritFrontPolyMode': False, 'inheritStipplePolygons': 0, 'inheritBackPolyMode': False, 'scissor': 0, 'inheritLineWidth': 0, 'lineWidth': 1, 'inheritXform': 0}
    apply(Viewer_8.vi.rootObject.Set, (), state)

    ## End Root Object

    ## Material for root
    if Viewer_8.vi.rootObject:
        pass  ## needed in case there no modif
    ## End Materials for root

    ## Clipping Planes for root
    if Viewer_8.vi.rootObject:
        Viewer_8.vi.rootObject.clipP = []
        Viewer_8.vi.rootObject.clipPI = []
        pass  ## needed in case there no modif
    ## End Clipping Planes for root

except:
    print "WARNING: failed to restore Viewer named Viewer in network masterNet"
    print_exc()
    Viewer_8=None

try:
    ## saving node OrthoSlice ##
    from Volume.VisionInterface.VolumeNodes import OrthoSlice
    OrthoSlice_9 = OrthoSlice(constrkw={}, name='OrthoSlice', library=vollib)
    masterNet.addNode(OrthoSlice_9,229,104)
    OrthoSlice_9.inputPortByName['axis'].widget.set(r"z", run=False)
    apply(OrthoSlice_9.inputPortByName['sliceNumber'].widget.configure, (), {'max': 58})
    OrthoSlice_9.inputPortByName['sliceNumber'].widget.set(12, run=False)
    OrthoSlice_9.inputPortByName['colorMap'].widget.set({'mini': -7.5834474563598633, 'initialValue': None, 'name': 'cmap', 'ramp': [[1.1920928955078125e-07, 0.0, 1.0, 1.0], [0.0, 0.26666653156280518, 1.0, 1.0], [0.0, 0.5333331823348999, 1.0, 1.0], [0.0, 0.79999983310699507, 1.0, 1.0], [0.0, 1.0, 0.93333333730697643, 1.0], [0.0, 1.0, 0.6666666865348817, 1.0], [0.0, 1.0, 0.40000003576278698, 1.0], [0.0, 1.0, 0.13333338499069225, 1.0], [0.13333326578140148, 1.0, 0.0, 1.0], [0.3999999165534962, 1.0, 0.0, 0.0], [0.66666662693023615, 1.0, 0.0, 1.0], [0.93333327770233154, 1.0, 0.0, 1.0], [1.0, 0.80000007152557373, 0.0, 1.0], [1.0, 0.53333336114883423, 0.0, 1.0], [1.0, 0.26666668057441711, 0.0, 1.0], [1.0, 7.7715611723760958e-16, 0.0, 1.0]], 'labelGridCfg': {'column': 0, 'row': 2}, 'maxi': 5.0850772857666016, 'master': 'ParamPanel', 'widgetGridCfg': {'column': 1, 'labelSide': 'left', 'row': 2}, 'labelCfg': {'text': 'colorMap'}, 'class': 'NEColorMap'}, run=False)
    OrthoSlice_9.inputPortByName['transparency'].widget.set(r"alpha", run=False)
    OrthoSlice_9.inputPortByName['name'].widget.set(r"", run=False)
    OrthoSlice_9.inputPortByName['geoms'].widget.set(r"OrthoSlice", run=False)
    apply(OrthoSlice_9.configure, (), {'paramPanelImmediate': 1, 'expanded': False})
except:
    print "WARNING: failed to restore OrthoSlice named OrthoSlice in network masterNet"
    print_exc()
    OrthoSlice_9=None

#masterNet.run()
masterNet.freeze()

## saving connections for network isorad ##
if Isocontour_0 is not None and indexedPolygons_1 is not None:
    try:
        masterNet.connectNodes(
            Isocontour_0, indexedPolygons_1, "coords", "coords", blocking=True
            , splitratio=[0.44620984620379395, 0.66406417981112509])
    except:
        print "WARNING: failed to restore connection between Isocontour_0 and indexedPolygons_1 in network masterNet"
if Isocontour_0 is not None and indexedPolygons_1 is not None:
    try:
        masterNet.connectNodes(
            Isocontour_0, indexedPolygons_1, "indices", "indices", blocking=True
            , splitratio=[0.59691666041602032, 0.44447724434572577])
    except:
        print "WARNING: failed to restore connection between Isocontour_0 and indexedPolygons_1 in network masterNet"
if Isocontour_0 is not None and indexedPolygons_1 is not None:
    try:
        masterNet.connectNodes(
            Isocontour_0, indexedPolygons_1, "normals", "vnormals", blocking=True
            , splitratio=[0.74070586411187955, 0.3021348326516301])
    except:
        print "WARNING: failed to restore connection between Isocontour_0 and indexedPolygons_1 in network masterNet"
if Isocontour_0 is not None and DistanceToPoint_3 is not None:
    try:
        masterNet.connectNodes(
            Isocontour_0, DistanceToPoint_3, "coords", "coords", blocking=True
            , splitratio=[0.59054721819879141, 0.35171170389657536])
    except:
        print "WARNING: failed to restore connection between Isocontour_0 and DistanceToPoint_3 in network masterNet"
if _0_0_0__4 is not None and DistanceToPoint_3 is not None:
    try:
        masterNet.connectNodes(
            _0_0_0__4, DistanceToPoint_3, "result", "point", blocking=True
            , splitratio=[0.59463499116988094, 0.50165256644915523])
    except:
        print "WARNING: failed to restore connection between _0_0_0__4 and DistanceToPoint_3 in network masterNet"
if ReadCCP4_5 is not None and Grid3DBB_2 is not None:
    try:
        masterNet.connectNodes(
            ReadCCP4_5, Grid3DBB_2, "grid", "grid", blocking=True
            , splitratio=[0.42748622347015086, 0.31420001073258191])
    except:
        print "WARNING: failed to restore connection between ReadCCP4_5 and Grid3DBB_2 in network masterNet"
if ReadCCP4_5 is not None and Isocontour_0 is not None:
    try:
        masterNet.connectNodes(
            ReadCCP4_5, Isocontour_0, "grid", "grid3D", blocking=True
            , splitratio=[0.51719067491844672, 0.59899795623272356])
    except:
        print "WARNING: failed to restore connection between ReadCCP4_5 and Isocontour_0 in network masterNet"
if DistanceToPoint_3 is not None and Color_Map_6 is not None:
    try:
        masterNet.connectNodes(
            DistanceToPoint_3, Color_Map_6, "dist", "values", blocking=True
            , splitratio=[0.2293481115082279, 0.57120388884860152])
    except:
        print "WARNING: failed to restore connection between DistanceToPoint_3 and Color_Map_6 in network masterNet"
if Color_Map_6 is not None and indexedPolygons_1 is not None:
    try:
        masterNet.connectNodes(
            Color_Map_6, indexedPolygons_1, "mappedColors", "colors", blocking=True
            , splitratio=[0.39509178986670201, 0.33853115465247008])
    except:
        print "WARNING: failed to restore connection between Color_Map_6 and indexedPolygons_1 in network masterNet"
if Set_Geom_Options_7 is not None and indexedPolygons_1 is not None:
    try:
        masterNet.connectNodes(
            Set_Geom_Options_7, indexedPolygons_1, "geomOptions", "geomOptions", blocking=True
            , splitratio=[0.58425977934923246, 0.33694869706627129])
    except:
        print "WARNING: failed to restore connection between Set_Geom_Options_7 and indexedPolygons_1 in network masterNet"
if Grid3DBB_2 is not None and Viewer_8 is not None:
    try:
        masterNet.connectNodes(
            Grid3DBB_2, Viewer_8, "Grid3DBB", "geometries", blocking=True
            , splitratio=[0.72273076667065173, 0.58624830826571417])
    except:
        print "WARNING: failed to restore connection between Grid3DBB_2 and Viewer_8 in network masterNet"
if Color_Map_6 is not None and Viewer_8 is not None:
    try:
        masterNet.connectNodes(
            Color_Map_6, Viewer_8, "legend", "geometries", blocking=True
            , splitratio=[0.29499947220113537, 0.32749932557137806])
    except:
        print "WARNING: failed to restore connection between Color_Map_6 and Viewer_8 in network masterNet"
if indexedPolygons_1 is not None and Viewer_8 is not None:
    try:
        masterNet.connectNodes(
            indexedPolygons_1, Viewer_8, "indexedPolygons", "geometries", blocking=True
            , splitratio=[0.43346151910852443, 0.3449797916387507])
    except:
        print "WARNING: failed to restore connection between indexedPolygons_1 and Viewer_8 in network masterNet"
if ReadCCP4_5 is not None and OrthoSlice_9 is not None:
    try:
        masterNet.connectNodes(
            ReadCCP4_5, OrthoSlice_9, "grid", "grid", blocking=True
            , splitratio=[0.42341328945300333, 0.44667633736132406])
    except:
        print "WARNING: failed to restore connection between ReadCCP4_5 and OrthoSlice_9 in network masterNet"
if OrthoSlice_9 is not None and Viewer_8 is not None:
    try:
        masterNet.connectNodes(
            OrthoSlice_9, Viewer_8, "OrthoSlice", "geometries", blocking=True
            , splitratio=[0.28124654350621692, 0.42435482413998848])
    except:
        print "WARNING: failed to restore connection between OrthoSlice_9 and Viewer_8 in network masterNet"
masterNet.runOnNewData.value = False


def loadSavedStates_Viewer_8(self=Viewer_8, event=None):
    ##
    ## Saving State for objects in Viewer
    ##

    ## Object root|cmap
    state = {'needsRedoDpyListOnResize': True, 'name': 'cmap', 'replace': False, 'visible': 1, 'immediateRendering': False, 'listed': True, 'anchor': [1, 0], 'position': [1, 0], 'protected': 0, 'transparent': True, 'pickable': True, 'size': [12, 120]}
    obj = self.vi.FindObjectByName('root|cmap')
    if obj:
        apply(obj.Set, (), state)

    ## Object root|Grid3DBB
    state = {'scissorAspectRatio': 1.0, 'inheritStippleLines': True, 'stippleLines': False, 'disableStencil': False, 'replace': False, 'visible': True, 'immediateRendering': False, 'inheritLighting': True, 'invertNormals': False, 'pivot': [0.0, 0.0, 0.0], 'rotation': [1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0], 'instanceMatricesFromFortran': [[1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0]], 'scissorH': 200, 'frontPolyMode': 'line', 'blendFunctions': ('GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA'), 'outline': False, 'vertexArrayFlag': False, 'scissorX': 0, 'scissorY': 0, 'listed': True, 'inheritPointWidth': True, 'pickable': True, 'pointWidth': 2, 'scissorW': 200, 'needsRedoDpyListOnResize': False, 'stipplePolygons': False, 'pickableVertices': True, 'inheritMaterial': 0, 'depthMask': 1, 'inheritSharpColorBoundaries': True, 'scale': [1.0, 1.0, 1.0], 'lighting': True, 'inheritCulling': True, 'inheritShading': True, 'shading': 'smooth', 'translation': [0.0, 0.0, 0.0], 'transparent': 0, 'sharpColorBoundaries': True, 'culling': 'back', 'name': 'Grid3DBB', 'backPolyMode': 'fill', 'inheritFrontPolyMode': False, 'inheritStipplePolygons': True, 'inheritBackPolyMode': True, 'scissor': 0, 'protected': 0, 'inheritLineWidth': True, 'lineWidth': 2, 'inheritXform': 1}
    obj = self.vi.FindObjectByName('root|Grid3DBB')
    if obj:
        apply(obj.Set, (), state)

    ## Material for Grid3DBB
    if obj:
        from opengltk.OpenGL import GL
        state = {'shininess': [50.0], 'specular': [[0.89999997615814209, 0.89999997615814209, 0.89999997615814209, 1.0]], 'binding': [10.0, 12.0, 10.0, 10.0, 10.0, 10.0], 'emission': [[0.0, 0.0, 0.0, 1.0]], 'ambient': [[0.10000000149011612, 0.10000000149011612, 0.10000000149011612, 1.0]]}
        apply(obj.materials[GL.GL_FRONT].Set, (), state)

        pass  ## needed in case there no modif
    ## End Materials for Grid3DBB

    ## Clipping Planes for Grid3DBB
    if obj:
        obj.clipP = []
        obj.clipPI = []
        pass  ## needed in case there no modif
    ## End Clipping Planes for Grid3DBB

    ## Object root|indexedPolygons
    state = {'scissorAspectRatio': 1.0, 'inheritStippleLines': True, 'stippleLines': False, 'disableStencil': False, 'replace': False, 'visible': True, 'immediateRendering': False, 'inheritLighting': True, 'invertNormals': False, 'pivot': [0.0, 0.0, 0.0], 'rotation': [1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0], 'instanceMatricesFromFortran': [[1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0]], 'scissorH': 200, 'frontPolyMode': 'line', 'blendFunctions': ('GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA'), 'outline': False, 'vertexArrayFlag': False, 'scissorX': 0, 'scissorY': 0, 'listed': True, 'inheritPointWidth': True, 'pickable': True, 'pointWidth': 2, 'scissorW': 200, 'needsRedoDpyListOnResize': False, 'stipplePolygons': False, 'pickableVertices': True, 'inheritMaterial': False, 'depthMask': 1, 'inheritSharpColorBoundaries': True, 'scale': [1.0, 1.0, 1.0], 'lighting': True, 'inheritCulling': True, 'inheritShading': True, 'shading': 'smooth', 'translation': [0.0, 0.0, 0.0], 'transparent': 0, 'sharpColorBoundaries': True, 'culling': 'back', 'name': 'indexedPolygons', 'backPolyMode': 'fill', 'inheritFrontPolyMode': False, 'inheritStipplePolygons': True, 'inheritBackPolyMode': True, 'scissor': 0, 'protected': False, 'inheritLineWidth': True, 'lineWidth': 2, 'inheritXform': 1}
    obj = self.vi.FindObjectByName('root|indexedPolygons')
    if obj:
        apply(obj.Set, (), state)

    ## Material for indexedPolygons
    if obj:
        pass  ## needed in case there no modif
    ## End Materials for indexedPolygons

    ## Clipping Planes for indexedPolygons
    if obj:
        obj.clipP = []
        obj.clipPI = []
        pass  ## needed in case there no modif
    ## End Clipping Planes for indexedPolygons

    ## Object root|OrthoSlice
    state = {'scissorAspectRatio': 1.0, 'inheritStippleLines': True, 'stippleLines': False, 'disableStencil': False, 'replace': False, 'visible': True, 'immediateRendering': False, 'inheritLighting': False, 'invertNormals': False, 'pivot': [0.0, 0.0, 0.0], 'rotation': [1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0], 'instanceMatricesFromFortran': [[1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0]], 'scissorH': 200, 'frontPolyMode': 'fill', 'blendFunctions': ('GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA'), 'outline': False, 'vertexArrayFlag': False, 'scissorX': 0, 'scissorY': 0, 'listed': True, 'inheritPointWidth': True, 'pickable': True, 'pointWidth': 2, 'scissorW': 200, 'needsRedoDpyListOnResize': False, 'stipplePolygons': 0, 'pickableVertices': True, 'inheritMaterial': 0, 'depthMask': 1, 'inheritSharpColorBoundaries': True, 'scale': [1.0, 1.0, 1.0], 'lighting': 0, 'inheritCulling': False, 'inheritShading': False, 'shading': 'flat', 'translation': [0.0, 0.0, 0.0], 'transparent': 0, 'sharpColorBoundaries': True, 'culling': 'none', 'name': 'OrthoSlice', 'backPolyMode': 'fill', 'inheritFrontPolyMode': False, 'inheritStipplePolygons': True, 'inheritBackPolyMode': False, 'scissor': 0, 'protected': 0, 'inheritLineWidth': True, 'lineWidth': 2, 'inheritXform': 1}
    obj = self.vi.FindObjectByName('root|OrthoSlice')
    if obj:
        apply(obj.Set, (), state)

    ## Material for OrthoSlice
    if obj:
        pass  ## needed in case there no modif
    ## End Materials for OrthoSlice

    ## Clipping Planes for OrthoSlice
    if obj:
        obj.clipP = []
        obj.clipPI = []
        pass  ## needed in case there no modif
    ## End Clipping Planes for OrthoSlice

    ## End Object root|OrthoSlice

    ##
        ## Saving State for Viewer
    self.vi.TransformRootOnly(1)
    ##

    ## Light Model
    ## End Light Model

    ## Light sources
    ## End Light sources 7

    ## Cameras
    ## Camera Number 0
    state = {'color': (0.0, 0.0, 0.0, 1.0), 'd2off': 1, 'height': 400, 'lookAt': [0.0, 0.0, 0.0], 'pivot': [0.0, 0.0, 0.0], 'translation': [0.0, 0.0, 0.0], 'sideBySideTranslation': 0.0, 'fov': 40.011250908291053, 'scale': [1.0, 1.0, 1.0], 'stereoMode': 'MONO', 'width': 400, 'sideBySideRotAngle': 3.0, 'boundingbox': 0, 'projectionType': 0, 'contours': False, 'd2cutL': 150, 'direction': [0.0, 0.0, -30.0], 'd2cutH': 255, 'far': 50.0, 'd1off': 4, 'lookFrom': [0.0, 0.0, 30.0], 'd1cutH': 60, 'antialiased': 0, 'rotation': [1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0], 'd1ramp': [0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 3.0, 3.0, 3.0, 3.0, 3.0, 3.0, 4.0, 4.0, 4.0, 4.0, 7.0, 9.0, 12.0, 14.0, 17.0, 19.0, 22.0, 24.0, 27.0, 29.0, 32.0, 34.0, 37.0, 44.0, 51.0, 57.0, 64.0, 71.0, 78.0, 84.0, 91.0, 98.0, 105.0, 111.0, 118.0, 125.0, 126.0, 128.0, 129.0, 130.0, 132.0, 133.0, 135.0, 136.0, 137.0, 139.0, 140.0, 141.0, 143.0, 144.0, 145.0, 147.0, 148.0, 149.0, 151.0, 152.0, 154.0, 155.0, 156.0, 158.0, 159.0, 160.0, 162.0, 163.0, 164.0, 166.0, 167.0, 168.0, 170.0, 171.0, 173.0, 174.0, 175.0, 177.0, 178.0, 179.0, 181.0, 182.0, 183.0, 185.0, 186.0, 187.0, 189.0, 190.0, 192.0, 193.0, 194.0, 196.0, 197.0, 197.0, 198.0, 198.0, 199.0, 199.0, 199.0, 200.0, 200.0, 200.0, 201.0, 201.0, 202.0, 202.0, 202.0, 203.0, 203.0, 204.0, 204.0, 204.0, 205.0, 205.0, 205.0, 206.0, 206.0, 207.0, 207.0, 207.0, 208.0, 208.0, 209.0, 209.0, 209.0, 210.0, 210.0, 210.0, 211.0, 211.0, 212.0, 212.0, 212.0, 213.0, 213.0, 214.0, 214.0, 214.0, 215.0, 215.0, 215.0, 216.0, 216.0, 217.0, 217.0, 217.0, 218.0, 218.0, 219.0, 219.0, 219.0, 220.0, 220.0, 220.0, 221.0, 221.0, 222.0, 222.0, 222.0, 223.0, 223.0, 224.0, 224.0, 224.0, 225.0, 225.0, 225.0, 226.0, 226.0, 227.0, 227.0, 227.0, 228.0, 228.0, 228.0, 229.0, 229.0, 230.0, 230.0, 230.0, 231.0, 231.0, 232.0, 232.0, 232.0, 233.0, 233.0, 233.0, 234.0, 234.0, 235.0, 235.0, 235.0, 236.0, 236.0, 237.0, 237.0, 237.0, 238.0, 238.0, 238.0, 239.0, 239.0, 240.0, 240.0, 240.0, 241.0, 241.0, 242.0, 242.0, 242.0, 243.0, 243.0, 243.0, 244.0, 244.0, 245.0, 245.0, 245.0, 246.0, 246.0, 247.0, 247.0, 247.0, 248.0, 248.0, 248.0, 249.0, 249.0, 250.0, 250.0, 250.0, 251.0, 251.0, 252.0, 252.0, 252.0, 253.0, 253.0, 253.0, 254.0, 254.0, 255.0, 255.0], 'suspendRedraw': False, 'd1cutL': 0, 'd2scale': 0.0, 'near': 0.10000000000000001, 'drawThumbnail': False, 'd1scale': 0.012999999999999999}
    apply(self.vi.cameras[0].Set, (), state)

    state = {'end': 43.551136183847859, 'density': 0.10000000000000001, 'color': (0.0, 0.0, 0.0, 1.0), 'enabled': 1, 'start': 17.079126677341922, 'mode': 'GL_LINEAR'}
    apply(self.vi.cameras[0].fog.Set, (), state)

    ## End Cameras

    ## Clipping planes
    ## End Clipping planes

    ## Root object
    state = {'scissorAspectRatio': 1.0, 'inheritStippleLines': 0, 'stippleLines': False, 'disableStencil': False, 'replace': True, 'visible': True, 'immediateRendering': False, 'inheritLighting': False, 'invertNormals': False, 'pivot': [70.136146545410156, 119.68631744384766, 96.355606079101562], 'rotation': [-0.91799319, 0.31828204, -0.23661187, 0.0, 0.12465535, 0.7979309, 0.5897181, 0.0, 0.37649655, 0.51186222, -0.7721706, 0.0, 0.0, 0.0, 0.0, 1.0], 'instanceMatricesFromFortran': [[1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0]], 'scissorH': 200, 'frontPolyMode': 'fill', 'blendFunctions': ('GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA'), 'outline': False, 'vertexArrayFlag': False, 'scissorX': 0, 'scissorY': 0, 'listed': True, 'inheritPointWidth': 0, 'pickable': 1, 'pointWidth': 3, 'scissorW': 200, 'needsRedoDpyListOnResize': False, 'stipplePolygons': False, 'pickableVertices': False, 'inheritMaterial': False, 'depthMask': 1, 'inheritSharpColorBoundaries': False, 'scale': [0.12026986, 0.12026986, 0.12026986], 'lighting': True, 'inheritCulling': False, 'inheritShading': False, 'shading': 'smooth', 'translation': [-69.665695, -122.89187, -96.355606], 'transparent': False, 'sharpColorBoundaries': True, 'culling': 'back', 'name': 'root', 'backPolyMode': 'fill', 'inheritFrontPolyMode': False, 'inheritStipplePolygons': 0, 'inheritBackPolyMode': False, 'scissor': 0, 'inheritLineWidth': 0, 'lineWidth': 1, 'inheritXform': 0}
    apply(self.vi.rootObject.Set, (), state)

    ## End Root Object

    ## Material for root
    if self.vi.rootObject:
        pass  ## needed in case there no modif
    ## End Materials for root

    ## Clipping Planes for root
    if self.vi.rootObject:
        self.vi.rootObject.clipP = []
        self.vi.rootObject.clipPI = []
        pass  ## needed in case there no modif
    ## End Clipping Planes for root

Viewer_8.restoreStates_cb = Viewer_8.restoreStatesFirstRun = loadSavedStates_Viewer_8
Viewer_8.menu.add_separator()
Viewer_8.menu.add_command(label='Restore states', command=Viewer_8.restoreStates_cb)

if __name__=='__main__':
    from sys import argv
    lNodePortValues = []
    if (len(argv) > 0) and argv[1].startswith('-'):
        lArgIndex = 2
    else:
        lArgIndex = 1
    while lArgIndex < len(argv) and argv[lArgIndex][-3:]!='.py':
        lNodePortValues.append(argv[lArgIndex])
        lArgIndex += 1
    masterNet.setNodePortValues(lNodePortValues)
    if '--help' in argv or '-h' in argv: # show help
        masterNet.helpForNetworkAsApplication()
    elif '-w' in argv: # run without Vision and exit
         # create communicator
        from NetworkEditor.net import Communicator
        masterNet.communicator = Communicator(masterNet)
        print 'Communicator listening on port:', masterNet.communicator.port

        import socket
        f = open(argv[0]+'.sock', 'w')
        f.write("%s %i"%(socket.gethostbyname(socket.gethostname()),
                         masterNet.communicator.port))
        f.close()

        # create communication socket
        import socket
        HOST = ''                 # Symbolic name meaning the local host
        PORT = 50010              # Arbitrary non-privileged port
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.bind((HOST, PORT))
        s.listen(5)
        s.setblocking(0)
        masterNet.socket = s
        masterNet.socketConnections = []
        masterNet.HOST = HOST
        masterNet.PORT = PORT

        masterNet.run()

    else: # stand alone application while vision is hidden
        if '-e' in argv: # run and exit
            masterNet.run()
        elif '-r' in argv or len(masterNet.userPanels) == 0: # no user panel => run
            masterNet.run()
            mainLoopVisionToRunNetworkAsApplication(masterNet.editor)
        else: # user panel
            mainLoopVisionToRunNetworkAsApplication(masterNet.editor)

