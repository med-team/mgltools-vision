## Network: dial
## file written by Vision
##

## saving node Dial
##
# loading library stdlib
from Vision.StandardNodes import stdlib
masterNet.editor.addLibrary(stdlib)

from Vision.StandardNodes import DialNE
node0 = DialNE(constrkw = {}, name='Dial', library=stdlib)
masterNet.addNode(node0,171,38)
widget = node0.inputPorts[0].widget
cfg = {'oneTurn': 100.0, 'lockType': 0, 'type': 'int', 'lockValue': 0, 'lockPrecision': 0, 'increment': 0.0, 'lockOneTurn': 0, 'lockShowLabel': 0, 'showLabel': 1, 'precision': 2, 'lockMin': 1, 'continuous': 1, 'max': None, 'lockBIncrement': 0, 'min': None, 'lockBMax': 0, 'lockBMin': 0, 'lockMax': 1, 'lockIncrement': 1, 'lockContinuous': 0}
apply( widget.configure, (), cfg)
node0.createOneWidget(node0.inputPorts[0])
widget = node0.inputPorts[0].widget
if restoreWidgetValues:
	widget.set(42,0)

## saving node print
##
from Vision.StandardNodes import Print
node1 = Print(constrkw = {}, name='print', library=stdlib)
masterNet.addNode(node1,173,199)

## saving connections for network dial
##
masterNet.connectNodes(node0, node1,0,0)
