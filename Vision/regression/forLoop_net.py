## Network: Network 0
## file written by Vision
##

## saving node Iterate
##
# loading library stdlib
from Vision.StandardNodes import stdlib
masterNet.editor.addLibrary(stdlib)

from Vision.StandardNodes import Iterate
node0 = Iterate(constrkw = {}, name='Iterate', library=stdlib)
masterNet.addNode(node0,240,150)

## saving node eval
##
from Vision.StandardNodes import Eval
node1 = Eval(constrkw = {}, name='eval', library=stdlib)
masterNet.addNode(node1,189,70)
widget = node1.inputPorts[0].widget
cfg = {}
apply( widget.configure, (), cfg)
node1.showInNodeWidget(node1.inputPorts[0].widget)
widget = node1.inputPorts[0].widget
if restoreWidgetValues:
	widget.set("range(10)",0)

## saving node print
##
from Vision.StandardNodes import Print
node2 = Print(constrkw = {}, name='print', library=stdlib)
masterNet.addNode(node2,192,225)

## saving connections for network Network 0
##
masterNet.connectNodes(node1, node0,0,0)
masterNet.connectNodes(node0, node2,0,0)
