import sys
from mglutil.regression import testplus

import test_basics
import test_libraries
import test_PIL

harness = testplus.TestHarness( __name__,
                                funs = [],
                                dependents = [test_basics.harness,
                                              test_libraries.harness,
                                              test_PIL.harness,
                                              ],
                                )

if __name__ == '__main__':
    testplus.chdir()
    print harness
    sys.exit( len( harness))

