#########################################################################
#
# Date: Oct. 2002  Authors: Daniel Stoffler, Michel Sanner
#
#       stoffler@scripps.edu
#       sanner@scripps.edu
#
# Copyright: Daniel Stoffler, Michel Sanner and TSRI
#
#########################################################################

import sys, string
from mglutil.regression import testplus
ed = None
withThreads = 0 # default is: multi-threading off

# allow for additional user input
if len(sys.argv):
    for myArg in sys.argv[1:]:
        if myArg[:11] == 'withThreads':
            withThreads = int(string.strip(myArg)[-1])


def startEditor():
    global ed
    from Vision.VPE import VisualProgramingEnvironment
    ed = VisualProgramingEnvironment(name='Vision', withShell=0)
    ed.root.update_idletasks()
    ed.configure(withThreads=withThreads)


def quitEditor():
    ed.master.after(1000, ed.exit_cb )


def pause(sleepTime=0.4):
    from time import sleep
    ed.master.update()
    sleep(sleepTime)


def test_saveImage():
    # test if the save image node works
    from NetworkEditor.net import Network
    net = Network('testing iamge')
    ed.addNetwork(net)
    ed.setNetwork(net)
    from Vision.PILNodes import ReadImage, SaveImage
    node1 = ReadImage()
    node2 = SaveImage()
    net.addNode(node1,75,75)
    net.addNode(node2,75,175)
    conn1 = net.connectNodes(node1, node2, 0, 0)
    node1.inputPorts[0].widget.set("lena.jpg")
    node2.inputPorts[1].widget.set("lenaSaved.jpg")
    import os
    assert os.path.exists("lenaSaved.jpg")
    os.system("rm -f lenaSaved.jpg")
    

harness = testplus.TestHarness( "test_PIL",
                                connect = (startEditor, (), {}),
                                funs = testplus.testcollect( globals()),
                                disconnect = quitEditor
                                )

if __name__ == '__main__':
    testplus.chdir()
    print harness
    sys.exit( len( harness))

