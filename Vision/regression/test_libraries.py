#########################################################################
#
# Date: Oct. 2002  Authors: Daniel Stoffler, Michel Sanner
#
#       stoffler@scripps.edu
#       sanner@scripps.edu
#
# Copyright: Daniel Stoffler, Michel Sanner and TSRI
#
#########################################################################

import sys, string
from mglutil.regression import testplus
ed = None
withThreads = 0 # default is: multi-threading off

# allow for additional user input
if len(sys.argv):
    for myArg in sys.argv[1:]:
        if myArg[:11] == 'withThreads':
            withThreads = int(string.strip(myArg)[-1])
           

def startEditor():
    global ed
    from Vision.VPE import VisualProgramingEnvironment
    ed = VisualProgramingEnvironment(name='Vision', withShell=0)
    ed.root.update_idletasks()
    ed.configure(withThreads=withThreads)
    pause()


def quitEditor():
    ed.master.after(1000, ed.exit_cb )


def pause(sleepTime=0.4):
    from time import sleep
    ed.master.update()
    sleep(sleepTime)


def test_loadStandardLibrary():
    from Vision.StandardNodes import stdlib
    ed.addLibrary(stdlib)


def test_loadMolkitLib():
    try:
        from MolKit.VisionInterface.MolKitNodes import molkitlib
        ed.addLibrary(molkitlib)
    except:
        print 'MOLKITLIB NOT FOUND!'


def test_loadVizLib():
    try:
        from DejaVu.VisionInterface.DejaVuNodes import vizlib
        ed.addLibrary(vizlib)
    except:
        print 'VILZIB (DejaVu) NOT FOUND!'


def test_loadSymLib():
    try:
        from symserv.VisionInterface.SymservNodes import symlib
        ed.addLibrary(symlib)
    except:
        print 'SYMLIB (DejaVu) NOT FOUND!'


def test_loadImageLib():
    try:
        from Vision.PILNodes import imagelib
        ed.addLibrary(imagelib)
    except:
        print 'SYMLIB (DejaVu) NOT FOUND!'


def test_nodesInLibraries():
    # test whatever nodes have been loaded so far. Note: this test relies
    # on libraries already loaded.
    libs = ed.libraries
    posx = 150
    posy = 150
    for lib in libs.keys():
        ed.ModulePages.selectpage(lib)
        ed.root.update_idletasks()
        for cat in libs[lib].libraryDescr.keys():
            for node in libs[lib].libraryDescr[cat]['nodes']:
                klass = node.nodeClass
                kw = node.kw
                args = node.args
                netNode = apply( klass, args, kw )
                print 'testing: '+node.name # begin node test
                #add node to canvas
                ed.currentNetwork.addNode(netNode,posx,posy)
                # show widget in node if available:
                widgetsInNode = netNode.getWidgetsForMaster('Node')
                if len(widgetsInNode.items()) is not None:
                    for port,widget in widgetsInNode.items():
                        netNode.createOneWidget(port)
                        ed.root.update_idletasks()
                    # and then hide it
                    for port,widget in widgetsInNode.items():
                        netNode.hideInNodeWidget(port.widget)
                        ed.root.update_idletasks()

                # show widgets in param panel if available:
                widgetsInPanel = netNode.getWidgetsForMaster('ParamPanel')
                if len(widgetsInPanel.items()):
                    netNode.paramPanel.show()
                    ed.root.update_idletasks()

                    #and then hide it
                    netNode.paramPanel.hide()
                    ed.root.update_idletasks()
                        
                # and now delete the node
                ed.currentNetwork.deleteNodes([netNode])
                ed.root.update_idletasks()
                
                print 'passed: '+node.name # end node test
    
harness = testplus.TestHarness( __name__,
                                connect = startEditor,
                                funs = testplus.testcollect( globals()),
                                disconnect = quitEditor
                                )

if __name__ == '__main__':
    testplus.chdir()
    print harness
    sys.exit( len( harness))
